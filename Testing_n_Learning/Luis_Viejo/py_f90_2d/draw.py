import numpy
from matplotlib import pyplot as plt

def read_ascii(path_to_img = None):


    myfile = open(path_to_img, "r")
    all_lines = myfile.readlines()
    myfile.close()

    data_lst = []
    for lin_char in all_lines:
        num = float(lin_char)
        print "num =", num
        data_lst.append(num)

    data_in = numpy.asarray(data_lst, dtype=numpy.float64)
    read_data = data_in.reshape((4, 6))
    print "data_in = \n", read_data
    return read_data

def read_file(path_to_img = None):

    data_in = numpy.fromfile(path_to_img, dtype=numpy.float64)

    #read_data = data_in.reshape((4, 6), order="FORTRAN")
    read_data = data_in.reshape((4, 6))

    #read_data = data_in.reshape((6, 4))
    #read_data = data_in.reshape((6, 4), order="FORTRAN")
    return read_data

def plot_img(read_data = None):

    plt.imshow( read_data , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):


    #img_arr = read_ascii("data_out.asc")

    img_arr = read_file("data_out.raw")

    plot_img(img_arr)
