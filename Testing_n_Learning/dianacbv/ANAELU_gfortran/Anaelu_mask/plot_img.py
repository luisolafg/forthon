import numpy
from matplotlib import pyplot as plt

def read_file(path_to_img = None):
    print "reading ", path_to_img, " file"
    xres = 2048
    yres = 2048

    #data_in = numpy.fromfile(path_to_img, dtype=numpy.uint16)
    data_in = numpy.fromfile(path_to_img, dtype=numpy.float64)

    #read_data = data_in.reshape((yres, xres), order="FORTRAN")
    read_data = data_in.reshape((yres, xres))
    print len(data_in)

    return read_data

def plott_img(arr):
    plt.imshow( arr , interpolation = "nearest" )
    plt.show()

if(__name__ == "__main__"):
    img_arr = read_file("img_w_cuts.raw")
    #img_arr = read_file("../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.bin")
    plott_img(img_arr)
