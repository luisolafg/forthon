program read_bin_file
    integer(kind = 4), allocatable  :: dat_unflip(:,:)
    integer(kind = 4), allocatable  :: dat_fliped(:,:)
    integer(kind = 2), allocatable  :: tmp_dat(:,:)
    integer                         :: col, row, max_col, max_row
    integer                         :: narg
    character(len = 255)            :: file_in = "", file_out = "", tmp_char = ""
    character(len = 255)            :: arg_file = "", arg_x_size = "", arg_y_size = ""
    !logical                         :: arggiven
    lun = 25
    max_col = 2300
    max_row = 2300
    file_in = "./PFN_phi_0.5_to_2.5_180s_01_01_06102016.raw.bin"

    narg = COMMAND_ARGUMENT_COUNT()

    if( narg == 1 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        write(*,*) "only one argument given, asumed as input file"
        write(*,*) "NO size of buffer given asumed 2300 * 2300"
        arg_x_size = "2300"
        arg_y_size = "2300"
        file_out = file_in(1:len(trim(file_in)) - 3)//"txt"
    elseif( narg == 2 )then
        write(*,*) "Only two arguments given"
        write(*,*) "assumed as input file and next argument as both sizes"
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        file_out = file_in(1:len(trim(file_in)) - 3)//"txt"
        arg_y_size = arg_x_size
    elseif( narg == 3 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        call GET_COMMAND_ARGUMENT(3, arg_y_size)
        file_out = file_in(1:len(trim(file_in)) - 3)//"txt"
    elseif( narg == 4 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        call GET_COMMAND_ARGUMENT(3, arg_y_size)
        call GET_COMMAND_ARGUMENT(4, arg_file)
        file_out = trim(arg_file)
    elseif(narg == 0) then
        write(*,*) "ERROR  ... No argument given"
        stop
    end if


    write(*,*) "narg =", narg

    write(*,*) "arg_x_size =", arg_x_size
    write(*,*) "arg_y_size =", arg_y_size

    read (arg_x_size, fmt = '(I5.5)') max_col
    read (arg_y_size, fmt = '(I5.5)') max_row

    write(*,*) "max_col =", max_col
    write(*,*) "max_row =", max_row
    write(*,*) "file_in  =", trim(file_in)
    write(*,*) "file_out =", trim(file_out)
    allocate(tmp_dat(1:max_row,1:max_col))
    allocate(dat_unflip(1:max_row,1:max_col))
    allocate(dat_fliped(1:max_row,1:max_col))

    open(unit = lun, file = trim(file_in), action = "read", access = "stream")
    read(unit=lun) tmp_dat
    close(unit=lun)

    write(unit = *,fmt = "(a)") 'open => end'
    do col = 1, max_col, 1
        do row = 1, max_row, 1
            if( tmp_dat(row, col) < 0 )then
                dat_unflip(row,col) = tmp_dat(row,col) + 65536
            else
                dat_unflip(row,col) = tmp_dat(row,col)
            end if
        end do
    end do

    do col = 1, max_col, 1
        do row = 1, max_row, 1
            dat_fliped(row,col) = dat_unflip(row,max_col - col + 1)
        end do
    end do

    write(unit = *, fmt="(a)") 'transform => end'
    !now writing to a text file
    lun = 30
    open(unit = lun, file = trim(file_out), status = "replace", action = "write")
    tmp_char = "original (bin) IMG = " // trim(file_in)
    write(lun,*) trim(tmp_char)
    tmp_char = "number of pixels in x direction =  " // trim(arg_x_size)
    write(lun,*) trim(tmp_char)
    tmp_char = "number of pixels in y direction =  " // trim(arg_y_size)
    write(lun,*) trim(tmp_char)
    tmp_char = "X(cols) growing faster "
    write(lun,*) trim(tmp_char)
    tmp_char = "????:"
    write(lun,*) trim(tmp_char)
    tmp_char = "txt IMG:"
    write(lun,*) trim(tmp_char)

    do col = 1, max_col, 1
        do row = 1, max_row, 1
            write(lun,*) dat_fliped(row,col)
        end do
    end do
    close(unit=lun)
    write(unit=*,fmt="(a)") 'write => end'

end program read_bin_file
