#define PCKWORD short int
#define MAXPIXELNUM 3450

typedef int pck_header[10];

int openfile(char *fname, PCKWORD *img, int doRotate);
int headerinfo(char *fname, pck_header fheader);

// sketch of 1st pixel and 0th array element!
//  y
// 1.0--1.0
//  |    |
//  |    |
// 0.0--1.0 x
//
int SetValue(int iwidth, PCKWORD *dataarray, double xcoord, double ycoord, PCKWORD value);
PCKWORD GetValue(unsigned int iwidth, PCKWORD *dataarray, double xcoord, double ycoord);
void ArrayIdx_2_XY(unsigned int iwidth, unsigned long aidx, double *xcoord, double *ycoord);
unsigned long XY_2_ArrayIdx(unsigned int iwidth, double xcoord, double ycoord);
