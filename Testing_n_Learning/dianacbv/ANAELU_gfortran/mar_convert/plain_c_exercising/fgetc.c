#include <stdio.h>

int main ()
{
   FILE *fp;
   int c;
   printf("\ncontent of file =\n");
   fp = fopen("file.txt","r");
   while(1)
   {
      c = fgetc(fp);
      if( feof(fp) )
      {
          break ;
      }
      printf("%c", c);
   }
   fclose(fp);
   printf("\n");
   return(0);
}