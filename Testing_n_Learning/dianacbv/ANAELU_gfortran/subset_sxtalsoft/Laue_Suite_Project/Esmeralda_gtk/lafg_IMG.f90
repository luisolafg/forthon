!! module lafg_image
!! This module contains most subroutines related with the GUI of LAFG
!! This subroutines are called from the main cycle (lafg_main program)
!! and from this module (lafg_image) are called most calculations and
!! data treatment subroutines
  module lafg_psl_image
    Use Data_Trt
    Use Laue_Suite_Global_Parameters
    Use Laue_TIFF_Read
    Use Laue_Mod
    Use laue_siml_modl
    Use CFML_String_Utilities,         only: Get_Logunit
    implicit none
    private
    public    Open_cfl, Open_img, write_in_hkl_mask, calc_hkl_mask, CLI_orient,   &
              show_peacks_console, read_experimental_pks_from_file, &
              write_file_w_experimental_pks, CLI_Find_Peak

    INTEGER                     :: img_calc
    CHARACTER(LEN=256)          :: FNAME = ' '
    logical                     :: opn = .false. , MousDw = .false. , auto_rescale=.true.
    real                        :: xinidrag, yinidrag, x_mov_old, y_mov_old, asp_rat_pic_old
    integer                     :: img_xmin, img_ymin, img_xmax, img_ymax
    integer                     :: x_max_siz=300, y_max_siz=150
    integer                     :: dst_min = 5, peak_ar = 6, m_peak = 6
    integer                     :: From_X_Ang = 0, From_Y_Ang = 0, From_Z_Ang = 0
    integer                     :: To_X_Ang = 25, To_Y_Ang = 25, To_Z_Ang = 25
    integer                     :: x_step = 1, y_step = 1, z_step = 1, ini_resol = 10

    CONTAINS

!! subroutine allocat_mask()
!! once the program knows the instrument resolution
!! in pixels, it prepares the variables
    subroutine allocat_mask()
        Nrow=LaueDiff%np_v
        Ncol=LaueDiff%np_h

        if(allocated(hkl_mask))then
            deallocate(hkl_mask)
        end if
        allocate(hkl_mask(1:Nrow,1:Ncol))

        if (allocated(Data2D)) then
            deallocate (Data2D)
        end if
        allocate(Data2D(1:Nrow, 1:Ncol,1:n_img))

        if (allocated(Mask2D)) then
            deallocate (Mask2D)
        end if
        allocate(Mask2D(1:Nrow, 1:Ncol,1:n_img))
        Mask2D = 0
        if (allocated(img_spdl)) then
            deallocate (img_spdl)
        end if
        allocate(img_spdl(1:n_img))


    return
    end subroutine allocat_mask

!! SUBROUTINE Open_img()
!! shows the section dialog and then opens an image
    SUBROUTINE Open_img(nom)
    Character(Len=512)  , intent(in)            :: nom
    integer                                     :: i, j
    logical                                     :: ok
      !  Image_File='fel_22.tif'
        Image_File=nom
        write(*,*) '<<<<'//trim(Image_File)//'>>>>'
        if (trim(Image_File)/='') then
            call allocat_mask()
            img_spdl=0
            Data2D=0

            call read_raw_image(Image_File,Nrow,Ncol,Data2D(:,:,n_img))
            if( LaueDiff%invert =="Yes" .or. LaueDiff%invert =="YES" .or. LaueDiff%invert =="yes" )then
                write(*,*) "LaueDiff%invert =", LaueDiff%invert
                Call Invert_Intensities_2D(Data2D(:,:,n_img))
            end if
            opn=.true.

            if (allocated(Peak_List))  deallocate(Peak_List)
            allocate(Peak_List(1:n_img))

            if( ok ) then
                opn=.true.
                call ini_coord()
            end if
            img_spdl(1)=ICondt%expose_phi
        else
            write(*,*) 'Canceled'
        end if
    RETURN
    END SUBROUTINE Open_img

!! SUBROUTINE Open_cfl()
!! shows the section dialog and then opens a CFL File
    subroutine Open_cfl(nom)
    Character(Len=*) , intent(in)              :: nom
    Character(Len=6)                             :: read_inst
    integer                                      :: i, j

      CFL_File = nom
      !  CFL_File = "feind_tst.cfl"
      n_img = 1
      img_n = 1
      write(*,*) 'opening CFL file'
      write(*,*) 'File ='//trim(CFL_File)//'<='

      Instrm_Loaded=.true.
      call Read_CFL_File(read_inst)
      if(Instrm_Loaded)then
          call allocat_mask()
          call ini_coord()
      end if

      write(*,*) "LaueDiff%xo =", LaueDiff%xo
      write(*,*) "LaueDiff%zo =", LaueDiff%zo

      write(*,*) "Image_File =", Image_File

    RETURN
    END SUBROUTINE Open_cfl

    subroutine ini_coord()
        img_xmin=1
        img_ymin=1
        img_xmax=Ncol
        img_ymax=Nrow
    return
    end subroutine ini_coord

!! subroutine calc_hkl_mask()
!! repaints or refreshes the image that contains both
!! patterns, experimental and calculated.
    subroutine calc_hkl_mask(X_ang, Y_ang, Z_ang)
        real , intent(in)  :: X_ang, Y_ang, Z_ang
        if(Instrm_Loaded .and. Cell_Loaded)then
            write(*,*) "Calculating"
            write(*,*) 'Ang(x,y,z) =', X_ang, Y_ang, Z_ang
            call Recalc(X_ang, Y_ang, Z_ang)
            !call write_in_hkl_mask()
        end if
    return
    end subroutine calc_hkl_mask


!! subroutine write_in_hkl_mask
    subroutine write_in_hkl_mask()
    real            :: x_pc,y_pc, x_milm, z_milm
!    integer         :: absum_new , absum_old
    integer         :: j
    integer         :: x_pix,y_pix
    logical         :: in_img
        write(*,*) "inside >>> subroutine write_in_hkl_mask()"
        hkl_mask=0
        do j=1,Ref_Lst%nref
            x_milm=Ref_Lst%LR(j)%x
            z_milm=Ref_Lst%LR(j)%z

            call mil_to_pix(LaueDiff,x_milm,z_milm,x_pc,y_pc)
            x_pix=nint(x_pc)
            y_pix=nint(y_pc)
            in_img = .true.
            if( x_pix>Ncol )then
                x_pix = Ncol
                in_img = .false.
            end if
            if( y_pix>Nrow )then
                y_pix = Nrow
                in_img = .false.
            end if

            if( x_pix<1 )then
                x_pix = 1
                in_img = .false.
            end if
            if( y_pix<1 )then
                y_pix = 1
                in_img = .false.
            end if
            if (in_img ) then
                hkl_mask(y_pix,x_pix)=j
            end if
        end do
    return
    end subroutine write_in_hkl_mask


    subroutine read_pk_fnd_param_from_file()
        integer                 :: lun
        call Get_LogUnit(lun)
        open(unit=lun, file="pk_par.dat", action="read")
            read(unit=lun,fmt=*) dst_min, peak_ar, m_peak
        close(lun)
    return
    end subroutine read_pk_fnd_param_from_file


    subroutine read_orient_param_from_file()
        integer                 :: lun
        write(*,*) 'test ... got here'
        call Get_LogUnit(lun)
        open(unit=lun, file="orient_par.dat", action="read")
    !                        orient_par.dat
            read(unit=lun,fmt=*) From_X_Ang, From_Y_Ang, From_Z_Ang
            read(unit=lun,fmt=*) To_X_Ang, To_Y_Ang, To_Z_Ang
            read(unit=lun,fmt=*) x_step, y_step, z_step
            read(unit=lun,fmt=*) ini_resol

            write(unit=*,fmt=*) "From_X_Ang, From_Y_Ang, From_Z_Ang =", From_X_Ang, From_Y_Ang, From_Z_Ang
            write(unit=*,fmt=*) "To_X_Ang, To_Y_Ang, To_Z_Ang       =", To_X_Ang, To_Y_Ang, To_Z_Ang
            write(unit=*,fmt=*) "x_step, y_step, z_step             =", x_step, y_step, z_step
            write(unit=*,fmt=*) "ini_resol                          =", ini_resol
        close(lun)
    return
    end subroutine read_orient_param_from_file


    subroutine CLI_Find_Peak()
            real        ::  xmin,ymin,xmax,ymax
            xmin = lbound(Data2D,2)
            ymin = lbound(Data2D,1)
            xmax = ubound(Data2D,2)
            ymax = ubound(Data2D,1)
            call read_pk_fnd_param_from_file()
            Fnd_param%d_min = dst_min
            Fnd_param%pk_ar = peak_ar
            Fnd_param%m_pk = m_peak
            write(*,*) "P find Par =", Fnd_param
            write(unit=*,fmt="(a,4f8.1)") " => Calling Peak_Find with x-y range: ",xmin,ymin,xmax,ymax
!!!!!!!!!!!!!!!!!Peak_Find(xmin,ymin,xmax,ymax,Data2D,Mask2D,ExclR,Fnd_par,Peaks)
            call Peak_Find(xmin,ymin,xmax,ymax,Data2D(:,:,1),Mask2D(:,:,1),ExclR,Fnd_param,Peak_List(1))
!            call Arrange_PeakList(Data2D(:,:,1),Mask2D(:,:,1),Peak_List(1)%Peak,Peak_List(1)%N_Peaks,Fnd_param)
    return

    end subroutine  CLI_Find_Peak


    subroutine write_file_w_experimental_pks()
        integer         :: lun, col,row
        call Get_LogUnit(lun)
        open(unit=lun, file="exp_pk.dat", status="replace", action="write")
        do col = 1, Ncol
            do row = 1, Nrow
                if( Mask2D(row,col,1) == 1)then
                    write(unit=lun,fmt='(I6,A2,I6)')  col, ', ', row
                end if
            end do
        end do
        close(lun)
    return
    end subroutine write_file_w_experimental_pks


    subroutine read_experimental_pks_from_file()
        integer             :: lun, col,row, err
        character(len=210)  :: line
        call Get_LogUnit(lun)
        open(unit=lun, file="exp_pk.dat", action="read")
        Mask2D(:,:,1) = 0
        do
            read(unit=lun,fmt="(a)",iostat=err) line
            if( err /= 0 )then
                exit
            end if
            read(unit=line,fmt=*) col, row
            Mask2D(row,col,1) = 1
        end do

        close(lun)
    return
    end subroutine read_experimental_pks_from_file

    subroutine show_peacks_console()
        integer         :: tot, col, row
        tot = 0
        do col = 1, Ncol
            do row = 1, Nrow
                if( Mask2D(row,col,1) == 1)then
                    write(*,*) "( X, Y) =", col, row
                    tot = tot + 1
                end if
            end do
        end do
        write(*,*) "Num of Peaks", tot

    return
    end subroutine show_peacks_console

    subroutine CLI_orient(a_x_r, a_y_r, a_z_r)
        real,                        intent (out)   :: a_x_r, a_y_r, a_z_r
        Type (Orient_angl_cond_type)                :: orien_cond                 ! orienting parameteres

        call read_orient_param_from_file()
        orien_cond%xfrom = From_X_Ang
        orien_cond%yfrom = From_Y_Ang
        orien_cond%zfrom = From_Z_Ang

        orien_cond%xto = To_X_Ang
        orien_cond%yto = To_Y_Ang
        orien_cond%zto = To_Z_Ang

        orien_cond%xstep = x_step
        orien_cond%ystep = y_step
        orien_cond%zstep = z_step
        orien_cond%resl  = ini_resol

        call F_A_Orient(orien_cond,a_x_r,a_y_r,a_z_r)

    return
    end subroutine CLI_orient


  END module lafg_psl_image
