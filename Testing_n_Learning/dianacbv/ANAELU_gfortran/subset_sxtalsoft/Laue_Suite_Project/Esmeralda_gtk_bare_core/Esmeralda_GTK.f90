! Copyright (C) 2014
! Luis Fuentes-Montero (Luiso)

! this is part of the GTK version of Esmeralda

! This is free software; you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 3, or (at your option)
! any later version.

! This software is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! Under Section 7 of GPL version 3, you are granted additional
! permissions described in the GCC Runtime Library Exception, version
! 3.1, as published by the Free Software Foundation.

! You should have received a copy of the GNU General Public License along with
! this program; see the files COPYING3 and COPYING.RUNTIME respectively.
! If not, see <http://www.gnu.org/licenses/>.
!

module GUI_gloval_vars
  use iso_c_binding
  !real(8)               :: x1(1:200), y1(1:200), x2(1:200), y2(1:200)
  real(8)                       :: x_rt = -1, y_rt = -1, x_st = -1, y_st = -1
  real(8)                       :: old_x_rt = -1, old_y_rt = -1;
  real(8)                       :: x_sn(1:10) = 0, y_sn(1:10) = 0
  integer                       :: num_ref
  type(c_ptr)                   :: ebox1, ebox2, ebox3
  type(c_ptr)                   :: main_drawing_area, small_drawing_area

  type(c_ptr)                   :: pixbuf
  real                          :: ang_x, ang_y, ang_z



  type(c_ptr) :: my_window
  type(c_ptr) :: my_scroll_box
  type(c_ptr) :: table
  type(c_ptr) :: button_calc, button_read

end module GUI_gloval_vars


module handlers
  use GUI_gloval_vars
  use iso_c_binding

  !********************************
  ! Gtk modules
  use cairo, only: cairo_arc, cairo_curve_to, cairo_get_target, &
       & cairo_line_to, cairo_move_to, cairo_new_sub_path, cairo_paint, &
       & cairo_rectangle, cairo_select_font_face, cairo_set_font_size, &
       & cairo_set_line_width, cairo_set_source_rgb, cairo_show_text, &
       & cairo_stroke, cairo_surface_write_to_png

  use gdk, only: gdk_device_get_name, gdk_device_get_source, &
       & gdk_event_get_source_device, gdk_keyval_from_name, gdk_keyval_name!, gdk_window_scroll

  use gtk, only: gtk_container_add, gtk_main, gtk_main_quit, &
       & gtk_widget_queue_draw, gtk_widget_show_all, gtk_init, TRUE, FALSE, &
       & GDK_BUTTON_PRESS, GDK_2BUTTON_PRESS, GDK_BUTTON_RELEASE, &
       & GDK_KEY_PRESS, GDK_ENTER_NOTIFY, GDK_LEAVE_NOTIFY, GDK_CONTROL_MASK, &
       & GDK_POINTER_MOTION_MASK, GDK_BUTTON_MOTION_MASK, gtk_style_context_scroll_animations, &
       & CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL, gtk_table_new, &
       & gtk_table_attach_defaults, gtk_button_new_with_label, gtk_entry_get_text, gtk_entry_new


  use gdk_events
  use gdk_pixbuf_hl
  use gtk_draw_hl
  use gtk_sup
  use gtk_hl

  use lafg_psl_image
  Use Laue_Suite_Global_Parameters

  implicit none
  integer(c_int) :: boolresult
  logical :: boolevent
  integer(kind=c_int) :: width, height

  logical :: rflag = .false.
  integer(kind=c_int) :: xp0, yp0



  integer(kind=c_short),allocatable ,dimension(:, :, :) :: image
contains
  ! User defined event handlers go here
  function delete_h (widget, event, gdata) result(ret)  bind(c)
    use iso_c_binding, only: c_ptr, c_int
    integer(c_int)    :: ret
    type(c_ptr), value, intent(in) :: widget, event, gdata
    !    run_status = FALSE
    call gtk_main_quit
    ret = FALSE
  end function delete_h

  function button_event_main_h(widget, event, gdata) result(rv) bind(c)
    integer(kind=c_int) :: rv
    type(c_ptr), value, intent(in) :: widget, event, gdata

    type(gdkeventbutton), pointer :: bevent
    type(c_ptr) :: hdevice, dcname, pixb
    character(len=64) :: dname, hdname
    integer(kind=c_int) :: xp1, yp1, xo, yo, xs, ys, ipick
    character(len=120), dimension(:), allocatable :: files

    rv = FALSE

    if (c_associated(event)) then
       call c_f_pointer(event,bevent)
    else
       return
    end if

    if (bevent%type == GDK_BUTTON_RELEASE) then
       write(*,*)  "Button release detected"

       call draw_pattern(widget)
       x_rt = -1
       y_rt = -1
       x_st = -1
       y_st = -1
    else
       write(*,*)  "Button press detected"
       x_rt = bevent%x
       y_rt = bevent%y
       x_st = bevent%x
       y_st = bevent%y
    end if
    write(*,*)

  end function button_event_main_h


  function button_event_small_h(widget, event, gdata) result(rv) bind(c)
    integer(kind=c_int) :: rv
    type(c_ptr), value, intent(in) :: widget, event, gdata

    type(gdkeventbutton), pointer :: bevent
    type(c_ptr) :: hdevice, dcname, pixb
    character(len=64) :: dname, hdname
    character(len=120), dimension(:), allocatable   :: files
    integer                                         :: i

    rv = FALSE

    if (c_associated(event)) then
       call c_f_pointer(event,bevent)
    else
       return
    end if

    if (bevent%type == GDK_BUTTON_RELEASE) then
        write(*,*)  "Button release detected"
        do i = 1, 10
            write(*,*) "x,y =", x_sn(i), y_sn(i)
        end do
       !call draw_pattern(widget)
    else
       write(*,*)  "Button press detected"
    end if
    write(*,*)

  end function button_event_small_h

  function motion_event_main_h(widget, event, gdata) bind(c) result(rv)
    integer(kind=c_int) :: rv
    type(c_ptr), value, intent(in) :: widget, event, gdata
    type(gdkeventscroll), pointer :: bevent

    integer(kind=c_int) :: i_dx, i_dy
    real dx, dy

    if (c_associated(event)) then
        call c_f_pointer(event,bevent)
   !     write(*, "(2I5,2A)", advance='no') int(bevent%x), &
    !        & int(bevent%y), "    from Main Draw area",  c_carriage_return
        x_rt = bevent%x
        y_rt = bevent%y
        ! next line proves that bevent%x and bevent%x are NOT integers
        ! write(*,*) "x,y =", bevent%x, bevent%y

!        !  void gdk_window_scroll (GdkWindow *window, gint dx, gint dy);
 !       subroutine gdk_window_scroll(window, dx, dy) bind(c)
  !      use iso_c_binding, only: c_ptr, c_int
   !         type(c_ptr), value :: window
    !        integer(c_int), value :: dx
     !       integer(c_int), value :: dy
      !  end subroutine


        dx = x_rt - old_x_rt
        dy = y_rt -old_y_rt

        old_x_rt = x_rt
        old_y_rt = y_rt

        write(*, "(2I5,2A)", advance='no') int(dx), &
        & int(dy), "    from Main Draw area",  c_carriage_return
        i_dx = int(dx)
        i_dy = int(dy)


!        subroutine gtk_style_context_scroll_animations(context, window, dx, dy) bind(c)
 !           use iso_c_binding, only: c_ptr, c_int
  !          type(c_ptr), value :: context
   !         type(c_ptr), value :: window
!            integer(c_int), value :: dx
 !           integer(c_int), value :: dy
  !      end subroutine

        call gtk_style_context_scroll_animations(widget, my_window, i_dx, i_dy)

        ! call draw_pattern(widget)
    end if
    rv = FALSE
  end function motion_event_main_h



  function motion_event_small_h(widget, event, gdata) bind(c) result(rv)
    integer(kind=c_int)             :: rv
    type(c_ptr), value, intent(in)  :: widget, event, gdata
    type(gdkeventscroll), pointer   :: bevent
    integer                         :: i

    if (c_associated(event)) then
        call c_f_pointer(event,bevent)
        write(*, "(2I5,A)", advance='no') int(bevent%x), &
            & int(bevent%y), c_carriage_return

        do i = 10, 2, -1
            x_sn(i) = x_sn(i-1)
            y_sn(i) = y_sn(i-1)
        end do
        x_sn(1) = bevent%x
        y_sn(1) = bevent%y
        call draw_small_pattern(widget)
    end if
    rv = FALSE
  end function motion_event_small_h


  function scroll_event_h(widget, event, gdata) bind(c) result(rv)
    integer(kind=c_int) :: rv
    type(c_ptr), value, intent(in) :: widget, event, gdata

    type(gdkeventscroll), pointer :: bevent
    type(c_ptr) :: hdevice, dcname
    character(len=64) :: dname, hdname

    write(*,*)  "Wheel event detected"
    if (c_associated(event)) then
       call c_f_pointer(event,bevent)
       write(*,*)  "Clicked at:", int(bevent%x), int(bevent%y)
       write(*,*)  "State, direction:", bevent%state, bevent%direction
       write(*,*)  "Root x,y:", int(bevent%x_root), int(bevent%y_root)
       dcname = gdk_device_get_name(bevent%device)
       call c_f_string(dcname, dname)
       hdevice = gdk_event_get_source_device(event)
       dcname = gdk_device_get_name(hdevice)
       call c_f_string(dcname, hdname)
       write(*,*)  "Device: ",trim(dname),' (',trim(hdname),') ', &
            & gdk_device_get_source(bevent%device)
    end if
    write(*,*)
    rv = FALSE
  end function scroll_event_h

  function button_calc_I_px_clicked(widget, gdata ) result(ret)  bind(c)
    integer(c_int)    :: ret
    integer(c_int)    :: dumy_ret
    type(c_ptr), value, intent(in) :: widget, gdata
    write(*,*) "Button Calc clicked!"
    write(*,*) "calling mask calculator"

    dumy_ret =  button_read_n_rot_clicked(widget, gdata )
    call calc_I_pat()

    write(*,*) "after mask calculator"

    call draw_pattern(main_drawing_area)
    ret = FALSE
  end function button_calc_I_px_clicked


  subroutine calc_I_pat()

    integer         :: row, col
    integer         :: min_col, min_row, max_col, max_row, siz
    write(*,*) "Hi there from calc_I_pat()"
    Data2D(:,:,:) = 0

    do col = 1, Ncol, 1
      do row = 1, Nrow, 1
        if(hkl_mask(row,col) /= 0 )then
          do siz = 1, 50, 1
            min_row = row - siz
            min_col = col - siz
            max_row = row + siz
            max_col = col + siz
            if( min_col > 0 .and. max_col < Ncol .and. &
            &   min_row > 0 .and. max_row < Nrow )then
              Data2D(min_row: max_row, min_col: max_col,:)  &
              & = Data2D(min_row: max_row, min_col: max_col,:) + 2
            end if

          end do
        end if
      end do
    end do

  end subroutine calc_I_pat


  function button_read_n_rot_clicked(widget, gdata ) result(ret)  bind(c)
    integer(c_int)    :: ret
    type(c_ptr), value, intent(in)                  :: widget, gdata
    !type(c_ptr)                                    :: ctext
    !character(len=11), dimension(:), allocatable   :: text
    character(len=20)                               :: my_string1, my_string2, my_string3
    character(kind=c_char), dimension(:), pointer   :: textptr1, textptr2, textptr3
    real                                            :: avl = 0, bvl = 0, cvl = 0


    write(*,*) "reading from entry box"

    call C_F_POINTER(gtk_entry_get_text(ebox1), textptr1, (/0/))
    call convert_c_f_strg_n_tst(textptr1, my_string1)

    call C_F_POINTER(gtk_entry_get_text(ebox2), textptr2, (/0/))
    call convert_c_f_strg_n_tst(textptr2, my_string2)

    call C_F_POINTER(gtk_entry_get_text(ebox3), textptr3, (/0/))
    call convert_c_f_strg_n_tst(textptr3, my_string3)
    !call c_f_string(textptr3, my_string3)

    if (trim(my_string1) == "") then           !fix me  needs to be les fragile
        avl = 0
    else
        read(unit = my_string1, fmt = *) avl
    end if
    if (trim(my_string2) == "") then           !fix me  needs to be les fragile
        bvl = 0
    else
        read(unit = my_string2, fmt = *) bvl
    end if
    if (trim(my_string3) == "") then           !fix me  needs to be les fragile
        cvl = 0
    else
        read(unit = my_string3, fmt = *) cvl
    end if
    !write(*,*) "my_string3 =  <<<", my_string3, ">>>"

    write(*,*) "avl, bvl, cvl = ", avl, bvl, cvl
    ang_x = avl
    ang_y = bvl
    ang_z = cvl

    call calc_hkl_mask(ang_x, ang_y, ang_z)
    call write_in_hkl_mask()
    call draw_pattern(main_drawing_area)

    ret = FALSE
  end function button_read_n_rot_clicked
  ! This is not a handler:
  subroutine convert_c_f_strg_n_tst(textptr, f_string)
    use iso_c_binding, only: c_char
    implicit none
    character(kind=c_char), dimension(:), pointer, intent(in) :: textptr
    character(len=*), intent(out) :: f_string
    integer :: i, j

    f_string=""
    i=1
    do while(textptr(i) .NE. char(0))
        if(    textptr(i) == "." .or. textptr(i) == "0" .or. textptr(i) == "1" &
        & .or. textptr(i) == "2" .or. textptr(i) == "3" .or. textptr(i) == "4" &
        & .or. textptr(i) == "5" .or. textptr(i) == "6" .or. textptr(i) == "7" &
        & .or. textptr(i) == "8" .or. textptr(i) == "9" )  then
            f_string(i:i) = textptr(i)
            i=i+1
        else
            write(*,*) "ERROR non digit entered, replazing with 0"
            f_string(i:i) = "0"
            i=i+1
        end if
    end do
  end subroutine convert_c_f_strg_n_tst



  subroutine draw_pattern(widget)
    type(c_ptr), value, intent(in) :: widget
    type(c_ptr) :: my_cairo_context
!    integer :: cstatus
    integer     :: i, row, col
    real(8)     :: xpos, ypos, scl
    real(8)     :: dx, dy
    real(8)     :: dif_x, dif_y, dif
    real(8)     :: rect_x_ini, rect_y_ini, rect_x_end, rect_y_end

    my_cairo_context = hl_gtk_drawing_area_cairo_new(widget)
    if (.not. c_associated(my_cairo_context)) then
       write(*,*)  "ERROR failed to create cairo context"
       return
    else
       ! write(*,*)  "creating cairo context"
    end if

    call cairo_set_source_rgb(my_cairo_context, 0.6_c_double, 0.6_c_double, &
         & 0.6_c_double)
    call cairo_rectangle(my_cairo_context, 0._c_double, 0._c_double,&
         & real(width, c_double), real(height, c_double))
    call cairo_paint(my_cairo_context)

    call cairo_set_source_rgb(my_cairo_context, 0._c_double, 0.5_c_double, 0.5_c_double)

    scl =1.0/5.0

    image(:,:,:) = 0
    dif = 5


    do col = 1, Ncol, 1
        do row = 1, Nrow, 1
            xpos = col * scl
            ypos = row * scl

            if( Data2D(row, col,1)/= 0 )then
                image(1,   int(xpos - 1):int(xpos + 1), int(ypos - 1):int(ypos + 1)) = 0
                image(2:3, int(xpos - 1):int(xpos + 1), int(ypos - 1):int(ypos + 1)) &
                & = Data2D(row, col, 1)
            end if

        end do
    end do


    do col = 1, Ncol, 1
        do row = 1, Nrow, 1
            xpos = col * scl
            ypos = row * scl

            if( hkl_mask(row, col) /= 0)then
                image(1,   int(xpos - 1):int(xpos + 1), int(ypos - 1):int(ypos + 1)) = 255
                image(2:3, int(xpos - 1):int(xpos + 1), int(ypos - 1):int(ypos + 1)) = 0
            end if

        end do
    end do

    call hl_gdk_pixbuf_set_pixels(pixbuf, image)
    call gdk_cairo_set_source_pixbuf(my_cairo_context, pixbuf, 0d0, 0d0)
    call cairo_paint(my_cairo_context)
    !  call cairo_destroy(tst_cairo_context)

    !call cairo_stroke(my_cairo_context)
    call gtk_widget_queue_draw(widget)
    call hl_gtk_drawing_area_cairo_destroy(my_cairo_context)

  end subroutine draw_pattern




  subroutine draw_small_pattern(widget)
    type(c_ptr), value, intent(in) :: widget
    type(c_ptr) :: my_cairo_context
    integer     :: i

    my_cairo_context = hl_gtk_drawing_area_cairo_new(widget)
    if (.not. c_associated(my_cairo_context)) then
       write(*,*)  "ERROR failed to create cairo context"
       return
    else
       ! write(*,*)  "creating cairo context"
    end if

    call cairo_set_source_rgb(my_cairo_context, 0.6_c_double, 0.6_c_double, &
         & 0.6_c_double)
    call cairo_paint(my_cairo_context)

    call cairo_set_source_rgb(my_cairo_context, 0._c_double, 0.5_c_double, 0.5_c_double)
    do i=1,9,1
        call cairo_move_to(my_cairo_context, x_sn(i), y_sn(i))
        call cairo_line_to(my_cairo_context, x_sn(i+1), y_sn(i+1))
    end do

    call cairo_stroke(my_cairo_context)
    call gtk_widget_queue_draw(widget)
    call hl_gtk_drawing_area_cairo_destroy(my_cairo_context)

  end subroutine draw_small_pattern

end module handlers


program Esmeralda_GTK
  use iso_c_binding, only: c_ptr, c_funloc
  use handlers
  use GUI_gloval_vars
  implicit none


  call Open_cfl("ori_SI.cfl")
  call gtk_init ()

  ! Properties of the main window :
  width = 1200
  height = 900
  my_window = hl_gtk_window_new("Future of Esmeralda"//c_null_char, &
       & delete_event = c_funloc(delete_h))

       main_drawing_area = hl_gtk_drawing_area_new(&
       & scroll=my_scroll_box, &
       & size = (/width, height /), &
       & ssize = (/ 600_c_int, 200_c_int /), &
       & button_press_event=c_funloc(button_event_main_h), &
       & button_release_event=c_funloc(button_event_main_h), &
       & scroll_event=c_funloc(scroll_event_h), &
       & motion_event=c_funloc(motion_event_main_h), &
       & event_exclude=GDK_POINTER_MOTION_MASK, &
       & event_mask=GDK_BUTTON_MOTION_MASK)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

       small_drawing_area = hl_gtk_drawing_area_new(&
       & size = (/100, 100 /), &
       & ssize = (/ 100_c_int, 100_c_int /), &
       & button_press_event=c_funloc(button_event_small_h), &
       & button_release_event=c_funloc(button_event_small_h), &
       & motion_event=c_funloc(motion_event_small_h), &
       & event_exclude=GDK_POINTER_MOTION_MASK, &
       & event_mask=GDK_BUTTON_MOTION_MASK)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  button_calc = gtk_button_new_with_label (" calc I(Px) "//c_null_char)
  button_read = gtk_button_new_with_label (" read n rotate "//c_null_char)
  ebox1 = hl_gtk_entry_new(editable=TRUE, len=19)
  ebox2 = hl_gtk_entry_new(editable=TRUE, len=19)
  ebox3 = hl_gtk_entry_new(editable=TRUE, len=19)

  table = gtk_table_new (15, 12, true)
  call gtk_container_add (my_window, table)

  call gtk_table_attach_defaults (table, my_scroll_box,       0, 10, 0, 15)

  call gtk_table_attach_defaults (table,  button_read,        10, 12, 4, 6)
  call gtk_table_attach_defaults (table,  button_calc,        10, 12, 8, 9)

  call gtk_table_attach_defaults (table,  ebox1,              10, 12, 1, 2)
  call gtk_table_attach_defaults (table,  ebox2,              10, 12, 2, 3)
  call gtk_table_attach_defaults (table,  ebox3,              10, 12, 3, 4)

  call gtk_table_attach_defaults (table, small_drawing_area, 10, 12, 10, 14)

  call g_signal_connect(button_calc, "clicked"//c_null_char, c_funloc(button_calc_I_px_clicked))
  call g_signal_connect(button_read, "clicked"//c_null_char, c_funloc(button_read_n_rot_clicked))

  write(*,*) "my_window =", my_window
  write(*,*) "main_drawing_area =", main_drawing_area
  write(*,*) "my_scroll_box =", my_scroll_box
  write(*,*) "table", table

  allocate(image(1:3, 1:width, 1:height))
  pixbuf = hl_gdk_pixbuf_new(image)

  call gtk_widget_show_all (my_window)
  call gtk_main()
  write(*,*)  "All done"

end program Esmeralda_GTK
