!! module lafg_image
!! This module contains most subroutines related with the GUI of LAFG
!! This subroutines are called from the main cycle (lafg_main program)
!! and from this module (lafg_image) are called most calculations and
!! data treatment subroutines
  module lafg_psl_image
    Use Data_Trt
    Use Laue_Suite_Global_Parameters
    Use Laue_TIFF_Read
    Use Laue_Mod
    Use laue_siml_modl
    Use CFML_String_Utilities,         only: Get_Logunit
    implicit none
    private
    public    Open_cfl, Open_img, write_in_hkl_mask, calc_hkl_mask

    INTEGER                     :: img_calc
    CHARACTER(LEN=256)          :: FNAME = ' '
    logical                     :: opn = .false. , MousDw = .false. , auto_rescale=.true.
    real                        :: xinidrag, yinidrag, x_mov_old, y_mov_old, asp_rat_pic_old
    integer                     :: img_xmin, img_ymin, img_xmax, img_ymax
    integer                     :: x_max_siz=300, y_max_siz=150
    integer                     :: dst_min = 5, peak_ar = 6, m_peak = 6
    integer                     :: From_X_Ang = 0, From_Y_Ang = 0, From_Z_Ang = 0
    integer                     :: To_X_Ang = 25, To_Y_Ang = 25, To_Z_Ang = 25
    integer                     :: x_step = 1, y_step = 1, z_step = 1, ini_resol = 10

    CONTAINS

!! subroutine allocat_mask()
!! once the program knows the instrument resolution
!! in pixels, it prepares the variables
    subroutine allocat_mask()
        Nrow=LaueDiff%np_v
        Ncol=LaueDiff%np_h

        if(allocated(hkl_mask))then
            deallocate(hkl_mask)
        end if
        allocate(hkl_mask(1:Nrow,1:Ncol))

        if (allocated(Data2D)) then
            deallocate (Data2D)
        end if
        allocate(Data2D(1:Nrow, 1:Ncol,1:n_img))

        if (allocated(Mask2D)) then
            deallocate (Mask2D)
        end if
        allocate(Mask2D(1:Nrow, 1:Ncol,1:n_img))
        Mask2D = 0
        if (allocated(img_spdl)) then
            deallocate (img_spdl)
        end if
        allocate(img_spdl(1:n_img))


    return
    end subroutine allocat_mask

!! SUBROUTINE Open_img()
!! shows the section dialog and then opens an image
    SUBROUTINE Open_img(nom)
    Character(Len=512)  , intent(in)            :: nom
    integer                                     :: i, j
    logical                                     :: ok
      !  Image_File='fel_22.tif'
        Image_File=nom
        write(*,*) '<<<<'//trim(Image_File)//'>>>>'
        if (trim(Image_File)/='') then
            call allocat_mask()
            img_spdl=0
            Data2D=0

            call read_raw_image(Image_File,Nrow,Ncol,Data2D(:,:,n_img))
            if( LaueDiff%invert =="Yes" .or. &
              & LaueDiff%invert =="YES" .or. &
              & LaueDiff%invert =="yes" )then
                write(*,*) "LaueDiff%invert =", LaueDiff%invert
                Call Invert_Intensities_2D(Data2D(:,:,n_img))
            end if
            opn=.true.

            if (allocated(Peak_List))  deallocate(Peak_List)
            allocate(Peak_List(1:n_img))

            if( ok ) then
                opn=.true.
                call ini_coord()
            end if
            img_spdl(1)=ICondt%expose_phi
        else
            write(*,*) 'Canceled'
        end if
    RETURN
    END SUBROUTINE Open_img

!! SUBROUTINE Open_cfl()
!! shows the section dialog and then opens a CFL File
    subroutine Open_cfl(nom)
    Character(Len=*) , intent(in)              :: nom
    Character(Len=6)                             :: read_inst
    integer                                      :: i, j

      CFL_File = nom
      !  CFL_File = "feind_tst.cfl"
      n_img = 1
      img_n = 1
      write(*,*) 'opening CFL file'
      write(*,*) 'File ='//trim(CFL_File)//'<='

      Instrm_Loaded=.true.
      call Read_CFL_File(read_inst)
      if(Instrm_Loaded)then
          call allocat_mask()
          call ini_coord()
      end if

      write(*,*) "LaueDiff%xo =", LaueDiff%xo
      write(*,*) "LaueDiff%zo =", LaueDiff%zo

      write(*,*) "Image_File =", Image_File

    RETURN
    END SUBROUTINE Open_cfl

    subroutine ini_coord()
        img_xmin=1
        img_ymin=1
        img_xmax=Ncol
        img_ymax=Nrow
    return
    end subroutine ini_coord

!! subroutine calc_hkl_mask()
!! repaints or refreshes the image that contains both
!! patterns, experimental and calculated.
    subroutine calc_hkl_mask(X_ang, Y_ang, Z_ang)
        real , intent(in)  :: X_ang, Y_ang, Z_ang
        if(Instrm_Loaded .and. Cell_Loaded)then
            write(*,*) "Calculating"
            write(*,*) 'Ang(x,y,z) =', X_ang, Y_ang, Z_ang
            call Recalc(X_ang, Y_ang, Z_ang)
            !call write_in_hkl_mask()
        end if
    return
    end subroutine calc_hkl_mask


!! subroutine write_in_hkl_mask
    subroutine write_in_hkl_mask()
    real            :: x_pc,y_pc, x_milm, z_milm
!    integer         :: absum_new , absum_old
    integer         :: j
    integer         :: x_pix,y_pix
    logical         :: in_img
        write(*,*) "inside >>> subroutine write_in_hkl_mask()"
        hkl_mask=0
        do j=1,Ref_Lst%nref
            x_milm=Ref_Lst%LR(j)%x
            z_milm=Ref_Lst%LR(j)%z

            call mil_to_pix(LaueDiff,x_milm,z_milm,x_pc,y_pc)
            x_pix=nint(x_pc)
            y_pix=nint(y_pc)
            in_img = .true.
            if( x_pix>Ncol )then
                x_pix = Ncol
                in_img = .false.
            end if
            if( y_pix>Nrow )then
                y_pix = Nrow
                in_img = .false.
            end if

            if( x_pix<1 )then
                x_pix = 1
                in_img = .false.
            end if
            if( y_pix<1 )then
                y_pix = 1
                in_img = .false.
            end if
            if (in_img ) then
                hkl_mask(y_pix,x_pix)=j
            end if
        end do
    return
    end subroutine write_in_hkl_mask


  END module lafg_psl_image
