  !!----   Program Convert_Cyclops_to_Cylindrical_Detector
  !!----
  !!----  The title explains all !!!
  !!----  Created: 30 July 2012 (JRC)
  !!----  Noise, flip and offset corrections are not yet implemented
  !!----
  !!----  Usage: > cyclops2cyl  buffer_file <cr>
  !!----
  !!----  The buffer file should contain the following lines:
  !!----       INSTRM   My_cyclops_instrm.inf   My_New_instrm.inf
  !!----       CYL_DIM  nrow_cyl  ncol_cyl  radius_cyl  height_cyl
  !!----       TRANSF   flip_h  flip_v  offset noise size slope
  !!----       ALPHAS   my_alpha_file_name    alpha_offset
  !!----       BACKGROUND  my_background_file_name
  !!----       IMAGES   average normalize norm_time
  !!----       Image_file1
  !!----       Image_file2
  !!----       ...........
  !!----       Image_filen
  !!----
  !!----  Or, in the case the user wants to include extra information in
  !!----  binary files with "visible" headers, some items can be given following
  !!----  the image file names, after the separator "::" in order to construct a header.
  !!----  The interpretable items are: Title, user,temp, magn, press,  omeg, xdis, ydis, zdis:
  !!----  Title an user are followed by alphanumeric information and all the others are followed
  !!----  by numeric values. The items are separated by ":".
  !!----
  !!----       INSTRM   My_cyclops_instrm.inf   My_New_instrm.inf
  !!----       CYL_DIM  nrow_cyl  ncol_cyl  radius_cyl  height_cyl
  !!----       TRANSF   flip_h  flip_v  offset noise size slope
  !!----       ALPHAS   my_alpha_file_name    alpha_offset
  !!----       BACKGROUND  my_background_file_name
  !!----       IMAGES   average normalize norm_time
  !!----       Image_file1  :: title my_title : temp 300.0 : omeg 34.12  : magn 3.5 : press 0.0
  !!----       Image_file2  :: title my_title : temp 300.0 : omeg 54.12  : magn 3.5 : press 0.0
  !!----       ...........
  !!----       Image_filen  :: title my_title : temp 300.0 : omeg 174.12 : magn 3.5 : press 0.0
  !!----
  !!----
  Module Read_Info_from_Buffer
    use CFML_IO_Formats,        only: File_List_Type
    use CFML_String_Utilities,  only: U_Case, L_Case

    implicit none
    private

    public :: Get_Instrm_FileNames, Get_Cyl_dim, Get_Image_tranformation, &
              Get_Alpha_file, Get_Background_file, Get_Image_lines, console_wait

    contains

    Subroutine console_wait(mess)
      Character(len=*),optional,intent(in) :: mess
      !--- Local variables ---!
      Character(len=1) :: ans
      if(present(mess)) then
        write(unit=*,fmt="(a)") " => Press <cr> to "//trim(mess)//"..."
        read(unit=*,fmt="(a)") ans
        if(mess(1:3) == "sto" .or. mess(1:3) == "Sto" .or. mess(1:3) == "STo" .or. mess(1:3) == "STO") stop
      else
        write(unit=*,fmt="(a)") " => Press <cr>(Enter) to continue ..."
        read(unit=*,fmt="(a)") ans
      end if
      return
    End Subroutine console_wait

    Subroutine Get_Instrm_FileNames(b_file,instrm_file,mod_ins)
      Type(File_List_Type), intent(in)  :: b_file
      Character(len=*),     intent(out) :: instrm_file,mod_ins
      !--- Local Variables ---!
      character(len=256) :: line
      character(len=5)   :: key
      logical            :: instr_given
      integer            :: nl,i

      ! Look for instrument file names
      instr_given=.false.
      do nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:5))
        if(key == "INSTR") then
          instrm_file=adjustl(line(6:))
          if(len_trim(instrm_file) == 0) exit
          i=index(instrm_file," ")
          if( i /= 0) then
             mod_ins=adjustl(instrm_file(i+1:))
             if(len_trim(mod_ins) == 0)  mod_ins= "cyc_cylind.inf"
             instrm_file=instrm_file(1:i-1)
          else
             mod_ins= "cyc_cylind.inf"
          end if
          instr_given=.true.
          exit
        end if
      end do
      if(.not. instr_given) then
        write(unit=*,fmt="(a)") " => Error! No instrument file given in the buffer file "
        call console_wait("stop")
      end if
      return
    End Subroutine Get_Instrm_FileNames

    Subroutine Get_Cyl_dim(b_file,nrow_cyl, ncol_cyl, r_cyl, V_cyl)
      Type(File_List_Type), intent(in)  :: b_file
      Integer,              intent(out) :: nrow_cyl, ncol_cyl
      Real,                 intent(out) :: r_cyl, V_cyl
      !--- Local Variables ---!
      character(len=256) :: line
      character(len=7)   :: key
      logical            :: cyl_given
      integer            :: nl,i

      cyl_given=.false.
      do  nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:7))
        if(key == "CYL_DIM") then
          read(unit=line(8:),fmt=*) nrow_cyl, ncol_cyl, r_cyl, V_cyl
          cyl_given=.true.
          exit
        end if
      end do
      if(.not. cyl_given) then
        write(unit=*,fmt="(a)") " => Error! CYL_DIM instruction not given in the buffer file "
        call console_wait("stop")
      end if
      return
    End Subroutine Get_Cyl_dim

    Subroutine Get_Image_tranformation(b_file,transf_given,noise_corr,flip_h,flip_v,offset_corr,siz,m)
      Type(File_List_Type), intent(in)  :: b_file
      logical,              intent(out) :: transf_given,noise_corr,flip_h,flip_v,offset_corr
      integer,              intent(out) :: siz,m
      !--- Local Variables ---!
      character(len=256) :: line
      character(len=6)   :: key
      integer            :: nl,i,ier

      !Initializing
      transf_given=.false.
      noise_corr=.false.
      flip_h=.false.
      flip_v=.false.
      offset_corr=.false.

      do nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:6))
        if(key == "TRANSF") then
          line=L_Case(line)
          i=index(line,"flip_h")
          if(i /= 0) flip_h=.true.
          i=index(line,"flip_v")
          if(i /= 0) flip_v=.true.
          i=index(line,"noise")
          if(i /= 0) then
            noise_corr=.true.
            read(unit=line(i+6:),fmt=*,iostat=ier) siz, m
            if(ier /= 0) then
              siz=3; m=60
            end if
          end if
          i=index(line,"offset")
          if(i /= 0) offset_corr=.true.
          transf_given=.true.
          exit
        end if
      end do
      return
    End Subroutine Get_Image_tranformation

    Subroutine Get_Alpha_file(b_file,a_given,a_file,a_offset)
      Type(File_List_Type), intent(in)  :: b_file
      logical,              intent(out) :: a_given
      character(len=*),     intent(out) :: a_file
      real,                 intent(out) :: a_offset
      !--- Local Variables ---!
      character(len=256) :: line
      character(len=6)   :: key
      integer            :: nl,i,ier

      a_given=.false.
      a_offset=0.0
      a_file=" "
      do nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:6))
        if(key == "ALPHAS") then
          a_file=adjustl(line(8:))
          i=index(a_file," ")
          read(unit=a_file(i:), fmt=*,iostat=ier) a_offset
          if(ier /= 0) a_offset=0.0
          a_file=a_file(1:i-1)
          a_given=.true.
          exit
        end if
      end do
      return
    End Subroutine Get_Alpha_file

    Subroutine Get_Background_file(b_file,b_corr,bck_file)
      Type(File_List_Type), intent(in)  :: b_file
      logical,              intent(out) :: b_corr
      character(len=*),     intent(out) :: bck_file
      !--- Local Variables ---!
      character(len=256)  :: line
      character(len=10)   :: key
      integer             :: nl

      b_corr=.false.
      bck_file=" "
      do nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:10))
        if(key == "BACKGROUND") then
          bck_file=adjustl(line(12:))
          b_corr=.true.
          exit
        end if
      end do
      return
    End Subroutine Get_Background_file

    Subroutine Get_Image_lines(b_file,norm_time, i1, i2, root_names, n_blocks,raw_output)
      Type(File_List_Type),                      intent(in)  :: b_file
      real,                                      intent(out) :: norm_time
      integer,         dimension(:), allocatable,intent(out) :: i1,i2
      character(len=*),dimension(:), allocatable,intent(out) :: root_names
      integer,                                   intent(out) :: n_blocks
      logical,                                   intent(out) :: raw_output
      !--- Local Variables ---!
      logical             :: av_given, new,images_given
      character(len=256)  :: line, root, root_old
      character(len=6)    :: key
      integer             :: img_ini,nl,i,ier

      av_given=.false.
      norm_time=0.0
      images_given=.false.
      raw_output = .false.
      do nl=1, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        key=U_Case(line(1:6))
        if(key == "IMAGES") then
          images_given=.true.
          img_ini=nl+1
          line=L_case(line)
          if(index(line, "average") /= 0) av_given=.true.
          if(index(line, "raw") /= 0)   raw_output=.true.
          i=index(line,"normalize")
          if(i /= 0) then
            read(unit=line(i+9:),fmt=*,iostat=ier)  norm_time
            if(ier /= 0) norm_time=0.0
          end if
          exit
        end if
      end do
      if(.not. images_given) then
        write(unit=*,fmt="(a)") " => ERROR: no IMAGES instruction given in the buffer file!"
        call console_wait("stop")
      end if

      n_blocks=b_file%nlines-img_ini+1
      if(allocated(i1)) deallocate(i1)
      if(allocated(i2)) deallocate(i2)
      allocate(i1(n_blocks),i2(n_blocks))
      if(allocated(root_names)) deallocate(root_names)
      allocate(root_names(n_blocks))

      if(.not. av_given) then !no averaging
        do i=1,n_blocks
          i1(i)=img_ini+i-1
          i2(i)=i1(i)
          line=adjustl(b_file%line(i1(i)))
          nl=index(line," ")
          line=line(1:nl-1)
          nl=index(line,".",back=.true.)
          root_names(i)=line(1:nl-1)
        end do
        return
      end if

      !Count the effective number of blocks and assign the indices of the blocks
      n_blocks=0
      new=.true.
      do nl=img_ini, b_file%nlines
        line=adjustl(b_file%line(nl))
        if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
        i=index(line,"::")
        if(i /= 0) line=line(1:i-1)
        i=index(line,"_",back=.true.) !It is assumed that the names is of the form XXXXXX_00nn.tif
        root=line(1:i-1)
        if(new) then
          root_old=root
          new=.false.
          n_blocks=n_blocks+1
          root_names(n_blocks)=root
          i1(n_blocks)=nl
        end if
        i2(n_blocks) = nl
        if(trim(root) /= trim(root_old)) then
          i2(n_blocks)=nl-1
          n_blocks=n_blocks+1
          root_old=root
          root_names(n_blocks)=root
          i1(n_blocks)=nl
        end if
      end do

      return
    End Subroutine Get_Image_lines

  End Module Read_Info_from_Buffer

  Program Convert_Cyclops_to_Cylindrical_Detector
    use CFML_GlobalDeps,         only: pi, tpi, dp
    use CFML_IO_Formats,         only: File_List_Type, File_To_FileList
    use CFML_String_Utilities,   only: Get_LogUnit, U_Case, L_Case, Get_Separator_Pos
    use Laue_Mod,                only: Laue_Instrument_Type, Read_Laue_Instrm, Write_Laue_Instrm,  &
                                       Error_Laue_Mod,Error_Mess_Laue_Mod
    use Laue_TIFF_READ,          only: Image_Conditions, read_tiff_image,TIFF_READ_Error,TIFF_READ_Error_Message, &
                                       Write_ICd
    Use Readnwrite_Binary_Files, only: Read_Binary_File_16bits
    Use Data_Trt,                only: Octagon_to_cylinder, offset_mov,flip_2D,noise_out
    Use ReadnWrite_2D_Detector_Images
    Use Read_Info_from_Buffer

    implicit none

    integer, dimension(:,:),allocatable :: datav, datsum
    real,    dimension(:,:),allocatable :: alphas,  bckg
    character(len=512)                  :: filebuf,instrm_file,filenam,fileout, mod_ins, line, &
                                           head_info, mess, alpha_file, bckg_file, bckg_hstring,&
                                           root_name
    character(len=1)                    :: answ

    integer                             :: nrow_cyl, ncol_cyl,nrow,ncol,ipr, narg, i_buf, ier, n, &
                                           siz,m, &  ! Diameter in pixels of the noisy points, slope. Default (3,60)
                                           bin_lun, i,j,k,itif, i_alpha, nl,img_ini, nim
    real                                :: r_cyl, V_cyl, tini, tfin, alpha_offset, max_value, &
                                           sig_factor, norm_factor, norm_time, bckg_time, total_time, time_factor

    real                                :: px,pz,cx,cz,ratiop,ratioc !Pixel sizes in rectangular and cylindrical geometries

    Type(Laue_Instrument_Type)          :: LaueDiff, LaueNew

    logical                             :: ok,arggiven=.false.,  cyl_given=.false., &
                                           offset_corr=.false., flip_h=.false.,flip_v=.false., noise_corr=.false., &
                                           transf_given=.false.,hbin=.false.,head=.false., ICd_given=.false., &
                                           alphas_given=.false., background_corr=.false.,normalize=.false., &
                                           average_given=.false., new, write_avg_image=.false., raw_output=.false.

    Type(Header_Type)                   :: header,bckg_header
    Type(Image_Conditions)              :: ICd
    Type(File_List_Type)                :: buffer_file

    Integer, dimension(:),allocatable           :: i1,i2
    character(len=256),dimension(:),allocatable :: root_names
    Integer                                     :: n_blocks

    call cpu_time(tini)
    narg=COMMAND_ARGUMENT_COUNT()


    if(narg > 0) then
            call GET_COMMAND_ARGUMENT(1,filebuf)
            arggiven=.true.
    end if

    write(unit=*,fmt="(/,a)")  "    ----------------------------------------------------- "
    write(unit=*,fmt="(a)")    "              PROGRAM: CYCLOPS_TO_CYLINDER "
    write(unit=*,fmt="(a)")    "         Program to correct raw CYCLOPS images from"
    write(unit=*,fmt="(a)")    "    efficiency, averaging, conversion to CYL geometry,etc "
    write(unit=*,fmt="(a)")    "          (Version 0.1, November 2012, JRC - ILL) "
    write(unit=*,fmt="(a,/)")  "    ----------------------------------------------------- "


    if(arggiven) then
      Call File_To_FileList(filebuf,buffer_file)
      Call Get_Instrm_FileNames(buffer_file,instrm_file,mod_ins)
    else
      write(unit=*,fmt="(a)") " => Error! No buffer file given! "
      write(unit=*,fmt="(a)") " => Usage of the program Convert_Cyclops_to_Cylindrical_Detector -> "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") " Prompt>  cyclops2cyl  buffer_file <cr> "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") " The buffer file should contain the following lines: "
      write(unit=*,fmt="(a)") "      INSTRM   My_cyclops_instrm.inf  My_New_instrm.inf "
      write(unit=*,fmt="(a)") "      CYL_DIM  nrow_cyl  ncol_cyl  radius_cyl  height_cyl "
      write(unit=*,fmt="(a)") "      TRANSF   flip_h  flip_v  offset noise size slope "
      write(unit=*,fmt="(a)") "      ALPHAS   my_alpha_file_name    alpha_offset"
      write(unit=*,fmt="(a)") "      BACKGROUND  my_background_file_name"
      write(unit=*,fmt="(a)") "      IMAGES   average normalize norm_time raw"
      write(unit=*,fmt="(a)") "      Image_file1 "
      write(unit=*,fmt="(a)") "      Image_file2 "
      write(unit=*,fmt="(a)") "      ........... "
      write(unit=*,fmt="(a)") "      Image_filen "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)")  " Or, in the case the user wants to include extra information in                        "
      write(unit=*,fmt="(a)")  " binary files with 'visible' headers, some items can be given following                "
      write(unit=*,fmt="(a)")  " the image file names, after the separator '::' in order to construct a header.        "
      write(unit=*,fmt="(a)")  " The interpretable items are: Title, user,temp, magn, press,  omeg, xdis, ydis, zdis:  "
      write(unit=*,fmt="(a)")  " Title an user are followed by alphanumeric information and all the others are followed"
      write(unit=*,fmt="(a)")  " by numeric values. The items are separated by ':'.                                    "
      write(unit=*,fmt="(a)")  "                                                                                       "
      write(unit=*,fmt="(a)")  "      INSTRM   My_cyclops_instrm.inf                                                   "
      write(unit=*,fmt="(a)")  "      CYL_DIM  nrow_cyl  ncol_cyl  radius_cyl  height_cyl                              "
      write(unit=*,fmt="(a)")  "      TRANSF   flip_h  flip_v  offset noise size slope "
      write(unit=*,fmt="(a)")  "      ALPHAS   my_alpha_file_name    alpha_offset"
      write(unit=*,fmt="(a)")  "      BACKGROUND  my_background_file_name"
      write(unit=*,fmt="(a)")  "      IMAGES   average normalize norm_time raw"
      write(unit=*,fmt="(a)")  "      Image_file1  :: title my_title : temp 300.0 : omeg 34.12  : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)")  "      Image_file2  :: title my_title : temp 300.0 : omeg 54.12  : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)")  "      ...........                                                                      "
      write(unit=*,fmt="(a)")  "      Image_filen  :: title my_title : temp 300.0 : omeg 174.12 : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)")  "       "
      call console_wait("stop")
    end if

    call Read_Laue_Instrm(instrm_file,LaueDiff)
    if(Error_Laue_Mod) then
      write(unit=*,fmt="(a)") " => "//trim(Error_Mess_Laue_Mod)
      call console_wait("stop")
    end if

    ncol=LaueDiff%np_h; nrow=LaueDiff%np_v
    write(unit=*,fmt="(a)")         " => Instrument values (Npix_hor,Npix_vert, D, H, V): "
    write(unit=*,fmt="(a,2i6,a)")   "    Npix_hor x Npix_vert  = (",LaueDiff%np_h,LaueDiff%np_v," )"
    write(unit=*,fmt="(a,f10.4,a)") "    Distance det-sample,D = ",LaueDiff%D," mm"
    write(unit=*,fmt="(a,f10.4,a)") "    Horizontal Length,  H = ",LaueDiff%H," mm"
    write(unit=*,fmt="(a,f10.4,a)") "    Vertical   Length,  V = ",LaueDiff%V," mm"


    !Read now the wished characteristics of the cylindrical image
    Call Get_Cyl_dim(buffer_file,nrow_cyl, ncol_cyl, r_cyl, V_cyl)

    !Pre-calculation of cylindrical pixel sizes and comparison with rectangular sizes
    !Rise a warning if there is a distortion introduced by the number of vertical pixels
    !in the cylindrical geometry
    px=LaueDiff%H/real(LaueDiff%np_h-1)
    pz=LaueDiff%V/real(LaueDiff%np_v-1)
    cx=2.0*pi*r_cyl/real(ncol_cyl-1)
    cz=V_cyl/real(nrow_cyl-1)
    ratiop=px/pz; ratioc=cx/cz
    write(unit=*,fmt="(3(a,f10.4))") " => Original    pixel sizes:  px=",px*1000.0,"um,   pz=",pz*1000.0,"um,  ratio(px/pz)=",ratiop
    write(unit=*,fmt="(3(a,f10.4))") " => Cylindrical pixel sizes:  cx=",cx*1000.0,"um,   cz=",cz*1000.0,"um,  ratio(cx/cz)=",ratioc
    ratioc=100.0*abs(ratiop/ratioc-1.0)
    if(ratioc > 6.0) then
      write(unit=*,fmt="(a,f6.2)")  " => Warning! a distortion of the image is expected -> 100*abs(ratiop/ratioc-1.0)= ",ratioc
      write(unit=*,fmt="(a)", advance="no")  " => Do you want to let the program calculate the appropriate number of vertical pixels? (y/n): "
      read(unit=*,fmt="(a)") answ
      if( answ == "y" .or. answ == "Y") then
        ! We impose ratioc=ratiop, so  cz=cx/ratiop
        ! cz=cx/ratiop = V_cyl/(nrow_cyl-1) => nrow_cyl= V_cyl*ratiop/cx+1
        nrow_cyl= nint(V_cyl*ratiop/cx+1.0)
        write(unit=*,fmt="(2(a,i5))")  " => New size of the cyclindrical image: nrow_cyl(vertical)=",nrow_cyl,", ncol_cyl(horizontal)=", ncol_cyl
      end if
    end if

    !Read now the transformation to be done
    Call Get_Image_tranformation(buffer_file,transf_given,noise_corr,flip_h,flip_v,offset_corr,siz,m)

    !Read now the efficiency correction file (if not given all alphas=1)
    Call Get_Alpha_file(buffer_file,alphas_given,alpha_file,alpha_offset)

    if(alphas_given) then
      nrow=2400; ncol=7680
      allocate(alphas(nrow,ncol))
      alphas=1.0
      alphas_given=.false.
      if(len_trim(alpha_file) /= 0) then !Reading the alpha file
        Call Get_LogUnit(i_alpha)
        open(unit=i_alpha, file=trim(alpha_file), status="old",form="unformatted", access="stream",iostat=ier)
        if(ier == 0) then
          alphas_given=.true.
          do i=1,nrow
            do j=1,ncol
              read(unit=i_alpha) alphas(i,j)
            end do
          end do
          close(unit=i_alpha)
        else
          write(unit=*,fmt="(a)") "Error reading the ALPHAS file: "//trim(alpha_file)
        end if
      end if
    end if

    !Read now the Background correction file
    Call Get_Background_file(buffer_file,background_corr,bckg_file)

    if(background_corr) then
      nrow=2400; ncol=7680
      allocate(bckg(nrow,ncol))
      bckg=0.0
      if(len_trim(bckg_file) /= 0) then !Reading the background file
        !First determine if it is a TIF or an hbin file
        if(index(bckg_file,".hbin") /= 0) then
          Call Read_Laue_Image(bckg_file,nrow,ncol,Datav,ok,header=bckg_header)

        else  !A TIF file with separated header is assumed

          Call Read_Laue_Image(bckg_file,nrow,ncol,Datav,ok)
          !Try to get header info from the ASCII file accompanying the image
          i=index(bckg_file,".",back=.true.)
          Call Get_Head_Info(bckg_file(1:i-1), bckg_hstring)
          if(len_trim(bckg_hstring) > 0) then
             write(unit=*,fmt="(a)") " =>BACKGROUND Info: "//trim(bckg_hstring)
          end if
          if(ok) Call Set_header(bckg_header,bckg_hstring)
        end if

        if(ok) then
          bckg=Datav  !Convert to real
          bckg_time=bckg_header%time(1)
        else
          background_corr=.false.
          write(unit=*,fmt="(a)") "Error reading the Background file: "//trim(bckg_file)
        end if
      end if
    end if

    write(unit=*,fmt="(a)") "       "
    write(unit=*,fmt="(a)")         " => Cylinder characteristics:      "
    write(unit=*,fmt="(a,2i6,a)")   "     Npix_hor x Npix_vert             = (",ncol_cyl,nrow_cyl," )"
    write(unit=*,fmt="(a,f10.4,a)") "     Distance det-sample (Radius)     = ",r_cyl," mm"
    write(unit=*,fmt="(a,f10.4,a)") "     H-Length of the deployed surface = ",r_cyl*tpi," mm"
    write(unit=*,fmt="(a,f10.4,a)") "     V-Length of the deployed surface = ",V_cyl," mm"

    if(flip_h)  write(unit=*,fmt="(a)") " => Final image flipped horizontally"
    if(flip_v)  write(unit=*,fmt="(a)") " => Final image flipped vertically"
    if(offset_corr) write(unit=*,fmt="(a)") " => Final image corrected from offset"
    if(noise_corr) write(unit=*,fmt="(2(a,i3))") " => Final image corrected from noisy points. Parameters -> size: ", &
                   siz," slope: ",m
    if(alphas_given) then
      write(unit=*,fmt="(a)")        " => Efficiency correction (alphas) read from file: "//trim(alpha_file)
      write(unit=*,fmt="(a,f8.2,a)") "    Applied offset =", alpha_offset," in formula: CorrImage=NINT((RawImage-Offset)*Alphas)"
    else
      write(unit=*,fmt="(a)") " => Efficiency correction (alphas) not applied! All alphas=1 ... "
    end if
    if(background_corr) then
      write(unit=*,fmt="(a)")        " => Background correction read from file: "//trim(bckg_file)
    else
      write(unit=*,fmt="(a)")        " => No background correction applied"
    end if

    !--- Get the lines containing the images and information about output type, averaging and normalising
    call Get_Image_lines(buffer_file,norm_time, i1, i2, root_names, n_blocks, raw_output)

    write(unit=*,fmt="(a,i3)") " => N_blocks: ",n_blocks
    write(unit=*,fmt="(a,f10.2)") " => Normalization time: ",norm_time
    do k=1,n_blocks
      write(unit=*,fmt="(a,3i4,a)") " => Block: ",k, i1(k),i2(k), "   "//trim(root_names(k))
    end do
    write(unit=*,fmt="(/,a)")  " ------------------------------------------"
    write(unit=*,fmt="(a)")    " => Start reading and correcting images ..."
    write(unit=*,fmt="(a,/)")  " ------------------------------------------"

    n=0
    new=.true.
    if(allocated(datsum)) deallocate(datsum)
    allocate(datsum(nrow,ncol))  !nrow=2400;ncol=7680 for cyclops
    LaueNew=LaueDiff

    do k=1,n_blocks
      root_name=root_names(k)
      datsum=0
      nim=0
      total_time=0.0
      do nl=i1(k),i2(k)
        line=buffer_file%line(nl)
        line=adjustl(line)
        if(line(1:1) == "!" .or.  line(1:1) == "#" .or. len_trim(line) == 0) cycle
        i=index(line,"::")
        if(i /= 0) then
          filenam=trim(line(1:i-1))
          head_info=line(i+2:)
        else
          filenam=trim(line)
          !Try to get header info from the ASCII file accompanying the image
          i=index(line,".",back=.true.)
          Call Get_Head_Info(line(1:i-1), head_info)
          if(len_trim(head_info) == 0) then
            head_info=" : title unknown : user unknown : temp  300.0  : omeg  0.0  "
          end if
        end if
        n=n+1
        if(allocated(datav)) deallocate(datav)
        allocate(datav(nrow,ncol))  !nrow=2400;ncol=7680 for cyclops
        Datav=0
        write(unit=*,fmt="(/,a)") " => Reading image from file: "//trim(filenam)
        i=len_trim(head_info)
        i=index(head_info(i/2+1:),":")+i/2
        write(unit=*,fmt="(a)") " => Header info "//head_info(1:i-1)
        write(unit=*,fmt="(a)") "                "//trim(head_info(i:))
        itif=index(filenam,".tif")
        if( itif /= 0) then
          Call Read_Laue_Image(filenam,nrow,ncol,Datav,ok,ICd)
          ICd_given=.true.
          if(ok .and. ncol <= 4000) call Write_ICd(ICd)
        else
          Call Read_Laue_Image(filenam,nrow,ncol,Datav,ok)
        end if
        if(ICd_given .and. ok .and. ncol <= 4000) then
          Call Set_header(header,head_info,ICd)
        else
          Call Set_header(header,head_info)
        end if

        !First of all correct the image from efficiency
        if(alphas_given) then
          Datav=nint((real(Datav)-alpha_offset)*alphas)
        end if

        ! Now correct for noisy points
        if(noise_corr) call noise_out(1.0,1.0,real(Ncol),real(Nrow),siz,m,Datav)

        datsum = datsum + Datav
        total_time= total_time + header%time(1)
      end do

      ! Now we have the sum of the k-block of images, we have to write the file with the
      ! proper root name. Firts correct for background and normalize.
      ! Correct for background
      if(background_corr) datsum = datsum - nint(bckg * total_time/bckg_time)
      where(datsum < 0) datsum = 0
      if(norm_time > 1.0) then
        time_factor=norm_time/total_time
        header%normalization_time=norm_time
      else
        time_factor=1.0
        header%normalization_time=total_time
      end if
      datsum=nint(real(datsum)*time_factor)
      max_value=maxval(Datsum)
      norm_factor=65535.0/max_value
      sig_factor= time_factor * norm_factor

      write(unit=*,fmt="(a,f12.1)") " => Maximum value: ",max_value
      write(unit=*,fmt="(a,f12.2)") " => Total Time   : ",total_time
      write(unit=*,fmt="(a,f12.5)") " => Time Factor  : ",time_factor
      write(unit=*,fmt="(a,f12.5)") " => Norm. Factor : ",norm_factor
      write(unit=*,fmt="(a,f12.5)") " => Sigma Factor : ",sig_factor

      if(allocated(datav)) deallocate(datav)
      allocate(datav(nrow,ncol))  !nrow=2400;ncol=7680 for cyclops
      Datav=nint(real(datsum)*norm_factor)      !Final image
      max_value=maxval(Datav)
      write(unit=*,fmt="(a,f12.1)") " => Final Maximum value: ",max_value
      header%sigma_factor=sig_factor     !The background has not been taken into account

      header%time(1)=total_time

      if(ok .or. itif /= 0) then
        !Transform to cylindrical geometry, Datav is re-allocated in Octagon_to_cylinder to the
        !size given by the user for the final cylindrical image: nrow_cyl, ncol_cyl
        call Octagon_to_cylinder(LaueNew%H,LaueNew%V,datav, nrow_cyl, ncol_cyl, r_cyl, V_cyl)

        !Now correct from offset and flips
        if(offset_corr) call offset_mov(datav,ncol_cyl/16)
        if(flip_h .or. flip_v) call flip_2D(datav,flip_v,flip_h)

        !Writing the new Image file


        if(raw_output) then
          fileout=trim(root_name)//"_cyl.raw"
          write(unit=*,fmt="(2(a,i5),a)") " => Writing image: "//trim(fileout)//"  of size: ",nrow_cyl," x ",ncol_cyl," pixels"
          call Write_RAW_Binary_Image(fileout,nrow_cyl,ncol_cyl,datav)
        else
          fileout=trim(root_name)//"_cyl.hbin"
          write(unit=*,fmt="(2(a,i5),a)") " => Writing image: "//trim(fileout)//"  of size: ",nrow_cyl," x ",ncol_cyl," pixels"
          Call Write_Header_2D_Image(fileout,header,ok,mess,bin_lun)
          if(.not. ok) then
             write(unit=*,fmt="(/a)") " => Transformed image not written for file: "//trim(filenam)
             write(unit=*,fmt="(a)") " => "//trim(mess)
             cycle
          end if
          Call Write_Binary_2D_Image(bin_lun,header,datav,1,ok,mess)
          if(.not. ok) then
             write(unit=*,fmt="(/a)") " => Error in transformed image file: "//trim(fileout)
             write(unit=*,fmt="(a)") " => "//trim(mess)
             cycle
          end if
        end if
      else
        write(unit=*,fmt="(a)") " => Error reading the image file: "//trim(filenam)
        n=n-1
      end if

    end do

    write(unit=*,fmt="(/,a,i4)") " => Total number of treated image files: ",n
    write(unit=*,fmt="(a,i4)") " => Total number of written image files: ",n_blocks
    !Complete the new instrument characteristics
    LaueNew%dtype = "Cyl"
    LaueNew%H=tpi*r_cyl
    LaueNew%V=V_cyl
    LaueNew%np_h=ncol_cyl
    LaueNew%np_v=nrow_cyl
    LaueNew%z_min=-0.5*LaueNew%V
    LaueNew%z_max=-LaueNew%z_min
    LaueNew%gap_min=0.0; LaueNew%gap_max=180.0
    LaueNew%gan_min=-180.0; LaueNew%gan_max=0.0

    if(offset_corr) then
      LaueNew%xo=0.5*real(LaueNew%Np_h)
      LaueNew%zo=0.5*real(LaueNew%Np_v)
      LaueNew%x_min=-0.5*LaueNew%H
      LaueNew%x_max=-LaueNew%x_min
    else
      LaueNew%xo=0.5*real(LaueNew%Np_h)+real(ncol_cyl)/16.0
      LaueNew%zo=0.5*real(LaueNew%Np_v)
      LaueNew%x_min=-(0.5*LaueNew%H+LaueNew%H/16.0)
      LaueNew%x_max= (0.5*LaueNew%H-LaueNew%H/16.0)
    end if
    !Writing the new instrument file
    call Get_LogUnit(ipr)
    open(unit=ipr, file=trim(mod_ins), status="replace", action="write")
    Call Write_Laue_Instrm(LaueNew, ipr)
    close(unit=ipr)
    write(unit=*,fmt="(a)") " => New instrument file: "//trim(mod_ins)
    call cpu_time(tfin)
    write(unit=*,fmt="(a)") " => Program finished normally ..."
    write(unit=*,fmt="(a,f8.3,a)") " => CPU-time: ",tfin-tini," seconds"
    call console_wait("stop")

    contains

    Subroutine Set_header(H,string,ICd)
      Type(Header_Type),              intent(in out) :: h
      character(len=*),               intent(in out) :: string
      Type(Image_Conditions),optional,intent(in)     :: ICd
      !--- Local variables ---!
      integer, dimension(15)            :: pos
      integer                           :: i,j,ncar,nitems,i1,i2
      character(len=80), dimension(16)  :: items
      character(len=10)                 :: key
      character(len=512)                :: Lstring
      if(present(ICd)) then
        Lstring=L_case(string)
        if(index(Lstring,"title") == 0) then
          string=trim(string)//" : "//"title "//trim(ICd%sample)//" "//trim(ICd%comment)
        end if
        if(index(Lstring,"user") == 0) then
          string=trim(string)//" : "//"user "//trim(ICd%user)//" "//trim(ICd%datetime)
        end if
        if(index(Lstring,"omeg") == 0) then
          string=trim(string)//" : "//"omeg "
          i=len_trim(string)
          write(unit=string(i+2:),fmt="(f9.4)") ICd%expose_phi
        end if
        if(index(Lstring,"temp") == 0) then
          string=trim(string)//" : "//"temp "
          i=len_trim(string)
          write(unit=string(i+2:),fmt="(f9.4)") 0.5*(ICd%temp_begin+ICd%temp_end)
        end if
      end if
      ! Extract the information contained in string for constructing the header
      ! Only a limited number of items are presently available
      ! The items in string are separated by ":"
      Call Get_Separator_Pos(string,":",pos,ncar)
      nitems=ncar+1
      i2=pos(1)-1
      i1=1
      items(1)=adjustl(string(i1:i2))
      do i=2,ncar
        i1=pos(i-1)+1
        i2=pos(i)-1
        items(i)=adjustl(string(i1:i2))
      end do
      items(nitems)=adjustl(string(pos(ncar)+1:))
      Call Init_Header(H)
      Call  Allocate_Header(H, &
                            environment=(/"   Temp-set"," Temp-regul","Temp-sample","Magn. Field","   Pressure"/), &
                            motors     =(/" Omega","  Motx","  Moty","  Motz"/),         &
                            env_units  =(/"Kelvin","Kelvin","Kelvin"," Tesla","GPasc."/), &
                            mot_units  =(/"Degrees","     mm","     mm","     mm"/)  )
      Do i=1,nitems
        key=L_case(items(i))
        j=index(key," ")
        if(j == 0) cycle
        key=key(1:j-1)
        Select Case (trim(key))
          case("title")
            H%Title=items(i)(6:)
          case("user")
            h%User_LC_Date_Time=items(i)(5:)
          case("temp")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(1,1)
            H%Envnt_Value(2,1) = H%Envnt_Value(1,1)
            H%Envnt_Value(3,1) = H%Envnt_Value(1,1)
          case("stemp")
            read(unit=items(i)(6:),fmt=*) H%Envnt_Value(1,1)
          case("rtemp")
            read(unit=items(i)(6:),fmt=*) H%Envnt_Value(2,1)
          case("temps")
            read(unit=items(i)(6:),fmt=*) H%Envnt_Value(3,1)
          case("magn")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(4,1)
          case("pres")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(5,1)
          case("omeg")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(1,1)
          case("xdis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(2,1)
          case("ydis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(3,1)
          case("zdis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(4,1)
          case("time")
            read(unit=items(i)(5:),fmt=*) H%time(1)
          case("monitor")
            read(unit=items(i)(8:),fmt=*) H%monitor(1)
        End Select
      end do
      if(present(ICd)) then
         H%time(1)=ICd%expose_time
      end if
      H%Nrow=nrow_cyl; H%Ncol=ncol_cyl
      H%Instrument_name="CYCLOPS (Cylindrical Shape)"
      if(present(ICd))  H%Instrument_name = ICd%Host
      H%Detector_type="CYL"
      H%Pix_Size_h=tpi*r_cyl/real(ncol_cyl-1)
      H%Pix_Size_v=V_cyl/real(nrow_cyl-1)
      H%Sample_Detector_dist=r_cyl
      H%Lambda_min=0.8; H%Lambda_max=5.0
      H%Scan_Type="ACQ"

      return
    End Subroutine Set_header

    Subroutine Get_Head_Info(afile, headInfo)
      character(len=*), intent(in)  :: afile
      character(len=*), intent(out) :: headInfo
      !--- Local variables ---!
      integer :: i, i_info,ier
      real, dimension(5)  ::  aux
      logical :: existe
      character(len=80) :: line,string

      headInfo=" "
      inquire(file=trim(afile),exist=existe)
      if(.not. existe) return
      Call Get_LogUnit(i_info)
      open(unit=i_info,file=trim(afile),status="old",action="read",position="rewind")
      do
        read(unit=i_info,fmt="(a)",iostat=ier) line
        if(ier /= 0) exit
        if(index(line,"Inst User") /= 0) then
           read(unit=i_info,fmt="(a)") line
           do i=1,len_trim(line)
            if(line(i:i) == ":") line(i:i)="."
           end do
           headInfo=trim(headInfo)//" : "//"user "//line(5:10)//" "//line(11:14)//" "//line(15:)
        else if( index(line,"Title") /= 0) then
           read(unit=i_info,fmt="(a)") line
           headInfo=trim(headInfo)//" : "//"title "//trim(line)
        else if (index(line,"Temp-s.pt") /= 0) then
           read(unit=i_info,fmt=*) aux
           string=" "
           write(unit=string,fmt="(2(a,f8.4))")  " : omeg ",aux(4)," : xdis ",aux(5)
           headInfo=trim(headInfo)//trim(string)
           read(unit=i_info,fmt=*) aux
           string=" "
           write(unit=string,fmt="(2(a,f8.4))")  " : ydis ",aux(1)," : zdis ",aux(2)
           headInfo=trim(headInfo)//trim(string)
           do i=1,6
            read(unit=i_info,fmt=*) aux
           end do
           string=" "
           write(unit=string,fmt="(a,f12.3)")  " : time ",aux(4)*0.001
           headInfo=trim(headInfo)//trim(string)
           do i=1,2
            read(unit=i_info,fmt=*) aux
           end do
           string=" "
           write(unit=string,fmt="(4(a,f8.4))")  " : stemp ",aux(1)," : rtemp ",aux(2)," : temps ",aux(3)," : magn ", aux(5)
           headInfo=trim(headInfo)//trim(string)
           exit
        end if
      end do

      close(unit=i_info)
      return
    End Subroutine Get_Head_Info

 End Program Convert_Cyclops_to_Cylindrical_Detector
