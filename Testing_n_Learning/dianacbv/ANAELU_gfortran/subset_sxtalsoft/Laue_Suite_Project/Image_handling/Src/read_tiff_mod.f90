!!----  Code transformed by J.Rodriguez-Carvajal (ILL) by putting
!!----  explicitly the intent of the subroutines arguments and changing
!!----  a little bit the style: elimination of formats and numerical labels,
!!----  change relational operators (e.g. .GE. by >= ... etc)
!!----  New routines in order to improve the efficiency on reading known simple
!!----  images have been included.
!!----  The original header is:
!---------------------------------------------------------------
!   module tiffreadsubs
!
!   Joe Haemer
!   August 2006
!
!   Definition of Subroutines to read a TIFF image
!
!   works only for uncomressed images, and only tested on
!   little-endian (PC) byte order files
!
!   should work for 1, 4, 8 bit per sample images
!   either monochrome, grayscale, or RGB
!
!   USES: module tifffieldtags
!
!   SUBROUTINES
!
!   readTiffInfo - scans file for image size and layout information
!                - stores tag information in tiffFieldTag module arrays
!                - return data enables dynamic allocation of image array
!                - necessary to call before readTiffImage
!
!   readTiffImage - after call to readTiffInfo, uses data from
!                   tiffFieldTag module to read TIFF image data
!
!   Additional subroutines:
!
!   readTiffInfo --
!       - readHeader
!       - readIFD
!       - readIFDEntry
!       - getSizeRes
!
!   readTiffImage --
!       - read24Array
!       - readStrip
!
!   Utility subroutines:
!       - readInt2Array
!       - readInt4Array
!       - readRational
!       - readInt4
!       - readInt2
!       - byteswapi2
!       - byteswapi4
!
!---------------------------------------------------------------

  Module tiff_readsubs

    use ReadnWrite_Binary_Files, only: Read_Binary_File_16bits

    implicit none

    private
    public :: readTiffInfo, readTiffImage, freadTiffImage

    !   for compilers with byte default
    !   (gfortran, sun, intel -assume byterecl)
    integer, parameter :: myrecl = 4

    !   for compilers with lword default
    !   (intel default)
    !    integer, parameter :: myrecl = 1

    logical,           public :: tiff_error=.false.
    character(len=256),public :: tiff_error_message= " "
    character(len=256),public :: tiff_warning_message= " "

    integer, parameter :: tagLB = 254, tagUB = 296, typeLB = 1, typeUB = 12

    !   resolution unit descriptions
    character(len=5), dimension(3) :: unitLabel = (/ 'units', ' dpi ', 'px/cm' /)

    !   labels of field type codes
    character(len=9) :: typeLabel, typeList(typeLB:typeUB) = &
    (/ 'BYTE     ', 'ASCII    ', 'SHORT    ', 'LONG     ', 'RATIONAL ', &
       'SBYTE    ', 'UNDEFINED', 'SSHORT   ', 'SLONG    ', 'SRATIONAL', &
       'FLOAT    ', 'DOUBLE   '/)


    !   length in bytes of field types
    integer(kind=1) :: typeSize, typeLengths(typeLB:typeUB) = &
                                  (/ 1, 1, 2, 4, 8, &
                                     1, 1, 2, 4, 8, &
                                              4, 8 /)

    !   array to hold key values from IFD's
    integer, dimension(tagLB:tagUB) :: keyCount, keySize, keyValOffset

    !   Description of IFD Entry tags
    !   These are required for grayscale images except 254, 269, 270, 274, 277, 284
    !   character(24) :: tagList(tagLB:tagUB) = 'NotRequired', tagLabel
    !   original initialization gives error on repeated data assignment
    !   (ok for intel, digital, but not gfortran)

    character(len=24) :: tagLabel
    character(len=24), dimension(tagLB:tagUB) :: tagList = (/                                                   &
    'NewSubfileType          ','NotRequired             ','ImageWidth              ','ImageLength             ',&
    'BitsPerSample           ','Compression             ','NotRequired             ','NotRequired             ',&
    'PhotometricInterpretaion','NotRequired             ','NotRequired             ','NotRequired             ',&
    'NotRequired             ','NotRequired             ','NotRequired             ','DocumentName            ',&
    'ImageDescription        ','NotRequired             ','NotRequired             ','StripOffsets            ',&
    'Orientation             ','NotRequired             ','NotRequired             ','SamplesPerPixel         ',&
    'RowsPerStrip            ','StripByteCounts         ','NotRequired             ','NotRequired             ',&
    'XResolution             ','YResolution             ','PlanarConfiguration     ','NotRequired             ',&
    'NotRequired             ','NotRequired             ','NotRequired             ','NotRequired             ',&
    'NotRequired             ','NotRequired             ','NotRequired             ','NotRequired             ',&
    'NotRequired             ','NotRequired             ','ResolutionUnit          ' /)

    contains

    !---------------------------------------------------------------

    subroutine readTiffInfo(inUnit, inFileName, byteOrder, imgSize, xRes, yRes, &
                            resUnitFlag, SPP, BPS, comp,lun)
      integer,                       intent(in)  :: inUnit
      character(len=*),              intent(in)  :: inFileName
      character(len=2),              intent(out) :: byteOrder
      integer,         dimension(2), intent(out) :: imgSize
      integer(kind=4), dimension(2), intent(out) :: xRes, yRes
      integer(kind=1),               intent(out) :: resUnitFlag
      integer,                       intent(out) :: SPP
      integer,         dimension(3), intent(out) :: BPS
      integer,                       intent(out) :: comp
      integer, optional,             intent(in)  :: lun

      integer           :: ios
      integer(kind=2)   :: tiffIDNum
      integer(kind=4)   :: firstIFDOffset, nextIFDOffset

      !   xRes, yRes: resolution numerator is in (1), denominator in (2)
      !   image: must be integer(2) b/c fortran integers are always signed
      !   SPP: samples per pixel, BPS: bits per sample, comp: compression flag

      !   Open image file
      !   ### changed recl to 4 for gfortran

      open(unit=inUnit, iostat=ios, file=inFileName, form='UNFORMATTED', &
           action='READ', status='OLD', access='DIRECT', recl=myrecl)

      if(present(lun))  then

       write(unit=lun, fmt='(a7,a32,a10,i3)') ' Opened ', inFileName, ' IOSTAT = ', ios

         call readHeader(inUnit, byteOrder, tiffIDNum, firstIFDOffset,lun)  !   Read header
         if(tiff_error) return

        !   Read Image file directory (IFD) into tiffFieldTags module arrays
         call readIFD(inUnit, firstIFDOffset, nextIFDOffset,lun)
         if(tiff_error) return

        !   Store image size, resolution from tiffFieldTags module arrays
         call getSizeRes(inUnit, imgSize, xRes, yRes, resUnitFlag, SPP, BPS, comp,lun)
         if(tiff_error) return
      else

         call readHeader(inUnit, byteOrder, tiffIDNum, firstIFDOffset)
         if(tiff_error) return
         call readIFD(inUnit, firstIFDOffset, nextIFDOffset)
         if(tiff_error) return
         call getSizeRes(inUnit, imgSize, xRes, yRes, resUnitFlag, SPP, BPS, comp)
         if(tiff_error) return

      end if
      !   Close image file
      close(unit=inUnit)
      if(present(lun)) write(unit=lun,fmt='(a6,tr1,a70)') ' Closed', inFileName
      return
    end subroutine readTiffInfo

   !---------------------------------------------------------------

    subroutine readHeader(inUnit, byteOrder, tiffIDNum, firstIFDOffset,lun)
      integer,          intent(in) :: inUnit
      character(len=2), intent(out):: byteOrder
      integer(kind=2),  intent(out):: tiffIDNum
      integer(kind=4),  intent(out):: firstIFDOffset
      integer, optional,intent(in) :: lun

      integer         :: i

      !   read header data
      read(unit=inUnit, rec=1) byteOrder, tiffIDNum
      read(unit=inUnit, rec=2) firstIFDOffset

      !   check endian-ness
      if (byteOrder == 'II') then
         if(present(lun))   write(unit=lun,fmt="(a)") ' => Little-endian'
      else
          if(present(lun))  write(unit=lun,fmt="(a)") ' => Big-endian, data may not be correct'
          tiff_warning_message=' => Big-endian, data may not be correct'
          call byteswapi2(tiffIDNum)
          call byteswapi4(firstIFDOffset)
      end if

      !   output header data
       if(present(lun))  write(unit=lun, fmt="(2a,a,i3,a,z7,a)") 'ByteOrder:',byteOrder, ' Magic #:',tiffIDNum,&
                                            ' First IFD Offset:',firstIFDOffset,'h'

      !   check if it is a TIFF file
      if (tiffIDNum == 42) then
          if(present(lun))  write(unit=lun,fmt="(a)") ' => is a TIFF file'
      else
          if(present(lun))  write(unit=lun,fmt="(a)") ' => error: Is NOT a TIFF file'
          tiff_error= .true.
          tiff_error_message= " => error: Is NOT a TIFF file "
      end if
      return
    end subroutine readHeader

!---------------------------------------------------------------

    subroutine readIFD(inUnit, thisIFDOffset, nextIFDOffset,lun)
      integer,          intent(in)  :: inUnit
      integer,          intent(in)  :: thisIFDOffset
      integer(kind=4),  intent(out) :: nextIFDOffset
      integer, optional,intent(in)  :: lun
      !--- Local variables
      integer                       :: thisRec
      integer                       :: i
      integer(kind=2)               :: dummyi2, IFDentryCount
      integer(kind=2),dimension(2)  :: nextIFDOffsetParts
      integer(kind=4)               :: nIFDO

      equivalence (nextIFDOffsetParts, nIFDO)

      !   find record number from byte offset
      thisRec = thisIFDOffset / 4 + 1

      !   get number of entries in directory
      read(unit=inUnit,rec=thisRec) IFDentryCount

      if(present(lun)) then
        write(unit=lun,fmt=*)
        write(unit=lun,fmt="(a,z12,a,i8,a,i6)") ' IFD Byte Offset: ',thisIFDOffset,'h, Record:', thisRec, &
                                    ', IFD Entries:',IFDentryCount
        !     read Image File Directory Entry (IFD)
        do i=1, IFDentryCount
            call readIFDEntry(inUnit, thisRec,lun)
            thisRec = thisRec + 3
        end do
      else
        do i=1, IFDentryCount
            call readIFDEntry(inUnit, thisRec)
            thisRec = thisRec + 3
        end do
      end if


!     find offset of next IFD
      read(unit=inUnit,rec=thisRec) dummyi2, nextIFDOffsetParts(1)
      read(unit=inUnit,rec=thisRec+1) nextIFDOffsetParts(2)
      nextIFDOffset = nIFDO
      if(present(lun)) write(unit=lun,fmt="(a,z7,a,i11)") ' => Next IFD Byte Offset: ',nextIFDOffset,'h',nextIFDOffset

    end subroutine readIFD

    !---------------------------------------------------------------

    subroutine readIFDEntry(inUnit, thisRec,lun)
      integer,          intent(in) :: inUnit
      integer,          intent(in) :: thisRec
      integer, optional,intent(in)  :: lun

      character(len=8)               :: offsetHeader
      integer(kind=2)                :: dummyi2, idTag, fieldType
      integer(kind=2), dimension(2)  :: valCountParts,valOffsetParts
      integer(kind=4)                :: valueCount, valueOffset

      equivalence (valCountParts, valueCount), (valOffsetParts, valueOffset)

      !   read Image File Directory Entry
      read(unit=inUnit,rec=thisRec)   dummyi2, idTag
      read(unit=inUnit,rec=thisRec+1) fieldType, valCountParts(1)
      read(unit=inUnit,rec=thisRec+2) valCountParts(2), valOffsetParts(1)
      read(unit=inUnit,rec=thisRec+3) valOffsetParts(2)

      !    write(6,'(I4,4Z4)') thisRec, valCountParts, valOffsetParts
      !   check bounds, assign labels for tags and types, store key values

      if ((fieldType >= typeLB).AND.(fieldType <= typeUB)) then
          typeLabel = typeList(fieldType)
          typeSize = typeLengths(fieldType)
      else
          typeLabel = 'Unknown Type'
          typeSize = 1
      end if

      if ((idTag >= tagLB) .AND. (idTag <= tagUB)) then
          tagLabel = tagList(idTag)
          keyCount(idTag) = valueCount
          keySize(idTag) = typeSize
          keyValOffset(idTag) = valueOffset
      else
          tagLabel = 'Not in Table'
      end if

      if(present(lun)) then
          !   check to see if Value or Offset is stored in last 4 bytes
          !   display directory entry values
          if ((valueCount == 1).AND.(typeSize <= 4)) then
              offsetHeader = ' Value:'
              write(unit=lun,fmt="(I6,1X,A24,1X,I2,'B ',A9,' N:',I6,A8,I10)") idTag, tagLabel,&
                                typeSize,typeLabel,valueCount,offsetHeader,valueOffset
          else
              offsetHeader = ' Offset:'
              write(unit=lun,fmt="(I6,1X,A24,1X,I2,'B ',A9,' N:',I6,A8,Z10,'h')") idTag, tagLabel,&
                                typeSize,typeLabel,valueCount,offsetHeader,valueOffset
          end if
      end if
      return
    end subroutine readIFDEntry

    !---------------------------------------------------------------

    subroutine getSizeRes(inUnit, imgSize, xRes, yRes, resUnitFlag, SPP, BPS, comp,lun)
      integer,                       intent(in)  :: inUnit
      integer,         dimension(2), intent(out) :: imgSize
      integer(kind=4), dimension(2), intent(out) :: xRes, yRes
      integer(kind=1),               intent(out) :: resUnitFlag
      integer,                       intent(out) :: SPP, comp
      integer,         dimension(3), intent(out) :: BPS
      integer,             optional, intent(in)  :: lun
      !--- Local variables
      integer(kind=4) :: xResOffset, yResOffset

      !   store image size directly from module key values
      imgSize(1) = keyValOffset(257)  ! ImageLength - number of Rows
      imgSize(2) = keyValOffset(256)  ! ImageWidth - number of Columns

      !   store Samples Per Pixel, Bits Per Sample and compression flag
      SPP = keyValOffset(277)
      comp = keyValOffset(259)
      BPS=0

      if (keyCount(258) == 1) then
          BPS(1) = keyValOffset(258)
      else
          call read24Array(inUnit, 258, BPS, 0)
      end if

      !   store resolution unit flag
      resUnitFlag = keyValOffset(296)

      !   get resolution offsets and read resolution from tiff file
      xResOffset = keyValOffset(282)
      yResOffset = keyValOffset(283)

      call readRational(inUnit, xResOffset, xRes, 1)
      call readRational(inUnit, yResOffset, yRes, 1)
      if(present(lun)) then
        write(unit=lun,fmt=*)
        write(unit=lun,fmt="(' Image Size:',I4,' x',I4,'px, Resolution: X('I4,'/',I4,'), Y(',I4,'/',I4,') ',A5)") &
                                  imgSize, xRes, yRes, unitLabel(resUnitFlag)
        write(unit=lun,fmt="(' Samples per pixel:',I2,', Bits per Sample:',3I2', Compression Flag:',I2)") SPP, BPS, comp
      end if
      return
    end subroutine getSizeRes

    !---------------------------------------------------------------

    subroutine freadTiffImage(inUnit, inFileName,imgSize,image,OK,Mess)
      integer,                                         intent(in) :: inUnit
      character(len=*),                                intent(in) :: inFileName
      integer,        dimension(2),                    intent(in) :: imgSize
      integer(kind=4),dimension(imgSize(1),imgSize(2)),intent(out):: image
      logical,                                         intent(out):: OK
      Character(len=*),                                intent(out):: Mess
      ! --- Local variables
      integer,  dimension(keyCount(273))  :: stripOffsets
      integer :: nRows, nCols, ios

      !  Open image file for reading the offset of the first byte of the image
      !  It is assumed that the image is stored contiguously using two bytes for
      !  unsigned integers as intensity. The subroutine assumes that the 2D image
      !  is stored by rows (C-like)
      open(unit=inUnit, iostat=ios, file=inFileName, form='UNFORMATTED', &
           action='READ', status='OLD', access='DIRECT', recl=myrecl)
      !   read StripOffsets & StripByteCounts
      call read24Array(inUnit, 273, stripOffsets, 0)      ! ### default is 0 for ifort
      close(unit=inUnit)
      call Read_Binary_File_16bits(inFileName,stripOffsets(1),imgSize(1),imgSize(2),Image,OK,Mess)
      where(image < 0) image=image+65536   !2**16
    end subroutine freadTiffImage

    subroutine readTiffImage(inUnit, inFileName, imgSize, SPP, BPS, comp, image,lun)
      integer,                                             intent(in) :: inUnit
      character(len=*),                                    intent(in) :: inFileName
      integer,        dimension(2),                        intent(in) :: imgSize
      integer,        dimension(3),                        intent(in) :: BPS
      integer,                                             intent(in) :: SPP, comp
      integer(kind=4),dimension(imgSize(1),imgSize(2),SPP),intent(out):: image
      integer,                                   optional, intent(in) :: lun
      ! --- Local variables
      integer,        dimension(keyCount(273))                      :: stripOffsets
      integer,        dimension(keyCount(279))                      :: stripByteCounts
      integer(kind=1),dimension(4*imgSize(1)*keyValOffset(278)*SPP) :: strip
      integer(kind=2),dimension(2*imgSize(1)*keyValOffset(278)*SPP) :: i2strip
      integer(kind=2)               :: tempi2
      integer(kind=1), dimension(2) :: b2
      integer :: i, j, k, n, ii, jj, kk, nRows, nCols, rowStart, rowEnd, &
                 stripsPerImage, rowsPerStrip,ios
      equivalence (b2,tempi2)
      !   check compression flag
      if (comp /= 1) then
          if(present(lun)) write(unit=lun, fmt="(' Compression mode ',I10,' not supported')") comp
          tiff_error= .true.
          tiff_error_message= " => Compression mode not supported "
          return
      end if

      !   Open image file
      open(unit=inUnit, iostat=ios, file=inFileName, form='UNFORMATTED', &
           action='READ', status='OLD', access='DIRECT', recl=myrecl)

      if(present(lun)) write(unit=lun, fmt='(a7,a32,a10,i3)') ' Opened ', inFileName, ' IOSTAT = ', ios

      !   get image and strip dimensions
      rowsPerStrip = keyValOffset(278)
      stripsPerImage = keyCount(273)
      nRows = imgSize(1)
      nCols = imgSize(2)

      !   read StripOffsets & StripByteCounts
      call read24Array(inUnit, 273, stripOffsets, 0)      ! ### default is 0 for ifort
      call read24Array(inUnit, 279, stripByteCounts, 0)   ! ### default is 0 for ifort

      !   display strip offsets and byte counts
      if(present(lun)) then
        write(unit=*,fmt=*)
        write(unit=*,fmt='(4a11)') 'Strip', 'Offset', 'ByteCount', 'ByteCount'
        do i = 1, stripsPerImage
           write(unit=*,fmt='(i11,2(z10,"h"),i11)') i, stripOffsets(i), stripByteCounts(i), stripByteCounts(i)
        end do
      end if

      !   read in image data
      do i=1,stripsPerImage
          call readStrip(inUnit, stripOffsets(i), stripByteCounts(i), strip)

          ! expand strip to array of int2
          if (BPS(1) == 8) then
              do j = 1, stripByteCounts(i)
                  if (strip(j) >= 0) then
                      i2strip(j) = strip(j)
                  else
                      i2strip(j) = strip(j) + 256
                  end if
              end do
          else if (BPS(1) == 4) then
              k = 0
              do j = 1, stripByteCounts(i)
                  if (strip(j) >= 0) then
                      tempi2 = strip(j)
                  else
                      tempi2 = strip(j) + 256
                  end if
                  i2strip(k+1) = ISHFT(tempi2,-2)
                  i2strip(k+2) = IAND(15,tempi2)
                  k = k + 2
              end do
          else if (BPS(1) == 1) then
              k = 0
              do j = 1, stripByteCounts(i)
                  if (strip(j) >= 0) then
                      tempi2 = strip(j)
                  else
                      tempi2 = strip(j) + 256
                  end if
                  do n = 1,8
                      i2strip(k+n) = IAND(2**(n-1),tempi2)
                  end do
                  k = k + 8
              end do
          else if (BPS(1) == 16) then
              k=0
              do j = 1, stripByteCounts(i)-2,2
                  b2(1:2) = strip(j:j+1)
                  k=k+1
                  i2strip(k) = tempi2
              end do
          end if

          ! store strip in image array

          rowStart = (i-1) * rowsPerStrip + 1
          rowEnd = MIN(rowStart + rowsPerStrip -1, nRows)
          j = 0
          do ii = rowStart, rowEnd
              do jj = 1, nCols
                  do kk = 1, SPP
                      j = j + 1
                      image(ii,jj,kk) = i2strip(j)
                  end do
              end do
          end do
      end do
      !Correct image for overflow of integer*2
      where(image < 0) image=image+65536   !2**16
      !   Close image file
      close(inUnit)

      if(present(lun)) then
        write(unit=lun, fmt=*)
        write(unit=lun, fmt="(' Read ',I6,' Samples in ',I6,' Rows and ',I6,' Columns in ',I6,' Strips')") &
                          SPP, nRows, nCols, stripsPerImage
        write(unit=lun, fmt=*)
        write(unit=lun, fmt='(a6,1x,a70)') ' Closed', inFileName
      end if
      return
    end subroutine readTiffImage

    !---------------------------------------------------------------

    subroutine read24Array(inUnit, tagNumber, valArray, swapFlag)
      integer,                                intent(in) :: inUnit
      integer,                                intent(in) :: tagNumber
      integer,dimension(keyCount(tagNumber)), intent(out):: valArray
      integer,                                intent(in) :: swapFlag
      !--- Local variables
      integer    :: i, byteOffset, numVals
      integer(2) :: int2Array(keyCount(tagNumber))
      integer(4) :: int4Array(keyCount(tagNumber))

      numVals = keyCount(tagNumber)

      ! read StripOffsets
      ! multiple strips

      if (numVals > 1) then
          byteOffset = keyValOffset(tagNumber)
          ! shorts (int2) for offsets
          if (keySize(tagNumber) == 2) then
              call readInt2Array(inUnit, numVals, byteOffset, int2Array, swapFlag)
              do i=1, numVals
                  valArray(i) = int2Array(i)
              end do
          else  ! longs (int4) for offsets
              call readInt4Array(inUnit, numVals, byteOffset, int4Array, swapFlag)
              do i=1, numVals
                  valArray(i) = int4Array(i)
              end do
          end if
      else  ! one strip
          valArray(1) = keyValOffset(tagNumber)
      end if

    end subroutine read24Array

!---------------------------------------------------------------

    subroutine readStrip(inUnit, stripOffset, stripByteCount, strip)
      integer,                                    intent(in) :: inUnit
      integer,                                    intent(in) :: stripOffset
      integer,                                    intent(in) :: stripByteCount
      integer(kind=1), dimension(stripByteCount), intent(out):: strip
      !--- Local variables
      integer :: startRec, endRec, numRecs, &
                 startDummyBytes, endExtraBytes, i, j, n, startB, endB
      integer(kind=1), dimension(4) :: dummy4i1

      startRec = stripOffset / 4 + 1
      endRec = (stripOffset + stripByteCount) / 4
      numRecs = endRec - startRec + 1

      startDummyBytes = stripOffset - (startRec - 1) * 4
      endExtraBytes = stripOffset + stripByteCount - endRec * 4

      !   Aligned on record boundaries
      if ((startDummyBytes == 0).AND.(endExtraBytes == 0)) then
          do i = 1, numRecs
              read(unit=inUnit, rec=(i+startRec-1)) (strip(n), n=(i-1)*4+1,i*4)
          end do

      !   General case unaligned to record boundaries
      else
          ! get first set of unaligned bytes
          read(unit=inUnit, rec=startRec) dummy4i1
          j=1
          do i = startDummyBytes+1, 4
              strip(j) = dummy4i1(i)
              j = j + 1
          end do

          ! loop over full 4-byte records
          do i = 2, numRecs
              read(unit=inUnit, rec=(i+startRec-1)) (strip(n), n=j,j+3)
              j = j + 4
          end do

          ! get unaligned extra bytes
          startB = stripByteCount - endExtraBytes + 1
          endB = stripByteCount
          read(unit=inUnit, rec=endRec+1) (strip(n), n=startB, endB)

      end if

    end subroutine readStrip

!---------------------------------------------------------------

    subroutine readInt2Array(inUnit, numVals, startOffset, int2Array, swapFlag)
      integer,                             intent(in) :: inUnit
      integer,                             intent(in) :: numVals
      integer,                             intent(in) :: startOffset
      integer(kind=2), dimension(NumVals), intent(out):: int2Array
      integer,                             intent(in) :: swapFlag


      integer         :: thisRec, n, i
      integer(kind=2) :: dummyi2

      !   start on record boundary
      if (INT(startOffset / 4.0) == (startOffset / 4.0)) then
          thisRec = startOffset / 4 + 1
          n=0
          do i=1,numVals,2
              read(unit=inUnit, rec=thisRec+n)    int2Array(i), int2Array(i+1)
              n=n+1
          end do
          ! odd number of values
          if (INT(numVals/2.0) /= (numVals/2.0)) then
              read(unit=inUnit, rec=thisRec+n)    int2Array(numVals)
          end if

      else  !   stard mid-record
          thisRec = INT(startOffset / 4) + 1
          read(unit=inUnit, rec=thisRec)          dummyi2, int2Array(1)
          n=1
          do i=2,numVals-1,2
              read(unit=inUnit, rec=thisRec+n)    int2Array(i), int2Array(i+1)
              n=n+1
          end do
          ! even number of values
          if (INT(numVals/2.0) == (numVals/2.0)) then
              read(unit=inUnit, rec=thisRec+n)    int2Array(numVals)
          end if
      end if

      if (swapFlag == 1) then
          do i = 1, numVals
              call byteswapi2(int2Array(i))
          end do
      end if

    end subroutine readInt2Array

!---------------------------------------------------------------

    subroutine readInt4Array(inUnit, numVals, startOffset, int4Array, swapFlag)
      integer,                             intent(in) :: inUnit
      integer,                             intent(in) :: numVals
      integer,                             intent(in) :: startOffset
      integer(kind=4), dimension(NumVals), intent(out):: int4Array
      integer,                             intent(in) :: swapFlag

      integer         :: thisRec, i
      integer(kind=2) :: dummyi2, tempi2, int4Parts(2)
      integer(kind=4) :: int4Val

      equivalence (int4Parts, int4Val)

      !   start on record boundary
      if (INT(startOffset / 4.0) == (startOffset / 4.0)) then
          thisRec = startOffset / 4 + 1
          do i=1,numVals
              read(unit=inUnit, rec=thisRec+i-1)    int4Array(i)
          end do
      else   !   start mid-boundary
          thisRec = INT(startOffset / 4) + 1
          read(unit=inUnit, rec=thisRec)          dummyi2, int4Parts(1)
          do i=1,numVals-1
              read(unit=inUnit, rec=thisRec+i)    int4Parts(2), tempi2
              int4Array(i)=int4Val
              int4Parts(1) = tempi2
          end do
          read(unit=inUnit, rec=thisRec+numVals)  int4Parts(2)
          int4Array(numVals) = int4Val
      end if

      if (swapFlag == 1) then
          do i = 1, numVals
              call byteswapi4(int4Array(i))
          end do
      end if

    end subroutine readInt4Array

    !---------------------------------------------------------------

    subroutine readRational(inUnit, byteOffset, ratVal, swapFlag)
      integer,                       intent(in) :: inUnit
      integer,                       intent(in) :: byteOffset
      integer(kind=4), dimension(2), intent(out):: ratVal
      integer,                       intent(in) :: swapFlag
      !   reads two integer(4) from word boundaries, which may start
      !   half way through 4-byte minimum record length
      logical :: bugFix
      integer :: thisRec
      integer(kind=2)               :: dummyi2
      integer(kind=2), dimension(4) :: rVParts
      integer(kind=4), dimension(2) :: rV

      equivalence (rVParts, rV)

      !   check if offset ends on record boundary or not, read accordingly

      if (INT(byteOffset / 4.0) == (byteOffset / 4.0)) then
          thisRec = byteOffset / 4 + 1
          read(unit=inUnit, rec=thisRec)   rV(1)
          read(unit=inUnit, rec=thisRec+1) rV(2)
      else
          thisRec = INT(byteOffset / 4) + 1
          read(unit=inUnit, rec=thisRec)   dummyi2, rVParts(1)
          read(unit=inUnit, rec=thisRec+1) rVParts(2),rVParts(3)
          read(unit=inUnit, rec=thisRec+2, err=900) rVParts(4)
          900 continue   ! Sun can't read record with part past end of file
      end if

      ratVal = rV

      !   check for bug in either gimp or windows TIFF resolution endianness

      if ((ratVal(1) > 10000).AND.(ratVal(2) > 10000)) then
          bugFix = .TRUE.
      else
          bugFix = .FALSE.
      end if

      if ((swapFlag == 1).AND.(bugFix)) then
          call byteswapi4(ratVal(1))
          call byteswapi4(ratVal(2))
      end if

    end subroutine readRational

!---------------------------------------------------------------

    subroutine readInt4(inUnit, byteOffset, intVal, swapFlag)
      integer,         intent(in) :: inUnit
      integer,         intent(in) :: byteOffset
      integer(kind=4), intent(out):: intVal
      integer,         intent(in) :: swapFlag
    !   reads one integer(4) from word boundaries, which may start
    !   half way through 4-byte minimum record length
      integer                      :: thisRec
      integer(kind=2)              :: dummyi2
      integer(kind=2),dimension(2) :: iVParts
      integer(kind=4)              :: iV

      equivalence (iVParts, iV)

      !   check if offset ends on record boundary or not, read accordingly

      if (INT(byteOffset / 4.0) == (byteOffset / 4.0)) then
          thisRec = byteOffset / 4 + 1
          read(unit=inUnit, rec=thisRec)   iV
      else
          thisRec = INT(byteOffset / 4) + 1
          read(unit=inUnit, rec=thisRec)   dummyi2, iVParts(1)
          read(unit=inUnit, rec=thisRec+1) iVParts(2)
      end if

      intVal = iV

      if (swapFlag == 1) then
          call byteswapi4(intVal)
      end if

    end subroutine readInt4

    !---------------------------------------------------------------

    subroutine readInt2(inUnit, byteOffset, intVal, swapFlag)
      integer,         intent(in) :: inUnit
      integer,         intent(in) :: byteOffset
      integer(kind=2), intent(out):: intVal
      integer,         intent(in) :: swapFlag
      !   reads one integer(2) from word boundaries, which may start
      !   half way through 4-byte minimum record length
      integer         :: thisRec
      integer(kind=2) :: dummyi2

      !   check if offset ends on record boundary or not, read accordingly

      if (INT(byteOffset / 4.0) == (byteOffset / 4.0)) then
          thisRec = byteOffset / 4 + 1
          read(unit=inUnit, rec=thisRec)   intVal
      else
          thisRec = INT(byteOffset / 4) + 1
          read(unit=inUnit, rec=thisRec)   dummyi2, intVal
      end if

      if (swapFlag == 1) then
          call byteswapi2(intVal)
      end if

    end subroutine readInt2

    !---------------------------------------------------------------

    subroutine byteswapi2(k)
      integer(kind=2), intent(in out) :: k
      !   from
      !   http://lists.apple.com/archives/Fortran-dev/2003/Oct/msg00017.html
      !   does a byteswap on integer2 number

      integer(kind=1), dimension(2) :: ii, jj
      integer(kind=2) i, j
      equivalence (i,ii)
      equivalence (j,jj)

      i = k
      jj(1) = ii(2)
      jj(2) = ii(1)
      k = j

    end subroutine byteswapi2

    !---------------------------------------------------------------

    subroutine byteswapi4(k)
      integer(kind=4), intent(in out) :: k
      !   adapted from
      !   http://lists.apple.com/archives/Fortran-dev/2003/Oct/msg00017.html
      !   does a byteswap on integer*4 number

      integer(kind=1), dimension(4) :: ii, jj
      integer(kind=4) :: i, j
      equivalence (i,ii)
      equivalence (j,jj)

      i = k
      jj(1) = ii(4)
      jj(2) = ii(3)
      jj(3) = ii(2)
      jj(4) = ii(1)
      k = j

    end subroutine byteswapi4

    !---------------------------------------------------------------

  End Module tiff_readsubs
