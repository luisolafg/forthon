 Module ReadnWrite_Binary_Files

   implicit none
   !!!
   !!! --- Code in Fortran 90 converted from "readbin.for" and "writebin.for"
   !!!     by Scott A. Belmonte,
   !!!     Juan Rodriguez-Carvajal (ILL)
   !!!     Additional subroutines based in Belmonte's examples are added.
   !!!     The original heading is reproduced here
   !**********************************************************************
   !**
   !**  File: readbin.for
   !**
   !**  Description: Routines for reading binary files.
   !**
   !**  Date: 8th January 2004
   !**
   !**********************************************************************
   !**
   !** Copyright (c) 2004 Scott A. Belmonte
   !** All rights reserved.
   !**
   !** Redistribution and use in source and binary forms, with or without
   !** modification, are permitted provided that the following conditions
   !** are met:
   !**
   !** Redistributions of source code must retain the above copyright
   !** notice, this list of conditions and the following disclaimer.
   !**
   !** Redistributions in binary form must reproduce the above copyright
   !** notice, this list of conditions and the following disclaimer in
   !** the documentation and/or other materials provided with the
   !** distribution.
   !**
   !** Neither the name of the copyright holder nor the names of any
   !** contributors may be used to endorse or promote products derived
   !** from this software without specific prior written permission.
   !**
   !** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
   !** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   !** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   !** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   !** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
   !** BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   !** EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
   !** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   !** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
   !** ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
   !** TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
   !** THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   !** SUCH DAMAGE.
   !**
   !**********************************************************************
      private

      public  :: Read_Binary_File_16bits,Read_Binary_File_32bits

      public  :: Write_Bin_File, Write_Bin_Uint16, Write_Bin_Int16, &
                 Write_Bin_Uint32, Write_Bin_Int32, Write_Bin_Real, &
                 Write_Bin_String, Write_Binary_File_2D_16bits,     &
                 Write_TIFF_File_2D_16bits, Write_Binary_File_2D_16bits_NoHeader

      private :: Read_Binary_File_1D_16bits,  Read_Binary_File_2D_16bits   !, Read_Binary_File_3D_16bits
      private :: Read_Binary_File_1D_32bits,  Read_Binary_File_2D_32bits   !, Read_Binary_File_3D_32bits
      private :: Read_Binary_File_1D_32bitsr, Read_Binary_File_2D_32bitsr  !, Read_Binary_File_3D_32bitsr

      Interface Read_Binary_file_16bits
        Module Procedure  Read_Binary_File_1D_16bits
        Module Procedure  Read_Binary_File_2D_16bits
        ! Module Procedure  Read_Binary_File_3D_16bits
      End Interface Read_Binary_file_16bits

      Interface Read_Binary_file_32bits
        Module Procedure  Read_Binary_File_1D_32bits
        Module Procedure  Read_Binary_File_2D_32bits
        Module Procedure  Read_Binary_File_1D_32bitsr
        Module Procedure  Read_Binary_File_2D_32bitsr
        ! Module Procedure  Read_Binary_File_3D_32bits
      End Interface Read_Binary_file_32bits

      Integer, Parameter :: Rec_Size = 2048 ! The file record size in bytes (For intel compiler this
                                            ! should be spcified explicitly with the options
                                            ! Windows-> /assume:byterecl
                                            ! Linux  -> -assume byterecl
      Integer, Parameter :: blocksz  = 8192 ! Size of the RAWDATA buffer  4*Rec_Size
      Integer, Parameter :: Unt = 111       ! The unit number that file is opened on
   !
      Integer(kind=1), save, dimension(Rec_Size) &
                                      :: Buffer    ! Read buffer for records
      Integer, save                   :: Last_Rec  ! The last record read into BUFFER
      Logical, save                   :: Big_End   ! .TRUE. if the file is big endian
      Logical, save                   :: Swap      ! If .TRUE. the bytes need to be swapped
   !                                               ! before they can be used


  ! !   labels of field type codes
  !
  ! integer, parameter :: tagLB = 254, tagUB = 296, typeLB = 1, typeUB = 12
  ! character(len=9) :: typeLabel, typeList(typeLB:typeUB) = &
  ! (/ 'BYTE     ', 'ASCII    ', 'SHORT    ', 'LONG     ', 'RATIONAL ', &
  !    'SBYTE    ', 'UNDEFINED', 'SSHORT   ', 'SLONG    ', 'SRATIONAL', &
  !    'FLOAT    ', 'DOUBLE   '/)
  ! !   length in bytes of field types
  ! integer(kind=1) :: typeSize, typeLengths(typeLB:typeUB) = &
  !                               (/ 1, 1, 2, 4, 8, &
  !                                  1, 1, 2, 4, 8, &
  !                                           4, 8 /)
  !
  !
  ! character(len=24), dimension(tagLB:tagUB) :: tagList = (/
  !       254                       255                      256                          257     &
  ! 'NewSubfileType          ','NotRequired             ','ImageWidth              ','ImageLength             ',&
  !       258                       259
  ! 'BitsPerSample           ','Compression             ','NotRequired             ','NotRequired             ',&
  !       262                       263                       264                         265
  ! 'PhotometricInterpretaion','NotRequired             ','NotRequired             ','NotRequired             ',&
  !       266                       267                       268                         269
  ! 'NotRequired             ','NotRequired             ','NotRequired             ','DocumentName            ',&
  !       270                       271                       272                         273
  ! 'ImageDescription        ','NotRequired             ','NotRequired             ','StripOffsets            ',&
  !       274                                                                             277
  ! 'Orientation             ','NotRequired             ','NotRequired             ','SamplesPerPixel         ',&
  !       278                       279                       280                         281
  ! 'RowsPerStrip            ','StripByteCounts         ','NotRequired             ','NotRequired             ',&
  !       282                       283                       284                         285
  ! 'XResolution             ','YResolution             ','PlanarConfiguration     ','NotRequired             ',&
  !       286                       287                       288                         289
  ! 'NotRequired             ','NotRequired             ','NotRequired             ','NotRequired             ',&
  !       290
  ! 'NotRequired             ','NotRequired             ','NotRequired             ','NotRequired             ',&
  !                                                           296
  ! 'NotRequired             ','NotRequired             ','ResolutionUnit          ' /)




 !**********************************************************************
   Contains

 !**********************************************************************
 !     Routine: Read_Binary_File_16bits(filenam,Offsti,Num_Pix,Datap,OK,Mess)
 !     Description:
 !        Extracts from a binary file data that is stored in the integer Matrix DATAP.
 !     Arguments:
 !          filenam : name of the input file (e.g. myfile.tif)
 !          Offsti  : initial offset (in bytes) for starting reading the data
 !          Num_Pix : Total number of pixels to be read (1D)
 !             or
 !        Ncol,Nrow : Dimensions of the matrix to be read (2D)  ... Num_Pix=Nrow*Ncol
 !          Datap   : Integer array to store the data
 !             OK   : is a logical that is true if everything is OK,
 !           Mess   : Error Message
 !**********************************************************************
    Subroutine Read_Binary_File_1D_16bits(filenam,Offsti,Num_Pix,Datap,OK,Mess,endian)
      Character(len=*),       intent(In    ) :: filenam
      Integer,                intent(In    ) :: Offsti
      Integer,                intent(In    ) :: Num_Pix
      Integer, dimension(:),  intent(   Out) :: Datap
      Logical,                intent(   Out) :: OK
      Character(len=*),       intent(   Out) :: Mess
      Character(len=*), optional, intent(In) :: endian
      ! Local variables
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 16bits =2
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=2
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz
      Do i = 1, Num_Blocks
          If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, blocksz, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Short(rawdata(j))
          End Do
          Offst = Offst + blocksz
      End Do

      !     Read the remainder of the data if any

      remainder = Mod((Num_Pix*bytesperpix), blocksz)
      If (remainder /= 0) Then
          If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, remainder, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Short(rawdata(j))
          End Do
      End If

      Where( Datap < 0 ) Datap=Datap + 65536

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_1D_16bits

    !  The following subroutine assumes by default that the 2D matrix in the binary file
    !  is stored by rows (C-like default). Providing the optional argument, Col, changes
    !  the default to a matrix stored by columns (Fortran-like default)
    !
    Subroutine Read_Binary_File_2D_16bits(filenam,Offsti,Nrow,Ncol,Datap,OK,Mess,Col,endian)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In Out) :: Offsti
      Integer,                    intent(In    ) :: Nrow,Ncol
      Integer, dimension(:,:),    intent(   Out) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: Col
      Character(len=*), optional, intent(In    ) :: endian
      ! Local variables
      Integer                            :: Num_Pix            ! Total number of pixels
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: ir, ic             ! Matrix indices
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 16bits =2
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=2
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      Num_Pix     = Nrow * Ncol
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz

      if(present(col)) then ! Matrix stored by columns

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Short(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Short(rawdata(j))
             End Do
         End If

      Else ! Matrix stored by rows

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Short(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Short(rawdata(j))
             End Do
         End If
      End If

      Where( Datap < 0 ) Datap=Datap + 65536

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_2D_16bits


 !**********************************************************************
 !     Routine: Read_Binary_File_32bits(filenam,Offsti,Num_Pix,Datap,OK,Mess)
 !     Description:
 !        Extracts from a binary file data that is stored in the integer Matrix DATAP.
 !     Arguments:
 !          filenam : name of the input file (e.g. myfile.tif)
 !          Offsti  : initial offset (in bytes) for starting reading the data
 !          Num_Pix : Total number of pixels to be read (1D)
 !             or
 !        Ncol,Nrow : Dimensions of the matrix to be read (2D)  ... Num_Pix=Nrow*Ncol
 !          Datap   : Integer array to store the data
 !             OK   : is a logical that is true if everything is OK,
 !           Mess   : Error Message
 !**********************************************************************
    Subroutine Read_Binary_File_1D_32bits(filenam,Offsti,Num_Pix,Datap,OK,Mess,endian)
      Character(len=*),       intent(In    ) :: filenam
      Integer,                intent(In    ) :: Offsti
      Integer,                intent(In    ) :: Num_Pix
      Integer, dimension(:),  intent(   Out) :: Datap
      Logical,                intent(   Out) :: OK
      Character(len=*),       intent(   Out) :: Mess
      Character(len=*),optional,intent(in)   :: endian
      ! Local variables
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 32bits =4
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=4
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz
      Do i = 1, Num_Blocks
          If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, blocksz, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Long(rawdata(j))
          End Do
          Offst = Offst + blocksz
      End Do

      !     Read the remainder of the data if any

      remainder = Mod((Num_Pix*bytesperpix), blocksz)
      If (remainder /= 0) Then
          If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, remainder, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Long(rawdata(j))
          End Do
      End If

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_1D_32bits

    !  The following subroutine assumes by default that the 2D matrix in the binary file
    !  is stored by rows (C-like default). Providing the optional argument, Col, changes
    !  the default to a matrix stored by columns (Fortran-like default)
    !
    Subroutine Read_Binary_File_2D_32bits(filenam,Offsti,Nrow,Ncol,Datap,OK,Mess,Col,endian)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In Out) :: Offsti
      Integer,                    intent(In    ) :: Nrow,Ncol
      Integer, dimension(:,:),    intent(   Out) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: Col
      Character(len=*), optional, intent(In    ) :: endian
      ! Local variables
      Integer                            :: Num_Pix            ! Total number of pixels
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: ir, ic             ! Matrix indices
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 32bits =4
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=4
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      Num_Pix     = Nrow * Ncol
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz

      if(present(col)) then ! Matrix stored by columns

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Long(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Long(rawdata(j))
             End Do
         End If

      Else ! Matrix stored by rows

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Long(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Long(rawdata(j))
             End Do
         End If
      End If

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_2D_32bits


    Subroutine Read_Binary_File_1D_32bitsr(filenam,Offsti,Num_Pix,Datap,OK,Mess,endian)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In    ) :: Offsti
      Integer,                    intent(In    ) :: Num_Pix
      Real(kind=4),dimension(:),  intent(   Out) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: endian
      ! Local variables
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 32bits =4
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=4
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz
      Do i = 1, Num_Blocks
          If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, blocksz, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Real(rawdata(j))
          End Do
          Offst = Offst + blocksz
      End Do

      !     Read the remainder of the data if any

      remainder = Mod((Num_Pix*bytesperpix), blocksz)
      If (remainder /= 0) Then
          If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
              Mess= ' Error reading in file: '//Trim(filenam)
              OK=.false.
              return
          End if
          Do j = 1, remainder, bytesperpix
              pixread = pixread + 1
              Datap(pixread) = Read_Bin_Real(rawdata(j))
          End Do
      End If

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_1D_32bitsr

    !  The following subroutine assumes by default that the 2D matrix in the binary file
    !  is stored by rows (C-like default). Providing the optional argument, Col, changes
    !  the default to a matrix stored by columns (Fortran-like default)
    !
    Subroutine Read_Binary_File_2D_32bitsr(filenam,Offsti,Nrow,Ncol,Datap,OK,Mess,Col,endian)
      Character(len=*),            intent(In    ) :: filenam
      Integer,                     intent(In Out) :: Offsti
      Integer,                     intent(In    ) :: Nrow,Ncol
      Real(kind=4), dimension(:,:),intent(   Out) :: Datap
      Logical,                     intent(   Out) :: OK
      Character(len=*),            intent(   Out) :: Mess
      Character(len=*), optional,  intent(In    ) :: Col
      Character(len=*), optional,  intent(In    ) :: endian

      ! Local variables
      Integer                            :: Num_Pix            ! Total number of pixels
      Integer(kind=1),Dimension(blocksz) :: rawdata            ! Raw data as read from disc
      Integer                            :: i, j               ! Loop counters
      Integer                            :: ir, ic             ! Matrix indices
      Integer                            :: Num_Blocks         ! Number of blocks in file
      Integer                            :: pixread            ! Number of pixels read so far
      Integer                            :: remainder          ! Number of remaining bytes after
                                                               ! all blocks have been read
      Integer                            :: Offst              ! Varying Offset
      Integer                            :: Bytesperpix        ! For 32bits =4
      OK=.true.
      Mess=" Everything is OK"
      Bytesperpix=4
      If (Open_Bin_File(Trim(filenam)) /= 0) Then
        Mess= ' Error opening file: '//Trim(filenam)
        OK=.false.
        return
      End If

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endian)) swap=.true.

      !     Start at pixel zero. The data starts at byte Offst
      !     Read data in blocks of BLOCKSZ bytes.
      Num_Pix     = Nrow * Ncol
      pixread     = 0
      Offst=Offsti
      Num_Blocks  = Num_Pix*bytesperpix/blocksz

      if(present(col)) then ! Matrix stored by columns

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Real(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ic= 1+ (pixread-1)/Nrow
                 ir= pixread-(ic-1)*Nrow
                 Datap(ir,ic) = Read_Bin_Real(rawdata(j))
             End Do
         End If

      Else ! Matrix stored by rows

         Do i = 1, Num_Blocks
             If (Read_Bin_File(rawdata, Offst, (Offst+blocksz-1)) /= 0)  Then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, blocksz, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Real(rawdata(j))
             End Do
             Offst = Offst + blocksz
         End Do

         !     Read the remainder of the data if any

         remainder = Mod((Num_Pix*bytesperpix), blocksz)
         If (remainder /= 0) Then
             If (Read_Bin_File(rawdata, Offst, (Offst+remainder-1)) /= 0)  then
                 Mess= ' Error reading in file: '//Trim(filenam)
                 OK=.false.
                 return
             End if
             Do j = 1, remainder, bytesperpix
                 pixread = pixread + 1
                 ir= 1+ (pixread-1)/Ncol
                 ic= pixread-(ir-1)*Ncol
                 Datap(ir,ic) = Read_Bin_Real(rawdata(j))
             End Do
         End If
      End If

      !     Close file
      Call Close_Bin_File()

    End Subroutine Read_Binary_File_2D_32bitsr


 !**********************************************************************
 !     Routine: Open_Bin_File
 !     Description:
 !        Opens a binary file whose name is stored in NAMEF.
 !        NB. Uses a fixed unit number. This means only one file
 !        can be open at a time. CLOSE_BIN_FILE must be called
 !        before OPEN_BIN_FILE can be called again.
 !        Returns: 0 file successfully opened,
 !                 1 error opening file
 !**********************************************************************

    Integer Function Open_Bin_File(Namef)
      Character (Len=*), Intent(In) :: Namef

      !     Variables

      Logical :: Opend            ! .TRUE. if a file is already open
      Integer :: ier

      !     Initialise the LAST_REC common. 0 means that BUFFER contains
      !     no valid record.

      Last_Rec = 0

      !     Check that a file isn't currently open. These routines
      !     can only work with one file at a time.

      Inquire(Unit=unt, Opened=Opend)
      If (Opend) Then
          Write(unit=*,fmt=*) '** OPEN_BIN_FILE error: File already opened.'
          Write(unit=*,fmt=*) '** Only one file can be opened at a time.'
          Open_Bin_File = 1   ! Error opening file
          return
      End If

      !     Open file

      Open(Unit=unt, File=trim(Namef), Status='OLD', Form='UNFORMATTED',  &
           Access='DIRECT', Recl=Rec_Size, iostat=ier)
      if(ier /= 0) then
        Open_Bin_File = 1
      else
        Open_Bin_File = 0
      end if
      Return

    End Function Open_Bin_File


 !**********************************************************************
 !     Routine: Close_Bin_File
 !     Description:
 !        Closes the binary file.
 !**********************************************************************

    Subroutine Close_Bin_File()
      Close(Unit=unt)
    End Subroutine Close_Bin_File


 !**********************************************************************
 !     Routine: Read_Bin_File
 !     Description:
 !        Reads bytes START_BYTE to END_BYTE, inclusive, from a binary
 !        file into the array DATA.
 !        NB. START_BYTE and END_BYTE are zero offset.
 !        Returns 0 if successful, 1 otherwise
 !**********************************************************************

    Integer Function Read_Bin_File(Datap, Start_Byte, End_Byte)
      Integer(kind=1), dimension(*),  Intent(In Out)  :: Datap   ! The array to read the bytes into
      Integer,                        Intent(In)      :: Start_Byte
      Integer,                        Intent(In)      :: End_Byte

      !     Variables

      Integer :: Start_Rec         ! Start record
      Integer :: End_Rec           ! End record
      Integer :: Total_Bytes       ! The total number of bytes to read
      Integer :: Next_Byte         ! The next byte to write into DATA
      Integer :: Cur_Rec           ! Current record being read
      Integer :: Skip_Bytes        ! Num bytes to skip in first record
      Integer :: Num_Bytes         ! Num bytes to read from first record
      Integer :: Stats             ! Status of the read
      Integer :: i                 ! Loop counter

      ! Check for errors in the parameters START_BYTE and END_BYTE.
      Do_Ext: Do  ! infinite loop used only for avoiding goto's with numeric labels
        If ((Start_Byte > End_Byte) .Or. (Start_Byte < 0)) Then
            Write(unit=*,fmt=*) '** READ_BIN_FILE Error: Bad START_BYTE or END_BYTE'
            Write(unit=*,fmt=*) '** START = ', Start_Byte, ', END = ', End_Byte
            exit Do_Ext
        End If

        !     Calculate the first and last records that have to be read, and
        !     the total number of bytes that have to be read.

        Start_Rec   = 1 + Start_Byte/Rec_Size
        End_Rec     = 1 + End_Byte/Rec_Size
        Total_Bytes = End_Byte - Start_Byte + 1

        Next_Byte = 1
        Cur_Rec   = Start_Rec

        !     Read the first record (unless it already is in BUFFER).

        Stats = 0
        If(Cur_Rec /= Last_Rec) Then
            Read(Unit=unt,Rec=Cur_Rec, Iostat=Stats) buffer
        End If

        !     Copy the required bytes from BUFFER to DATA.

        Skip_Bytes = Start_Byte - (Start_Rec - 1)*Rec_Size
        Num_Bytes  = Min(Rec_Size - Skip_Bytes, Total_Bytes)
        Do i = 1, Num_Bytes
            Datap(i) = buffer(i + Skip_Bytes)
        End Do
        Next_Byte = Next_Byte + Num_Bytes
        If(Stats /= 0) exit Do_Ext

        !     Read anymore whole records directly into DATAP.

        Do Cur_Rec = Start_Rec + 1, End_Rec - 1
            Stats = Read_Bin_Record(Datap(Next_Byte), Cur_Rec)
            If(Stats /= 0) exit Do_Ext
            Next_Byte = Next_Byte + Rec_Size
        End Do

        !     If needed, read the last record into BUFFER, then transfer to DATAP.

        Cur_Rec = End_Rec
        If (Cur_Rec /= Start_Rec) Then
            Read(Unit=unt,Rec=Cur_Rec, Iostat=Stats) buffer
            Do i = Next_Byte, Total_Bytes
                Datap(i) = buffer(i - Next_Byte + 1)
            End Do
        End If

        !     Store the number of the record that BUFFER contains, then return.

        Last_Rec = Cur_Rec
        Read_Bin_File = 0
        Return         !Quit the subroutine with zero as code error
      End Do  Do_Ext

      !     Error occured. Set LAST_REC = 0 so we don't reuse BUFFER contents.
      !     Ignore errors that occur when reading END_REC since this
      !     might be the last record in the file and may cause an error
      !     because it is less than REC_SIZE in size.

      If (Cur_Rec == End_Rec) Then
          Last_Rec = Cur_Rec
          Read_Bin_File = 0
      Else
          Last_Rec = 0
          Read_Bin_File = 1
      End If
      Return

    End Function Read_Bin_File

 !**********************************************************************
 !     Routine: Read_Bin_Record
 !     Description:
 !        Reads the record RECORD for the open file on unit UNT into
 !        ARRAY.
 !        Returns 0 if successful, 1 otherwise
 !**********************************************************************

    Integer Function Read_Bin_Record(array, Recordd)
      Integer(kind=1), dimension(Rec_Size), Intent(In Out):: array
      Integer,                              Intent(In)    :: Recordd
      !     Variables
      Integer :: Stats

      Read(Unit=unt,Rec=Recordd, Iostat=Stats) array
      Read_Bin_Record = Stats
      Return
    End Function Read_Bin_Record


 !**********************************************************************
 !     Routine: Read_Bin_Short
 !     Description:
 !        Converts the 2 bytes in RAW to INTEGER swapping
 !        the bytes if necessary.
 !**********************************************************************

    Integer Function Read_Bin_Short(raw)
      Integer(kind=1), Dimension(2), Intent(In)   :: raw
      !     Variables
      Integer(kind=1), dimension(2) :: b
      Integer(Kind=2)               :: i2
      Equivalence (b, i2)
      If (swap) Then
          b(1) = raw(2)
          b(2) = raw(1)
      Else
          b(1) = raw(1)
          b(2) = raw(2)
      End If
      Read_Bin_Short = i2
      Return
    End Function Read_Bin_Short

 !**********************************************************************
 !     Routine: Read_Bin_Long
 !     Description:
 !        Converts the 4 bytes in RAW to INTEGER swapping
 !        the bytes if necessary.
 !**********************************************************************

    Integer Function Read_Bin_Long(raw)
      Integer(kind=1), dimension(4), Intent(In) :: raw
      !     Variables
      Integer(kind=1), dimension(4) ::  b
      Integer(kind=4)               ::  i4
      Equivalence (b, i4)

      If (swap) Then
          b(1) = raw(4)
          b(2) = raw(3)
          b(3) = raw(2)
          b(4) = raw(1)
      Else
          b(1) = raw(1)
          b(2) = raw(2)
          b(3) = raw(3)
          b(4) = raw(4)
      End If

      Read_Bin_Long = i4
      Return
    End Function Read_Bin_Long

 !**********************************************************************
 !     Routine: Read_Bin_Real
 !     Description:
 !        Converts the 4 bytes in RAW to REAL swapping
 !        the bytes if necessary.
 !**********************************************************************

    Real Function Read_Bin_Real(raw)
      Integer(kind=1), dimension(4), Intent(In) :: raw
      !     Variables
      Integer(kind=1), dimension(4) ::  b
      Real(kind=4)                 ::  r4
      Equivalence (b, r4)

      If (swap) Then
          b(1) = raw(4)
          b(2) = raw(3)
          b(3) = raw(2)
          b(4) = raw(1)
      Else
          b(1) = raw(1)
          b(2) = raw(2)
          b(3) = raw(3)
          b(4) = raw(4)
      End If

      Read_Bin_Real = r4
      Return
    End Function Read_Bin_Real

 !**********************************************************************
 !     Function: Bigend_Cpu
 !     Description:
 !        Returns .TRUE. if the machine is big endian
 !**********************************************************************
    Logical Function Bigend_Cpu()
      !Variables
      Integer(kind=1), dimension(2) :: b
      Integer(kind=2)    :: i2
      Equivalence (b, i2)
      i2 = 1
      Bigend_Cpu = (b(1) /= 1)
      Return
    End Function Bigend_Cpu

 !!!-----------------------------------
 !!!  Writing functions and subroutines
 !!!  Writing functions and subroutines
 !!!  Writing functions and subroutines
 !!!  Writing functions and subroutines
 !!!-----------------------------------

    Subroutine Write_TIFF_File_2D_16bits(filenam,xres,yres,Nrow,Ncol,Datap,OK,Mess,Col,endianess)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In    ) :: xres,yres
      Integer,                    intent(In    ) :: Nrow,Ncol
      Integer, dimension(:,:),    intent(In    ) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: Col
      Character(len=*), optional, intent(In    ) :: endianess

      ! Local variables

      Integer,        parameter :: i42=42,Integ_Size = 2           ! Size of Unsigned Integer in bytes
      integer,        parameter :: nfields=12, WhiteIsZero=0

      Integer,        parameter :: Hdr_Size=8  ! Nfields    Tags       Off_next_IFD    2*int4(xres,yres)
      Integer,        parameter :: Tag_Size =    2     +   12*nfields +     4    +         16
      Integer                   :: filelen                        ! Length of file in bytes
      Integer                   :: Off_ifd, Off_xres, Off_yres    ! offset of the IFD
      Integer(kind=1), dimension(Hdr_Size + Nrow*Ncol*Integ_Size + Tag_Size):: buffer    ! Buffer to hold the file
      Integer                   :: i, Indx, ir, ic, Num_Pix, iok,  tag
      character(len=2)          :: endian


      OK=.true.
      Mess=" Everything is OK"
      Num_Pix  = Nrow * Ncol
      Off_ifd  = Hdr_Size + Num_Pix * Integ_Size
      filelen  = Off_ifd  + Tag_size
      Off_xres = Off_ifd  + 2 + 12*nfields + 4
      Off_yres = Off_ifd  + 8

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endianess)) then
        swap=.true.
        Big_End=.true.
      end if

      !Header for writing a TIFF file  (8 bytes)
      endian="II"
      if(Big_End) endian="MM"
      Indx = 1
      Indx = Indx + Write_Bin_String(buffer(Indx:), endian)   !Bytes  1:2   "II" (little endian) or "MM" (big endian)
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), i42, swap) !Bytes  3:4   42
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), Off_ifd, swap)   !Bytes  5:8   Offset (Offst) of the first IFD with respect of the starting of the file
      !                                                       !             (number of the byte in which start the first IFD)
      !Store the image immediately after the header  (offset of the image should be off=8)
      if(present(col)) then ! Store Matrix by columns

        Do i = 1, Num_Pix
          ic= 1+ (i-1)/Nrow
          ir= i-(ic-1)*Nrow
          Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Datap(ir,ic), swap)
        End Do

      Else ! Matrix stored by rows

        Do i = 1, Num_Pix
          ir= 1+ (i-1)/Ncol
          ic= i-(ir-1)*Ncol
          Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Datap(ir,ic), swap)
        End Do

      End If

      !Now writes the IFD starting in byte Off_ifd
      ! IFD (Image File Directory):  The offset Off_ifd should be added to the numbering of bytes for
      !                              obtaining the absolute localisation in the whole file
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), nfields, swap) !Bytes  1:2    N,  Number of fields (entries) described below  (TIFF field = IFD entry)
      !First Tag: PhotometricInterpretation
      Tag= 262  ! Tag = 262 (106.H)
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3),WhiteIsZero, swap)   !Bytes 9:12  Value (or value offset if > 4 bytes)
      !

      !Second Tag: Compression (=1, )
      Tag= 259
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3),1, swap)    !Bytes 9:12  compression=1
      !

      !Third Tag: ImageWidth  (number of colums)
      Tag= 256
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT or Long)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), Ncol, swap)   !Bytes 9:12  Value (or value offset if > 4 bytes)
      !

      !Fourth Tag: ImageLength  (number of rows)
      Tag= 257
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT or Long)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), Nrow, swap)   !Bytes 9:12  Value (or value offset if > 4 bytes)
      !

      !Fifth Tag: BitsPerSample
      Tag= 258
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT or Long)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 16, swap)   !Bytes 9:12  Value (or value offset if > 4 bytes)
      !

      !Sixth Tag: ResolutionUnit
      Tag= 296
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)   !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)   !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 3, swap)   !Bytes 9:12  3: cm as units
      !

      !Seventh Tag: XResolution
      Tag= 282
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 5, swap)   !Bytes 3:4 Field type (RATIONAL)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 2, swap)   !Bytes 5:8   Number of values
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), off_xres, swap)   !Bytes 9:12  Offset of the numerator followed by denominator
      !
      !Eigth Tag: YResolution
      Tag= 283
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap) !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 5, swap)   !Bytes 3:4 Field type (RATIONAL)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 2, swap)   !Bytes 5:8   Number of values
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), off_yres, swap)   !Bytes 9:12  Offset of the numerator followed by denominator
      !
      !Nineth Tag: RowsPerStrip
      Tag= 278
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap)  !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)    !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)    !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), nrow, swap) !Bytes 9:12  all rows are in a single strip
      !

      !Tenth Tag: StripOffsets
      Tag= 273
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap)  !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)    !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)    !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 8, swap)    !Bytes 9:12  Offset of the first (unique) strip of the image
      !

      !Eleventh Tag: StripByteCounts
      Tag= 279
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap)  !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)    !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)    !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), Num_pix*Integ_Size, swap)    !Bytes 9:12  Number of bytes of the unique strip

      !Twelveth Tag: SamplesPerPixel
      Tag= 277
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Tag, swap)  !Bytes 1:2 The tag that identifies the field
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), 3, swap)    !Bytes 3:4 Field type (SHORT)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)    !Bytes 5:8   Number of values, the "count" (lenght) of the indicated type.
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)    !Bytes 9:12  One sample per pixel

      !
      !Write offset of the next IFD  (here is zero) integer*4
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 0, swap)

      !Write value of the numerator of xres
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), xres, swap)
      !Write value of the denominator of xres
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)

      !Write value of the numerator of yres
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), yres, swap)
      !Write value of the denominator of yres
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), 1, swap)

      !     Write buffer to a file.
      iok = Write_Bin_File(trim(filenam), buffer, filelen)
      Select Case (iok)
        case (1)
          Mess='** Error opening file for output: '//Trim(filenam)
          ok=.false.
        case (2)
          Mess='** Error while writing file: '//Trim(filenam)
          ok=.false.
        case default
          Mess=" Everything is OK"
      End Select

    End Subroutine Write_TIFF_File_2D_16bits

    !  The following subroutine assumes by default that the 2D matrix to be written in the binary file
    !  is stored by rows (C-like default). Providing the optional argument, Col, changes
    !  the default to a matrix stored by columns (Fortran-like default)
    !
    Subroutine Write_Binary_File_2D_16bits(filenam,Nrow,Ncol,Datap,OK,Mess,Col,endianess)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In    ) :: Nrow,Ncol
      Integer, dimension(:,:),    intent(In    ) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: Col
      Character(len=*), optional, intent(In    ) :: endianess
      ! Local variables

      Integer,        parameter :: Integ_Size = 2           ! Size of Unsigned Integer in bytes
      integer,        parameter :: i42=42
      Integer,        parameter :: Hdr_Size=8
      Integer                   :: filelen                  ! Length of file in bytes
      Integer(kind=1), dimension(Hdr_Size + Nrow*Ncol*Integ_Size):: buffer    ! Buffer to hold the file
      Integer            :: i, Indx, ir, ic, Num_Pix, iok
      character(len=2)   :: endian


      OK=.true.
      Mess=" Everything is OK"
      Num_Pix     = Nrow * Ncol
      filelen = Hdr_Size + Num_Pix * Integ_Size

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endianess)) then
        swap=.true.
        Big_End=.true.
      end if

      !Header for writing a TIFF file  (8 bytes)
      endian="II"
      if(Big_End) endian="MM"
      Indx = 1
      Indx = Indx + Write_Bin_String(buffer(Indx:), endian)
      Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), i42, swap)
      Indx = Indx + Write_Bin_Uint32(buffer(Indx:Indx+3), filelen, swap)

      if(present(col)) then ! Store Matrix by columns

        Do i = 1, Num_Pix
          ic= 1+ (i-1)/Nrow
          ir= i-(ic-1)*Nrow
          Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Datap(ir,ic), swap)
        End Do

      Else ! Matrix stored by rows

        Do i = 1, Num_Pix
          ir= 1+ (i-1)/Ncol
          ic= i-(ir-1)*Ncol
          Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Datap(ir,ic), swap)
        End Do

      End If
      !     Write buffer to a file.
      iok = Write_Bin_File(trim(filenam), buffer, filelen)
      Select Case (iok)
        case (1)
          Mess='** Error opening file for output: '//Trim(filenam)
        case (2)
          Mess='** Error while writing file: '//Trim(filenam)
        case default
          Mess=" Everything is OK"
      End Select

    End Subroutine Write_Binary_File_2D_16bits

    !  The following subroutine assumes by default that the 2D matrix to be written in the binary file
    !  is stored by rows (C-like default). Providing the optional argument, Col, changes
    !  the default to a matrix stored by columns (Fortran-like default)
    !
    Subroutine Write_Binary_File_2D_16bits_NoHeader(filenam,Nrow,Ncol,Datap,OK,Mess,Col,endianess)
      Character(len=*),           intent(In    ) :: filenam
      Integer,                    intent(In    ) :: Nrow,Ncol
      Integer, dimension(:,:),    intent(In    ) :: Datap
      Logical,                    intent(   Out) :: OK
      Character(len=*),           intent(   Out) :: Mess
      Character(len=*), optional, intent(In    ) :: Col
      Character(len=*), optional, intent(In    ) :: endianess
      ! Local variables

      Integer,        parameter :: Integ_Size = 2           ! Size of Unsigned Integer in bytes
      Integer                   :: filelen                  ! Length of file in bytes
      Integer(kind=1), dimension(Nrow*Ncol*Integ_Size):: buffer    ! Buffer to hold the file
      Integer            :: i, Indx, ir, ic, Num_Pix, iok
      character(len=2)   :: endian


      OK=.true.
      Mess=" Everything is OK"
      Num_Pix     = Nrow * Ncol
      filelen = Num_Pix * Integ_Size

      !     Little-endian file. If the machine has a different endian than
      !     the file then set SWAP to .TRUE.

      Big_End = .FALSE.
      swap = (Bigend_Cpu() .NEQV. Big_End)
      if(present(endianess)) then
        swap=.true.
        Big_End=.true.
      end if

      Indx = 1

      if(present(col)) then ! Store Matrix by columns

        Do i = 1, Num_Pix
          ic= 1+ (i-1)/Nrow
          ir= i-(ic-1)*Nrow
          Indx = Indx + Write_Bin_Uint16(buffer(Indx), Datap(ir,ic), swap)
        End Do

      Else ! Matrix stored by rows

        Do i = 1, Num_Pix
          ir= 1+ (i-1)/Ncol
          ic= i-(ir-1)*Ncol
          Indx = Indx + Write_Bin_Uint16(buffer(Indx:Indx+1), Datap(ir,ic), swap)
        End Do

      End If
      !     Write buffer to a file.
      iok = Write_Bin_File(trim(filenam), buffer, filelen)
      Select Case (iok)
        case (1)
          Mess='** Error opening file for output: '//Trim(filenam)
        case (2)
          Mess='** Error while writing file: '//Trim(filenam)
        case default
          Mess=" Everything is OK"
      End Select

    End Subroutine Write_Binary_File_2D_16bits_NoHeader


 !**********************************************************************
 !     Routine: Write_Bin_File
 !     Description:
 !        Writes NUMBYTES bytes from BUFFER to a binary file called
 !        NAMEF. Returns non-zero if the write fails.
 !**********************************************************************

    Integer Function Write_Bin_File(Namef, buffer, numbytes)
      Character (Len=*),                   Intent(In)   :: Namef
      Integer(kind=1),dimension(numbytes), Intent(In)   :: buffer ! The data to write
      Integer,                             Intent(In)   :: numbytes

      Integer  :: iErr

      Open(Unit=Unt, File=Namef, Status='REPLACE', Form='UNFORMATTED',  &
           Access='DIRECT', Recl=numbytes, Iostat=iErr)
      If(iErr == 0) then
         Write(Unit=Unt, Rec=1, Iostat=iErr) buffer
         If(iErr == 0) then
            Close(Unit=Unt)
            Write_Bin_File = 0
         Else
            Write_Bin_File = 2
         End if
      Else
         Write_Bin_File = 1
      End If
      Return
    End Function Write_Bin_File



 !**********************************************************************
 !     Routine: Write_Bin_Uint16
 !     Description:
 !        Writes an unsigned 16-bit integer to BUFFER. The bytes
 !        will be swapped if SWAP is true. The function returns
 !        the number of bytes written to BUFFER.
 !**********************************************************************

    Integer Function Write_Bin_Uint16(buffer, Datap, swap)
      Integer(kind=1),dimension(2), Intent(Out)  :: buffer
      Integer,                      Intent(In)   :: Datap
      Logical,                      Intent(In)   :: swap

!       Variables

      Integer(kind=1),dimension(2) :: tmpbuf
      Integer(kind=2)              :: i2
      Equivalence (tmpbuf, i2)

      If (Datap < 0) Then
          i2 = 0
      Else If (Datap > 65535) Then
          i2 = 65535  !2^16 - 1
      Else
          i2 = Datap
      End If

      If (swap) Then
          buffer(1) = tmpbuf(2)
          buffer(2) = tmpbuf(1)
      Else
          buffer(1) = tmpbuf(1)
          buffer(2) = tmpbuf(2)
      End If
      Write_Bin_Uint16 = 2
      Return
    End Function Write_Bin_Uint16



 !**********************************************************************
 !     Routine: Write_Bin_Int16
 !     Description:
 !        Writes a signed 16-bit integer to BUFFER. The bytes
 !        will be swapped if SWAP is true. The function returns
 !        the number of bytes written to BUFFER.
 !**********************************************************************

    Integer Function Write_Bin_Int16(buffer, Datap, swap)
      Integer(kind=1),dimension(2), Intent(Out)  :: buffer
      Integer,                      Intent(In)   :: Datap
      Logical,                      Intent(In)   :: swap

!       Variables

      Integer(kind=1),dimension(2) :: tmpbuf
      Integer(kind=2)              :: i2
      Equivalence (tmpbuf, i2)

      If (Datap < -32768) Then
          i2 = -32768
      Else If (Datap > 32767) Then
          i2 = 32767
      Else
          i2 = Datap
      End If

      If (swap) Then
          buffer(1) = tmpbuf(2)
          buffer(2) = tmpbuf(1)
      Else
          buffer(1) = tmpbuf(1)
          buffer(2) = tmpbuf(2)
      End If

      Write_Bin_Int16 = 2
      Return
    End Function Write_Bin_Int16


 !**********************************************************************
 !     Routine: Write_Bin_Uint32
 !     Description:
 !        Writes an unsigned 32-bit integer to BUFFER. The bytes
 !        will be swapped if SWAP is true. The function returns
 !        the number of bytes written to BUFFER.
 !**********************************************************************

    Integer Function Write_Bin_Uint32(buffer, Datap, swap)
      Integer(kind=1),dimension(4), Intent(Out)  :: buffer
      Integer,                      Intent(In)   :: Datap
      Logical,                      Intent(In)   :: swap

      !     Variables
      Integer(kind=1),dimension(4) :: tmpbuf
      Integer(kind=4)              :: i4
      Equivalence (tmpbuf, i4)

      If (Datap < 0) Then
          i4 = 0
      Else
          i4 = Datap
      End If

      If (swap) Then
          buffer(1) = tmpbuf(4)
          buffer(2) = tmpbuf(3)
          buffer(3) = tmpbuf(2)
          buffer(4) = tmpbuf(1)
      Else
          buffer(1) = tmpbuf(1)
          buffer(2) = tmpbuf(2)
          buffer(3) = tmpbuf(3)
          buffer(4) = tmpbuf(4)
      End If

      Write_Bin_Uint32 = 4
      Return
    End Function Write_Bin_Uint32


 !**********************************************************************
 !     Routine: Write_Bin_Int32
 !     Description:
 !        Writes an signed 32-bit integer to BUFFER. The bytes
 !        will be swapped if SWAP is true. The function returns
 !        the number of bytes written to BUFFER.
 !**********************************************************************

    Integer Function Write_Bin_Int32(buffer, Datap, swap)

      Integer(kind=1),dimension(4), Intent(Out)  :: buffer
      Integer,                      Intent(In)   :: Datap
      Logical,                      Intent(In)   :: swap

      !     Variables
      Integer(kind=1),dimension(4) :: tmpbuf
      Integer(kind=4)              :: i4
      Equivalence (tmpbuf, i4)

      i4 = Datap

      If (swap) Then
          buffer(1) = tmpbuf(4)
          buffer(2) = tmpbuf(3)
          buffer(3) = tmpbuf(2)
          buffer(4) = tmpbuf(1)
      Else
          buffer(1) = tmpbuf(1)
          buffer(2) = tmpbuf(2)
          buffer(3) = tmpbuf(3)
          buffer(4) = tmpbuf(4)
      End If

      Write_Bin_Int32 = 4
      Return
    End Function Write_Bin_Int32

 !**********************************************************************
 !     Routine: Write_Bin_Real
 !     Description:
 !        Writes a 32-bit real to BUFFER. The bytes will be swapped
 !        if SWAP is true. The function returns the number of bytes
 !        written to BUFFER.
 !**********************************************************************

    Integer Function Write_Bin_Real(buffer, Datap, swap)

      Integer(kind=1),dimension(4), Intent(Out)  :: buffer
      Real(kind=4),                 Intent(In)   :: Datap
      Logical,                      Intent(In)   :: swap

      !     Variables
      Integer(kind=1),dimension(4) :: tmpbuf
      Real(kind=4)                 :: r4
      Equivalence (tmpbuf, r4)

      r4 = Datap

      If (swap) Then
          buffer(1) = tmpbuf(4)
          buffer(2) = tmpbuf(3)
          buffer(3) = tmpbuf(2)
          buffer(4) = tmpbuf(1)
      Else
          buffer(1) = tmpbuf(1)
          buffer(2) = tmpbuf(2)
          buffer(3) = tmpbuf(3)
          buffer(4) = tmpbuf(4)
      End If

      Write_Bin_Real = 4
      Return
    End Function Write_Bin_Real


 !**********************************************************************
 !     Routine: Write_Bin_String
 !     Description:
 !        Writes the string in DATA to BUFFER
 !**********************************************************************

    Integer Function Write_Bin_String(buffer, Datap)

      Integer(kind=1),dimension(:), Intent(In Out)  :: buffer
      Character (Len=*),            Intent(In)      :: Datap

      !     Variables

      Integer :: i

      Do i = 1, Len(Datap)
          buffer(i) = Ichar(Datap(i:i))
      End Do
      Write_Bin_String = i - 1
      Return
    End Function Write_Bin_String

 End Module ReadnWrite_Binary_Files
