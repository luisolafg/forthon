  !!----   Program TIFF_RAW_BIN_TO_HBIN
  !!----
  !!----  The title explains all !!! Similar to cyclops to cylinder but without
  !!----  doing the transformation octagon-to-cylinder
  !!----  Created: 8 August 2012 (JRC)
  !!----
  !!----  Usage: > tifrawbin2hbin  buffer_file <cr>
  !!----
  !!----  The buffer file should contain the following lines:
  !!----       INSTRM   My_instrm.inf
  !!----       CYL_DIM  nrow_cyl  ncol_cyl
  !!----       Image_file1
  !!----       Image_file2
  !!----       ...........
  !!----       Image_filen
  !!----
  !!----  Or, in the case the user wants to include extra information in
  !!----  binary files with "visible" headers, some items can be given following
  !!----  the image file names, after the separator "::" in order to construct a header.
  !!----  The interpretable items are: Title, user,temp, magn, press,  omeg, xdis, ydis, zdis:
  !!----  Title an user are followed by alphanumeric information and all the others are followed
  !!----  by numeric values. The items are separated by ":".
  !!----
  !!----       INSTRM   My_cyclops_instrm.inf
  !!----       CYL_DIM  nrow_cyl  ncol_cyl  radius_cyl  height_cyl
  !!----       Image_file1  :: title my_title : temp 300.0 : omeg 34.12  : magn 3.5 : press 0.0
  !!----       Image_file2  :: title my_title : temp 300.0 : omeg 54.12  : magn 3.5 : press 0.0
  !!----       ...........
  !!----       Image_filen  :: title my_title : temp 300.0 : omeg 174.12 : magn 3.5 : press 0.0
  !!----
  !!----
  Program tifrawbin2hbin
    use CFML_GlobalDeps,         only: pi, tpi, dp
    use CFML_String_Utilities,   only: Get_LogUnit, L_Case, Get_Separator_Pos
    use Laue_Mod,                only: Laue_Instrument_Type, Read_Laue_Instrm, Write_Laue_Instrm
    use Laue_TIFF_READ,          only: Image_Conditions, read_tiff_image,TIFF_READ_Error,TIFF_READ_Error_Message, &
                                       Write_ICd
    Use Readnwrite_Binary_Files, only: Read_Binary_File_16bits
    Use Data_Trt,                only: flip_2D,noise_out, binning_matrix
    Use ReadnWrite_2D_Detector_Images

    implicit none

    integer, dimension(:,:),allocatable :: datas, datan
    character(len=256)                  :: filebuf,instrm_file,filenam,fileout, mod_ins, line, &
                                           head_info, mess
    integer                             :: nrow_cyl, ncol_cyl,nrow,ncol,ipr, narg, i_buf, ier, n, &
                                           siz,m, &  ! Diameter in pixels of the noisy points, slope. Default (3,60)
                                           bin_lun, nbin, p,q,k,l, i,j
    real                                :: r_cyl, V_cyl, tini,tfin
    Type(Laue_Instrument_Type)          :: LaueDiff, LaueNew
    logical                             :: ok,arggiven=.false., instr_given=.false., bin_given=.false., &
                                           flip_h=.false.,flip_v=.false., noise_corr=.false., &
                                           transf_given=.false.,hbin=.false.,head=.false., ICd_given=.false.
    Type(Header_Type)                   :: header
    Type(Image_Conditions)              :: ICd

    call cpu_time(tini)
    narg=COMMAND_ARGUMENT_COUNT()


    if(narg > 0) then
            call GET_COMMAND_ARGUMENT(1,filebuf)
            arggiven=.true.
    end if

    if(arggiven) then
      Call Get_LogUnit(i_buf)
      open(unit=i_buf, file=filebuf, status="old",action="read", position="rewind")
      do
        read(unit=i_buf,fmt="(a)",iostat=ier) line
        if(ier /= 0) exit
        if(line(1:5) == "INSTR") then
          instrm_file=adjustl(line(6:))
          i=index(instrm_file," ")
          if( i /= 0) then
             mod_ins=adjustl(instrm_file(i+1:))
             if(len_trim(mod_ins) == 0)  mod_ins= "cyc_cylind.inf"
             instrm_file=instrm_file(1:i-1)
          else
             mod_ins= "cyc_cylind.inf"
          end if
          instr_given=.true.
          exit
        end if
      end do
      if(.not. instr_given) then
        write(unit=*,fmt="(a)") " => Error! No instrument file given in the buffer file: "//trim(filebuf)
        stop
      end if
    else
      write(unit=*,fmt="(a)") " => Error! No buffer file given! "
      write(unit=*,fmt="(a)") " => Usage of the program TIF_RAW_BIN_to_HBIN -> "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") " Prompt>  tifrawbin2hbin  buffer_file <cr> "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") " The buffer file should contain the following lines: "
      write(unit=*,fmt="(a)") "      INSTRM   My_instrm.inf  My_New_instrm.inf "
      write(unit=*,fmt="(a)") "      BINNING  odd_number "
      write(unit=*,fmt="(a)") "      Image_file1 "
      write(unit=*,fmt="(a)") "      Image_file2 "
      write(unit=*,fmt="(a)") "      ........... "
      write(unit=*,fmt="(a)") "      Image_filen "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") "     "
      write(unit=*,fmt="(a)") " Or, in the case the user wants to include extra information in                        "
      write(unit=*,fmt="(a)") " binary files with 'visible' headers, some items can be given following                "
      write(unit=*,fmt="(a)") " the image file names, after the separator '::' in order to construct a header.        "
      write(unit=*,fmt="(a)") " The interpretable items are: Title, user,temp, magn, press,  omeg, xdis, ydis, zdis:  "
      write(unit=*,fmt="(a)") " Title an user are followed by alphanumeric information and all the others are followed"
      write(unit=*,fmt="(a)") " by numeric values. The items are separated by ':'.                                    "
      write(unit=*,fmt="(a)") "                                                                                       "
      write(unit=*,fmt="(a)") "      INSTRM   My_instrm.inf   My_New_instrm.inf                                   "
      write(unit=*,fmt="(a)") "      BINNING  odd_number                               "
      write(unit=*,fmt="(a)") "      Image_file1  :: title my_title : temp 300.0 : omeg 34.12  : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)") "      Image_file2  :: title my_title : temp 300.0 : omeg 54.12  : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)") "      ...........                                                                      "
      write(unit=*,fmt="(a)") "      Image_filen  :: title my_title : temp 300.0 : omeg 174.12 : magn 3.5 : press 0.0 "
      write(unit=*,fmt="(a)") "       "
      stop
    end if

    call Read_Laue_Instrm(instrm_file,LaueDiff)
    ncol=LaueDiff%np_h; nrow=LaueDiff%np_v
    r_cyl=LaueDiff%D
    V_cyl=LaueDiff%V
    write(unit=*,fmt="(a)")         " => Instrument values (Npix_hor,Npix_vert, D, H, V): "
    write(unit=*,fmt="(a,2i6,a)")   "    Npix_hor x Npix_vert  = (",LaueDiff%np_h,LaueDiff%np_v," )"
    write(unit=*,fmt="(a,f10.4,a)") "    Distance det-sample,D = ",LaueDiff%D," mm"
    write(unit=*,fmt="(a,f10.4,a)") "    Horizontal Length,  H = ",LaueDiff%H," mm"
    write(unit=*,fmt="(a,f10.4,a)") "    Vertical   Length,  V = ",LaueDiff%V," mm"
    !Read now the wished characteristics of the cylindrical image
    do
      read(unit=i_buf,fmt="(a)",iostat=ier) line
      if(ier /= 0) exit
      if(line(1:7) == "BINNING") then
        read(unit=line(8:),fmt=*) nbin
        bin_given=.true.
        if(nbin == 1) then
          ncol_cyl=ncol; nrow_cyl=nrow
        else
          k=0
          p=(nbin+1)/2; q=nbin/2
          do i=p,nrow-p,nbin
             k=k+1
             l=0
             do j=p,ncol-p,nbin
                l=l+1
             end do
          end do
          !dimension in pixels of the new cylinder (k,l)
          ncol_cyl=l; nrow_cyl=k
        end if
        exit
      end if
    end do
    if(.not. bin_given) then
        write(unit=*,fmt="(a)") " => Error! No binning factor given in the buffer file: "//trim(filebuf)
        stop
    end if
    do
      read(unit=i_buf,fmt="(a)",iostat=ier) line
      if(ier /= 0) exit
      if(line(1:6) == "TRANSF") then
        line=L_Case(line)
        i=index(line,"flip_h")
        if(i /= 0) flip_h=.true.
        i=index(line,"flip_v")
        if(i /= 0) flip_v=.true.
        i=index(line,"noise")
        if(i /= 0) then
          noise_corr=.true.
          read(unit=line(i+6:),fmt=*,iostat=ier) siz, m
          if(ier /= 0) then
            siz=3; m=60
          end if
        end if
        transf_given=.true.
        exit
      end if
    end do
    if(.not. transf_given) then
      write(unit=*,fmt="(a)") " => Error! No TRANSF instruction given in the buffer file: "//trim(filebuf)
      stop
    end if

    write(unit=*,fmt="(a)") "       "
    write(unit=*,fmt="(a)")         " => Cylinder characteristics:      "
    write(unit=*,fmt="(a,2i6,a)")   "     Npix_hor x Npix_vert           = (",ncol_cyl,nrow_cyl," )"
    write(unit=*,fmt="(a,f10.4,a)") "     Distance det-sample (Radius)   = ",r_cyl," mm"
    write(unit=*,fmt="(a,f10.4,a)") "   H-Length of the deployed surface = ",r_cyl*tpi," mm"
    write(unit=*,fmt="(a,f10.4,a)") "   V-Length of the deployed surface = ",V_cyl," mm"

    if(flip_h)  write(unit=*,fmt="(a)") " => Final image flipped horizontally"
    if(flip_v)  write(unit=*,fmt="(a)") " => Final image flipped vertically"
    if(noise_corr) write(unit=*,fmt="(2(a,i3))") " => Final image corrected from noisy points. Parameters -> size: ", &
                   siz," slope: ",m
    if(allocated(datan)) deallocate(datan)
    allocate(datan(nrow_cyl,ncol_cyl))

    n=0
    do
      read(unit=i_buf,fmt="(a)",iostat=ier) line
      if( ier /= 0 .or. Len_Trim(line) == 0 ) exit
      line=adjustl(line)
      if(line(1:1) == "!" .or.  line(1:1) == "#" .or. len_trim(line) == 0) cycle
      i=index(line,"::")
      if(i /= 0) then
        filenam=trim(line(1:i-1))
        head_info=line(i+2:)
        hbin=.true.
      else
        filenam=trim(line)
      end if
      n=n+1
      if(allocated(datas)) deallocate(datas)
      allocate(datas(nrow,ncol))  !nrow=2400;ncol=7680 for cyclops

      write(unit=*,fmt="(a)") " => Reading image from file: "//trim(filenam)
      i=index(filenam,".tif")
      if( i /= 0) then
        Call Read_Laue_Image(filenam,nrow,ncol,Datas,ok,ICd)
        ICd_given=.true.
        call Write_ICd(ICd)
      else
        Call Read_Laue_Image(filenam,nrow,ncol,Datas,ok)
      end if
      if(noise_corr) call noise_out(1.0,1.0,real(Ncol),real(Nrow),siz,m,Datas)

      if(ok) then
        LaueNew=LaueDiff
        !Modify the number of pixels if needed

        if(nbin == 1) then
          datan=datas
        else
          call Binning_Matrix(Datas,nbin,Datan)
        end if

        !Now correct from flips
        if(flip_h .or. flip_v) call flip_2D(datan,flip_v,flip_h)

        !Writing the new Image file
        i=index(filenam, ".", back=.true.)
        if(hbin) then
          if(ICd_given) then
            Call Set_header(header,head_info,ICd)
          else
            Call Set_header(header,head_info)
          end if
          fileout=filenam(1:i-1)//".hbin"
          Call Write_Header_2D_Image(fileout,header,ok,mess,bin_lun)
          if(.not. ok) then
             write(unit=*,fmt="(/a)") " => Transformed image not written for file: "//trim(filenam)
             write(unit=*,fmt="(a)") " => "//trim(mess)
             cycle
          end if
          Call Write_Binary_2D_Image(bin_lun,header,datan,1,ok,mess)
          if(.not. ok) then
             write(unit=*,fmt="(/a)") " => Error in transformed image file: "//trim(fileout)
             write(unit=*,fmt="(a)") " => "//trim(mess)
             cycle
          end if
        else
          fileout=filenam(1:i-1)//".bin"
          call Write_Simple_Binary_Image(fileout, nrow_cyl, ncol_cyl, datan)
        end if
      else
        write(unit=*,fmt="(a)") " => Error reading the image file: "//trim(filenam)
        n=n-1
      end if
    end do
    write(unit=*,fmt="(a,i4)") " => Total number of the image files: ",n
    !Complete the new instrument characteristics
    LaueNew%np_h=ncol_cyl
    LaueNew%np_v=nrow_cyl
    LaueNew%xo=0.5*real(LaueNew%Np_h)
    LaueNew%zo=0.5*real(LaueNew%Np_v)
    LaueNew%x_min=-0.5*LaueNew%H
    LaueNew%x_max=-LaueNew%x_min

    !Writing the new instrument file
    call Get_LogUnit(ipr)
    open(unit=ipr, file=trim(mod_ins), status="replace", action="write")
    Call Write_Laue_Instrm(LaueNew, ipr)
    close(unit=ipr)
    write(unit=*,fmt="(a)") " => New instrument file: "//trim(mod_ins)
    call cpu_time(tfin)
    write(unit=*,fmt="(a)") " => Program finished normally ..."
    write(unit=*,fmt="(a,f8.3,a)") " => CPU-time: ",tfin-tini," seconds"

    contains

    Subroutine Set_header(H,string,ICd)
      Type(Header_Type),              intent(in out) :: h
      character(len=*),               intent(in out) :: string
      Type(Image_Conditions),optional,intent(in)     :: ICd
      !--- Local variables ---!
      integer, dimension(15)            :: pos
      integer                           :: i,j,ncar,nitems,i1,i2
      character(len=80), dimension(16)  :: items
      character(len=10)                 :: key
      character(len=512)                :: Lstring
      if(present(ICd)) then
        Lstring=L_case(string)
        if(index(Lstring,"title") == 0) then
          string=trim(string)//" : "//"title "//trim(ICd%sample)//" "//trim(ICd%comment)
        end if
        if(index(Lstring,"user") == 0) then
          string=trim(string)//" : "//"user "//trim(ICd%user)//" "//trim(ICd%datetime)
        end if
        if(index(Lstring,"omeg") == 0) then
          string=trim(string)//" : "//"omeg "
          i=len_trim(string)
          write(unit=string(i+2:),fmt="(f9.4)") ICd%expose_phi
        end if
        if(index(Lstring,"temp") == 0) then
          string=trim(string)//" : "//"temp "
          i=len_trim(string)
          write(unit=string(i+2:),fmt="(f9.4)") 0.5*(ICd%temp_begin+ICd%temp_end)
        end if
      end if
      ! Extract the information contained in string for constructing the header
      ! Only a limited number of items are presently available
      ! The items in string are separated by ":"
      Call Get_Separator_Pos(string,":",pos,ncar)
      nitems=ncar+1
      i2=pos(1)-1
      i1=1
      items(1)=adjustl(string(i1:i2))
      do i=2,ncar
        i1=pos(i-1)+1
        i2=pos(i)-1
        items(i)=adjustl(string(i1:i2))
      end do
      items(nitems)=adjustl(string(pos(ncar)+1:))
      Call Init_Header(H)
      Call  Allocate_Header(H, &
                            environment=(/"Temp-sample","Magn. Field","  Pressure "/), &
                            env_units=  (/" Kelvin ",   " Tesla  ",   "GPascals"/),          &
                            motors=     (/"X_disp", "Y_disp", "Z_disp", "Omega "/),       &
                            mot_units=  (/"   mm  ","   mm  ","   mm  ","Degrees"/) )
      Do i=1,nitems
        key=L_case(items(i))
        j=index(key," ")
        if(j == 0) cycle
        key=key(1:j-1)
        Select Case (trim(key))
          case("title")
            H%Title=items(i)(6:)
          case("user")
            h%User_LC_Date_Time=items(i)(5:)
          case("temp")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(1,1)
          case("magn")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(2,1)
          case("pres")
            read(unit=items(i)(5:),fmt=*) H%Envnt_Value(3,1)
          case("xdis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(1,1)
          case("ydis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(2,1)
          case("zdis")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(3,1)
          case("omeg")
            read(unit=items(i)(5:),fmt=*) H%Motor_Value(4,1)
        End Select
      end do
      if(present(ICd)) then
         H%time(1)=ICd%expose_time
      end if
      H%Nrow=nrow_cyl; H%Ncol=ncol_cyl
      H%Instrument_name="CYCLOPS (Cylindrical Shape)"
      if(present(ICd))  H%Instrument_name = ICd%Host
      H%Detector_type="CYL"
      H%Pix_Size_h=tpi*r_cyl/real(ncol_cyl-1)
      H%Pix_Size_v=V_cyl/real(nrow_cyl-1)
      H%Sample_Detector_dist=r_cyl
      H%Lambda_min=0.8; H%Lambda_max=5.0
      H%Scan_Type="ACQ"

      return
    End Subroutine Set_header

 End Program tifrawbin2hbin