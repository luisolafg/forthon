!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
!!----  This module gathers calculations related to the Laue Diffraction Technique
!!----  It is under strong development and some procedures are not tested (or completed)
!!----  It is a central part of the Laue Suite Project at ILL
!!----  Some of the procedures that are provisionally here may be moved to more general
!!----  modules existing in CrysFML.
!!----
!!----  Started: October 2009    (JRC)
!!----  Last update: 9 July 2010
!!----  Contributors: Juan Rodriguez-Carvajal
!!----                Luis Fuentes-Montero
!!----
!!----  The reference system adopted here corresponds to that defined by Busing and Levy
!!----  in Acta Cryst. (1967). 22, 457
!!----  The observable Laue reflections should have negative y-coordinates in the fixed
!!----  Laboratory Cartesian system. The U and B matrices are those of B-L.
!!----
Module Laue_Mod
 use CFML_GlobalDeps
 Use CFML_IO_Messages,               only: Error_message
 use CFML_Math_General,              only: Sort, co_prime ,sind, cosd, atand, asind,atan2d,acosd, tand, &
                                           primes
 use CFML_Math_3D,                   only: Get_Spheric_Coord,Invert_A, Cross_Product
 use CFML_IO_Formats,                only: file_list_type
 use CFML_Crystallographic_Symmetry, only: Space_Group_type
 use CFML_Reflections_Utilities,     only: Reflection_List_Type, Hkl_Gen_Sxtal, get_maxnumref, Hkl_Equiv_List, &
                                           hkl_Absent
 use CFML_String_Utilities,          only: l_case, u_case, Get_LogUnit,Getword
 use CFML_crystal_metrics,           only: Crystal_Cell_Type, Rot_Matrix
 use CFML_Geometry_Calc,             only: Matrix_Rx, Matrix_Ry, Matrix_Rz, angle_uv, Get_PhiTheChi, &
                                           Get_Anglen_Axis_From_RotMat
 use CFML_Geometry_SXTAL,            only: genb, genub

 implicit none
 private
 public :: Allocate_Laue_Ref_List, Set_Rotation_Matrix, Calc_Laue_Spot_XZ_Angles,               &
           Set_Ld_Laue_Limits, Nodal_Indices, Calc_Angular_Laue_Limits, Calc_Laue_Xz_Position,  &
           Print_Laue_Ref_List, Init_Laue_Ref, Gaussian_2D, Read_Experimental_Laue_Spots,       &
           Calc_Gam_Nu_From_Xz, Calc_Spherical_Angles_From_Zv, Calc_Spherical_Angles_From_Xz,   &
           Set_Angular_Matrix, Matching_1D, Matching_2D,  Matching_4V, Get_U_R_From_Xzhkl,      &
           Get_Rotations_Vc_Vlab, Get_Gam_Nu_From_Xz, Get_Xy_Stereo_Proy_From_Xz, Calc_RD_rOD,  &
           Pix_To_Mil,Mil_To_Pix, Get_Orient_from_xz_spots, Read_Laue_Instrm, Write_Laue_Instrm,&
           Ren_Moffat, Calc_Laue_Image, Calc_Laue_BackGround, Get_Orientation_from_xz_spots,    &
           Get_Phi_xyz, Get_Phi_zyx, Calc_Reflection_Orbit, Laue_Mult, Get_Matrix_moving_v_to_u,&
           uvw_Equiv_List, Read_Twinlaw, Write_Twinlaw, Get_Scattered_wavevector_from_xz,       &
           Get_Reciprocal_vector_from_Scattered_wavevector,Calc_Monochromatic_Spot_XZ_Angles,   &
           Update_Instrument, Read_Offsets, Corr_xz_Chebychev, Set_Sample, Corr_MOSFLM,         &
           Init_Offsets

    Logical,           public :: Error_Laue_Mod=.false.
    Logical,           public :: apply_offsets=.false.   ! One per image, Normally equal to LaueDiff
                                                         ! Offset and distorsions applied.
    character(len=120),public :: Error_Mess_Laue_Mod=" "

    !!----
    !!----  Type, public :: Laue_Ref_Type
    !!----      Integer           :: stats   ! Status of the reflection
    !!----                                -3 ! Oposite side of Ewald spheres (Non observed, all angles = 0.0)
    !!----                                -2 ! Outside the given resolution         "
    !!----                                -1 ! Outside Ewald spheres                "
    !!----                                 0 ! Observed reflection
    !!----                                 1 ! Beyond Nu-limit
    !!----                                 2 ! Beyond Gamma-limit
    !!----                                 3 ! Outside the detector area
    !!----                                 4 ! Unindexed experimental peak
    !!----      integer             :: domain  ! Index for identifying the twin
    !!----      integer             :: mult    ! Laue multiplicity
    !!----      integer             :: nodal   ! Nodal index (1 for a nodal reflection: coprime integers)
    !!----      real                :: ds      ! D-spacing in Angstroms of the reflection
    !!----      real                :: gamma   ! Gamma angle in L-system
    !!----      real                :: nu      ! Nu angle in L-system
    !!----      real                :: Ttheta  ! 2Theta angle
    !!----      real                :: Lambda  ! Wavelength
    !!----      real,   dimension(3):: h       ! Reciprocal space coordinates
    !!----      real,   dimension(3):: zv      ! Cartesian coordinates of h in the laboratory system
    !!----      real                :: Obs_int ! Observed intensity
    !!----      real                :: Cal_int ! Calculated intensity
    !!----      real                :: Sigma   ! Standard deviation of the observed intensity
    !!----      real                :: x,z     ! coordinates of the spot in mm
    !!----      real                :: stheta  ! spherical angle Theta of the scattering vector in L-system
    !!----      real                :: sphi    ! spherical angle Phi   of the scattering vector in L-system
    !!----      integer,dimension(3):: kindex  ! Indices for propagation vectors
    !!----                                       i1: makes reference to the propagation vector numbered i1
    !!----                                       i2: makes reference to the arm i2 of the star of vector i1
    !!----                                       i3: harmonic number
    !!----                                       h=(hx,hy,hz)= h0 + i3 * k(i1,i2)
    !!----  End Type Laue_Ref_Type
    !!----
    !!----
    !!----  Created: October 2009    (JRC)
    !!----  Updated: 6 January 2010
    !!----

    Type, public :: Laue_Ref_Type
        Integer             :: stats=-1    ! Status of the reflection (=0, observed)
        integer             :: domain=1    ! Index for identifying the twin
        integer             :: mult=0      ! Laue multiplicity
        integer             :: nodal=0     ! Nodal index (1 for a nodal reflection: coprime integers)
        real                :: ds=0.0      ! D-spacing in Angstroms of the reflection
        real                :: gamma=0.0   ! Gamma angle in L-system
        real                :: nu=0.0      ! Nu angle in L-system
        real                :: Ttheta=0.0  ! 2Theta angle
        real                :: Lambda=0.0  ! Wavelength
        real,   dimension(3):: h=0.0       ! Reciprocal space coordinates
        real,   dimension(3):: zv=0.0      ! Cartesian coordinates of h in the laboratory system
        real                :: Obs_int=0.0 ! Observed intensity
        real                :: Cal_int=0.0 ! Calculated intensity
        real                :: Sigma=0.0   ! Standard deviation of the observed intensity
        real                :: x=0.0,z=0.0 ! coordinates of the spot in mm
        real                :: stheta=0.0  ! spherical angle Theta of the scattering vector in L-system
        real                :: sphi=0.0    ! spherical angle Phi   of the scattering vector in L-system
        integer,dimension(3):: kindex=0    ! Indices for propagation vectors
                                       ! i1: makes reference to the propagation vector numbered i1
                                       ! i2: makes reference to the arm i2 of the star of vector i1
                                       ! i3: harmonic number
                                       ! h=(hx,hy,hz)= h0 + i3 * k(i1,i2)
    End Type Laue_Ref_Type

    !!----
    !!----  Type, public :: Laue_Ref_List_Type
    !!----     integer :: nref
    !!----     Type(Laue_Ref_Type), dimension(:), allocatable :: LR
    !!----  End Type  Laue_Ref_List_Type
    !!----
    !!----  List of Laue_Ref_Type objects
    !!----
    !!----  Created: October 2009 (JRC)
    !!----  Updated: October 2009
    !!----
    Type, public :: Laue_Ref_List_Type
       integer :: nref
       Type(Laue_Ref_Type), dimension(:), allocatable :: LR
    End Type  Laue_Ref_List_Type

    !!----
    !!----   Type, public             :: Laue_Instrument_Type
    !!----       Character(len=80)    :: Info       ! Information about the instrument
    !!----       Character(len=80)    :: Name       ! Name of the instrument
    !!----       Character(len=4)     :: dtype      ! Rec or Cyl
    !!----       Character(len=3)     :: r_ord      ! xyz, yxz, ... Order of detector tilt calculations: xyz means Rx Ry Rz,
    !!----                                          ! so the rotation around z is the first applied, then around y and finally around x.
    !!----       real                 :: D          ! Distance (or radius) of the detector centre to the crystal
    !!----       real                 :: ga_d,nu_d  ! Angles gamma and nu of the detector centre
    !!----       real                 :: tiltx_d    ! Tilt of the flat detector around x-axis (normally = nu_d)
    !!----       real                 :: tilty_d    ! Tilt of the flat detector around y-axis (normally = 0)
    !!----       real                 :: tiltz_d    ! Tilt of the flat detector around z-axis (normally = 0)
    !!----       real                 :: H,V        ! Horizontal and Vertical dimension of detector in mm, or (if V=0) diameter of circular detector
    !!----       integer              :: np_h,np_v  ! Number of horizontal and vertical pixels
    !!----       real, dimension(3)   :: rOD        ! Vector position of the detector origin in the laboratory system
    !!----       real, dimension(3,3) :: RD         ! Rotation matrix giving the orientation of the D-frame w.r.t. the L-frame
    !!----                                             RD= Rx(tiltz_d) Ry(tilty_d) Rz(tiltz_d)  or with the permutation given by r_order
    !!----       logical              :: tilted     ! True if RD /= I
    !!----       logical              :: displaced  ! True if rOD /= (0,0,0)
    !!----       logical              :: flip_hor   ! True -> flip the image horizontally as soon as read
    !!----       logical              :: flip_ver   ! True -> flip the image vertically as soon as read
    !!----  information about limits of the most important variables
    !!----  needed to set the status of Laue reflections.
    !!----      real  :: L_min,L_max,L_central ! Lambda minimum , Lambda maximun, Central Lambda (monochromatic)
    !!----      real  :: gap_min,gap_max  ! gamma minimum , gamma  maximun  (Positive values) | This change is needed due to odd positions of
    !!----      real  :: gan_min,gan_max  ! gamma minimum , gamma  maximun  (Negative values) | flat detectors.
    !!----      real  :: nu_min,nu_max
    !!----      real  :: d_min         !Resolution limit ... if not given d_min= 2/L_min
    !!----      real  :: x_min,x_max   !in mm
    !!----      real  :: z_min,z_max
    !!----      real  :: xo,zo         !Origin of detector system in pixels. Normally (Np_h/2,Np_v/2)
    !!----      real, dimension(2) :: dism  !Displacement of the Origin of detector system w.r.t. (Np_h/2,Np_v/2) in mm
    !!----                                  !This is applied in calculating the position of reflections in mm or other characteristics
    !!----                                  !coming from input x,z coordinates in mm.
    !!----   End Type Laue_Instrument_Type
    !!----
    !!----
    !!----  Characteristics of a single Laue detector
    !!----
    !!----  Created: October 2009 (JRC)
    !!----  Updated: June 2010    (JRC & LFM)
    !!----
    Type, public             :: Laue_Instrument_Type
        Character(len=80)    :: Info="Default Laue Diffractometer as VIVALDI"            ! Information about the instrument
        Character(len=80)    :: Name="LAUE_DIFF "  ! Name of the instrument
        Character(len=3)     :: dtype="Cyl"        ! Rec or Cyl
        Character(len=3)     :: r_ord='xzy'        ! xyz, yxz, ... Order of detector tilt calculations: xyz means Rx Ry Rz,
                                                   ! so the rotation around z is the first applied, then around y and finally around x.
        Character(len=3)     :: invert='no'
        real                 :: D=159.155          ! Distance (or radius) of the detector centre to the crystal
        real                 :: ga_d=0.0,nu_d=0.0  ! Angles gamma and nu of the detector centre
        real                 :: tiltx_d=0.0        ! Tilt of the flat detector around x-axis (normally = nu_d)
        real                 :: tilty_d=0.0        ! Tilt of the flat detector around y-axis (normally = 0)
        real                 :: tiltz_d=0.0        ! Tilt of the flat detector around z-axis (normally = 0)
        real                 :: H=800.0,V=400.0    ! Horizontal and Vertical dimension of detector in mm
        integer              :: np_h,np_v          ! Number of horizontal and vertical pixels
        real, dimension(3)   :: rOD                ! Vector position of the detector origin in the laboratory system
        real, dimension(3,3) :: RD=reshape((/1.0,0.0,0.0,         & ! Rotation matrix giving the orientation of the D-frame w.r.t. the L-frame
                                             0.0,1.0,0.0,         &
                                             0.0,0.0,1.0/),(/3,3/))
        logical              :: tilted=.false.     ! True if RD /= I
        logical              :: displaced=.false.  ! True if rOD /= (0,0,0)
        logical              :: flip_hor=.false.   ! True -> flip the image horizontally as soon as read
        logical              :: flip_ver=.false.   ! True -> flip the image vertically as soon as read
        real  :: L_min,L_max,L_central      ! Lambda minimum , Lambda maximun, Central Lambda (used in quasi monochromatic case)
        real  :: gap_min,gap_max  ! gamma minimum , gamma  maximun  (Positive values) | This change is needed due to odd positions of
        real  :: gan_min,gan_max  ! gamma minimum , gamma  maximun  (Negative values) | flat detectors.
        real  :: nu_min,nu_max
        real  :: d_min         !Resolution limit ... if not given d_min= 2/L_min
        real  :: x_min,x_max   !in mm
        real  :: z_min,z_max
        real  :: xo,zo         !Origin of detector system in pixels. Normally (Np_h/2,Np_v/2)
        real, dimension(2) :: dism  !Displacement of the Origin of detector system w.r.t. (Np_h/2,Np_v/2) in mm
                                    !This is applied in calculating the position of reflections in mm or other characteristics
                                    !coming from input x,z coordinates in mm: dism=(/xo-Np_h/2,zo-Np_v/2)*conv to mm
    End Type Laue_Instrument_Type

    !!----
    !!----  Type, public          :: Laue_Composite_Detector_Type
    !!----      Character(len=3)  :: dtype      ! Sph, Cyl or Gen
    !!----      Character(len=3)  :: r_ord      ! xyz, yxz, ... Order of detector tilt calculations: xyz means Rx Ry Rz,
    !!----                                      ! so the rotation around z is the first applied, then around y and finally around x.
    !!----      integer           :: ndet       ! number of flat rectangular detectors
    !!----      real,dimension(16):: D          ! Distances (or radii) of the detector centre to the crystal
    !!----      real,dimension(16):: ga_d,nu_d  ! Angles gamma and nu of the detector centre
    !!----      real,dimension(16):: tiltx_d    ! Tilt of the flat detector around x-axis (normally = nu_d)
    !!----      real,dimension(16):: tilty_d    ! Tilt of the flat detector around y-axis (normally = 0)
    !!----      real,dimension(16):: tiltz_d    ! Tilt of the flat detector around z-axis (normally = 0)
    !!----      real,dimension(16):: H,V        ! Horizontal and Vertical dimension of detector in mm
    !!----  End Type Laue_Composite_Detector_Type
    !!----
    !!----  Multidetector formed by rectangular surfaces. Three types of geometry are described
    !!----  Spherical envelope:   All detectors are tangent to the surface of a sphere. The position
    !!----                        of the centre of each detector is described by spherical coordinates
    !!----                        R_d,ga_d,nu_d. In the ideal case R_d is the same for all detector
    !!----                        but they are described explicitly for a real detector system.
    !!----  Cylindrical envelope: All detectors are tangent to the surface of a cylinder with
    !!----                        their local z-axis parallel to the symmetry axis of the cylinder.
    !!----                        The position of the centre of each detector is described by
    !!----                        cylidrical coordinates in the laboratory system (Rho_d,ga_d,z_d)
    !!----                        In the ideal case Rho_d is the same for all detectors. In practice
    !!----                        The z_d coordinate is transformed on the fly to nu_d=Atan(Z_d/Rho_d)
    !!----
    !!----      For the above two geometries it is expected that all detectors are identical even
    !!----      if their horizontal and vertical dimensions are described individually
    !!----
    !!----  General geometry:     Each detector is described individually using spherical coordinates
    !!----                        for its centre and distance to the crystal, together with  the value
    !!----                        of tilt angles Tiltx_d,Tilty_d,Tiltz_d around the local x,y,z axes that
    !!----                        coincides with the laboratory system (except for the distance to the
    !!----                        origin) when the coordinates ga_d,nu_d are zero.
    !!----
    !!----       The tilt angles are zero for the spherical geometry and for the cylindrical geometry
    !!----       tiltx_d=nu_d. Small shifhts are expected in a real detector system
    !!----
    !!----      Created: February 2010
    !!----      Updated: February 2010
    !!----
    !!----
    Type, public                :: Laue_Composite_Detector_Type
        Character(len=80)       :: Info="Default Laue Diffractometer as CYCLOPS"            ! Information about the instrument
        Character(len=80)       :: Name="LAUE_DIFF "  ! Name of the instrument
        Character(len=3)        :: dtype="Cyl"        ! Sph, Cyl or Gen
        Character(len=3)        :: r_ord='xzy'        ! xyz, yxz, ... Order of detector tilt calculations: xyz means Rx Ry Rz,
                                                      ! so the rotation around z is the first applied, then around y and finally around x.
        integer                 :: ndet=16            ! number of flat rectangular detectors
        real,dimension(16)      :: D                  ! Distances (or radii) of the detector centre to the crystal
        real,dimension(16)      :: ga_d,nu_d          ! Angles gamma and nu of the detector centre
        real,dimension(16)      :: tiltx_d            ! Tilt of the flat detector around x-axis (normally = nu_d)
        real,dimension(16)      :: tilty_d            ! Tilt of the flat detector around y-axis (normally = 0)
        real,dimension(16)      :: tiltz_d            ! Tilt of the flat detector around z-axis (normally = 0)
        real,dimension(16)      :: H,V                ! Horizontal and Vertical dimension of detector in mm
        real, dimension(3,16)   :: rOD                ! Vector position of the detector origin in the laboratory system
        real, dimension(3,3,16) :: RD !=reshape((/1.0,0.0,0.0,  & ! Rotation matrix giving the orientation of the D-frame w.r.t. the L-frame
                                      !       0.0,1.0,0.0,      &
                                      !       0.0,0.0,1.0/),(/3,3/))

    End Type Laue_Composite_Detector_Type


    !!----    Type, public :: Laue_MultiDetector_Type
    !!----        integer                                              :: ndet       ! number of flat rectangular detectors
    !!----        Type(Laue_Instrument_Type),allocatable, dimension(:) :: Ldet
    !!----    End Type Laue_MultiDetector_Type
    !!----
    !!----    Type containing a number ndet of detectors and an array of Laue_Instrument_Type
    !!----    objects. It is useful for describing a set of flat rectangular detectors
    !!----
    !!----     Created: June 2010 (JRC)
    !!----
    Type, public :: Laue_MultiDetector_Type
        integer                                              :: ndet       ! number of flat rectangular detectors
        Type(Laue_Instrument_Type),allocatable, dimension(:) :: Ldet
    End Type Laue_MultiDetector_Type


    !!----
    !!---- The forthcoming types have been created for storing orienting conditions
    !!---- and orienting solutions  (JRC)
    !!----
    !!----    Orient_cond_type      : Conditions for the first orienting algorithm
    !!----    Orient_Solution_Type  : Contains the number of indexed spots, angles & UB-matrix
    !!----    Orient_Solution_List  : List of Orient_Solution_Type objects and pointer for ordering
    !!----    Refl_type             : Reflection indices, d-spacing and Cartesian coordinates in the L-system
    !!----    Refl_List_type        : List of Relf_type objects
    !!----
    !!----    Created: May 2010 (JRC)
    !!----    Updated: May 2010
    !!----
    Type, public         :: Orient_cond_type
        character(len=3) :: ang_Type="XYZ"    !Type of orienting angles: XYZ -> Phix, Phiy, Phiz;  EUL -> Omega,Chi,Phi
        logical          :: coprime=.false.   !Generate only co-prime indices if true.
        integer          :: nmax_ref=30       !Maximum number of reflections to for indexing in the list
        integer          :: nmax_sol=20       !Maximum number of solutions (provided by the user)
        integer          :: norient=4         !Maximun number of reflections to be considered as orienting
        integer          :: maxind=3          !Maximun Miller index for indexing nodal reflections
        integer          :: exhaust=0         !If different from zero perform an exhaustive search
        real             :: angtol=2.5        !Angular tolerance in degrees for comparing angles between reflections
        real             :: dmmtol=1.5        !Distance tolerance (in mm) for indexing spots
        real             :: ang_min=30.0      !Minimal angle between two orienting reflections
        real             :: ang_max=150.0     !Maximal angle between two orienting reflections
        logical          :: strict=.true.     !More than 3/4 of observed reflections must be indexed
    End Type Orient_cond_type

    Type, public :: Orient_Solution_Type
        integer              :: num_indx  ! Number of indexed spots in the list
        real                 :: rms       ! Root mean square of the solution
        real, dimension(3)   :: Ang       ! Three orienting angles ( Phix, Phiy, Phiz) or (Omega,Chi,Phi)
        real, dimension(3,3) :: UB        ! UB-matrix as defined by Busing-Levy
    End Type Orient_Solution_Type

    Type, public :: Orient_Solution_List
        integer                                               :: nsol
        integer,                    dimension(:), allocatable :: ind   !pointer for ordering the solutions
        Type(Orient_Solution_Type), dimension(:), allocatable :: Sol
    End Type Orient_Solution_List

    !Simplified reflection types for indexing purposes
    Type, public :: Refl_type
      integer, dimension(3) :: h
      real,    dimension(3) :: hc
      real                  :: ds
    End Type Refl_type

    Type, public :: Refl_List_type
      integer :: nref
      Type(Refl_type), dimension(:), allocatable :: hkl
    End Type Refl_List_type

    ! Types for twinning

    Type, public :: twin_type
      character(len=80)       :: Twin_name
      integer                 :: ityp
      integer                 :: n_twins
      real, dimension(3,3,48) :: Twin_Mat
      real, dimension(3,3,48) :: Twin_Matinv
      real, dimension(3,48)   :: Twin_axis
      real, dimension(48)     :: Twin_ang
    End Type twin_type

    !Type describing the shape, and orientation of the sample

    Type, public :: Sample_type
     Character(len=12)  :: shapen="Sphere" ! Shape name, e.g. sphere, ellipsoid, cylinder, ...
     real :: xsz=1.0,ysz=1.0,zsz=1.0       ! Sizes of the crystal along x,y,z in mm
     real :: eth=0.0,etk=0.0,etl=0.0       ! Mosaicity coefficients
     real :: ah=0.0, av=0.0                ! Horizontal and vertical divergence of the incident beam
     real :: phx=0.0,phy=0.0,phz=0.0       ! Rotation angles to put the inital shape to the position of the crystal
                                           ! when all motors are set to zero
     real,dimension(3,3):: tip_pos=0.0     !Coordinates of extreme points obtained from rotations + spindle
                                           !Allows calculating approximately the orientation of the spot ellipse in detector
    End Type Sample_type

    !Types for 2D-profile refinements

    Type, public :: Ren_Moffat_Profile_type
      real :: xp,zp        ! Position of the centre of the peak in mm (Calculated from cell parameters, etc ...)
      real :: eps          ! Angular shift with respect to the radial orientation of the pseudo-ellipse
      real :: dx,dz        ! Local position shift with respect to the centre (in mm)
      real :: a,sa,ta      ! variables for calculating the pseudo half-axes (A,B) of the pseudo-ellipse
      real :: b,sb,tb      ! A=a + sa * (x-xp) + ta (z-zp) ;  B=b + sb * (x-xp) + tb (z-zp)
      real :: ga,gb        ! Curtosis exponents (between ca. 0.5 and 1.5)
    End Type Ren_Moffat_Profile_type

    ! Type for Distortion of Lauegen/MOSFLM type

    Type, public :: Image_Distortion_Type
      real :: Tilt  =0.0 ! in units of 0.01 degrees
      real :: Twist =0.0 ! in units of 0.01 degrees
      real :: Bulge =0.0 ! in units of 0.01 degrees
      real :: Cross =0.0 ! spdxy
    End Type Image_Distortion_Type

    ! Offset variables and Chebychev coefficients have been moved to here in order to
    ! be introduced in Esmeralda and Laue_Simulator
    Type, public :: Offsets_Type
      Real :: Dist    = 0.0
      Real :: Tilt_x  = 0.0
      Real :: Tilt_y  = 0.0
      Real :: Tilt_z  = 0.0
      Real :: Gamma_c = 0.0
      Real :: Nu_c    = 0.0
      Real :: rOD_x   = 0.0
      Real :: rOD_y   = 0.0
      Real :: rOD_z   = 0.0
      Real :: orig_x  = 0.0
      Real :: orig_z  = 0.0
      Real :: Spindle = 0.0
      Real, dimension(3) :: Cell=0.0,Cellang=0.0 !New cell parameters
      Real, dimension(3) :: Orient=0.0           !New orientation angles (Phix,Phiy,Phiz of U-matrix)
      Type(Image_Distortion_Type) :: distort
    End type Offsets_Type

    Type(Offsets_type), public, save :: Off_set, sigma_Off_set

    integer, parameter,                  public :: N_Cheb_Max=20      !Maximum number of Chebychev coefficients
    integer,                       save, public :: Ncoeff=8           !Effective number of coefficients
    Real, dimension(N_Cheb_Max,2), save, public :: Cheb=0.0           !Chebychev coefficients for shifts in positions (x,z)
    Real, dimension(N_Cheb_Max,2), save, public :: sigma_Cheb=0.0     !Standard deviations of Chebychev coefficients
    Logical,                             public :: Cheb_Ref=.false.


 contains

    !!----
    !!---- Subroutine Init_Offsets(Off)
    !!----   Type(Offsets_type),  intent(in out) :: Off
    !!----
    !!---- Initialize the components of the Offsets_type object Off.
    !!----
    !!----  Created: September 2012  (JRC)
    !!----  Updated: September 2012
    !!----

    Subroutine Init_Offsets(Off)
      Type(Offsets_type),  intent(in out) :: Off
      Off%Dist    = 0.0
      Off%Tilt_x  = 0.0
      Off%Tilt_y  = 0.0
      Off%Tilt_z  = 0.0
      Off%Gamma_c = 0.0
      Off%Nu_c    = 0.0
      Off%rOD_x   = 0.0
      Off%rOD_y   = 0.0
      Off%rOD_z   = 0.0
      Off%orig_x  = 0.0
      Off%orig_z  = 0.0
      Off%Spindle = 0.0
      Off%Orient  = 0.0
      Off%distort%Tilt   =0.0
      Off%distort%Twist  =0.0
      Off%distort%Bulge  =0.0
      Off%distort%Cross  =0.0
      return
    End Subroutine Init_Offsets

    !!----
    !!---- Subroutine Init_Laue_Instrm(LaueDiff)
    !!----   Type(Laue_Instrument_Type),  intent(in out) :: LaueDiff
    !!----
    !!---- Initialize the components of the Laue_Instrument_Type object LaueDiff.
    !!----
    !!----  Created: October 2009
    !!----  Updated: June 2010
    !!----

    Subroutine Init_Laue_Instrm(LaueDiff)
       Type(Laue_Instrument_Type),  intent(in out) :: LaueDiff
        LaueDiff%Info="Default Laue Diffractometer as VIVALDI"
        LaueDiff%Name="LAUE_DIFF"
        LaueDiff%dtype="Cyl"
        LaueDiff%r_ord='xzy'
        LaueDiff%D=159.155
        LaueDiff%ga_d=0.0
        LaueDiff%nu_d=0.0
        LaueDiff%tiltx_d=0.0
        LaueDiff%tilty_d=0.0
        LaueDiff%tiltz_d=0.0
        LaueDiff%H=800.0
        LaueDiff%V=400.0
        LaueDiff%np_h=4000
        LaueDiff%np_v=2000
        LaueDiff%rOD=(/0.0,0.0,0.0/)
        LaueDiff%RD=reshape((/1.0,0.0,0.0,  &
                              0.0,1.0,0.0,  &
                              0.0,0.0,1.0/),(/3,3/))
        LaueDiff%L_min=0.7
        LaueDiff%L_max=4.0
        LaueDiff%d_min=1.0

        LaueDiff%gap_max = (0.5*LaueDiff%H/LaueDiff%D)*to_deg
        LaueDiff%gap_min = 0.0
        LaueDiff%gan_max = 0.0
        LaueDiff%gan_min = -LaueDiff%gap_max

        LaueDiff%x_max  = 0.5*LaueDiff%H
        LaueDiff%x_min  = -LaueDiff%x_max
        LaueDiff%nu_max = atan2d(0.5*LaueDiff%V,LaueDiff%D)
        LaueDiff%nu_min = -LaueDiff%nu_max
        LaueDiff%z_max  =  0.5*LaueDiff%V
        LaueDiff%z_min  =  - LaueDiff%z_max
        LaueDiff%xo     =   0.5*real(LaueDiff%np_h)
        LaueDiff%zo     =   0.5*real(LaueDiff%np_v)
        LaueDiff%dism   =  0.0
        LaueDiff%tilted=.false.     ! True if RD /= I
        LaueDiff%displaced=.false.  ! True if rOD /= (0,0,0)
        LaueDiff%flip_hor=.false.   ! True -> flip the image horizontally as soon as read
        LaueDiff%flip_ver=.false.   ! True -> flip the image vertically as soon as read
        return
    End Subroutine Init_Laue_Instrm

    !!----
    !!---- Elemental Subroutine Init_Laue_Ref(Ref)
    !!----   type(Laue_Ref_Type), intent (in out) :: Ref
    !!----
    !!----  Initialize the components of the Laue_Ref_Type object Ref.
    !!----
    !!----  Created: October 2009
    !!----  Updated: January 2010
    !!----
    Elemental Subroutine Init_Laue_Ref(Ref)
      type(Laue_Ref_Type), intent (in out) :: Ref
        Ref%stats   = 0
        Ref%mult    = 0
        Ref%nodal   = 0
        Ref%ds      = 0.0
        Ref%gamma   = 0.0
        Ref%nu      = 0.0
        Ref%Ttheta  = 0.0
        Ref%Lambda  = 0.0
        Ref%h       = 0.0
        Ref%zv      = 0.0
        Ref%Obs_int = 0.0
        Ref%Cal_int = 0.0
        Ref%Sigma   = 0.0
        Ref%x       = 0.0
        Ref%z       = 0.0
        Ref%stheta  = 0.0
        Ref%sphi    = 0.0
        Ref%kindex  = 0
        return
    End subroutine Init_Laue_Ref

    !!----
    !!---- Subroutine Allocate_Laue_Ref_list(L,n)
    !!----   type(Laue_Ref_List_Type), intent (in out) :: L
    !!----   integer,                  intent (in)     :: n
    !!----
    !!---- Allocate the components of the Laue_Ref_List_Type object L.
    !!----
    !!----  Created: October 2009
    !!----  Updated: November 2009
    !!----
    Subroutine Allocate_Laue_Ref_list(L,n)
      type(Laue_Ref_List_Type), intent (in out) :: L
      integer,                  intent (in)     :: n
      integer :: i

      if (allocated(L%LR)) deallocate (L%LR)
      allocate(L%LR(n))
      L%nref=n
      do i=1,n
        L%LR(i)%stats   = 0
        L%LR(i)%mult    = 0
        L%LR(i)%nodal   = 0
        L%LR(i)%ds      = 0.0
        L%LR(i)%gamma   = 0.0
        L%LR(i)%nu      = 0.0
        L%LR(i)%Ttheta  = 0.0
        L%LR(i)%Lambda  = 0.0
        L%LR(i)%h       = 0.0
        L%LR(i)%zv      = 0.0
        L%LR(i)%Obs_int = 0.0
        L%LR(i)%Cal_int = 0.0
        L%LR(i)%Sigma   = 0.0
        L%LR(i)%x       = 0.0
        L%LR(i)%z       = 0.0
        L%LR(i)%stheta  = 0.0
        L%LR(i)%sphi    = 0.0
        L%LR(i)%kindex  = 0
      end do
    End Subroutine Allocate_Laue_Ref_list

    !!----
    !!----  Subroutine Calc_RD_rOD(LaueDiff)
    !!----    Type(Laue_Instrument_Type), intent(in out)  :: LaueDiff
    !!----
    !!----  Subroutine calculating the rotation matrix LaueDiff%RD
    !!----  and the translation vector LaueDiff%rOD (in Cartesian coordinates)
    !!----  to position a flat rectangular detector in an arbitrary position in
    !!----  the L-system.
    !!----  It is supposed that the angular components of the tilt angles of
    !!----  the detector, the distance from the sample to detector and the
    !!----  polar angles of the detector centre are already contained in
    !!----  LaueDiff components.
    !!----
    !!----  Created: June 2010 (JRC)
    !!----  Updated: June 2010
    !!----
    Subroutine Calc_RD_rOD(LaueDiff)
      Type(Laue_Instrument_Type), intent(in out)  :: LaueDiff
      !--- Local variables ---!
      real, dimension(3,3) :: Rx,Ry,Rz
      Rx=Matrix_Rx(LaueDiff%tiltx_d,"D")
      Ry=Matrix_Ry(LaueDiff%tilty_d,"D")
      Rz=Matrix_Rz(LaueDiff%tiltz_d,"D")
      Select case(LaueDiff%r_ord)
         case('xyz','XYZ')
           LaueDiff%RD=Matmul(Rx,matmul(Ry,Rz))
         case('xzy','XZY')
           LaueDiff%RD=Matmul(Rx,matmul(Rz,Ry))
         case('zxy','ZXY')
           LaueDiff%RD=Matmul(Rz,matmul(Rx,Ry))
         case('zyx','ZYX')
           LaueDiff%RD=Matmul(Rz,matmul(Ry,Rx))
         case('yxz','YXZ')
           LaueDiff%RD=Matmul(Ry,matmul(Rx,Rz))
         case('yzx','YZX')
           LaueDiff%RD=Matmul(Ry,matmul(Rz,Rx))
      End Select
      if(LaueDiff%dtype == "Rec") then
       ! For cylindrical detectors LaueDiff%rOD is directly read in the instrument file
        LaueDiff%rOD=(/sind(LaueDiff%ga_d)*cosd(LaueDiff%nu_d),cosd(LaueDiff%ga_d)*cosd(LaueDiff%nu_d),sind(LaueDiff%nu_d)/)
        LaueDiff%rOD=LaueDiff%D*LaueDiff%rOD
      end if
      return
    End Subroutine Calc_RD_rOD

    !!----
    !!---- Subroutine Get_Matrix_moving_v_to_u(v,u,R)
    !!----   real, dimension(3),  intent(in)  :: v,u
    !!----   real, dimension(3,3),intent(out) :: R
    !!----
    !!----   Subroutine to get the orthogonal matrix that rotates a vector v
    !!----   to orient it along the vector u. Makes use of Cross_Product and
    !!----   Rot_matrix (Gibbs matrix)
    !!----
    !!----    Created: February 2010 (JRC)
    !!----    Updated: November 2011 (JRC)
    !!----
    !!
    Subroutine Get_Matrix_moving_v_to_u(v,u,R)
      real, dimension(3),  intent(in)  :: v,u
      real, dimension(3,3),intent(out) :: R
      !--- Local variables ---!
      integer                        :: i,iu,iv
      real, parameter                :: ep=1.0e-5
      real                           :: mv,mu,mvu,phi,c
      logical                        :: co_linear
      real, dimension(3)             :: vu
      integer, dimension(1)          :: im
      real, parameter, dimension(3,3):: ident=reshape((/1.0,0.0,0.0, &
                                                        0.0,1.0,0.0, &
                                                        0.0,0.0,1.0/),(/3,3/))

      !First determine if the two input vectors are co-linear
      im=maxloc(abs(v))
      iv=im(1)
      im=maxloc(abs(u))
      iu=im(1)
      co_linear=.true.
      if(iu == iv) then ! may be co-linear
        if(abs(u(iu)) > ep) then
          c=v(iv)/u(iu)
          do i=1,3
            if(abs( v(i)-c*u(i) ) > ep ) then
               co_linear=.false.
               exit
            end if
          end do
        end if
      else
        co_linear=.false.
      end if
      if(co_linear) then
        mvu=v(iv)*u(iu)
        if(mvu < 0.0) then   !opposed vectors
          R=-ident
        else                 !parallel vectors
          R=ident
        end if
      else
        ! non co-linear
        vu=Cross_Product(v,u)      !Rotation axis
        mv=sqrt(dot_product(v,v))
        mu=sqrt(dot_product(u,u))
        mvu=sqrt(dot_product(vu,vu))
        phi=mvu/(mv*mu)
        if(phi >= 1.0) then
          phi = 90.0
        else if(phi <= -1.0) then
          phi = -90.0
        else
          phi=asind(phi)      !Angle between the two input vectors
        end if
        R=Rot_matrix(vu,phi)  !Gibbs matrix
      end if
      return
    End Subroutine Get_Matrix_moving_v_to_u

    !!----
    !!---- Subroutine Get_gam_nu_from_xz(LaueDiff,x,z,gamma,nu,ok)
    !!---- Type(Laue_Instrument_Type), intent(in)  :: LaueDiff  !Instrument
    !!---- Real,                       intent(in)  :: x,z       !input coordinates in mm
    !!---- Real,                       intent(out) :: gamma,nu  !output angles of diffracted beam
    !!---- logical, optional,          intent(out) :: ok
    !!----
    !!----   Subroutine to get the the polar angles of the diffracted beam from the values
    !!----   of x and z coordinates (in mm) of the spot in a 2D detector
    !!----
    !!----    Created: April 2010 (JRC)
    !!----    Updated: June 2010
    !!----
    !!
    Subroutine Get_gam_nu_from_xz(LaueDiff,x,z,gamma,nu,ok,nolim)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      Real,                       intent(in)  :: x,z
      Real,                       intent(out) :: gamma,nu
      logical, optional,          intent(out) :: ok
      character(len=*), optional, intent(in)  :: nolim

      real, dimension(3) :: xl,xd
      real               :: xovR,xc,zc

      !Correct the position w.r.t. the displacement of the origin w.r.t. the centre of the detector
      xc = x + LaueDiff%dism(1)
      zc = z + LaueDiff%dism(2)

      if(.not. present(nolim)) then
        if(xc < LaueDiff%x_min .or. xc > LaueDiff%x_max .or. zc < LaueDiff%z_min  .or. zc > LaueDiff%z_max) then
           if(present(ok)) ok=.false.
           gamma=0.0
           nu=0.0
           return
        end if
      end if
      !The calculation below assumes the coordinates are given w.r.t. the centre of detector
      if(present (ok)) ok=.true.

      Select Case(LaueDiff%dtype)
        Case("Rec")
          xd=(/xc,0.0,zc/)
        Case("Cyl")
          xovR=xc/LaueDiff%D
          xd=(/LaueDiff%D*sin(xovR),LaueDiff%D*cos(xovR),zc/) !Vector position of the spot in the Cyl system
      End Select

      xl=matmul(LaueDiff%RD,xd)
      xl=xl+LaueDiff%rOD
      xl=xl/sqrt(dot_product(xl,xl))    ! = u=(/ sind(gamma)*cosd(nu), cosd(gamma)*cosd(nu),sind(nu)/)
      nu=asind(xl(3))
      gamma=atan2d(xl(1),xl(2))

      Return
    End Subroutine Get_gam_nu_from_xz

    !!----
    !!---- Subroutine Get_xy_Stereo_Proy_from_xz(LaueDiff,X,Z,Xpr,Ypr)
    !!----   Type(Laue_Instrument_Type), intent(in)  :: LaueDiff  !Instrument
    !!----   Real,                       intent(in)  :: X,Z       !input coordinates in mm
    !!----   Real,                       intent(out) :: Xpr,Ypr   !Stereographic Projection coordinates
    !!----
    !!----   Subroutine to get the screen coordinates of the stereographic projection from
    !!----   the spot coordinates in the detector (in mm). Uses the subroutine Get_gam_nu_from_xz
    !!----   for obtaining gamma and nu of the diffracted beam. The unitary vector along the diffracted
    !!----   beam is calculated in the standard (BL) system and from that the Xpr, Ypr coordinates
    !!----   are calculated in a system having Z in the direction oposite to Y(BL) and Y along Z(BL)
    !!----   maintaining the same orientation for X.
    !!----
    !!----    Created: April 2010 (LFM)
    !!----    Updated: April 2010
    !!----
    !!

    Subroutine Get_xy_Stereo_Proy_from_xz(LaueDiff,X,Z,Xpr,Ypr)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      Real,                       intent(in)  :: X,Z
      Real,                       intent(out) :: Xpr,Ypr

      Real               :: d_th, azm, proy_d
      Real, dimension(3) :: u
      logical            :: ok

      call Get_Scattered_wavevector_from_xz(LaueDiff,X,Z,u,ok)
      if( .not. ok) then
       Xpr=0.0;  Ypr=0.0
       return
      end if
      d_th=acos(u(2))
      azm=atan2(u(3),u(1))
      proy_d=tan((pi-d_th)*0.25)
      Ypr=proy_d*sin(azm)
      Xpr=proy_d*cos(azm)

    return
    End Subroutine Get_xy_Stereo_Proy_from_xz

    !!----Subroutine Get_Scattered_wavevector_from_xz(LaueDiff,X,Z,u,ok)
    !!----  Type(Laue_Instrument_Type), intent(in)    :: LaueDiff     !Instrument
    !!----  Real,                       intent(in)    :: X,Z          !input coordinates in mm
    !!----  Real, dimension(3),         intent(out)   :: u            !scattering vector
    !!----  logical,                    intent(out)   :: ok
    !!----  Subroutine to get the unitary vector of the scattered vave vector
    !!----  of a particular spot in the laboratory system
    !!----  Created: Oct 2011 (LFM)
    !!----
    !!
    Subroutine Get_Scattered_wavevector_from_xz(LaueDiff,X,Z,u,ok)
      Type(Laue_Instrument_Type), intent(in)    :: LaueDiff     !Instrument
      Real,                       intent(in)    :: X,Z          !input coordinates in mm
      Real, dimension(3),         intent(out)   :: u            !scattering vector
      logical, optional,          intent(out)   :: ok

      Real               :: gamma, nu
      Call Get_gam_nu_from_xz(LaueDiff,X,Z,gamma,nu,ok)
      if( .not. ok) then
        u=0.0
        return
      end if
      u=(/ sind(gamma)*cosd(nu), cosd(gamma)*cosd(nu),sind(nu)/)
    return
    End Subroutine Get_Scattered_wavevector_from_xz

    !!----Subroutine Get_Reciprocal_vector_from_Scattered_wavevector(u,r)
    !!----  Real, dimension(3),         intent(in)    :: u            !Scattered wave vector
    !!----  Real, dimension(3),         intent(out)   :: r            !Reciprocal vector
    !!----  Subroutine to get the unitary vector of the Reciprocal vector
    !!----  from Scattered wave vector in the laboratory system
    !!----  Scattered wave vector must be of length 1.0
    !!----  Created: Oct 2011 (LFM)
    !!----
    !!
    Subroutine Get_Reciprocal_vector_from_Scattered_wavevector(u,r)
      Real, dimension(3),         intent(in)    :: u            !Scattered wave vector
      Real, dimension(3),         intent(out)   :: r            !Reciprocal vector

      r=u-(/0.0, 1.0, 0.0 /)
      r=r/sqrt(dot_product(r,r))

    return
    End Subroutine Get_Reciprocal_vector_from_Scattered_wavevector

    !!----
    !!---- Subroutine Get_u_R_from_xzhkl(LaueDiff,x,z,B,h,u,R,ok)
    !!---- Type(Laue_Instrument_Type), intent(in)  :: LaueDiff  !Instrument
    !!---- Real,                       intent(in)  :: x,z       !input coordinates in mm
    !!---- Real, dimension(3,3),       intent(in)  :: B         !Busing-Levy B-matrix
    !!---- Real, dimension(3),         intent(in)  :: h         !input hkl indices
    !!---- Real, dimension(3),         intent(out) :: u         !unitary vector of the scattering vector in the L-system giving x,z spot
    !!---- Real, dimension(3,3),       intent(out) :: R         !Rotation matrix to be applied to the standard orientation
    !!---- logical,                    intent(out) :: ok         to put h along the scattering vector (u) corresponding to x,z spot
    !!----
    !!----   Subroutine to get the unitary vector of the scattering vector of a particular spot
    !!----   in the laboratory system and the rotation matrix to put the reflection h=(h,k,l)
    !!----   from the standard orientation along the scattering vector corresponding to the
    !!----   given spot.
    !!----
    !!----    Created: February 2010 (JRC)
    !!----    Updated: February 2010
    !!----
    !!
    Subroutine Get_u_R_from_xzhkl(LaueDiff,x,z,B,h,u,R,ok)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      Real,                       intent(in)  :: x,z
      Real, dimension(3,3),       intent(in)  :: B
      Real, dimension(3),         intent(in)  :: h
      Real, dimension(3),         intent(out) :: u
      Real, dimension(3,3),       intent(out) :: R
      logical,                    intent(out) :: ok
      ! --- Local variables ---!
      real, dimension(3)   :: hc
      call Get_Scattered_wavevector_from_xz(LaueDiff,x,z,u,ok)
      if( .not. ok) then
       u=0.0
       R=0.0
       return
      end if
      hc=matmul(B,h)
      call Get_Matrix_moving_v_to_u(hc,u,R)
      return
    End Subroutine Get_u_R_from_xzhkl

    !!----
    !!---- Subroutine Get_Rotations_Vc_VLab(axis,UB,Vc,VL,Phi_angs,dirl,R)
    !!---- Character(len=1),           intent(in)  :: axis      ! ='x' or 'y' (second rotation angle
    !!---- Real, dimension(3,3),       intent(in)  :: UB        !Busing-Levy UB-matrix
    !!---- Real, dimension(3),         intent(in)  :: Vc        !Input hkl or uvw indices
    !!---- Real, dimension(3),         intent(in)  :: VL        !Vector in the Laboratory system along which the vector Vc should be oriented
    !!---- Real, dimension(3),         intent(out) :: Phi_angs  !Rotation angles (phi_z3,phi_y2,phi_z1) to be applied to the crystal
    !!----                                                      !in order to orient Vc // VL
    !!---- Character(len=*), optional, intent(in)  :: dirl      !if given the vector components are referred to the direct lattice
    !!----                                                      !otherwise is referred to the reciprocal basis
    !!---- Real,dimension(3,3),optional,intent(out):: R         !Rotation matrix performing the operation
    !!----
    !!----  Subroutine to get the angles of three clockwise rotations along the z, x/y and z axes in order
    !!----  to align the crystallographic direction Vc (given in the direct lattice if "dirl" is present)
    !!----  along the vector VL given in the laboratory system. It is supposed that the orienting device
    !!----  is initially set having its horizontal rotation axis along xL/yL and its vertical rotation
    !!----  axis along zL. The rotations to orient Vc along VL should be applied in the following
    !!----  order: First Rz(Phi_angs(3)), second Rx(Phi_angs(2)) and finally  Rz(Phi_angs(1))
    !!----
    !!----    Created: February 2010  (JRC)
    !!----    Updated: February 2010   (Still to be tested properly)
    !!----
    Subroutine Get_Rotations_Vc_VLab(axis,UB,Vc,VL,Phi_angs,dirl,R)
    Character(len=1),           intent(in)  :: axis      ! ='x' or 'y' (second rotation angle
    Real, dimension(3,3),       intent(in)  :: UB        !Busing-Levy UB-matrix
    Real, dimension(3),         intent(in)  :: Vc        !Input hkl or uvw indices
    Real, dimension(3),         intent(in)  :: VL        !Vector in the Laboratory system along which the vector Vc should be oriented
    Real, dimension(3),         intent(out) :: Phi_angs  !Rotation angles (phi_z,phi_x/y,phi_z) to be applied to the crystal
                                                         !in order to orient Vc // VL
    Character(len=*), optional, intent(in)  :: dirl      !if given the vector components are referred to the direct lattice
                                                         !otherwise is referred to the reciprocal basis
    Real,dimension(3,3),optional,intent(out):: R         !Rotation matrix performing the operation

      ! --- Local variables ---!
      real                 :: utheta, uphi, htheta, hphi, mu
      real, dimension(3)   :: hr,hL
      real, dimension(3,3) :: GD, Rx, Rza, Rzb


      if(present(dirl)) then         !Transform the direct lattice indices to a reciprocal direction
        GD=matmul(transpose(UB),UB)  !Reciprocal metric tensor (provisionally stored in GD)
        GD=Invert_A(GD)              !Direct metric tensor is the inverse of the reciprocal metric tensor
        hr=matmul(GD,Vc)             !Components of Vc w.r.t. reciprocal lattice basis
      else
        hr=Vc                        !Components of Vc w.r.t. reciprocal lattice basis
      end if
      hL=matmul(UB,hr)               !Laboratory components of Vc
      call Get_Spheric_Coord(VL,mu,utheta,uphi,"D") !spherical components of VL
      if(uphi < 0.0)  uphi=uphi+360.0
      call Get_Spheric_Coord(hL,mu,htheta,hphi,"D") !spherical components of Vc
      if(hphi < 0.0)  hphi=hphi+360.0

      if(axis == 'x' .or. axis == 'X') then
        Phi_angs(3)= 90.0-hphi     !The correponding matrix puts hc in the zy plane hc' (Matrix_R -> anti-clockwise)
        Phi_angs(2)= htheta-utheta !The correponding matrix moves hc' to the same theta angle as u in the zy-plane: hc"
        Phi_angs(1)= uphi-90.0     !The correponding matrix puts hc" in the same direction as u
      else
        Phi_angs(3)=-hphi          !The correponding matrix puts hc in the zx plane hc' (Matrix_R -> anti-clockwise)
        Phi_angs(2)= utheta-htheta !The correponding matrix moves hc' to the same theta angle as u in the zx-plane: hc"
        Phi_angs(1)= uphi          !The correponding matrix puts hc" in the same direction as u
      end if
      if(present(R)) then
        Rza= Matrix_Rz(Phi_angs(3),"D")
        if(axis == 'x' .or. axis == 'X') then
           Rx = Matrix_Rx(Phi_angs(2),"D")
        else
           Rx = Matrix_Ry(Phi_angs(2),"D")
        end if
        Rzb= Matrix_Rz(Phi_angs(1),"D")
        R =Matmul(Rzb,matmul(Rx,Rza))
      end if
      return

    End Subroutine Get_Rotations_Vc_VLab

    !!----
    !!----  subroutine Print_Laue_Ref_list(L,ind,ipr)
    !!----    type(Laue_Ref_List_Type),       intent(in) :: L
    !!----    integer, dimension(:),optional, intent(in) :: ind
    !!----    integer,              optional, intent(in) :: ipr
    !!----
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated: November 2010
    !!----
    Subroutine Print_Laue_Ref_list(LaueDiff,SpG,L,ind,ipr)
      Type(Laue_Instrument_Type),     intent(in)     :: LaueDiff
      type(Space_Group_Type),         intent(in)     :: SpG
      type(Laue_Ref_List_Type),       intent(in out) :: L
      integer, dimension(:),optional, intent(in)     :: ind
      integer,              optional, intent(in)     :: ipr
      ! Local variables
      character(len=40) :: comment
      integer :: i,j,lun
      lun=6
      if(present(ipr)) lun=ipr
      write(unit=lun,fmt="(/,/,a)") "   => List of Laue reflections:"
      if(present(ind)) then
        write(unit=lun,fmt="(/,a)")   &
        "   h   k   l   Mul   Nod   Lambda    D-spc    Gamma     Nu     2Theta    X(mm)    Z(mm)"// &
        "       Hx       Hy       Hz               F2           sTheta      sPhi"
        do i=1,L%nref
          j=ind(i)
          Call Laue_Mult(LaueDiff,SpG,L%LR(j))
          write(unit=lun,fmt="(3i4,2i6,2f9.5,3f9.3,2f9.2,a,3f9.5,a,f12.4,2f14.3)") &
                               nint(L%LR(j)%h), L%LR(j)%mult, L%LR(j)%nodal, L%LR(j)%Lambda, &
                               L%LR(j)%ds, L%LR(j)%gamma, L%LR(j)%nu, L%LR(j)%Ttheta,        &
                               L%LR(j)%x,  L%LR(j)%z,"  (",L%LR(j)%zv," )  ",L%LR(j)%Cal_Int,&
                               L%LR(j)%sTheta,L%LR(j)%sPhi
        end do
      else
        write(unit=lun,fmt="(/,a)")   &
        "   h   k   l   Mul   Nod   Lambda    D-spc    Gamma     Nu     2Theta       Hx       Hy       Hz         Status"
        do i=1,L%nref
          Select Case(L%LR(i)%stats)
            case(-3)
              comment=" Oposite side of Ewald spheres "
            case(-2)
              comment=" Outside the given resolution "
            case(-1)
              comment=" Outside Ewald spheres "
            case( 0)
              comment="  "
              Call Laue_Mult(LaueDiff,SpG,L%LR(i))
            case( 1)
              comment=" Outside Nu-limits "
            case( 2)
              comment=" Outside Gamma-limits "
            case( 3)
              comment=" Outside detector area "
            case( 4)
              comment=" Non-indexed experimental peak "
            case default
              comment=" This comment should not be here! "
          End Select

          write(unit=lun,fmt="(3i4,2i6,2f9.5,3f9.3,a,3f9.5,a)") &
                               nint(L%LR(i)%h), L%LR(i)%mult, L%LR(i)%nodal, L%LR(i)%Lambda, &
                               L%LR(i)%ds, L%LR(i)%gamma, L%LR(i)%nu, L%LR(i)%Ttheta,        &
                               "  (",L%LR(i)%zv," )  <-- "//trim(comment)
        end do
      end if
    End Subroutine Print_Laue_Ref_list

    !!----
    !!----  Subroutine Calc_Laue_Imagen(LaueDiff,L,sample,Datam)
    !!----    Type(Laue_Instrument_Type), intent(in) :: LaueDiff
    !!----    Type(Laue_Ref_List_Type),   intent(in) :: L
    !!----    real, dimension(3),         intent(in) :: sample
    !!----    Integer, dimension(:,:),    intent(out):: Datam
    !!----
    !!----  Calculate a Laue image according to the instrument LaueDiff and the list of reflections L.
    !!----  sample=(/size,mosaic,divergence/) helps to calculate profile parameters.
    !!----  The complete image is calculated into the array Datam.
    !!----  (To be improved progressively)
    !!----
    !!----    Created: August 2010 (JRC)
    !!----    Updated: August 2010 (JRC)
    !!----
    Subroutine Calc_Laue_Imagen(LaueDiff,L,sample,Datam)
      Type(Laue_Instrument_Type), intent(in) :: LaueDiff
      Type(Laue_Ref_List_Type),   intent(in) :: L
      Type(Sample_Type),          intent(in) :: sample
      Integer, dimension(:,:), allocatable,    intent(out):: Datam
      ! Local variables
      Type(Ren_Moffat_Profile_type)   :: p
      real, dimension(:), allocatable :: prof
      integer :: i,j,k,j1,j2,k1,k2,npix
      real    :: sx,sz,x,z, mosaic, delta,suma
      real, dimension(3) :: h,eta

      !Fixing values of P (Gaussian Profile in practice)
      p%eps=0.0;  p%dx=0.0; p%dz=0.0; p%sa=0.0;p%ta=0.0; p%sb=0.0; p%tb=0.0; p%ga=1.0; p%gb=1.0
      sx=(LaueDiff%x_max -LaueDiff%x_min)/real(LaueDiff%Np_h-1)
      sz=(LaueDiff%z_max -LaueDiff%z_min)/real(LaueDiff%Np_v-1)
      if(allocated(Datam)) deallocate(Datam)
      allocate(Datam(LaueDiff%Np_v,LaueDiff%Np_h))
      Datam=11
      do i=1,L%nref
           !Determine the pixels in which the contribution of the reflections is significant
           p%xp=L%LR(i)%x;  p%zp=L%LR(i)%z
           mosaic=sample%etl*(L%LR(i)%h(3) * L%LR(i)%ds)  !anisotropy w.r.t. c-axis
           p%a=sample%xsz*(1.0+abs(sind(L%LR(i)%nu)) + tand(sample%ah) )
           p%b=sample%xsz*(1.0+abs(sind(L%LR(i)%nu)) + tand(sample%ah) + tan(mosaic))
           delta= 4.0 * max(p%a,p%b)
           j1=max(nint((p%xp-delta-LaueDiff%x_min)/sx)+1,1)
           j2=min(nint((p%xp+delta-LaueDiff%x_min)/sx)+1,LaueDiff%Np_h)
           k1=max(nint((p%zp-delta-LaueDiff%z_min)/sz)+1,1)
           k2=min(nint((p%zp+delta-LaueDiff%z_min)/sz)+1,LaueDiff%Np_v)
           suma=0.0 ; npix=(k2-k1+1)*(j2-j1+1)
           if(allocated(prof)) deallocate(prof)
           allocate(prof(npix))
           npix=0
           do j=j1,j2     !Integral of the profile
             x= LaueDiff%x_min +real(j-1)*sx
             do k=k1,k2
               z= LaueDiff%z_min +real(k-1)*sz
               npix=npix+1
               prof(npix)=Ren_Moffat(x,z,P)
               suma=suma+prof(npix)
             end do
           end do
           prof(1:npix)=prof(1:npix)/suma   !Normalised profile
           npix=0
           do j=j1,j2
             do k=k1,k2
               npix=npix+1
               datam(k,j) = datam(k,j) + L%LR(i)%Cal_Int*prof(npix)
             end do
           end do
      end do
      return
    End Subroutine Calc_Laue_Imagen


    Subroutine Calc_Laue_Image(LaueDiff,L,Spindle,sample,Datam)
      Type(Laue_Instrument_Type), intent(in) :: LaueDiff
      Type(Laue_Ref_List_Type),   intent(in) :: L
      real,                       intent(in) :: Spindle
      Type(Sample_Type),          intent(in) :: sample
      real, dimension(:,:), allocatable, intent(out):: Datam
      ! Local variables
      real, dimension(4) :: s
      real, dimension(3) :: ellipse
      real, dimension(:), allocatable :: prof
      integer :: i,j,k,j1,j2,k1,k2,npix
      real    :: sx,sz, mosaic, delta,suma, f
      real, dimension(2) :: x,xo


      sx=(LaueDiff%x_max -LaueDiff%x_min)/real(LaueDiff%Np_h-1)
      sz=(LaueDiff%z_max -LaueDiff%z_min)/real(LaueDiff%Np_v-1)
      if(allocated(Datam)) deallocate(Datam)
      allocate(Datam(LaueDiff%Np_v,LaueDiff%Np_h))
      Datam=11
      f=10.0
      do i=1,L%nref
           !Determine the pixels in which the contribution of the reflections is significant
           Call Calc_Ellipse_Params(LaueDiff, Sample, Spindle, L%LR(i), ellipse)
           xo(2)=L%LR(i)%x;  xo(1)=L%LR(i)%z
           delta= 4.0 * max(ellipse(1),ellipse(2))
           j1=max(nint((xo(2)-delta-LaueDiff%x_min)/sx)+1,1)
           j2=min(nint((xo(2)+delta-LaueDiff%x_min)/sx)+1,LaueDiff%Np_h)
           k1=max(nint((xo(1)-delta-LaueDiff%z_min)/sz)+1,1)
           k2=min(nint((xo(1)+delta-LaueDiff%z_min)/sz)+1,LaueDiff%Np_v)
           suma=0.0 ; npix=(k2-k1+1)*(j2-j1+1)
           if(allocated(prof)) deallocate(prof)
           allocate(prof(npix))
           s=(/ellipse(1),ellipse(2),ellipse(3),f/)
           npix=0
           do j=j1,j2     !Integral of the profile
             x(2)= LaueDiff%x_min +real(j-1)*sx
             do k=k1,k2
               x(1)= LaueDiff%z_min +real(k-1)*sz
               npix=npix+1
               prof(npix)=Gaussian_2Da(x,xo,s)
               suma=suma+prof(npix)
             end do
           end do
           prof(1:npix)=prof(1:npix)/suma   !Normalised profile
           npix=0
           do j=j1,j2
             do k=k1,k2
               npix=npix+1
               datam(k,j) = datam(k,j) + L%LR(i)%Cal_Int*prof(npix)
             end do
           end do
      end do
      f=maxval(datam)
      datam=(65000.0/f)*datam
      return
    End Subroutine Calc_Laue_Image

    Subroutine Set_Sample(smpl)
      Type(Sample_Type), intent(in out) :: smpl
      real, dimension(3)   :: ang
      real, dimension(3,3) :: Rot,pos
      ang=(/smpl%phx, smpl%phy, smpl%phz/)
      call Set_Rotation_Matrix(ang,Rot)
      pos=reshape((/smpl%xsz,0.0,0.0, &
                   0.0,smpl%ysz,0.0, &
                   0.0,0.0,smpl%zsz/),(/3,3/))
      smpl%tip_pos=matmul(Rot,0.5*pos)
      return
    End Subroutine Set_Sample

    Subroutine Calc_Ellipse_Params(LaueD, smpl, Spindle, R, ellip)
      Type(Laue_Instrument_Type),intent(in)     :: LaueD
      Type(Sample_Type),         intent(in)     :: smpl
      real,                      intent(in)     :: Spindle
      Type(Laue_Ref_Type),       intent(in)     :: R
      real, dimension(3),        intent(out)    :: ellip
      !--- Local variables ---!
      integer                   :: i,j,n,k,km
      integer, parameter        :: npoints=100
      integer, dimension(2)     :: ij
      real                      :: Gamma, nu, dmax, dist
      real, dimension(2)        :: v,vmax,vi
      real, dimension(3)        :: shift
      real, dimension(3,3)      :: Rot,pos
      real, dimension(2,npoints):: points
      Type(Laue_Ref_Type)       :: Rl
      character(len=10)         :: shapen

      if(R%stats /= 0) return
      Rl=R
      ellip=0.0
      shapen=l_case(smpl%shapen)
      if(trim(shapen) == "sphere") then
        if(abs(smpl%av-smpl%ah) < 0.001) then
          ellip(1)=0.5*smpl%xsz+tand(smpl%av*0.5)*LaueD%D
          ellip(2)=ellip(1)
          ellip(3)=0.0
          return
        else
          ellip(1)=0.5*smpl%xsz+tand(smpl%av*0.5)*LaueD%D
          ellip(2)=0.5*smpl%xsz+tand(smpl%ah*0.5)*LaueD%D
          if(ellip(2) > ellip(1)) then
            ellip(3)=0.0
          else
            ellip(3)=90.0
          endif
          return
        end if
      end if

      ! Now the size is anisotropic

      Rot=Matrix_Rz(-spindle,"D")
      pos=matmul(Rot,smpl%tip_pos) !Final positions of extreme dimensions
      n=0
      gamma=Rl%gamma; nu=Rl%nu
      do i=1,3
        do j=-1,1
          shift=j*pos(:,i)
          Rl%gamma=gamma; Rl%nu=nu
          call Calc_Laue_XZ_Position(LaueD,Rl,shift)
          n=n+1
          points(:,n)=(/Rl%z,Rl%x/)

          Rl%gamma=gamma+0.5*smpl%ah; Rl%nu=nu
          call Calc_Laue_XZ_Position(LaueD,Rl,shift)
          n=n+1
          points(:,n)=(/Rl%z,Rl%x/)

          Rl%gamma=gamma-0.5*smpl%ah; Rl%nu=nu
          call Calc_Laue_XZ_Position(LaueD,Rl,shift)
          n=n+1
          points(:,n)=(/Rl%z,Rl%x/)

          Rl%gamma=gamma; Rl%nu=nu+0.5*smpl%av
          call Calc_Laue_XZ_Position(LaueD,Rl,shift)
          n=n+1
          points(:,n)=(/Rl%z,Rl%x/)

          Rl%gamma=gamma; Rl%nu=nu-0.5*smpl%av
          call Calc_Laue_XZ_Position(LaueD,Rl,shift)
          n=n+1
          points(:,n)=(/Rl%z,Rl%x/)
        end do
      end do

      dmax=0.0
      k=0
      do i=1,n-1
         do j=i+1,n
            v=points(:,j)-points(:,i)
            dist=sqrt(dot_product(v,v))
            if(dist > dmax) then
               dmax=dist
               vmax=v
               ij=(/i,j/)
            end if
            k=k+1
          end do
      end do
      ellip(1)=dmax*0.5
      ellip(3)=atan2d(vmax(1),vmax(2))
      vi=points(:,ij(1))
      vmax=vmax/dmax
      dmax=0.0
      do i=1,n
        v=points(:,i)-vi
        v=dot_product(vmax,v)*vmax - v
        dist=sqrt(dot_product(v,v))
        if(dist > dmax) then
          dmax=dist
        end if
      end do
      ellip(2)=dmax
      return
    End Subroutine Calc_Ellipse_Params

    !!----
    !!----  Subroutine Calc_Laue_BackGround(LaueDiff,L,sizd,DataI,Mask2D,Data_bck)
    !!----    Type(Laue_Instrument_Type),           intent(in) :: LaueDiff
    !!----    Type(Laue_Ref_List_Type),             intent(in) :: L
    !!----    Integer,                              intent(in) :: sizd
    !!----    Integer,         dimension(:,:),      intent(in) :: DataI
    !!----    Integer(kind=1), dimension(:,:),      intent(in) :: Mask2D
    !!----    Integer, dimension(:,:), allocatable, intent(out):: Data_bck
    !!----
    !!----  Calculate the background of a Laue image according to the instrument LaueDiff
    !!----  and the list of reflections L. A circular region of radius sizd (in pixels) is
    !!----  selected around each reflection centre, the intensity in that region is interpolated
    !!----  linearly considering boundary background intensities. A final smoothing is
    !!----  performed.
    !!----
    !!----    Created: August 2010 (JRC)
    !!----    Updated: August 2010 (JRC)
    !!----
    Subroutine Calc_Laue_BackGround(LaueDiff,L,sizd,DataI,Mask2D,Data_bck)
      Type(Laue_Instrument_Type),           intent(in) :: LaueDiff
      Type(Laue_Ref_List_Type),             intent(in) :: L
      Integer,                              intent(in) :: sizd
      Integer,         dimension(:,:),      intent(in) :: DataI
      Integer(kind=1), dimension(:,:),      intent(in) :: Mask2D
      Integer, dimension(:,:), allocatable, intent(out):: Data_bck
      ! Local variables
      integer(kind=1), dimension(:,:), allocatable :: Mask
      integer,         dimension(:,:), allocatable :: Back_H
      integer,           dimension(:), allocatable :: vect,vector
      integer :: i,j,k,j1,j2,k1,k2,npix, on, n
      real    :: sx,sz,xp,zp,x,z, d2,d2s, st, delta, vini,vfin
      !
      sx=(LaueDiff%x_max -LaueDiff%x_min)/real(LaueDiff%Np_h-1)
      sz=(LaueDiff%z_max -LaueDiff%z_min)/real(LaueDiff%Np_v-1)
      if(allocated(Data_bck)) deallocate(Data_bck)
      allocate(Data_bck(LaueDiff%Np_v,LaueDiff%Np_h))
      !if(allocated(Mask)) deallocate(Mask)
      allocate(Mask(LaueDiff%Np_v,LaueDiff%Np_h))
      allocate(Back_H(LaueDiff%Np_v,LaueDiff%Np_h))
      n=max(LaueDiff%Np_v,LaueDiff%Np_h)
      allocate(vector(n),vect(n))
      Data_bck=DataI; Back_H=DataI
      Mask=0 ; vect=0; vector=0

      delta = LaueDiff%H * real(sizd) /real(LaueDiff%np_h-1)   !sizd in mm
      d2s=delta * delta
      delta= 2.0 * delta
      ! Calculation of the Mask for the background
      do_ref: do i=1,L%nref
           xp=L%LR(i)%x;  zp=L%LR(i)%z
           j1=max(nint((xp-delta-LaueDiff%x_min)/sx)+1,1)
           j2=min(nint((xp+delta-LaueDiff%x_min)/sx)+1,LaueDiff%Np_h)
           k1=max(nint((zp-delta-LaueDiff%z_min)/sz)+1,1)
           k2=min(nint((zp+delta-LaueDiff%z_min)/sz)+1,LaueDiff%Np_v)
           npix=0
           do j=j1,j2      !Exclude peaks close to an excluded region
             do k=k1,k2
               if(Mask2D(k,j) == -1) cycle do_ref
             end do
           end do
           do j=j1,j2
              x= LaueDiff%x_min +real(j-1)*sx
             do k=k1,k2
               z= LaueDiff%z_min +real(k-1)*sz
               d2=(x-xp)**2 +(z-zp)**2
               if( d2 < d2s ) Mask(k,j) = 1
             end do
           end do
      end do do_ref

      do j=2, LaueDiff%Np_h-1       ! BackGround Vertical pass
        on=0; k1=0; k2=0
        do i=2, LaueDiff%Np_v-1
          if(Mask2D(i,j) == -1 ) cycle
          if(Mask(i,j) == 1 .and. on == 0) then
             vini=Data_bck(i-1,j)
             on=1
             k1=i
             cycle
          end if
          if(Mask(i,j) == 0 .and. on == 1) then
             vfin=Data_bck(i,j)
             on=0
             k2=i-1
          end if
          if(k1 /= 0 .and. k2 /= 0 .and. k2 > k1) then
              npix=k2-k1+1
              st=(vfin-vini)/real(npix)
              do k=k1,k2
                Data_bck(k,j) = vini + (k-k1+1)*st
              end do
              k1=0;k2=0
          end if
        end do
      end do
      !do i=2, LaueDiff%Np_v-1
      ! vector(1:LaueDiff%Np_h)=Data_bck(i,1:LaueDiff%Np_h)
      ! do j=3, LaueDiff%Np_h-2
      !   vect(j)= (-3*vector(j-2)+12*vector(j-1)+17*vector(j)+12*vector(j+1)-3*vector(j+2)) / 35
      !  end do
      !  Data_bck(i,3:LaueDiff%Np_h-2)=vect(3:LaueDiff%Np_h-2)
      !end do

      do i=2, LaueDiff%Np_v-1       ! BackGround Horizontal pass
        on=0; k1=0; k2=0
        do j=2, LaueDiff%Np_h-1
          if(Mask2D(i,j) == -1 ) cycle
          if(Mask(i,j) == 1 .and. on == 0) then
             vini=Back_H(i,j-1)
             on=1
             k1=j
             cycle
          end if
          if(Mask(i,j) == 0 .and. on == 1) then
             vfin=Back_H(i,j)
             on=0
             k2=j-1
          end if
          if(k1 /= 0 .and. k2 /= 0 .and. k2 > k1) then
              npix=k2-k1+1
              st=(vfin-vini)/real(npix)
              do k=k1,k2
                Back_H(i,k) = vini + (k-k1+1)*st
              end do
              k1=0;k2=0
          end if
        end do
      end do
      !do j=2, LaueDiff%Np_h-1
      ! vector(1:LaueDiff%Np_v)=Back_H(1:LaueDiff%Np_v,j)
      ! do i=3, LaueDiff%Np_v-2
      !   vect(i)= (-3*vector(i-2)+12*vector(i-1)+17*vector(i)+12*vector(i+1)-3*vector(i+2)) / 35
      !  end do
      !  Back_H(3:LaueDiff%Np_v-2,j)=vect(3:LaueDiff%Np_v-2)
      !end do
      !Average of the two background images
       Data_bck = (Back_H+Data_bck)/2
       !Restore the background in excluded regions
       do j=1,LaueDiff%Np_h
          do i=1,LaueDiff%Np_v
             if(Mask2D(i,j) == -1) Data_bck(i,j)=DataI(i,j)
          end do
       end do
      return
    End Subroutine Calc_Laue_BackGround

    !!----
    !!----  Subroutine Nodal_Indices(v,vn,n)
    !!----    integer, dimension(:), intent(in ) :: v
    !!----    integer, dimension(:), intent(out) :: vn
    !!----    integer,               intent(out) :: n
    !!----
    !!----  Provides the vector of co-primes integer vn parallel to the input vector v
    !!----  and the integer factor n, so that v= n vn.
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated: January 2012 (JRC)
    !!----
    Subroutine Nodal_Indices(v,vn,n)
      integer, dimension(:), intent(in ) :: v
      integer, dimension(:), intent(out) :: vn
      integer,               intent(out) :: n
      !---- Local variables ----!
      integer                            :: i,j,max_ind,k,im,dimv
      vn=v
      n=1
      max_ind=maxval(abs(vn))
      !---- If the maximum value of the indices is 1 they are already coprimes
      if (max_ind <= 1) return
      !---- Indices greater than 1
      dimv=size(v)
      im=0
      do i=1,size(primes)
         if(primes(i) > max_ind) then  !primes is an array from CrysFML_Math_Gen
            im=i
            exit
         end if
      end do
      if(im == 0) return
      do_p: do i=1,im
        k=primes(i)
        do
          do j=1,dimv
             if( mod(vn(j),k) /= 0) cycle do_p
          end do
          n=n*k
          vn=vn/k
        end do
      end do do_p

      return
    End Subroutine Nodal_Indices

    !!----
    !!----  Subroutine Set_LD_Laue_Limits(lmin,lmax,L,dmin)
    !!----    Type(Laue_Instrument_Type), intent(in out)    :: LaueDiff
    !!----    real,                   intent(in)            :: lmin,lmax
    !!----    real, optional,         intent(in)            :: dmin
    !!----
    !!----  Change the wavelength and resolution limits of the
    !!----  object L of type Laue_Instrument_Type
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated: February 2009
    !!----
    Subroutine Set_LD_Laue_Limits(lmin,lmax,LaueDiff,dmin)
      Type(Laue_Instrument_Type), intent(in out)  :: LaueDiff
      real,                   intent(in)      :: lmin,lmax
      real, optional,         intent(in)      :: dmin

      LaueDiff%L_min=lmin; LaueDiff%L_max=lmax
      if(present(dmin)) then
         LaueDiff%d_min=dmin
      else
         LaueDiff%d_min=lmin/2.0
      end if
      return
    End Subroutine Set_LD_Laue_Limits

    !!----
    !!----  Subroutine Pix_To_Mil(LaueDiff,xpc,ypc,xmm,ymm)
    !!----    Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
    !!----    real, intent(in)  :: xpc,ypc
    !!----    real, intent(out) :: xmm,ymm
    !!----
    !!----  Conversion from pixels to mm for the instrument LaueDiff
    !!----  The origin for pixels is on the left bottom corner starting at (1,1)
    !!----  The origin for mm is in the position LaueDiff%(xo,zo) in pixels of
    !!----  the detecting surface.
    !!----
    !!----    Created: April 2010 (JRC & LFM)
    !!----    Updated:  June 2010 (LFM)
    !!----    Updated:  July 2010 (JRC)
    !!----
    Subroutine Pix_To_Mil(LaueDiff,xpc,ypc,xmm,ymm)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      real,                       intent(in)  :: xpc,ypc
      real,                       intent(out) :: xmm,ymm
      xmm= LaueDiff%H * (xpc-LaueDiff%xo)/real(LaueDiff%np_h-1)
      ymm= LaueDiff%V * (ypc-LaueDiff%zo)/real(LaueDiff%np_v-1)
      return
    End Subroutine Pix_To_Mil


    !!----
    !!----  Subroutine Mil_To_Pix(LaueDiff,xmm,ymm,xpc,ypc)
    !!----    Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
    !!----    real,  intent(in)  :: xmm,ymm
    !!----    real,  intent(out) :: xpc,ypc
    !!----
    !!----  Conversion from mm to pixels for the instrument LaueDiff
    !!----  The origin for pixels is on the left bottom corner starting at (1,1)
    !!----  The origin for mm is in the position LaueDiff%(xo,zo) in pixels of
    !!----  the detecting surface.
    !!----
    !!----    Created: April 2010 (JRC & LFM)
    !!----    Updated:  June 2010 (LFM)
    !!----    Updated:  July 2010 (JRC)
    !!----
    Subroutine Mil_To_Pix(LaueDiff,xmm,ymm,xpc,ypc)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      real,                       intent(in)  :: xmm,ymm
      real,                       intent(out) :: xpc,ypc
      xpc=xmm*real(LaueDiff%np_h-1)/LaueDiff%H+LaueDiff%xo
      ypc=ymm*real(LaueDiff%np_v-1)/LaueDiff%V+LaueDiff%zo
      return
    End Subroutine Mil_To_Pix

    !!----
    !!----  Subroutine Calc_Angular_Laue_Limits(LaueDiff)
    !!----    Type(Laue_Instrument_Type), intent(in )     :: LaueDiff
    !!----
    !!----  Change the angular limits of the object L of type Laue_Instrument_Type
    !!----  according to the information given in LaueDiff.
    !!----  This subroutine treats Laue diffractometers having a single cylindrical
    !!----  or flat detector. The cylinder is supposed to be vertical with its axis
    !!----  along ZL. The flat detector can be places anywhere. A conservative
    !!----  option for calculating the gamma and nu limits has been adopted.
    !!----  Still in testing phase.
    !!----
    !!----    Created: October 2009  (JRC)
    !!----    Updated: July 2010  (LFM)
    !!----
    Subroutine Calc_Angular_Laue_Limits(LaueDiff)
      Type(Laue_Instrument_Type), intent(in out)     :: LaueDiff

      !--- Local Variables ---!
      real  :: ga_min, ga_max, nu_min, nu_max, Cat_opt, Cat_ady, Delta_ang

      !Aproximate calculation of angularmp_gam Laue limits from detector characteristics
      LaueDiff%nu_max = atan2d(0.5*LaueDiff%V,LaueDiff%D)
      LaueDiff%nu_min = -LaueDiff%nu_max

      LaueDiff%z_max  =  0.5*LaueDiff%V
      LaueDiff%z_min  =  - LaueDiff%z_max

      Select Case(LaueDiff%dtype)

        Case("Rec")

            Cat_opt = sqrt((LaueDiff%V/2)**2 + (LaueDiff%H/2)**2)
            Cat_ady = LaueDiff%D
            Delta_ang = atan2d(Cat_opt,Cat_ady)

            !write(unit=*,fmt="(a,f8.3)") ' => Delta_angl =',Delta_ang

            ga_min = LaueDiff%ga_d - Delta_ang
            ga_max = LaueDiff%ga_d + Delta_ang

            nu_min = LaueDiff%nu_d - Delta_ang
            nu_max = LaueDiff%nu_d + Delta_ang

            !write(unit=*,fmt="(a,4f8.3)") ' => Temporal ga_min, ga_max, nu_min, nu_max', ga_min, ga_max, nu_min, nu_max


            if( nu_min<= -90.0 )then
                LaueDiff%gap_min=0.0
                LaueDiff%gap_max=180.0

                LaueDiff%gan_min=-180.0
                LaueDiff%gan_max=0.0
                nu_min = -90.0

            elseif( nu_max>= 90.0 )then
                LaueDiff%gap_min=0.0
                LaueDiff%gap_max=180.0

                LaueDiff%gan_min=-180.0
                LaueDiff%gan_max=0.0
                nu_max = 90.0
            else

                if( LaueDiff%ga_d < 90.0 .and. LaueDiff%ga_d > -90.0 )then
                    if( ga_min > 0.0 )then
                        LaueDiff%gan_min=0.01
                        LaueDiff%gan_max=-0.01

                        LaueDiff%gap_min=ga_min
                        LaueDiff%gap_max=ga_max

                    elseif( ga_max < 0.0 )then
                        LaueDiff%gan_min=ga_min
                        LaueDiff%gan_max=ga_max

                        LaueDiff%gap_min=0.01
                        LaueDiff%gap_max=-0.01

                    else
                        LaueDiff%gan_min=ga_min
                        LaueDiff%gan_max=0.0

                        LaueDiff%gap_min=0.0
                        LaueDiff%gap_max=ga_max

                    end if
                else
                    if( ga_max < 180.0 .and. ga_min > 0.0 )then
                        LaueDiff%gap_min=ga_min
                        LaueDiff%gap_max=ga_max

                        LaueDiff%gan_max=0.01
                        LaueDiff%gan_min=-0.01

                    elseif( ga_max < 0.0 .and. ga_min > -180.0 )then
                        LaueDiff%gan_min=ga_min
                        LaueDiff%gan_max=ga_max

                        LaueDiff%gap_max=0.01
                        LaueDiff%gap_min=-0.01

                    else
                        if(LaueDiff%ga_d >= 0)then
                            LaueDiff%gan_min=-180.0
                            LaueDiff%gap_max=180.0

                            LaueDiff%gan_max= -180.0 + (ga_max - 180.0)
                            LaueDiff%gap_min= ga_min

                        else
                            LaueDiff%gan_min=-180.0
                            LaueDiff%gap_max=180.0

                            LaueDiff%gan_max= ga_max
                            LaueDiff%gap_min= 180.0 + (ga_min + 180.0)

                        end if
                    end if

                end if

            end if


            LaueDiff%nu_max = nu_max
            LaueDiff%nu_min = nu_min
            LaueDiff%z_max  =  0.5*LaueDiff%V-LaueDiff%dism(2)
            LaueDiff%z_min  = -0.5*LaueDiff%V-LaueDiff%dism(2)
            LaueDiff%x_max  = 0.5*LaueDiff%H
            LaueDiff%x_min  = -LaueDiff%x_max

        Case("Cyl")       !The ideal geometry for a cylindrical detector is considered here
          LaueDiff%gap_max = (0.5*LaueDiff%H/LaueDiff%D)*to_deg
          LaueDiff%gap_min = 0.0
          LaueDiff%gan_max = 0.0
          LaueDiff%gan_min = -LaueDiff%gap_max
          LaueDiff%x_max  = 0.5*LaueDiff%H-LaueDiff%dism(1)
          LaueDiff%x_min  = -0.5*LaueDiff%H-LaueDiff%dism(1)

        Case Default
          LaueDiff%gap_max = 180.0
          LaueDiff%gan_min =-180.0
          LaueDiff%gap_min =   0.0
          LaueDiff%gan_max =   0.0
          LaueDiff%x_max  = pi*LaueDiff%D
          LaueDiff%x_min  = -LaueDiff%x_max

      End Select
    return
    End Subroutine Calc_Angular_Laue_Limits


    !!----
    !!----  Subroutine Calc_Laue_Spot_XZ_Angles(LaueDiff,R,RL,opt,dk_min)
    !!----    Type(Laue_Instrument_Type), intent(in)    :: LaueDiff
    !!----    Type(Laue_Ref_Type),        intent(in out):: R  !Laue reflection
    !!----    real, dimension(3,3),       intent(in)    :: RL !Matrix trasforming hkl to laboratory cartesian system
    !!----                                                    !Normally RL = Rx Ry Rz UB
    !!----    integer, optional,          intent(in)    :: opt
    !!----    real,    optional,          intent(in)    :: dk_min !ds-min for satellites
    !!----
    !!----  This subroutine calculates angular components of the object R of type Laue_Ref_Type
    !!----  according to the information given in other components of R, the Laue limits contained
    !!----  in LaueDiff and the matrix RL transforming hkl-indices (contained as components of R) to
    !!----  the laboratory system. If the optional argument "opt" is provided the subroutine
    !!----  calculates only the spherical angles of the reflection without taking into account
    !!----  the status of the reflection. The coordinates x-z in the detector system are also calculated.
    !!----  The subroutine calculates also the status of the reflection contained in the
    !!----  component R%stats
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated: January 2010 (optional argument opt, JRC)
    !!----    Updated: July 2010    (taking into account negative and positive gammas, JRC)
    !!----
    Subroutine Calc_Laue_Spot_XZ_Angles(LaueDiff,R,RL,opt,dk_min)
      Type(Laue_Instrument_Type), intent(in)    :: LaueDiff
      Type(Laue_Ref_Type),        intent(in out):: R  !Laue reflection
      real, dimension(3,3),       intent(in)    :: RL !Matrix trasforming hkl to laboratory cartesian system
                                                      !Normally RL = Rx Ry Rz UB
      integer, optional,          intent(in)    :: opt
      real,    optional,          intent(in)    :: dk_min

      R%lambda=0.0; R%gamma=0.0; R%nu=0.0; R%stats=0; R%mult=0; R%nodal=0

      R%zv=matmul(RL,R%h) !Cartesian coordinates of h in the Lab system

      if(present(opt)) then
        R%ds=1.0/sqrt(dot_product(R%zv,R%zv))
        call Calc_Spherical_angles_from_zv(R)
        return
      end if
      if( R%zv(2) >= 0.0) then
        R%stats=-3 !Oposite side of Ewald spheres
        return
           !  Determine the wavelength of the reflection  Lambda=-2y d^2, sinTheta=-yd
      else !  sin Nu= Lambda*z, tanGamma= Lamda*x/(1+lambda*y)
           !
        R%ds=1.0/sqrt(dot_product(R%zv,R%zv))
        if(R%ds < LaueDiff%d_min) then
          R%stats=-2 !outside given resolution
          return
        end if
        if(present(dk_min)) then
         if(R%ds < dk_min) then
           R%stats=-2 !outside given resolution for k-vector satellites
           return
         end if
        end if
        R%lambda=-2.0*R%zv(2)*R%ds*R%ds
        if(R%lambda < LaueDiff%L_min .or. R%lambda > LaueDiff%L_max) then
          R%stats=-1 !outside Ewald spheres
          return
        else !Calculate theta, gamma and nu
          R%nu= asind(R%lambda*R%zv(3))
          if(R%nu < LaueDiff%nu_min .or. R%nu > LaueDiff%nu_max) then
           R%stats = 1   ! Beyond Nu-limit
           return
          end if

          R%gamma=atan2d( R%lambda*R%zv(1), 1+R%lambda*R%zv(2))
          if(R%gamma >= 0.0) then
             if(R%gamma < LaueDiff%gap_min .or. R%gamma > LaueDiff%gap_max) then
              R%stats = 2  ! Beyond Gamma-limit
              return
             end if
          else
             if(R%gamma < LaueDiff%gan_min .or. R%gamma > LaueDiff%gan_max) then
              R%stats = 2  ! Beyond Gamma-limit
              return
             end if
          end if

          call Calc_Laue_XZ_Position(LaueDiff,R)
          if(R%x < LaueDiff%x_min .or. R%x > LaueDiff%x_max .or. R%z < LaueDiff%z_min .or. R%z > LaueDiff%z_max) then
            R%stats = 3  ! Outside the detector
            return
          end if
          R%ttheta=2.0*asind(-R%zv(2)*R%ds) !the multiplication by 2 was lacking before July 2011!!
         !
         ! The calculation of the multiplicity and nodal harmonic has been
         ! moved to another place. It is only needed when output the list
         ! of reflections.
         ! The calculation is done taking into account systematic absences
         !
        end if
      end if
    End Subroutine Calc_Laue_Spot_XZ_Angles

    !!----
    !!----  Subroutine Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,R,RL,eta,maxdiv,opt,dk_min)
    !!----    Type(Laue_Instrument_Type), intent(in)    :: LaueDiff
    !!----    Type(Laue_Ref_Type),        intent(in out):: R  !Laue reflection
    !!----    real, dimension(3,3),       intent(in)    :: RL !Matrix trasforming hkl to laboratory cartesian system
    !!----                                                    !Normally RL = Omega Chi Phi UB
    !!----    real,                       intent(in)    :: eta,maxdiv  ! Mosaic spread, maximum divergence
    !!----                                                             ! all in radians and small.
    !!----    integer, optional,          intent(in)    :: opt
    !!----    real,    optional,          intent(in)    :: dk_min !ds-min for satellites
    !!----
    !!----  This subroutine derives from Calc_Laue_Spot_XZ_Angles but adapted for monochromatic
    !!----  radiation and taking into account the isotropic mosaicity of the crystal, the horizontal
    !!----  and vertical divergence of the incident beam, and the width of the lambda distribution.
    !!----  We use all the Laue types for reflections and use straightforward vectorial calculations.
    !!----  The lattice point is represented by a sphere of radius r=1/2.eta.s, with eta the mosaic
    !!----  spread in radians and s the modulus of the reciprocal lattice vector. Two limiting Ewald
    !!----  spheres of radii (Lambda-Delta, Lambda+Delta), with Delta=x.Lambda (x=DeltaLambda)/Lambda)
    !!----  are considered. The horizontal and vertical divergence of the incident beam is considered
    !!----  (for the purposes of detecting if the reflection, or its tail, is visible in the detector)
    !!----  by calculating an effective mosaicity eta_eff=sqrt(eta^2+ max_div^2).
    !!----
    !!----
    !!----    Created: January 2012 (JRC)
    !!----    Updated: January 2012 (JRC)
    !!----
    Subroutine Calc_Monochromatic_Spot_XZ_Angles(LaueDiff,R,RL,eta,maxdiv,opt,dk_min)
      Type(Laue_Instrument_Type),      intent(in)    :: LaueDiff
      Type(Laue_Ref_Type),             intent(in out):: R  !Laue reflection
      real, dimension(3,3),            intent(in)    :: RL !Matrix trasforming hkl to laboratory cartesian system
                                                           !Normally RL = Omega Chi Phi UB
      real,                            intent(in)    :: eta,maxdiv  ! Mosaic spread, maximum divergence
                                                                    ! all in radians and small.
      integer, optional,               intent(in)    :: opt
      real,    optional,               intent(in)    :: dk_min

      real                 :: lambda, radius, eff_eta, delta
      real, dimension(3)   :: s
      real, dimension(3,3) :: mhor_div, mver_div

      R%lambda=0.0; R%gamma=0.0; R%nu=0.0; R%stats=0; R%mult=1; R%nodal=0

      R%zv=matmul(RL,R%h) !Cartesian coordinates of h in the Lab system (z4=Omega Chi Phi UB h)

      if(present(opt)) then
        R%ds=1.0/sqrt(dot_product(R%zv,R%zv))
        call Calc_Spherical_angles_from_zv(R)
        return
      end if
      if( R%zv(2) >= 0.0) then
        R%stats=-3 !Oposite side of Ewald spheres
        return
           !  Determine the wavelength of the reflection  Lambda=-2y d^2, sinTheta=-yd
      else !  sin Nu= Lambda*z, tanGamma= Lamda*x/(1+lambda*y)
           !  First check other conditions
        R%ds=1.0/sqrt(dot_product(R%zv,R%zv))
        if(R%ds < LaueDiff%d_min) then
          R%stats=-2 !outside the user-provided resolution
          return
        end if
        if(present(dk_min)) then
         if(R%ds < dk_min) then
           R%stats=-2 !outside the user-provided resolution for k-vector satellites
           return
         end if
        end if

        R%lambda=-2.0*R%zv(2)*R%ds*R%ds !Calculation as if it were polychromatic

        if(R%lambda < LaueDiff%L_min .or. R%lambda > LaueDiff%L_max) then
          !  R%stats=-1  Normally it is outside Ewald spheres but not return because we have to
          !  check if due to mosaic spread or divergence part of the diffraction domain
          !  is in contact with some of the Ewald spheres
          !  The combined effect of crystal mosaicity and divergence of the incident beam
          !  is for the moment approximated by an effective mosaicity giving rise to spherical
          !  diffraction domain
          eff_eta=sqrt(eta*eta+maxdiv*maxdiv)
          radius=0.5*eff_eta/R%ds

          call Get_s_delta(LaueDiff%L_Central,R%zv,s,delta)

          if(delta <= radius) then !Part of the diffraction domain touch the Central Ewald sphere
              R%lambda = LaueDiff%L_Central
              R%nodal=1 !Diffraction domain touch the central Ewald sphere
          else          !Check now with the shortest lambda limit
              call Get_s_delta(LaueDiff%L_min,R%zv,s,delta)
              if(delta <= radius) then !Part of the diffraction domain touch the Lambda_Min Ewald sphere
                  R%nodal=2 !Diffraction domain touch the Lambda_Min Ewald sphere
                  R%lambda=-2.0*s(2)*R%ds*R%ds !This is an approximation using s(2) and maintaining R%ds
              else !Check now with the largest lambda limit
                  call Get_s_delta(LaueDiff%L_max,R%zv,s,delta)
                  if(delta <= radius) then !Part of the diffraction domain touch the Lambda_Max Ewald sphere
                      R%nodal=3 !Diffraction domain touch the Lambda_Max Ewald sphere
                      R%lambda=-2.0*s(2)*R%ds*R%ds
                  else !The reflection is not excited
                      R%stats=-1
                      return
                  end if
              end if
          end if
        else
          R%lambda = LaueDiff%L_Central
          s=R%zv     !Scattering vector supposed at the centre of the diffraction domain
          R%nodal=0  !Here nodal is used to label the type of excitation. This is for centre of
                     !diffraction domain within the two limiting Ewald spheres
        end if

        !The reflection has part of its diffraction domain in contact with one of the
        !Ewald spheres: the reflection is excited. Calculate theta, gamma and nu
        !checking if it is observable in the conditions of the experiment

        R%nu= asind(R%lambda*s(3))
        if(R%nu < LaueDiff%nu_min .or. R%nu > LaueDiff%nu_max) then
         R%stats = 1   ! Beyond Nu-limit
         return
        end if

        R%gamma=atan2d( R%lambda*s(1), 1+R%lambda*s(2))
        if(R%gamma >= 0.0) then
           if(R%gamma < LaueDiff%gap_min .or. R%gamma > LaueDiff%gap_max) then
            R%stats = 2  ! Beyond Gamma-limit
            return
           end if
        else
           if(R%gamma < LaueDiff%gan_min .or. R%gamma > LaueDiff%gan_max) then
            R%stats = 2  ! Beyond Gamma-limit
            return
           end if
        end if

        call Calc_Laue_XZ_Position(LaueDiff,R)
        if(R%x < LaueDiff%x_min .or. R%x > LaueDiff%x_max .or. R%z < LaueDiff%z_min .or. R%z > LaueDiff%z_max) then
          R%stats = 3  ! Outside the detector
          return
        end if
        R%ttheta=2.0*asind(-s(2)*R%ds)

      end if
    End Subroutine Calc_Monochromatic_Spot_XZ_Angles

    Subroutine Get_s_delta(Lambda,z4,s,delta)
      real,               intent(in)  :: Lambda
      real, dimension(3), intent(in)  :: z4
      real, dimension(3), intent(out) :: s
      real,               intent(out) :: delta
      !---- local variables ----!
      real, dimension(3) :: v,u,s0

      s0=(/0.0, 1.0/Lambda, 0.0/)
      v= s0 + z4
      u=v/sqrt(dot_product(v,v))/Lambda
      delta=sqrt(dot_product(v-u,v-u))
      s=u-s0
      return
    End Subroutine Get_s_delta

    Subroutine Laue_Mult(LaueDiff,SpG,R)
      Type(Laue_Instrument_Type), intent(in)    :: LaueDiff
      Type(Space_Group_Type),     intent(in)    :: SpG
      Type(Laue_Ref_Type),        intent(in out):: R  !Laue reflection
      !--- Local Variables ---!
      integer :: i
      real    :: lamb0,dma,d,lamb
      integer               :: mult,n
      integer, dimension(3) :: h,hn

      call Nodal_Indices(nint(R%h),h,n)
      R%nodal=n
      dma=R%ds*real(n)  ! 2sinTheta=lamb0*s0
      lamb0=R%Lambda*real(n)
      mult=0
      i=0
      do
        i=i+1
        if( i == n ) then
          mult=mult+1
          cycle
        end if
        d=dma/real(i)
        hn=i*h
        if(d < LaueDiff%d_min) exit  !Resolution test
        lamb=lamb0/real(i)
        if(lamb < LaueDiff%L_min) exit
        if(hkl_Absent(hn,SpG)) cycle
        if(lamb <= LaueDiff%L_max) mult=mult+1
      end do
      R%mult=mult
      return
    End Subroutine Laue_Mult

    !!----
    !!----  Subroutine Calc_Laue_XZ_Position(LaueDiff,R,shift)
    !!----    Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
    !!----    Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection
    !!----    real, dimension(3),optional,intent(in)     :: shift
    !!----
    !!----  This subroutine calculates the position of the spot in the detector system (R%x,R%z)
    !!----  according to the information given in other components of R and in LaueDiff
    !!----  The optional shift vector is useful for calculating diffracted peaks by finite size
    !!----  samples: diffraction by a part of the sample (out of centre) give displaced (x,z)
    !!----  coordinates.
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated:  August 2010 (JRC), July 2012 (JRC, shift and origin displacement added)
    !!----
    Subroutine Calc_Laue_XZ_Position(LaueDiff,R,shift)
      Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
      Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection
      real, dimension(3),optional,intent(in)     :: shift
      !--- Local variables ---!
      real, dimension(3) :: xl,xd,u,v,ro,pos_det
      real, dimension(2) :: vd,rod
      real               :: p,a,b,c,p1,p2,xc,zc
      logical            :: ok

      R%x=0.0; R%z=0.0
      if(R%Stats /= 0) return
      pos_det = LaueDiff%rOD
      if(present(shift)) pos_det = pos_det + shift

      !First calculate the position (x,z) w.r.t. ideal centre of detector
      Select Case(LaueDiff%dtype)
        Case("Rec")

          u=(/sind(R%gamma)*cosd(R%nu), cosd(R%gamma)*cosd(R%nu), sind(R%nu)/)
          p=dot_product(LaueDiff%RD(:,2),pos_det/dot_product(LaueDiff%RD(:,2),u))
          xl=p*u
          xd=matmul(transpose(LaueDiff%RD),xl-pos_det)
          xc=xd(1)
          zc=xd(3)

        Case("Cyl")

          if(LaueDiff%tilted .or. LaueDiff%displaced .or. present(shift) ) then   !Tilted/Displaced cylindrical detector
            u=(/sind(R%gamma)*cosd(R%nu), cosd(R%gamma)*cosd(R%nu), sind(R%nu)/)
            v=matmul(LaueDiff%RD,u);              vd=(/v(1),v(2)/)
            ro=matmul(LaueDiff%RD,pos_det); rod=(/ro(1),ro(2)/)
            a=dot_product(vd,vd)
            b=-2.0*dot_product(vd,rod)
            c=dot_product(rod,rod)-LaueDiff%D*LaueDiff%D
            Call Quadratic_Eqn_Solver(a,b,c,p1,p2,ok)
            if(ok) then !Select positive p
               p=max(p1,p2)
               zc=p*v(3)-ro(3)
               xc=LaueDiff%D*atan2(p*v(1)-ro(1),p*v(2)-ro(2))
            end if
          else                 !Ideal cylindrical detector
            zc = LaueDiff%D * tand(R%nu)
            xc = R%gamma*to_rad *  LaueDiff%D
          end if

      End Select

      !Apply the shift in mm corresponding to the difference of the origin in pixels w.r.t. (Np_h/2,Np_v/2)
      R%x=xc-LaueDiff%dism(1)
      R%z=zc-LaueDiff%dism(2)

    End Subroutine Calc_Laue_XZ_Position

    Subroutine Quadratic_Eqn_Solver(a,b,c,x1,x2,ok)
      real, intent(in)     :: a,b,c
      real, intent(out)    :: x1,x2
      logical, intent(out) :: ok
      !
      real            :: delta, sqd
      real, parameter :: eps=1.0e-35

      x1=0.0; x2=0.0; ok=.false.

      if(abs(a) <= eps) then      ! Linear equation in practice
         if(abs(b) <= eps) return ! no solution
         x1=-c/b
         x2=x1
         ok=.true.
         return
      end if

      delta= b*b-4.0*a*c   !discriminant
      if(delta < 0.0) then ! no real solution
        return
      else if( abs(delta) <= eps) then ! a single solution
        x1= -0.5*b/a
        x2=x1
      else            !positive delta
        sqd = sqrt(delta)
        x1=0.5*(-b+sqd)/a
        x2= 0.5*(-b-sqd)/a
      end if
      ok=.true.
      return
    End Subroutine Quadratic_Eqn_Solver

    !!----
    !!---- Subroutine Calc_gam_nu_from_xz(LaueDiff,R)
    !!----   Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
    !!----   Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection
    !!----
    !!----  Procedure for calculating the pseudo-spherical angles (gamma,nu) in the laboratory
    !!----  system of the diffracted beam corresponding to the spot of coordinates (R%x,R%z)
    !!----  The angles are stored in the components R%gamma and R%nu. General positions of the
    !!----  detectors (flat of cylindrical) are now considered (Aug. 2010).
    !!----
    !!----    Created: October 2009 (JRC)
    !!----    Updated:  August 2010 (JRC), July 2012 (JRC, adding correction w.r.t.centre)
    !!----
    Subroutine Calc_gam_nu_from_xz(LaueDiff,R)
      Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
      Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection

      real, dimension(3) :: xl,xd
      real               :: ga,xc,zc

      R%gamma=0.0; R%nu=0.0
      if(R%Stats /= 0) return

      !Correct the input xz coordinates w.r.t. the centre of the detector displacement
      xc=R%x+LaueDiff%dism(1)
      zc=R%z+LaueDiff%dism(2)

      ! First check if it is an ideal cyclindrical detector, make the calculation and return
      if(LaueDiff%dtype == "Cyl" .and. .not. LaueDiff%tilted .and.  .not. LaueDiff%displaced ) then
         R%nu= atand(zc/LaueDiff%D)
         R%gamma = xc / to_rad /  LaueDiff%D
         return
      end if

      ! If not, select between detector types for defining the spot position vector in the
      ! reference system attached to the detector
      Select Case(LaueDiff%dtype)
        Case("Rec")
          xd(1) = xc;  xd(2)=0.0;  xd(3)=zc !Coordinates of the spot in the detector system
        Case("Cyl")
          ga=xc/LaueDiff%D
          xd=(/LaueDiff%D*sin(ga), LaueDiff%D*cos(ga), zc/)
      End Select

      xl=matmul(LaueDiff%RD,xd)      !Aplication of the tilts of the detector to obtain the coordinates of the spot in the L-system
      xl=xl+LaueDiff%rOD             !Translation of the detector to the final position (final coordinates of the spot in the L-sytem)
      xl=xl/sqrt(dot_product(xl,xl)) ! = u =(/ sind(gamma)*cosd(nu), cosd(gamma)*cosd(nu), sind(nu)/)
      R%nu=asind(xl(3))
      R%gamma=atan2d(xl(1),xl(2))
      return
    End Subroutine Calc_gam_nu_from_xz

    !!----
    !!---- Subroutine Calc_Reflection_Orbit(LaueDiff,Rf,UB,spind_ini,spind_fin,npoints,lun)
    !!----   Type(Laue_Instrument_Type), intent(in) :: LaueDiff
    !!----   Type(Laue_Ref_Type),        intent(in) :: Rf  !Laue reflection
    !!----   real, dimension(3,3),       intent(in) :: UB !Busing-Levy UB-matrix
    !!----   real,                       intent(in) :: spind_ini,spind_fin  !Initial and final spindle angles
    !!----   integer,                    intent(in) :: npoints    !Number of points to calculate
    !!----   integer,                    intent(in) :: lun        !Logical unit of the file in which the results are output
    !!----
    !!----  Subroutine printing in the file with logical unit "lun" the orbit of a
    !!----  particular reflection when the spindle angle is moved from spind_ini to spind_fin.
    !!----
    !!----    Created: October 2010 (JRC)
    !!----    Updated: October 2010 (JRC)
    !!----
    Subroutine Calc_Reflection_Orbit(LaueDiff,Rf,UB,spind_ini,spind_fin,npoints,lun)
      Type(Laue_Instrument_Type), intent(in) :: LaueDiff
      Type(Laue_Ref_Type),        intent(in) :: Rf  !Laue reflection
      real, dimension(3,3),       intent(in) :: UB !Busing-Levy UB-matrix
      real,                       intent(in) :: spind_ini,spind_fin  !Initial and final spindle angles
      integer,                    intent(in) :: npoints    !Number of points to calculate
      integer,                    intent(in) :: lun        !Logical unit of the file in which the results are output
      !--- Local variables ---!
      Type(Laue_Ref_Type)  :: R
      real, dimension(3,3) :: M_matrix,Rm
      character(len=40)    :: comment
      real                 :: step, spindle
      integer              :: i


      step = (spind_fin-spind_ini)/real(npoints-1)
      R=Rf   !Asign the input reflection to the local varible
      call Calc_Laue_Spot_XZ_Angles(LaueDiff,R,UB,1)  !Calculates the d-spacing of the reflection
      write(unit=lun,fmt="(/,/,a,3i4,2(a,f9.3))") "   => Orbit of reflection (",nint(R%h), &
                         "), when spindle goes from:",spind_ini," to:",spind_fin
      write(unit=lun,fmt="(a,f10.5,a)") "   => The d-spacing of the reflection is: ",R%ds, " angstroms"

      write(unit=lun,fmt="(/,a)")   &
        "    #   Spindle   Mult   Nod  Lambda    Gamma     Nu     2Theta    X(mm)    Z(mm)"// &
        "       Hx       Hy       Hz            Status"

      do i=1,npoints
        spindle = spind_ini + real(i-1)*step
        Rm=Matrix_Rz(-spindle,"D")
        M_matrix = Matmul(Rm,UB)
        call Calc_Laue_Spot_XZ_Angles(LaueDiff,R,M_matrix)
        Select Case(R%stats)
          case(-3)
            comment=" Oposite side of Ewald spheres "
          case(-2)
            comment=" Outside the given resolution "
          case(-1)
            comment=" Outside Ewald spheres "
          case( 0)
            comment="  "
          case( 1)
            comment=" Outside Nu-limits "
          case( 2)
            comment=" Outside Gamma-limits "
          case( 3)
            comment=" Outside detector area "
          case default
            comment=" This comment should not be here! "
        End Select
        if(R%stats == 0) then
           write(unit=lun,fmt="(i5,f10.4,2i6,f9.5,3f9.3,2f9.2,a,3f9.5,a)") &
           i, spindle, R%mult, R%nodal, R%Lambda, R%Gamma, R%nu, R%Ttheta, R%x, R%z, "  (",R%zv," )  "
        else
           write(unit=lun,fmt="(i5,f10.4,tr66,a,3f9.5,a)") i, spindle, "  (",R%zv," )  <-- "//trim(comment)
        end if
      end do
      return
    End Subroutine Calc_Reflection_Orbit

    !!---- Subroutine Calc_Spherical_angles_from_zv(R)
    !!----   Type(Laue_Ref_Type), intent(in out) :: R  !Laue reflection
    !!----
    !!----   Subroutine calculating the spherical angles in the laboratory system
    !!----   of the scattering vector corresponding to a Laue reflection. It
    !!----   completes the construction of the components of the Laue_Ref_Type R.
    !!----   The calculations are done from the Cartesian components of the scattering vector: R%zv
    !!----
    !!----   Created: January 2010 (JRC)
    !!----   Updated: January 2010
    !!----
    Subroutine Calc_Spherical_angles_from_zv(R)
      Type(Laue_Ref_Type), intent(in out) :: R  !Laue reflection
      real :: mu
      call Get_Spheric_Coord(R%zv,mu,R%stheta,R%sphi,"D")
      if(R%sphi < 0.0)  R%sphi=R%sphi+360.0
      return
    End Subroutine Calc_Spherical_angles_from_zv

    !!---- Subroutine Calc_Spherical_angles_from_xz(LaueDiff,R,zop)
    !!----   Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
    !!----   Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection
    !!----   Integer,   optional,        intent(in)     :: zop
    !!----
    !!----   Subroutine calculating the spherical angles in the laboratory system
    !!----   of the scattering vector corresponding to a Laue reflection, it
    !!----   completes the construction of the components of the Laue_Ref_Type R.
    !!----   starting from its spot coordinates: R%x,R%z
    !!----   If the optional argument zop is given, the cartesian coordinates of
    !!----   the direction of the scattering vector are stored in R%zv
    !!----
    !!----   Created: January 2010 (JRC)
    !!----   Updated: January 2010
    !!----
    Subroutine Calc_Spherical_angles_from_xz(LaueDiff,R,zop)
      Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
      Type(Laue_Ref_Type),        intent(in out) :: R  !Laue reflection
      Integer,   optional,        intent(in)     :: zop
      real, dimension(3) :: u
      real               :: mu

      R%stheta=0.0; R%sphi=0.0
      if(R%Stats /= 0) return
      call Calc_gam_nu_from_xz(LaueDiff,R)
      u=(/ sind(R%gamma)*cosd(R%nu), cosd(R%gamma)*cosd(R%nu)-1.0,sind(R%nu)/)
      if(present(zop)) R%zv=u
      call Get_Spheric_Coord(u,mu,R%stheta,R%sphi,"D")
      if(R%sphi < 0.0)  R%sphi=R%sphi+360.0
      return
    End Subroutine Calc_Spherical_angles_from_xz

    !!----
    !!---- Subroutine Read_Laue_Instrm(filenam,LaueDiff,inp)
    !!----    character(len=*),            intent(in)  :: filenam
    !!----    Type(Laue_Instrument_Type),  intent(out) :: LaueDiff
    !!----    Integer, optional,                intent(in)  :: inp
    !!----
    !!----    Subroutine reading the file 'filenam' where the characteristics
    !!----    of the a Laue instrument are written. This subroutine replaces
    !!----    the old one using CrysFML module CFML_ILL_Instrm_Data.f90
    !!----
    !!---- Created: June - 2010 (JRC)
    !!---- Updated: July - 2010 (JRC), June - 2012 (JRC)
    !!
    Subroutine Read_Laue_Instrm(filenam,LaueDiff,inp)
       !---- Argument ----!
       character(len=*),                 intent(in)  :: filenam
       Type(Laue_Instrument_Type),       intent(out) :: LaueDiff
       Integer, optional,                intent(in)  :: inp

       !---- Local variables ----!
       character(len=120) :: line
       character(len=12)  :: key
       integer            :: i, lun, ier
       logical            :: read_orig
       real, dimension(2) :: disp
       real               :: xm,zm


       if(present(inp)) then
         lun=inp
       else
         call Get_LogUnit(lun)
         open(unit=lun,file=trim(filenam),status="old", action="read", position="rewind",iostat=ier)
         if (ier /= 0) then
            Error_Laue_Mod=.true.
            Error_Mess_Laue_Mod="Error opening the file: "//trim(filenam)
            return
         end if
       end if

       Call Init_Laue_Instrm(LaueDiff)   !Set the characteristics of the instrument similar to those of VIVALDI
       read_orig=.false.
       do
         read(unit=lun,fmt="(a)",iostat=ier) line
         if(ier /= 0) exit

         line=adjustl(line)
         if(line(1:1) == "!" .or. line(1:1) == "#" .or. len_trim(line) == 0) cycle
         i=index(line," ")
         key=u_case(line(1:i-1))
         Select Case(key)
            Case("END")
                exit

            Case("INFO")
                LaueDiff%info= adjustl(line(i+1:))

            Case("NAME")
                LaueDiff%name= adjustl(line(i+1:))

            Case("DET_TYPE")
                LaueDiff%dtype = adjustl(line(i+1:))

            Case("DIST_DET")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%D
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the distance sample-detector"
                  return
                end if

            Case("DIM_XZP")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%H,    LaueDiff%V, &
                                                       LaueDiff%np_h, LaueDiff%np_v
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the dimension and pixels of the detector"
                  return
                end if

            Case("XZ_ORIGIN")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%xo, LaueDiff%zo
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading Xo and Zo of detector centre"
                  return
                end if
                read_orig=.true.

            Case("GN_CENTRE")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%ga_d, LaueDiff%nu_d
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading Gamma and Nu of detector centre"
                  return
                end if
                if (abs(LaueDiff%ga_d) > 0.0001 .or. abs(LaueDiff%nu_d) > 0.0001) LaueDiff%displaced=.true.

            Case("DISP_CENTRE")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%rOD
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading displacement of detector centre"
                  return
                end if
                if (abs(LaueDiff%rOD(1)) > 0.0001 .or. abs(LaueDiff%rOD(2)) > 0.0001  &
                   .or. abs(LaueDiff%rOD(3)) > 0.0001 ) LaueDiff%displaced=.true.

            Case("ROT_ORDER")
                LaueDiff%r_ord=adjustl(line(i+1:))

            Case("TILT_ANGLES")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%Tiltx_d, LaueDiff%Tilty_d, LaueDiff%Tiltz_d
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the Tilt Angles"
                  return
                end if
                if (abs(LaueDiff%Tiltx_d) > 0.0001 .or. abs(LaueDiff%Tilty_d) > 0.0001  &
                   .or. abs(LaueDiff%Tiltz_d) > 0.0001 ) LaueDiff%Tilted=.true.
            ! Limits
            Case("RESOL")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%d_min
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the resolution d_min value"
                  return
                end if
            Case("REVERT_PIX")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%invert
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading if the intensities should be inverted or not"
                  return
                end if
            Case("WAV_LIMITS") !First attempt to read central lambda
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%L_min, LaueDiff%L_max, LaueDiff%L_Central
                if(ier /= 0) then
                  read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%L_min, LaueDiff%L_max
                  if(ier /= 0) then
                     Error_Laue_Mod=.true.
                     Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the wavelength limits"
                     return
                  else
                    LaueDiff%L_Central=0.5*(LaueDiff%L_min+LaueDiff%L_max)
                  end if
                end if
            Case("GAM+_LIMITS")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%gap_min, LaueDiff%gap_max
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the Gamma(positive) limits"
                  return
                end if

            Case("GAM-_LIMITS")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%gan_min, LaueDiff%gan_max
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the Gamma(negative) limits"
                  return
                end if

            Case("NU_LIMITS")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%nu_min, LaueDiff%nu_max
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the Nu limits"
                  return
                end if

            Case("X_LIMITS")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%x_min, LaueDiff%x_max
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the X limits"
                  return
                end if

            Case("Z_LIMITS")
                read(unit=line(i+1:),fmt=*,iostat=ier) LaueDiff%z_min, LaueDiff%z_max
                if(ier /= 0) then
                  Error_Laue_Mod=.true.
                  Error_Mess_Laue_Mod="Error in file: "//trim(filenam)//", reading the Z limits"
                  return
                end if

            Case("FLIP")
                if(index(line,"H") /= 0)  LaueDiff%flip_hor=.true.
                if(index(line,"V") /= 0)  LaueDiff%flip_ver=.true.

         End Select
       End do

       if(.not. read_orig) then
         LaueDiff%xo=0.5*real(LaueDiff%Np_h)
         LaueDiff%zo=0.5*real(LaueDiff%Np_v)
         LaueDiff%dism=(/0.0,0.0/)
       else
         if(LaueDiff%dtype=="Cyl")then
           disp=(/ 0.5*real(LaueDiff%Np_h), 0.5*real(LaueDiff%Np_v) /) !Coordinates of the detector centre in pixels
           call Pix_To_Mil(LaueDiff,disp(1),disp(2),xm,zm)             !Coordinates in mm
           LaueDiff%dism=(/-xm,-zm/)  !displacement w.r.t. centre of the (xo,zo) origin in mm
         else
           LaueDiff%dism=(/0.0,0.0/)
         end if
       end if

       if(LaueDiff%dtype == "Oct") then  !Cyclops type ... Calculate D interms of H
          LaueDiff%D = LaueDiff%H/16.0/tan(pi/8.0_dp)
       End if

       if(.not. present(inp)) close(unit=lun)
       call Calc_RD_rOD(LaueDiff)

       return
    End Subroutine Read_Laue_Instrm

    !!----
    !!---- Subroutine Write_Laue_Instrm(LaueDiff,lun)
    !!----    Type(Laue_Instrument_Type),       intent(in) :: LaueDiff
    !!----    Integer                           intent(in) :: lun
    !!----
    !!----    Subroutine writing the characteristics
    !!----    of the a Laue instrument in the logical unit lun.
    !!----
    !!---- Created: June - 2010 (JRC)
    !!---- Updated: July - 2010 (JRC)
    !!
    Subroutine Write_Laue_Instrm(LaueDiff,lun)
       !---- Arguments ----!
       Type(Laue_Instrument_Type),       intent(in) :: LaueDiff
       Integer,                          intent(in) :: lun
       character(len=3) :: fliphv

       Write(unit=lun,fmt="(a)")           "INFO        "//trim(LaueDiff%info)
       Write(unit=lun,fmt="(a)")           "NAME        "//trim(LaueDiff%name)
       Write(unit=lun,fmt="(a)")           "DET_TYPE    "//LaueDiff%dtype
       Write(unit=lun,fmt="(a, f9.3)")     "DIST_DET    ", LaueDiff%D
       Write(unit=lun,fmt="(a,2f9.3,2i6)") "DIM_XZP     ", LaueDiff%H, LaueDiff%V, &
                                                           LaueDiff%np_h, LaueDiff%np_v
       if(LaueDiff%dtype == "Cyl")  &
       Write(unit=lun,fmt="(a,3f9.3)")     "DISP_CENTRE ", LaueDiff%rOD
       Write(unit=lun,fmt="(a,2f9.3)")     "GN_CENTRE   ", LaueDiff%ga_d, LaueDiff%nu_d
       Write(unit=lun,fmt="(2(a,2f9.1))")  "XZ_ORIGIN   ", LaueDiff%xo, LaueDiff%zo, &
       " raster units (pixels) => In mm units: ",LaueDiff%dism
       Write(unit=lun,fmt="(a)")           "ROT_ORDER   "//LaueDiff%r_ord
       Write(unit=lun,fmt="(a,3f9.3)")     "TILT_ANGLES ", LaueDiff%Tiltx_d, LaueDiff%Tilty_d, LaueDiff%Tiltz_d
       Write(unit=lun,fmt="(a, f9.3)")     "RESOL       ", LaueDiff%d_min
       Write(unit=lun,fmt="(a,2f9.3)")     "WAV_LIMITS  ", LaueDiff%L_min,  LaueDiff%L_max
       Write(unit=lun,fmt="(a,2f9.3)")     "GAM+_LIMITS ", LaueDiff%gap_min, LaueDiff%gap_max
       Write(unit=lun,fmt="(a,2f9.3)")     "GAM-_LIMITS ", LaueDiff%gan_min, LaueDiff%gan_max
       Write(unit=lun,fmt="(a,2f9.3)")     "NU_LIMITS   ", LaueDiff%nu_min, LaueDiff%nu_max
       Write(unit=lun,fmt="(a,2f9.3)")     "X_LIMITS    ", LaueDiff%x_min,  LaueDiff%x_max
       Write(unit=lun,fmt="(a,2f9.3)")     "Z_LIMITS    ", LaueDiff%z_min,  LaueDiff%z_max
       fliphv=" "
       if(LaueDiff%flip_hor) fliphv(1:1)="H"
       if(LaueDiff%flip_ver) fliphv(3:3)="V"
       if( LaueDiff%flip_hor .or. LaueDiff%flip_hor) then
          Write(unit=lun,fmt="(a)")  "FLIP        "//fliphv
       end if
       Write(unit=lun,fmt="(a)")           "END"
       return
    End Subroutine Write_Laue_Instrm


    !!---- Subroutine Set_Rotation_Matrix(ang,Rot)
    !!----   real, dimension(3),   intent( in) :: ang
    !!----   real, dimension(3,3), intent(out) :: Rot
    !!----
    !!----  Subroutine calculating the rotation matrix Rot corresponding to
    !!----  the application (active rotations) of the following succesive rotations:
    !!----
    !!----  Rot = Rx(ang(3)) . Ry(ang(2)) . Rz(ang(1))
    !!----
    !!----    Created: October 2009  (JRC)
    !!----    Updated: November 2009 (JRC)
    !!----

    Subroutine Set_Rotation_Matrix(ang,Rot)
      real, dimension(3),   intent( in) :: ang
      real, dimension(3,3), intent(out) :: Rot
      !Local variables
      real, dimension(3,3) :: Rx,Ry,Rz
      Rx=Matrix_Rx(ang(1),"D")
      Ry=Matrix_Ry(ang(2),"D")
      Rz=Matrix_Rz(ang(3),"D")
      Rot=Matmul(Rx,matmul(Ry,Rz))
      return
    End Subroutine Set_Rotation_Matrix

    !!----
    !!---- Function Gaussian_2D(x,xo,s)  result (g2d)
    !!----   real, dimension(2), intent (in) :: x
    !!----   real, dimension(2), intent (in) :: xo
    !!----   real, dimension(3), intent (in) :: s
    !!----
    !!----   Function calculating the profile at "x" of a normalised 2-D Gaussian function
    !!----   centred at position xo. The FWHM parameters are given in terms of the variance
    !!----   -covariance matrix. The three independen coefficients are stored in s as:
    !!----           S(1) =sigx  S(2)=sigy   S(3)=cov_xy
    !!----
    !!----    Created: October 2009   (JRC)
    !!----    Updated: November 2009  (JRC)
    !!----
    Pure Function Gaussian_2D(x,xo,s)  result (g2d)
      real, dimension(2), intent (in) :: x
      real, dimension(2), intent (in) :: xo
      real, dimension(3), intent (in) :: s   !S(1) =sigx  S(2)=sigy   S(3)=cov_xy
      real :: g2d
      ! Local variables
      real :: ex, det, rho

      g2d=0.0
      det= abs( s(1)*s(2) - s(3)*s(3) )
      if(det < 1.0e-8) return
      det=1.0/(sqrt(det)*2.0*pi)
      rho=s(3)/sqrt(s(1)*s(2))
      ex = ( (x(1)-xo(1))/s(1) )**2
      ex = ( (x(2)-xo(2))/s(2) )**2  + ex
      ex = (-2.0*rho*(x(1)-xo(1))*(x(2)-xo(2))/s(1)/s(2) ) + ex
      g2d= det* exp(-0.5*ex/(1.0-rho*rho))
      return
    End Function Gaussian_2D

    !!----
    !!---- Function Gaussian_2Da(x,xo,s)  result (g2d)
    !!----   real, dimension(2), intent (in) :: x
    !!----   real, dimension(2), intent (in) :: xo
    !!----   real, dimension(4), intent (in) :: s
    !!----
    !!----   Function calculating the profile at "x" of a normalised 2-D Gaussian function
    !!----   centred at position xo. The FWHM parameters are given in terms of the ellipse
    !!----   axes and the rotation angle Theta. The four independent coefficients are stored in s as:
    !!----           S(1) = a,  S(2)= b,   S(3)= Theta,  S(4)= f
    !!----   The half-axes a,b, correspond to the ellipse obtained by the intersection with
    !!----   a plane at a height h equal to h=1/f of the peak height.
    !!----
    !!----    Created: June 2012   (JRC)
    !!----    Updated: June 2012   (JRC)
    !!----
    Pure Function Gaussian_2Da(x,xo,s)  result (g2d)
      real, dimension(2), intent (in) :: x
      real, dimension(2), intent (in) :: xo
      real, dimension(4), intent (in) :: s   !S(1) =a  S(2)=b   S(3)=theta  S(4)=f
      real :: g2d
      ! Local variables
      real :: ex, det, a,b,c,aa,bb,lf,a2,b2,st,ct,st2,ct2

      g2d=0.0
      lf=log(S(4))
      aa=s(1); bb=s(2); st=sind(s(3)); ct=cosd(s(3))
      a2=aa*aa; b2=bb*bb; st2=st*st; ct2=ct*ct
      a=st2/a2+ct2/b2; b=ct2/a2+st2/b2; c=2.0*st*ct*(1.0/a2-1.0/b2)
      det= lf/(aa*bb*pi)
      if(det < 1.0e-8) return
      ex = a*( (x(1)-xo(1)) )**2 + b*( (x(2)-xo(2)) )**2  +  &
           c*( (x(1)-xo(1))*(x(2)-xo(2)) )
      g2d= det * exp( -lf*ex )
      return
    End Function Gaussian_2Da
    !!----
    !!---- Pure Function Ren_Moffat(x,z,P)  result (prv)
    !!----   real,                          intent (in) :: x,z
    !!----   Type(Ren_Moffat_Profile_type), intent (in) :: P
    !!----   real                                       :: prv
    !!----
    !!----   Function calculating the profile at "x,z" of a 2-D profile function
    !!----   proposed by Zhong Ren and Keith Moffat (J. Appl.Cryst. 28, 461-481, 1995).
    !!----   The position and shape parameters are packed in the Ren_Moffat_Profile_type, P.
    !!----   This function is not normalised, the integral should be calculated numerically.
    !!----
    !!----    Created:   July 2010  (JRC)
    !!----    Updated: August 2010  (JRC)
    !!----
    Pure Function Ren_Moffat(x,z,P)  result(prv)
      real,                          intent(in) :: x,z
      Type(Ren_Moffat_Profile_type), intent(in) :: P
      real                                      :: prv
      ! Local variables
      real :: ex, dx, dz, a, b, phi, car, sar, cA, cB
      prv=0.0
      ! Calculation of phi
      dx=x-p%xp ; dz=z-p%zp
      phi=atan2d(p%zp,p%xp)+p%eps
      car=cosd(phi) ; sar=sind(phi)
      ! Calculation of a, b
      a=p%a + p%sa*dx + p%ta*dz ; b=p%b + p%sb*dx + p%tb*dz
      !Calculation of exponential components
      dx = dx+p%dx ; dz = dz+p%dz
      cA= ( dx*car + dz*sar)/a
      cB= (-dx*sar + dz*car)/b
      cA= (cA * cA)**p%ga
      cB= (cB * cB)**p%gb
      ex=cA+cB
      prv=exp(-ex)
      return
    End Function Ren_Moffat


    !!----
    !!---- Subroutine Set_Angular_Matrix(L,resol,A,Theta,Phi)
    !!----   type(Laue_Ref_List_Type),        intent(in)     :: L
    !!----   real,                            intent(in)     :: resol
    !!----   integer(kind=1), dimension(:,:), intent(in out) :: A
    !!----   real, optional,                  intent(in)     :: Theta,Phi
    !!----
    !!----   Subroutine setting up a 2D-matrix A of integers in which a mapping is done
    !!----   between the unitary scattering vectors spherical components of the calculated
    !!----   reflections contained in the list L and the elements of the matrix.
    !!----   The matrix is indexed as A(i,j), i=0,....180 and j=0,359 for a resolution of
    !!----   resol=1 degree. The value of the matrix is zero except for spherical angles
    !!----   corresponding to calculated (or observed) reflections within the resolution resol.
    !!----
    !!----    Created: January 2010  (JRC)
    !!----    Updated: January 2010  (JRC)
    !!----
    Subroutine Set_Angular_Matrix(L,resol,A,Theta,Phi)
      type(Laue_Ref_List_Type),        intent(in)     :: L
      real,                            intent(in)     :: resol
      integer(kind=1), dimension(:,:), intent(in out) :: A
      real, optional,                  intent(in)     :: Theta,Phi   !Rotation angles applied to calculated values
      ! local variables
      integer :: i,j,n,li,lj
      real    :: th,ph, tt, pp
      A=0
      th=0.0 ; ph = 0.0
      if(present(Theta) .and. present(Phi)) then
        th=Theta
        ph=Phi
      end if
      li=size(A,1)
      lj=size(A,2)
      do n=1, L%nref
         tt=L%LR(n)%stheta+th
         pp=L%LR(n)%sphi+ph
         if(tt >= 180.0) tt = 360.0 - tt
         if(pp >= 360.0) pp = pp - 360.0
         i=nint(tt/resol)
         j=nint(pp/resol)
         if( i > li .or. j > lj .or. i < 0 .or. j < 0) cycle
         A(i,j)=1
      end do
    End Subroutine Set_Angular_Matrix

    !!----
    !!---- Subroutine Read_Experimental_Laue_spots(lun,LaueDiff,L,Spindle,ok,mess,file_dat)
    !!----   integer,                      intent(in)  :: lun
    !!----   Type(Laue_Instrument_Type),   intent(in ) :: LaueDiff
    !!----   type(Laue_Ref_List_Type),     intent(out) :: L  !Experimental Laue data
    !!----   real,                         intent(out) :: Spindle
    !!----   logical,                      intent(out) :: ok
    !!----   character(len=*),             intent(out) :: mess
    !!----   Type(file_list_type),optional,intent(in)  :: file_dat
    !!----
    !!----
    !!----   Subroutine for reading experimental spot coordinates that are stored in
    !!----   the object L of type Laue_Ref_List_Type.
    !!----   The subroutine reads from the logical unit lun or from the file_list_type object
    !!----   file_dat (if given), looking for the keyword "xz_spots".
    !!----   Once it is found, the subroutine reads the number of reflections Nspots and then
    !!----   reads the Nspots lines in free format containing x,z coordinates.
    !!----   The Norient item is also read in the same line (following Nspots) if it is given
    !!----   as zero (or not given) then Norient=Nspots, otherwise on output Norient is the
    !!----   number of first input reflections to be considered as "orienting"
    !!----
    !!----    Created: January 2010 (JRC)
    !!----    Updated: January 2010 (JRC)
    !!----
    Subroutine Read_Experimental_Laue_spots(lun,LaueDiff,L,Spindle,ok,mess,file_dat)
      integer,                      intent(in)  :: lun
      Type(Laue_Instrument_Type),   intent(in ) :: LaueDiff
      type(Laue_Ref_List_Type),     intent(out) :: L       !Experimental Laue data
      real,                         intent(out) :: Spindle
      logical,                      intent(out) :: ok
      character(len=*),             intent(out) :: mess
      Type(file_list_type),optional,intent(in)  :: file_dat
      !---- Local variables ----!
      integer :: i,j,ier,nspots,ini,zop,ic
      character(len=130) :: line
      character(len=20),dimension(10) :: dire

      mess=" "
      ok=.false.
      nspots=0

      if(present(file_dat)) then
        do i=1,file_dat%nlines
           line=adjustl(l_case(file_dat%line(i)))
           if(line(1:8)=="xz_spots") then
             call Getword(Line, Dire, Ic)
             read(unit=dire(2),fmt=*,iostat=ier) nspots
             if(ier /= 0) then
               mess=" Error reading the number of spots following xz_spots keyword!  "
               return
             end if
             if(Ic > 2) then
               read(unit=dire(3),fmt=*,iostat=ier) spindle
               if(ier /= 0) spindle=0.0
             else
               spindle=0.0
             end if
             ini=i
             exit
          end if
        end do

      else

        rewind(unit=lun)
        do
          read(unit=lun,fmt="(a)",iostat=ier) line
          if(ier /= 0) then
             mess=" Keyword: xz_spots ... not found in file!  "
             return
          end if
          line=adjustl(l_case(line))
          if(line(1:8)=="xz_spots") then
             call Getword(Line, Dire, Ic)
             read(unit=dire(2),fmt=*,iostat=ier) nspots
             if(ier /= 0) then
               mess=" Error reading the number of spots following xz_spots keyword!  "
               return
             end if
             if(Ic > 2) then
               read(unit=dire(3),fmt=*,iostat=ier) spindle
               if(ier /= 0) spindle=0.0
             else
               spindle=0.0
             end if
             exit
          end if
        end do
      end if
      if(nspots == 0) then
        mess=" No spots found in file! "
        return
      end if

      call Allocate_Laue_Ref_list(L,nspots)

      !Fill as much as possible the reflection list taking the information from the instrument
      if(present(file_dat)) then
        do i=1,L%nref
           j=ini+i
           read(unit=file_dat%line(j),fmt=*,iostat=ier) L%LR(i)%x, L%LR(i)%z, L%LR(i)%Obs_Int
             if(ier /= 0) then
               write(unit=mess,fmt="(a,i3,a)") " Error reading the ",i,"th spot coordinates"
               return
             end if
             !write(unit=*,fmt="(i6,2f10.4)")  i,  L%LR(i)%x, L%LR(i)%z
           call Calc_Spherical_angles_from_xz(LaueDiff,L%LR(i),zop)  !x,z,gamma,nu, theta, phi and zv
        end do
      else
        do i=1,L%nref
           read(unit=lun,fmt=*,iostat=ier) L%LR(i)%x, L%LR(i)%z, L%LR(i)%Obs_Int
             if(ier /= 0) then
               write(unit=mess,fmt="(a,i3,a)") " Error reading the ",i,"th spot coordinates"
               return
             end if
           call Calc_Spherical_angles_from_xz(LaueDiff,L%LR(i),zop)  !x,z,gamma,nu, theta, phi and zv
        end do
      end if
      ok=.true.
      return
    End Subroutine Read_Experimental_Laue_spots


    !!----
    !!---- Subroutine Matching_1D(va,vb,na,nb,tol,ia,ib,nc)
    !!----   real,dimension(:),             intent(in)  :: va,vb
    !!----   integer,                       intent(in)  :: na,nb
    !!----   real,                          intent(in)  :: tol
    !!----   integer,dimension(:),          intent(out) :: ia,ib
    !!----   integer,                       intent(out) :: nc
    !!----
    !!----   Procedure for constructing the pointer of coincidences between two
    !!----   sets of "na" and "nb" real numbers stored in the arrays "va" and "vb", respectively.
    !!----   Two values are coincident if there difference is smaller in absolute value
    !!----   than "tol". The program numbers the coincidences with the index "nc" (that in output
    !!----   is equal to the total number of coincidences). The integer arrays "ia" and "ib",
    !!----   contain the indices of the values of "va" and "vb" that coincides.
    !!----
    !!----    Created: January 2010 (JRC)
    !!----    Updated: January 2010
    !!----
    Subroutine Matching_1D(va,vb,na,nb,tol,ia,ib,nc)
      real,dimension(:),             intent(in)  :: va,vb
      integer,                       intent(in)  :: na,nb
      real,                          intent(in)  :: tol
      integer,dimension(:),          intent(out) :: ia,ib
      integer,                       intent(out) :: nc
      !
      integer :: i,j

      ia=0; ib=0
      nc=0
      if(na > nb) return
      do i=1,na
        do j=1,nb
          if(abs(va(i)-vb(j)) <= tol) then
            nc=nc+1
            ia(nc)= i
            ib(nc)= j
          end if
        end do
      end do
      return
    End Subroutine matching_1D
    !!----
    !!---- Subroutine Matching_2D(va,vb,na,nb,tol,ia,ib,nc)
    !!----   real,dimension(:,:),           intent(in)  :: va,vb
    !!----   integer,                       intent(in)  :: na,nb
    !!----   real,                          intent(in)  :: tol
    !!----   integer,dimension(:),          intent(out) :: ia,ib
    !!----   integer,                       intent(out) :: nc
    !!----
    !!----   Same procedure as before except that the arrays "va" and "vb" are 2-D
    !!----   Expected to be allocated outside the procedure as va(2,na) and vb(2,nb)
    !!----   Here the number of coincidences cannot exceed na (see the exit statement)
    !!----
    !!----    Created: January 2010 (JRC)
    !!----    Updated: January 2010
    !!----
    Subroutine Matching_2D(va,vb,na,nb,tol,ia,ib,nc)
      real,dimension(:,:),           intent(in)  :: va,vb
      integer,                       intent(in)  :: na,nb
      real,                          intent(in)  :: tol
      integer,dimension(:),          intent(out) :: ia,ib
      integer,                       intent(out) :: nc
      !
      integer :: i,j

      ia=0; ib=0
      nc=0
      if(na > nb) return
      do i=1,na
        do j=1,nb
          if(abs(va(1,i)-vb(1,j)) <= tol .and. abs(va(2,i)-vb(2,j)) <= tol) then
            nc=nc+1
            ia(nc)= i
            ib(nc)= j
            exit
          end if
        end do
      end do

      return
    end subroutine matching_2D

    !!----
    !!---- subroutine matching_4V(va1,va2,vb1,vb2,na,nb,tol,ia,ib,nc)
    !!----   real,dimension(:),             intent(in)  :: va1,va2,vb1,vb2
    !!----   integer,                       intent(in)  :: na,nb
    !!----   real,                          intent(in)  :: tol
    !!----   integer,dimension(:),          intent(out) :: ia,ib
    !!----   integer,                       intent(out) :: nc
    !!----
    !!----   Same procedure as before except that the arrays "va1,2" and "vb1,2" are 1-D
    !!----   Expected to be allocated outside the procedure as va1(na),va2(na),vb1(nb) and vb2(nb)
    !!----   Here the number of coincidences cannot exceed na (see the exit statement)
    !!----
    !!----    Created: January 2010 (JRC)
    !!----    Updated: January 2010
    !!----
    Subroutine Matching_4V(va1,va2,vb1,vb2,na,nb,tol,ia,ib,nc)
      real,dimension(:),             intent(in)  :: va1,va2,vb1,vb2
      integer,                       intent(in)  :: na,nb
      real,                          intent(in)  :: tol
      integer,dimension(:),          intent(out) :: ia,ib
      integer,                       intent(out) :: nc
      !
      integer :: i,j

      ia=0; ib=0
      nc=0
      if(na > nb) return
      do i=1,na
        do j=1,nb
          if(abs(va1(i)-vb1(j)) <= tol .and. abs(va2(i)-vb2(j)) <= tol) then
            nc=nc+1
            ia(nc)= i
            ib(nc)= j
            exit
          end if
        end do
      end do

      return
    End Subroutine matching_4V

    !!----
    !!---- Subroutine Get_Orient_from_xz_spots(Cell,SpG,LaueDiff,ORefL,OrCnd,S,mess,lun)
    !!----   Type (Crystal_Cell_Type),   intent(in)      :: Cell
    !!----   Type (space_group_type),    intent(in)      :: SpG
    !!----   Type (Laue_Instrument_Type),intent(in)      :: LaueDiff
    !!----   Type (Laue_Ref_list_type),  intent(in out)  :: ORefL
    !!----   Type (Orient_cond_Type),    intent(in)      :: OrCnd
    !!----   Type (Orient_Solution_List),intent(out)     :: S
    !!----   character(len=*),           intent(out)     :: mess
    !!----   integer, optional,          intent(in)      :: lun !Logical unit for printing
    !!----
    !!----   Subroutine providing the UB-matrix and orienting angles of
    !!----   a crystal (supposed to be at spindle angle equal to zero). The unit cell
    !!----   is known and stored in the object "Cell". The space group is stored in
    !!----   in the object SpG. The instrument characteristics are stored in LaueDiff.
    !!----   The observed reflections are stored in ORefL. The conditions for the
    !!----   algorithm are stored in OrCnd and the solution list in S. If the logical
    !!----   unit lun is provided, the results are written in a file previously
    !!----   assigned to this logical unit (See documentation of the algorithm and
    !!----   the definition of the types for more details)
    !!----
    !!----   If the orienting device of the crystal has non-zero values of the
    !!----   angles and the total rotation matrix is R (for instance if only a
    !!----   spindle angle is available as a motor turning around the zL-axis R=Rz(Phi))
    !!----   the UB-matrix provided by this subroutine is in fact M = R.UB(B_L), where
    !!----   the matrix UB(B-L) is the Busing-Levy UB-matrix. This is then obtained
    !!----   as:   UB(B-L) = R^(-1) M.
    !!----
    !!----    Created: May  2010 (JRC)
    !!----    Updated: June 2010 (JRC)
    !!----


    Subroutine Get_Orient_from_xz_spots(Cell,SpG,LaueDiff,ORefL,OrCnd,S,mess,lun)
      Type (Crystal_Cell_Type),   intent(in)      :: Cell
      Type (space_group_type),    intent(in)      :: SpG
      Type (Laue_Instrument_Type),intent(in)      :: LaueDiff
      Type (Laue_Ref_list_type),  intent(in out)  :: ORefL
      Type (Orient_cond_Type),    intent(in)      :: OrCnd
      Type (Orient_Solution_List),intent(out)     :: S
      character(len=*),           intent(out)     :: mess
      integer, optional,          intent(in)      :: lun !Logical unit for printing

      !---- Local variables ----!
      Type (Refl_List_type)               :: Rfl
      Type (Laue_Ref_Type)                :: Ref
      Type (Reflection_List_Type)         :: hkl
      Type (Laue_Ref_list_type)           :: RefL
      real                                :: stlmax     !Minimum and Maximum Sin(Theta)/Lambda
      real                                :: rms, dist
      real,    dimension(3,3)             :: ub,bm,binv,Um
      real,    dimension(3)               :: h1,h2
      integer, dimension(3)               :: ord,hcp
      integer, dimension(:),  allocatable :: ind,ia,ib,io,ic
      integer, dimension(:,:),allocatable :: irefo,irefc
      integer                             :: MaxNumRef,i,j,k, n, j1,j2,ip, ier,minindx
      integer                             :: m, nsol, nobs,ncalc, nc, ns, nxz, is, maxref
      real,    dimension(:),  allocatable :: ang_calc, ang_obs
      real,    dimension(1)               :: angx

       stlmax=1.0/(2.0*LaueDiff%d_min)
       ord=(/3,2,1/)
       MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult=SpG%Multip)
       MaxNumRef=MaxNumRef*Spg%NumOps*max(Spg%Centred,1)
       call Hkl_gen_sxtal(cell,SpG,0.0,stlmax,MaxNumRef,hkl,ord)
       if(LaueDiff%L_max < 2.0 * LaueDiff%d_min) then  !ITC vol C, p. 28
         n= 0.25*pi*(LaueDiff%L_max-LaueDiff%L_min)*Cell%CellVol/real(Spg%NumLat)/LaueDiff%d_min**4
       else
         n = 4.0/3.0*pi*((1.0/LaueDiff%L_min)**3-(1.0/LaueDiff%L_max)**3)*Cell%CellVol/real(Spg%NumLat)
         n = min(hkl%nref,n)
       end if
       ns=n

       call Allocate_Laue_Ref_list(RefL,ns)   !Allocate the Laue reflections that may be stimulated

       call genb(Cell,Bm)  !Calculation of the Busing-Levy B-matrix (standard orientation, U=I)
       binv=invert_A(Bm)   !Inverse of B-matrix, used for calculating U

       if(allocated(Rfl%hkl)) deallocate(Rfl%hkl)
       allocate(Rfl%hkl(hkl%nref))
       if(allocated(ind)) deallocate(ind)
       allocate(ind(hkl%nref))

       !Order generated reflections by ascending sinTheta/Lambda and create a Reflect_List_type
       call sort(hkl%Ref(:)%s,hkl%nref,ind)
       m=0

       !Select from the hkl object a list of reflections with co-prime integers up to a maximum index
       !generating an appropriate integer pointer

       do j=1,hkl%nref
          i=ind(j)
          hcp=hkl%Ref(i)%h
          if(maxval(abs(hcp)) > OrCnd%maxind) cycle
          if(OrCnd%coprime .and. .not. Co_Prime(hcp,OrCnd%maxind)) cycle
          if(hkl%Ref(i)%s > stlmax) cycle
          m=m+1
          Rfl%hkl(m)%h=hcp
          Rfl%hkl(m)%hc=matmul(Bm,real(hcp))
          Rfl%hkl(m)%ds=0.5/hkl%Ref(i)%s
       end do

       Rfl%nref=m  !the Reflect_List_Type Rfl-variable is ordered with ascending sinTheta/Lambda
       !Calculate the inter-reflection angles
       m=Rfl%nref*(Rfl%nref-1)/2
       if(allocated(ang_calc)) deallocate(ang_calc)
       allocate(ang_calc(m))
       if(allocated(irefc)) deallocate(irefc)
       allocate(irefc(2,m))
       m=0
       do i=1,Rfl%nref-1
         do j=i+1,Rfl%nref
            m=m+1
            ang_calc(m)=Angle_UV(Rfl%hkl(i)%hc,Rfl%hkl(j)%hc)
            irefc(1,m)=i
            irefc(2,m)=j
         end do
       end do
       ncalc=m

       maxref=OrCnd%nmax_ref
       if(maxref > ORefl%nref)  maxref = ORefl%nref
       !Calculate the angles between experimental spots
       if(allocated(io)) deallocate (io)
       allocate(io(maxref))

       !Calculate the inter-reflection angles
       m=maxref*(maxref-1)/2
       if(allocated(ang_obs)) deallocate(ang_obs)
       allocate(ang_obs(m))
       if(allocated(irefo)) deallocate(irefo)
       allocate(irefo(2,m))

       m=0
       do i=1,OrCnd%Norient-1
         do j=i+1,OrCnd%Norient
            m=m+1
            ang_obs(m)=Angle_Uv(ORefL%LR(i)%zv,ORefL%LR(j)%zv)
            irefo(1,m)=i
            irefo(2,m)=j
         end do
       end do
       nobs=m

       if(allocated(ia)) deallocate (ia)
       if(allocated(ib)) deallocate (ib)
       allocate(ia(ncalc),ib(ncalc))

       if(allocated(S%Sol)) deallocate(S%Sol)
       allocate(S%Sol(OrCnd%nmax_sol))
       if(allocated(S%ind)) deallocate(S%ind)
       allocate(S%ind(OrCnd%nmax_sol))

       nsol=0
       minindx=(3*ORefL%nref)/4
       if(.not. OrCnd%strict) minindx=4
       do_obs: do m=1,nobs
         angx(1)= ang_obs(m)  !Testing one angle at a time corresponds to selecting 2 orienting reflections

         if(angx(1) < OrCnd%ang_min .or. angx(1) > OrCnd%ang_max) cycle do_obs
         j1=irefo(1,m)
         j2=irefo(2,m)
         call matching_1D(angx,ang_calc,1,ncalc,OrCnd%angtol,ia,ib,nc)

         if(nc > 0) then
           do_coinc: do i=1,nc    !Testing all coincidences
              j= irefc(1,ib(i))   !Reflections corresponding to a given angle
              k= irefc(2,ib(i))   !    "           "                "
              h1=Rfl%hkl(j)%h
              h2=Rfl%hkl(k)%h
              call GenUB(Bm,h1,h2,ORefL%LR(j1)%zv,ORefL%LR(j2)%zv,ub, ier)
              if(ier /= 0) cycle do_coinc
              ! A matrix UB has been obtained ... Verify that all reflections can be indexed
              ! and it is not equivalent to other previous solution
              if(nsol >= 1) then
                do n=1,nsol
                 if(sum(abs(abs(ub)-abs(S%Sol(n)%ub))) < 0.002) cycle do_coinc
                end do
              end if
               n=0
               call Init_Laue_Ref(Ref)
               do ip=1,hkl%nref   !Generate the reflections corresponding to the UB matrix
                 Ref%h=hkl%Ref(ip)%h
                 Call Calc_Laue_Spot_XZ_Angles(LaueDiff,Ref,UB)
                 if(Ref%stats == 0) then  !Only for the allowed reflections in the given orientation
                   n=n+1
                   RefL%LR(n)=Ref
                 end if
               end do
               RefL%nref=n  !Number of visible spots in the current orientation

               if(allocated(ic)) deallocate (ic)
               allocate(ic(n))
               !Below we test all observed reflections
               call matching_4V(ORefL%LR(:)%x,ORefL%LR(:)%z,RefL%LR(:)%x,RefL%LR(:)%z,maxref,n,OrCnd%dmmtol,io,ic,nxz)

               if( nxz > minindx ) then ! Probable solution found  (indexing more that 3/4 of observed reflections)
                 nsol=nsol+1
                 if(nsol > OrCnd%nmax_sol) then
                  mess="Maximum number of solution reached"
                  nsol=OrCnd%nmax_sol
                  exit do_obs
                 end if
                 if(present(lun)) then
                    write(unit=lun,fmt="(a,i6,a,3i4,tr4,3i4)") " => Solution #",nsol, "  => Orienting reflections: ",&
                                                          Rfl%hkl(j)%h,Rfl%hkl(k)%h
                    write(unit=lun,fmt="(a/3(/tr10,3f12.5))") " =>  UB-matrix: ",transpose(UB)
                    write(unit=lun,fmt="(a)") &
                    "    Spot#      Xobs      Zobs     Xcalc     Zcalc      Dist       H    K    L"
                 end if
                 rms=0.0
                 do is=1,nxz
                    j=io(is)
                    k=ic(is)
                    ORefL%LR(j)%h=RefL%LR(k)%h  ! Put the proper indices  removed    /RefL%LR(k)%nodal
                    dist=(ORefL%LR(j)%x-RefL%LR(k)%x)**2+(ORefL%LR(j)%z-RefL%LR(k)%z)**2
                    rms=rms+dist
                    dist=sqrt(dist)
                   if(present(lun)) write(unit=lun,fmt="(i9,5f10.2,tr3,3i5)") j,ORefL%LR(j)%x,ORefL%LR(j)%z, RefL%LR(k)%x    ,&
                                               RefL%LR(k)%z ,dist,nint(ORefL%LR(j)%h)
                 end do
                 rms=sqrt(rms/real(nxz))
                 S%Sol(nsol)%rms= rms
                 S%Sol(nsol)%ub = UB
                 S%Sol(nsol)%num_indx = nxz

                 !write(unit=*,fmt="(a,f8.4,a)") " => Root mean square error: ",rms," mm"
                 if(present(lun)) write(unit=lun,fmt="(a,f8.4,a/a,i4)") " => Root mean square error: ",rms," mm", &
                                                                        " => Number of indexed spots: ",nxz
               end if
           end do do_coinc
           if(nsol == 0 ) cycle
           if(OrCnd%exhaust /= 0) then
             cycle
           else
             exit      ! Solutions with two orienting reflections have been found
           end if
         end if
       end do do_obs

       S%nsol=nsol
       if(nsol > 0) then
          call sort(S%Sol(:)%rms,nsol,S%ind(:))
          write(unit=mess,fmt="(a,i4)") " Number of solutions found: ",nsol
       else
          mess= " No solution has been found! change the conditions ... think! "
       end if

       !Complete the solutions by calculting the orienting angles
       Select Case(OrCnd%ang_type)
          case("XYZ")
            do i=1,nsol
              Um=matmul(S%Sol(i)%ub,binv) !Rotation matrix from which we can calculate the angles
              call get_Phi_xyz(Um,S%Sol(i)%ang(1),S%Sol(i)%ang(2),S%Sol(i)%ang(3),"D")
            end do

          case("EUL")
            do i=1,nsol
              Um=matmul(S%Sol(i)%ub,binv) !Rotation matrix from which we can calculate the angles
              !call get_PhiTheChi(Um,Omega,Chi,Phi,Code) !Phix, Phiy, Phiz
              call get_PhiTheChi(Um,S%Sol(i)%ang(1),S%Sol(i)%ang(2),S%Sol(i)%ang(3),"D")
            end do

       End Select
       return
    End Subroutine Get_Orient_from_xz_spots

    Subroutine Get_Orientation_from_xz_spots(ns,Bm,Binv,LaueDiff,hkl,ind,ORefL,OrCnd,S,mess,lun)
      Integer,                    intent(in)      :: ns !Number of estimated excited reflections
      Real, dimension(3,3),       intent(in)      :: Bm,Binv  !B-L matrices
      Type (Laue_Instrument_Type),intent(in)      :: LaueDiff
      Type (Reflection_List_Type),intent(in)      :: hkl
      Integer, dimension(:),      intent(in)      :: ind     !Integer pointer according to ordering of hkl
      Type (Laue_Ref_list_type),  intent(in out)  :: ORefL
      Type (Orient_cond_Type),    intent(in)      :: OrCnd
      Type (Orient_Solution_List),intent(out)     :: S
      character(len=*),           intent(out)     :: mess
      integer, optional,          intent(in)      :: lun !Logical unit for printing

      !---- Local variables ----!
      Type (Refl_List_type)               :: Rfl
      Type (Laue_Ref_Type)                :: Ref
      Type (Laue_Ref_list_type)           :: RefL
      real                                :: stlmax     !Minimum and Maximum Sin(Theta)/Lambda
      real                                :: rms, dist
      real,    dimension(3,3)             :: ub,Um
      real,    dimension(3)               :: h1,h2
      integer, dimension(3)               :: hcp
      integer, dimension(:),  allocatable :: ia,ib,io,ic
      integer, dimension(:,:),allocatable :: irefo,irefc
      integer                             :: i,j,k, n, j1,j2,ip, ier
      integer                             :: m, nsol, nobs,ncalc, nc, nxz, is, maxref
      real,    dimension(:),  allocatable :: ang_calc, ang_obs
      real,    dimension(1)               :: angx

      call Allocate_Laue_Ref_list(RefL,ns)   !Allocate the Laue reflections that may be stimulated

       if(allocated(Rfl%hkl)) deallocate(Rfl%hkl)
       allocate(Rfl%hkl(hkl%nref))

       m=0

       !Select from the hkl object a list of reflections with co-prime integers up to a maximum index
       !generating an appropriate integer pointer
       stlmax=1.0/(2.0*LaueDiff%d_min)
       do j=1,hkl%nref
          i=ind(j)
          hcp=hkl%Ref(i)%h
          if(maxval(abs(hcp)) > OrCnd%maxind) cycle
          if(OrCnd%coprime .and. .not. Co_Prime(hcp,OrCnd%maxind)) cycle
          if(hkl%Ref(i)%s > stlmax) cycle
          m=m+1
          Rfl%hkl(m)%h=hcp
          Rfl%hkl(m)%hc=matmul(Bm,real(hcp))
          Rfl%hkl(m)%ds=0.5/hkl%Ref(i)%s
       end do

       Rfl%nref=m  !the Reflect_List_Type Rfl-variable is ordered with ascending sinTheta/Lambda
       !Calculate the inter-reflection angles
       m=Rfl%nref*(Rfl%nref-1)/2
       if(allocated(ang_calc)) deallocate(ang_calc)
       allocate(ang_calc(m))
       if(allocated(irefc)) deallocate(irefc)
       allocate(irefc(2,m))
       m=0
       do i=1,Rfl%nref-1
         do j=i+1,Rfl%nref
            m=m+1
            ang_calc(m)=Angle_UV(Rfl%hkl(i)%hc,Rfl%hkl(j)%hc)
            irefc(1,m)=i
            irefc(2,m)=j
         end do
       end do
       ncalc=m


       !Calculate the angles between experimental spots
       maxref=OrCnd%nmax_ref
       if(maxref > ORefl%nref)  maxref = ORefl%nref
       if(allocated(io)) deallocate (io)
       allocate(io(maxref))

       !Calculate the inter-reflection angles
       m=maxref*(maxref-1)/2
       if(allocated(ang_obs)) deallocate(ang_obs)
       allocate(ang_obs(m))
       if(allocated(irefo)) deallocate(irefo)
       allocate(irefo(2,m))

       m=0
       do i=1,OrCnd%Norient-1
         do j=i+1,OrCnd%Norient
            m=m+1
            ang_obs(m)=Angle_Uv(ORefL%LR(i)%zv,ORefL%LR(j)%zv)
            irefo(1,m)=i
            irefo(2,m)=j
         end do
       end do
       nobs=m
       if(allocated(ia)) deallocate (ia)
       if(allocated(ib)) deallocate (ib)
       allocate(ia(ncalc),ib(ncalc))

       if(allocated(S%Sol)) deallocate(S%Sol)
       allocate(S%Sol(OrCnd%nmax_sol))
       if(allocated(S%ind)) deallocate(S%ind)
       allocate(S%ind(OrCnd%nmax_sol))

       nsol=0

       do_obs: do m=1,nobs
         angx(1)= ang_obs(m)  !Testing one angle at a time corresponds to selecting 2 orienting reflections
         if(angx(1) < OrCnd%ang_min .or. angx(1) > OrCnd%ang_max) cycle do_obs
         j1=irefo(1,m)
         j2=irefo(2,m)
         call matching_1D(angx,ang_calc,1,ncalc,OrCnd%angtol,ia,ib,nc)

         if(nc > 0) then
           do_coinc: do i=1,nc    !Testing all coincidences
              j= irefc(1,ib(i))   !Reflections corresponding to a given angle
              k= irefc(2,ib(i))   !    "           "                "
              h1=Rfl%hkl(j)%h
              h2=Rfl%hkl(k)%h
              call GenUB(Bm,h1,h2,ORefL%LR(j1)%zv,ORefL%LR(j2)%zv,ub, ier)
              if(ier /= 0) cycle do_coinc
              ! A matrix UB has been obtained ... Verify that all reflections can be indexed
              ! and it is not equivalent to other previous solution
              if(nsol >= 1) then
                do n=1,nsol
                 if(sum(abs(abs(ub)-abs(S%Sol(n)%ub))) < 0.002) cycle do_coinc
                end do
              end if
               n=0
               call Init_Laue_Ref(Ref)
               do ip=1,hkl%nref   !Generate the reflections corresponding to the UB matrix, z1 vectors
                 Ref%h=hkl%Ref(ip)%h
                 Call Calc_Laue_Spot_XZ_Angles(LaueDiff,Ref,UB)
                 if(Ref%stats == 0) then  !Only for the allowed reflections in the given orientation
                   n=n+1
                   RefL%LR(n)=Ref
                 end if
               end do
               RefL%nref=n  !Number of visible spots in the current orientation

               if(allocated(ic)) deallocate (ic)
               allocate(ic(n))
               !Below we test all observed reflections
               call matching_4V(ORefL%LR(:)%x,ORefL%LR(:)%z,RefL%LR(:)%x,RefL%LR(:)%z,maxref,n,OrCnd%dmmtol,io,ic,nxz)

               if( nxz > (3*maxref)/4) then ! Probable solution found  (indexing more that 3/4 of observed reflections)
                 nsol=nsol+1
                 if(nsol > OrCnd%nmax_sol) then
                  mess="Maximum number of solution reached"
                  nsol=OrCnd%nmax_sol
                  exit do_obs
                 end if
                 if(present(lun)) then
                    write(unit=lun,fmt="(a,i6,a,3i4,tr4,3i4)") " => Solution #",nsol, "  => Orienting reflections: ",&
                                             Rfl%hkl(j)%h,Rfl%hkl(k)%h
                    write(unit=lun,fmt="(a/3(/tr10,3f12.5))") " =>  UB-matrix: ",transpose(UB)
                    write(unit=lun,fmt="(a)") &
                    "    Spot#      Xobs      Zobs     Xcalc     Zcalc      Dist       H    K    L"
                 end if
                 rms=0.0
                 do is=1,nxz
                    j=io(is)
                    k=ic(is)
                    ORefL%LR(j)%h=RefL%LR(k)%h       !/RefL%LR(k)%nodal
                    dist=(ORefL%LR(j)%x-RefL%LR(k)%x)**2+(ORefL%LR(j)%z-RefL%LR(k)%z)**2
                    rms=rms+dist
                    dist=sqrt(dist)
                   if(present(lun)) write(unit=lun,fmt="(i9,5f10.2,tr3,3i5)") j,ORefL%LR(j)%x,ORefL%LR(j)%z, RefL%LR(k)%x    ,&
                                               RefL%LR(k)%z ,dist,nint(ORefL%LR(j)%h)
                 end do
                 rms=sqrt(rms/real(nxz))
                 S%Sol(nsol)%rms= rms
                 S%Sol(nsol)%ub = UB
                 S%Sol(nsol)%num_indx = nxz

                 if(present(lun)) write(unit=lun,fmt="(a,f8.4,a/a,i4)") " => Root mean square error: ",rms," mm", &
                                                                        " => Number of indexed spots: ",nxz
               end if
           end do do_coinc
           if(nsol == 0 ) cycle
           if(OrCnd%exhaust /= 0) then
             cycle
           else
             exit      ! Solutions with two orienting reflections have been found
           end if
         end if
       end do do_obs

       S%nsol=nsol
       if(nsol > 0) then
          call sort(S%Sol(:)%rms,nsol,S%ind(:))
          write(unit=mess,fmt="(a,i4)") " Number of solutions found: ",nsol
       else
          mess= " No solution has been found! change the conditions ... think! "
       end if
       !Complete the solutions by calculting the orienting angles
       Select Case(OrCnd%ang_type)
          case("XYZ")
            do i=1,nsol
              Um=matmul(S%Sol(i)%ub,binv) !Rotation matrix from which we can calculate the angles
              call get_Phi_xyz(Um,S%Sol(i)%ang(1),S%Sol(i)%ang(2),S%Sol(i)%ang(3),"D")
            end do

          case("EUL")
            do i=1,nsol
              Um=matmul(S%Sol(i)%ub,binv) !Rotation matrix from which we can calculate the angles
              !call get_PhiTheChi(Um,Omega,Chi,Phi,Code) !Phix, Phiy, Phiz
              call get_PhiTheChi(Um,S%Sol(i)%ang(1),S%Sol(i)%ang(2),S%Sol(i)%ang(3),"D")
            end do

       End Select
       return
    End Subroutine Get_Orientation_from_xz_spots

    !!----
    !!---- Subroutine nGet_Phi_xyz(Mt,Phix,Phiy,Phiz,code)
    !!----   real(kind=cp), dimension(3,3), intent (in)  :: Mt
    !!----   real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
    !!----   Character(len=*), optional,    intent (in)  :: code
    !!----
    !!----
    !!----   Subroutine providing the orienting angles corresponding to an orthogonal
    !!----   matrix Mt representing an active rotation. The angles correspond to
    !!----   rotations around XL, YL and ZL. The matrix is Mt=R(Phix) R(Phiy) R(Phiz).
    !!----   If Code is present and with values "D" or "d", the angles are returned
    !!----   in degrees, otherwise they are returned in radians.
    !!----
    !!----    Created: May  2010 (JRC)
    !!----    Updated: June 2010 (JRC)
    !!----
!    Subroutine nGet_Phi_xyz(Mt,Phix,Phiy,Phiz,code)
!      real(kind=cp), dimension(3,3), intent (in)  :: Mt
!      real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
!      Character(len=*), optional,    intent (in)  :: code
!
!       !---- Local Variables ----!
!       real(kind=cp)                :: eps=0.0002
!       real(kind=cp), dimension(3,3):: MTT
!       real(kind=cp), parameter, dimension(3,3) :: &
!                      identity = reshape ( (/1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0/),(/3,3/))
!
!       Phix=0.0; Phiy=0.0; Phiz=0.0
!       MTT=transpose(Mt)
!       MTT=matmul(MTT,Mt)-identity
!       if (sum(abs(MTT)) > eps) then
!          write(unit=*,fmt="(a)") " => Error in Get_Phi_xyz ... the input matrix is not orthogonal! "
!          return
!       end if
!       MTT=Mt-identity
!       if (sum(abs(MTT)) < eps) return !Identity matrix  => zero angles
!
!       !M(1,1)= cos(y)cos(z)                    M(1,2)=-cos(y)sin(z)                    M(1,3)=       sin(y)
!       !M(2,1)= cos(x)sin(z)+sin(x)sin(y)cos(z) M(2,2)= cos(x)cos(z)-sin(x)sin(y)sin(z) M(2,3)=-sin(x)cos(y)
!       !M(3,1)= sin(x)sin(z)-cos(x)sin(y)cos(z) M(3,2)= sin(x)cos(z)+cos(x)sin(y)sin(z) M(3,3)= cos(x)cos(y)
!       if (abs(Mt(1,3)-1.0) < eps) then
!          Phiy=pi*0.5
!          Phiz=atan2(Mt(2,1),Mt(2,2))
!          Phix=atan2(Mt(3,2),Mt(2,2))
!       else if(abs(Mt(1,3)+1.0) < eps) then
!          Phiy=-pi*0.5
!          Phiz=atan2(Mt(2,1),Mt(2,2))
!          Phix=atan2(Mt(3,2),Mt(2,2))
!       else
!          Phiy= asin( Mt(1,3))
!          Phiz=atan2(-Mt(1,2),Mt(1,1))
!          Phix=atan2(-Mt(2,3),Mt(3,3))
!       end if
!       if (present(Code)) then
!          if (code(1:1)=="D" .or. code(1:1)=="d") then
!             Phix=Phix*to_deg
!             Phiy=Phiy*to_deg
!             Phiz=Phiz*to_deg
!             if(Phix < 0.0) Phix=Phix+360.0
!             if(Phiy < 0.0) Phiy=Phiy+360.0
!             if(Phiz < 0.0) Phiz=Phiz+360.0
!          end if
!       end if
!
!       return
!    End Subroutine nGet_Phi_xyz

    !!----
    !!---- Subroutine Get_Phi_xyz(Mt,Phix,Phiy,Phiz,code)
    !!----   real(kind=cp), dimension(3,3), intent (in)  :: Mt
    !!----   real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
    !!----   Character(len=*), optional,    intent (in)  :: code
    !!----
    !!----
    !!----   Subroutine providing the orienting angles corresponding to an orthogonal
    !!----   matrix Mt representing an active rotation. The angles correspond to
    !!----   rotations around XL, YL and ZL. The matrix is Mt=R(Phix) R(Phiy) R(Phiz).
    !!----   If Code is present and with values "D" or "d", the angles are returned
    !!----   in degrees, otherwise they are returned in radians.
    !!----
    !!----    Created: May  2010 (JRC)
    !!----    Updated: September 2010 (LFM)
    !!----

    Subroutine Get_Phi_xyz(Mt,Phix,Phiy,Phiz,code)
      real(kind=cp), dimension(3,3), intent (in)  :: Mt
      real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
      Character(len=*), optional,    intent (in)  :: code

       !---- Local Variables ----!
       real(kind=cp), dimension(3,3):: GuIMat

       GuIMat=Mt
       Phix=-atan2(GuIMat(2,3),GuIMat(3,3))
       if( Phix<0 )then
           Phix=Phix+2.0*pi
       end if
       GuIMat=matmul(Matrix_Rx(-Phix),GuIMat)

       Phiy=atan2(GuIMat(1,3),GuIMat(3,3))
       if( Phiy<0 )then
           Phiy=Phiy+2.0*pi
       end if
       GuIMat=matmul(Matrix_Ry(-Phiy),GuIMat)

       Phiz=atan2(GuIMat(2,1),GuIMat(1,1))
       if( Phiz<0 )then
           Phiz=Phiz+2.0*pi
       end if
       GuIMat=matmul(Matrix_Rz(-Phiz),GuIMat)  !Should be now the identity matrix
       if (present(Code)) then
          if (code(1:1)=="D" .or. code(1:1)=="d") then
             Phix=Phix*to_deg
             Phiy=Phiy*to_deg
             Phiz=Phiz*to_deg
          end if
       end if

       return
    End Subroutine Get_Phi_xyz

    !!----
    !!---- Subroutine Get_Phi_zyx(Mt,Phix,Phiy,Phiz,code)
    !!----   real(kind=cp), dimension(3,3), intent (in)  :: Mt
    !!----   real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
    !!----   Character(len=*), optional,    intent (in)  :: code
    !!----
    !!----
    !!----   Subroutine providing the orienting angles corresponding to an orthogonal
    !!----   matrix Mt representing an active rotation. The angles correspond to
    !!----   rotations around XL, YL and ZL. The matrix is Mt=R(Phiz) R(Phiy) R(Phix).
    !!----   If Code is present and with values "D" or "d", the angles are returned
    !!----   in degrees, otherwise they are returned in radians.
    !!----   This subroutine is similar to the previous one but provided here for
    !!----   calculating rotation angles attached to a goniometric head that has
    !!----   two perpendicular rotations Phix and Phiy, both perpendicular to the
    !!----   omega (spindle) angle of the diffractometer. The goniometric head should
    !!----   be mounted with the upper rotation axis along XL, so that the second
    !!----   rotation is along YL. The omega rotation, Phiz, should be set to
    !!----   zero before mounting the goniometric head.
    !!----
    !!----    Created: December  2010 (JRC)
    !!----    Updated: December  2010 (JRC)
    !!----
    Subroutine Get_Phi_zyx(M,Phix,Phiy,Phiz,code)
      real(kind=cp), dimension(3,3), intent (in)  :: M
      real(kind=cp),                 intent (out) :: Phix,Phiy,Phiz
      Character(len=*), optional,    intent (in)  :: code

       !---- Local Variables ----!
       real(kind=cp), dimension(3,3):: GuIMat

       !M(1,1)= cos(y)cos(z)  M(1,2)=-cos(x)sin(z)+sin(x)sin(y)cos(z)  M(1,3)= sin(x)sin(z)+cos(x)sin(y)cos(z)
       !M(2,1)= cos(y)sin(z)  M(2,2)= cos(x)cos(z)+sin(x)sin(y)sin(z)  M(2,3)=-sin(x)cos(z)+cos(x)sin(y)sin(z)
       !M(3,1)=-sin(y)        M(3,2)= sin(x)cos(y)                     M(3,3)= cos(x)cos(y)
       GuIMat=M
       Phix=atan2(GuIMat(3,2),GuIMat(3,3))
       GuIMat=matmul(GuIMat,Matrix_Rx(-Phix))

       Phiy=atan2(-GuIMat(3,1),GuIMat(3,3))
       GuIMat=matmul(GuIMat,Matrix_Ry(-Phiy))

       Phiz=atan2(-GuIMat(1,2),GuIMat(1,1))
       GuIMat=matmul(GuIMat,Matrix_Rz(-Phiz))  !Should be now the identity matrix
       if (present(Code)) then
          if (code(1:1)=="D" .or. code(1:1)=="d") then
             Phix=Phix*to_deg
             Phiy=Phiy*to_deg
             Phiz=Phiz*to_deg
             If(Phix > 180.0) Phix= Phix-360.0
             If(Phiy > 180.0) Phiy= Phiy-360.0
             If(Phiz > 180.0) Phiz= Phiz-360.0
          end if
       end if

       return
    End Subroutine Get_Phi_zyx

    Subroutine uvw_Equiv_List(u,Spacegroup,Mul,ulist)
       !---- Arguments ----!
       real, dimension(3),                     intent (in) :: u
       Type (Space_Group_Type),                intent (in) :: SpaceGroup
       integer,                                intent(out) :: mul
       real, dimension(3,SpaceGroup%numops*2), intent(out) :: ulist

       !---- Local Variables ----!
       logical           :: esta
       real, dimension(3):: v
       integer           :: i,j,ng

       ulist = 0.0
       ng=SpaceGroup%numops
       mul=1
       ulist(:,1)=u(:)
       do i=2,ng
          v = Matmul(SpaceGroup%SymOp(i)%Rot,u)
          esta=.false.
          do j=1,mul
             if (uvw_Equal(v,ulist(:,j)) .or. uvw_Equal(-v,ulist(:,j))) then
                esta=.true.
                exit
             end if
          end do
          if (esta) cycle
          mul=mul+1
          ulist(:,mul) = v
       end do

       j=mul
       mul=mul*2
       do i=j+1,mul
          ulist(:,i)=-ulist(:,i-j)
       end do

       return
    End Subroutine uvw_Equiv_List

    Function uvw_Equal(u,v) Result (Info)
       !---- Arguments ----!
       real, dimension(3), intent(in) :: u,v
       logical                        :: info
       real, parameter :: epsr=0.001
       info=.false.
       if (abs(u(1)-v(1)) <= epsr .and. abs(u(2)-v(2)) <= epsr .and. &
           abs(u(3)-v(3)) <= epsr) info=.true.

       return
    End Function uvw_Equal

!!----
!!----    Subroutine Read_Twinlaw(Twin,lun,fich_cfl)
!!----      Type(twin_type),        intent (out)          :: Twin
!!----      Logical,                intent (out)          :: read_ok
!!----      integer,                intent (in), optional :: lun !logical unit of the file to be read
!!----      Type (file_list_type),  intent (in), optional :: fich_cfl
!!----
!!----
!!----    Subroutine to read the necessary items for defining a twin-law.
!!----
!!----
    Subroutine Read_Twinlaw(Twin,read_ok,lun,fich_cfl)
      Type(twin_type),        intent (out)          :: Twin
      Logical,                intent (out)          :: read_ok
      integer,                intent (in), optional :: lun !logical unit of the file to be read
      Type (file_list_type),  intent (in), optional :: fich_cfl
      integer :: ier,i,n, nmat
      character(len=132) :: line
      real, dimension(3) :: angls

      read_ok=.false.
      !The different twins have the identity matrix at starting point
      Twin%Twin_name=" No-name "
      Twin%ityp=0
      Twin%Twin_Mat=0.0
      do i=1,48
        do n=1,3
          Twin%Twin_Mat(n,n,i)=1.0
        end do
      end do
      nmat=0
      n=0
      do
        if(present(lun)) then
          read(unit=lun,fmt="(a)",iostat=ier) line
          if(ier /= 0) exit
        else if(present(fich_cfl)) then
          n=n+1
          if(n > fich_cfl%nlines) exit
          line=fich_cfl%line(n)
        end if
        line=adjustl(line)
        if(line(1:1) == "!" .or. line(1:1) == "#") cycle
        if(line(1:8) == "TWIN_nam") then
          Twin%twin_name = line(9:)
        else if(line(1:8) == "TWIN_typ") then
          Read(unit=line(9:),fmt=*,iostat=ier) Twin%ityp
          if(ier /= 0) then
           call Error_message (" => Error reading the TWIN law file, item: TWIN_typ")
           return
          end if
          if(Twin%ityp == 4) then
             nmat=1  ! The domain #1 has always rotation matrix equal to identity
          end if
          if(Twin%ityp < 1 .or. Twin%ityp > 4) then
           call Error_message (" => Illegal TWIN_typ: It should be 1,2 or 3!")
           return
          end if
        else if (line(1:8) == "TWIN_rot") then   !This is for Twin%ityp = 4
          nmat=nmat+1
          Read(unit=line(9:),fmt=*,iostat=ier) twin%twin_axis(:,nmat),twin%twin_ang(nmat)
          if(ier /= 0) then
           call Error_message (" => Error reading the TWIN law file, item: TWIN_rot")
           return
          end if
          !Calculation of the rotation matrix
          Twin%Twin_Mat(:,:,nmat)=Rot_Matrix(twin%twin_axis(:,nmat),twin%twin_ang(nmat))
          Twin%Twin_Matinv(:,:,nmat)=transpose(Twin%Twin_Mat(:,:,nmat))

        else if (line(1:8) == "TWIN_uma") then !This is another alternative for Twin%ityp = 4
          nmat=nmat+1
          Read(unit=line(9:),fmt=*,iostat=ier) angls
          if(ier /= 0) then
           call Error_message (" => Error reading the TWIN law file, item: TWIN_rot")
           return
          end if
          !Calculation of the rotation matrix
          Call Set_Rotation_Matrix(angls,Twin%Twin_Mat(:,:,nmat))
          Twin%Twin_Matinv(:,:,nmat)=transpose(Twin%Twin_Mat(:,:,nmat))
          Call Get_Anglen_Axis_From_RotMat(Twin%Twin_Mat(:,:,nmat),Twin%Twin_axis(:,nmat),Twin%Twin_ang(nmat))

        else if (line(1:8) == "TWIN_mat") then   !This is for cases Twin%ityp /= 4
          nmat=nmat+1
          Read(unit=line(9:),fmt=*,iostat=ier) (Twin%Twin_Mat(i,:,nmat),i=1,3)
          if(ier /= 0) then
           call Error_message (" => Error reading the TWIN law file, item: TWIN_mat")
           return
          end if
          Twin%Twin_Matinv(:,:,nmat)=Invert_A(Twin%Twin_Mat(:,:,nmat)) !The twin matrix here is not a rotation matrix

        else if (line(1:8) == "TWIN_end") then
          exit
        end if
      end do
      Twin%N_twins=nmat
      if(Twin%ityp == 0 .and. Twin%N_twins > 1) then
        call Error_message (" => Error: The type of twin is unknown! TWIN_typ not given!")
        return
      end if
      read_OK=.true.
      return
    End Subroutine Read_Twinlaw

    !!----  Subroutine Read_Offsets(filenam,Offsets,ok)
    !!----    Character(len=*),           intent(in)  :: filenam
    !!----    Type(Offsets_Type),         intent(out) :: Offsets
    !!----    Logical,                    intent(out) :: ok
    !!----
    !!----  Subroutine reading offsets, new cell parameters and orientation associated
    !!----  to a single image that has been treated by Ref_Laue_UB
    !!----
    !!----  Created: February 2012 (JRC)
    !!----
    Subroutine Read_Offsets(filenam,Offsets,ok)
      Character(len=*),           intent(in)  :: filenam
      Type(Offsets_Type),         intent(out) :: Offsets
      Logical,                    intent(out) :: ok

      Character(len=132) :: line
      Character(len=15)  :: key
      integer :: lun, ier, i, j

      Call Get_LogUnit(lun)
      open(unit=lun,file=filenam,status="old",action="read",position="rewind")
      ok=.true.
      do
        read(unit=lun,fmt="(a)",iostat=ier) line
        if(ier /= 0) exit
        key=adjustl(l_case(line(1:15)))
        j=index(key," ")
        if(j /= 0) key=key(1:j-1)

        Select Case(trim(key))
          Case("cell")
            read(unit=line(5:),fmt=*,iostat=ier) Offsets%Cell,Offsets%Cellang
          Case("orient")
            read(unit=line(7:),fmt=*,iostat=ier) Offsets%Orient
          Case("dist")
            read(unit=line(5:),fmt=*,iostat=ier) Offsets%Dist
          Case("tilt_x")
            read(unit=line(7:),fmt=*,iostat=ier) Offsets%tilt_x
          Case("tilt_y")
            read(unit=line(7:),fmt=*,iostat=ier) Offsets%tilt_y
          Case("tilt_z")
            read(unit=line(7:),fmt=*,iostat=ier) Offsets%tilt_z
          Case("gamma_c")
            read(unit=line(8:),fmt=*,iostat=ier) Offsets%gamma_c
          Case("nu_c")
            read(unit=line(5:),fmt=*,iostat=ier) Offsets%nu_c
          Case("rod_x")
            read(unit=line(6:),fmt=*,iostat=ier) Offsets%rOD_x
          Case("rod_y")
            read(unit=line(6:),fmt=*,iostat=ier) Offsets%rOD_y
          Case("rod_z")
            read(unit=line(6:),fmt=*,iostat=ier) Offsets%rOD_z
          Case("spindle")
            read(unit=line(8:),fmt=*,iostat=ier) Offsets%spindle
          Case("chebychev")
            read(unit=line(10:),fmt=*,iostat=ier) Ncoeff
            do i=1,2
              read(unit=lun,fmt=*,iostat=ier) Cheb(1:Ncoeff,i)
            end do
        End Select
        if(ier /= 0) then
          ok=.false.
          close(unit=lun)
          return
        end if
      end do
      close(unit=lun)
      return
    End Subroutine Read_Offsets

    !!----  Subroutine Update_Instrument(LaueDiff,Offsets,M_LaueDiff)
    !!----    Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
    !!----    Type(Offsets_Type),         intent(in)  :: Offsets
    !!----    Type(Laue_Instrument_Type), intent(out) :: M_LaueDiff
    !!----
    !!----  Subroutine constructing a modified instrument (M_LaueDiff) with respect to the
    !!----  input one (LaueDiff), by applying the appropriate offsets encapsulated in the
    !!----  structure Offsets. This subroutine should be called each time an image is
    !!----  accompanied by a .corr file. The spindle offset should be applied immediately
    !!----  to the read spindle of the image.
    !!----
    !!----  Created: February 2012 (JRC), updated July-2012 (JRC)
    !!----
    Subroutine Update_Instrument(LaueDiff,Offsets,M_LaueDiff)
      Type(Laue_Instrument_Type), intent(in)  :: LaueDiff
      Type(Offsets_Type),         intent(in)  :: Offsets
      Type(Laue_Instrument_Type), intent(out) :: M_LaueDiff
      real :: xm,zm
      real, dimension(2) :: disp

      M_LaueDiff = LaueDiff     ! Make all components equal except those below
      M_LaueDiff%tilted  = .true.
      M_LaueDiff%displaced=.true.
      M_LaueDiff%D       = LaueDiff%D       + Offsets%Dist        ! Dist_offset
      M_LaueDiff%Tiltx_d = LaueDiff%Tiltx_d + Offsets%Tilt_x      ! Tilt_x_offset
      M_LaueDiff%Tilty_d = LaueDiff%Tilty_d + Offsets%Tilt_y      ! Tilt_x_offset
      M_LaueDiff%Tiltz_d = LaueDiff%Tiltz_d + Offsets%Tilt_z      ! Tilt_x_offset
      M_LaueDiff%ga_d    = LaueDiff%ga_d    + Offsets%Gamma_c     ! Gamma_c_offset
      M_LaueDiff%nu_d    = LaueDiff%nu_d    + Offsets%Nu_c        ! Nu_c_offset
      M_LaueDiff%rOD(1)  = LaueDiff%rOD(1)  + Offsets%rOD_x       ! rOD_x_offset
      M_LaueDiff%rOD(2)  = LaueDiff%rOD(2)  + Offsets%rOD_y       ! rOD_y_offset
      M_LaueDiff%rOD(3)  = LaueDiff%rOD(3)  + Offsets%rOD_z       ! rOD_z_offset
      M_LaueDiff%xo      = LaueDiff%xo      + Offsets%Orig_x      ! Orig_x_offset
      M_LaueDiff%zo      = LaueDiff%zo      + Offsets%Orig_z      ! Orig_z_offset

      !Displacement in mm w.r.t. centre
      disp=(/ 0.5*real(M_LaueDiff%Np_h), 0.5*real(M_LaueDiff%Np_v) /)
      call Pix_To_Mil(M_LaueDiff,disp(1),disp(2),xm,zm)
      M_LaueDiff%dism=(/-xm,-zm/)

      call Calc_RD_rOD(M_LaueDiff)
      return
    End Subroutine Update_Instrument
!!--
!!--
    Subroutine Write_Twinlaw(Twin,lun,cell)
      Type(twin_type),                   intent(in out) :: Twin
      integer,                           intent(in)     :: lun !logical unit of the file to be written
      type(Crystal_Cell_Type), optional, intent(in)     :: cell
      integer :: i,j,l
      real    :: rmodul
      real, dimension(3,3) :: tinv,a,b, og
      real, dimension(  3) :: h

      write(unit=lun,fmt="(/,a)") "    INFORMATION GIVEN IN THE TWIN-LAW COMMANDS"
      write(unit=lun,fmt="(a,/)") "    ------------------------------------------"
      write(unit=lun,fmt="(a,a)") " => Name of the twin-law: ", trim(Twin%twin_name)
      Select case (Twin%ityp)
        case(1)
           write(unit=lun,fmt="(a)")  " => Type of the twin-law:  TWIN_typ=1 "
           write(unit=lun,fmt="(a)")  "         The domains contributing to an observation have indices      "
           write(unit=lun,fmt="(a)")  "         obtained from the input (h,k,l) by multiplying the vector    "
           write(unit=lun,fmt="(a)")  "         (hkl) by the matrix P, so (hkl)n = P (hkl) {n=1, ndomains}   "
           write(unit=lun,fmt="(a)")  "         The domains contributing to each observation are those that  "
           write(unit=lun,fmt="(a)")  "         give integer indices (hkl)n after applying the matrix P.     "
           write(unit=lun,fmt="(a)")  "         The new indices are (hkl)n.                                  "
        case(2)
           write(unit=lun,fmt="(a)")  " => Type of the twin-law:  TWIN_typ=2 "
           write(unit=lun,fmt="(a)")  "         The input indices are given w.r.t. the super-lattice. It is   "
           write(unit=lun,fmt="(a)")  "         supposed that a sub-lattice exist and the orientation of the  "
           write(unit=lun,fmt="(a)")  "         superlattice could have different orientational domains.      "
           write(unit=lun,fmt="(a)")  "         Each domain is entered as a matrix relating the superlattice  "
           write(unit=lun,fmt="(a)")  "         direct cell to the parent sub-lattice.                        "
        case(3)
           write(unit=lun,fmt="(a)")  " => Type of the twin-law:  TWIN_typ=3 "
           write(unit=lun,fmt="(a)")  "         The input indices correspond to the first domain. The orientation "
           write(unit=lun,fmt="(a)")  "         of the first domain with respect to a cartesian frame is given    "
           write(unit=lun,fmt="(a)")  "         by the user as well as for the other domains.                     "
           write(unit=lun,fmt="(a)")  "         Each domain is given by the orientation in the Cartesian frame    "
           write(unit=lun,fmt="(a)")  "         of each cell parameter a, b, c. Only the direction is needed.      "
        case(4)
           write(unit=lun,fmt="(a)")  " => Type of the twin-law:  TWIN_typ=4 "
           write(unit=lun,fmt="(a)")  "         The input indices correspond to the first domain. The orientation       "
           write(unit=lun,fmt="(a)")  "         of the n-domain (n/=1) is obtained by a rotation matrix for which       "
           write(unit=lun,fmt="(a)")  "         only the direction in the Busing-Levy cartesian frame attached to the   "
           write(unit=lun,fmt="(a)")  "         crystal and the value of the angle are given. The program calculates    "
           write(unit=lun,fmt="(a)")  "         the Cartesian vectors  z1(n)= U R(n) B h for h indices corresponding to "
           write(unit=lun,fmt="(a)")  "         all equivalent reflections plus those that have a close value of        "
           write(unit=lun,fmt="(a)")  "         d-spacing. If |z1(n)-z1(1)| < delta the corresponding indices may       "
           write(unit=lun,fmt="(a)")  "         contribute to the total intensity.                                      "
           write(unit=lun,fmt="(a)")  "          "
      End Select

      write(unit=lun,fmt="(a,i3)")    " => Number of matrices describing the twin-law: ",Twin%N_twins

      if(Twin%ityp /= 4) then
        do i=1,Twin%N_twins
            write(unit=lun,fmt="(a,i3)")          " => Input matrix for domain Number: ", i
            do j=1,3
             write(unit=lun,fmt="(a,3f10.5)")  "                   ", Twin%Twin_Mat(j,:,i)
            end do
        end do
      else
        do i=2,Twin%N_twins
            write(unit=lun,fmt="(/,a,i3)")        " => Input axis and angle for domain Number: ", i
            write(unit=lun,fmt="(a,3f10.5,a,f10.5)") "            [uvw]=[",Twin%Twin_axis(:,i),"]   ang=",Twin%Twin_ang(i)
            write(unit=lun,fmt="(a)")             "    Rotation matrix: "
            do j=1,3
             write(unit=lun,fmt="(a,3f10.5)")     "                   ", Twin%Twin_Mat(j,:,i)
            end do
        end do
      end if

      !----------------------------------
      If(Twin%ityp == 1) Then    !itwin
      !----------------------------------
        write(unit=lun,fmt="(a)")  " => Final direct and inverse matrices acting on (hkl): "
        Do l=1,Twin%N_twins
          Twin%Twin_Matinv(:,:,l) =  invert_A(Twin%Twin_Mat(:,:,l))
          write(unit=lun,fmt="(a,i3)")                    " => Matrices for domain Number: ", l
          do j=1,3
           write(unit=lun,fmt="(a,3f10.5,a,3f10.5)")  "         ", Twin%Twin_Mat(j,:,l),"     ",Twin%Twin_Matinv(j,:,l)
          end do
        End Do
      !----------------------------------
      Else If(Twin%ityp == 2) Then    !itwin
      !----------------------------------
        tinv=invert_A(Twin%Twin_Mat(:,:,1))
        write(unit=lun,fmt="(a)")  " => Final direct and inverse matrices acting on (hkl): "
        Do l=1,Twin%N_twins
          Twin%Twin_Mat(:,:,l)  =  matmul(Twin%Twin_Mat(:,:,l),tinv)
          Twin%Twin_Matinv(:,:,l) =  invert_A(Twin%Twin_Mat(:,:,l))
          write(unit=lun,fmt="(a,i3)")                    " => Matrices for domain Number: ", l
          do j=1,3
           write(unit=lun,fmt="(a,3f10.5,a,3f10.5)")  "         ", Twin%Twin_Mat(j,:,l),"     ",Twin%Twin_Matinv(j,:,l)
          end do
        End Do
      !---------------------------------------
      Else If(Twin%ityp == 3  ) Then
      !---------------------------------------
       if(present(cell)) then
        write(unit=lun,fmt="(a)")  " => Final direct and inverse matrices acting on (hkl): "
        Do l=1,Twin%N_twins
          Do i=1,3
            h(:)= Twin%Twin_Mat(i,:,l)
            rmodul= Sqrt(h(1)*h(1)+h(2)*h(2)+h(3)*h(3))
            b(i,:)=cell%cell(i)*h(:)/rmodul         !M(L)
          End Do
          a=invert_A(b)                          !A=M(L)-1
          tinv=transpose(a)                    !TINV= (M(L)-1)t
          If(l == 1) Then
            a=transpose(b)                     !  A=M(1)t
            og=matmul(a,cell%gr)               !  M(1)t * GG
          End If
          b=matmul(cell%gd,tinv)               !GD *  (M(L)-1)t
          Twin%Twin_Mat(:,:,l)=matmul(b,og)             !GD *  (M(L)-1)t  * M(1)t * GG
          Twin%Twin_Matinv(:,:,l) =  invert_A(Twin%Twin_Mat(:,:,l))  !B=A-1
          write(unit=lun,fmt="(a,i3)")                    " => Matrices for domain Number: ", l
          do j=1,3
           write(unit=lun,fmt="(a,3f10.5,a,3f10.5)")  "         ", Twin%Twin_Mat(j,:,l),"     ",Twin%Twin_Matinv(j,:,l)
          end do
        End Do
       else
         call Error_message (" => Error: fow TWIN_typ=3, cell must be given!")
         return
       end if
      !----------------------------------
      End If   !itwin == 2, 3,..
      !----------------------------------
      return
    End Subroutine Write_Twinlaw

    !!----  Subroutine Corr_xz_Chebychev(pos,LDiff,nl,c,shift,der)
    !!----    Type(Laue_Instrument_Type),     intent(in)  :: LDiff
    !!----    real, dimension(2),             intent(in)  :: pos   ! Input x,z position
    !!----    Integer,                        intent(in)  :: nl    ! Degree of polynomial
    !!----    real, dimension(:,:),           intent(in)  :: c     ! (nl,2) Chebychev coefficients
    !!----    real, dimension(2),             intent(out) :: shift ! Shifts to be applied to x,z positions
    !!----    real, dimension(:,:), optional, intent(out) :: der   ! (nl,2)
    !!----
    !!----    Subroutine for calculating shifts of peaks due to unknown distortions of the image.
    !!----    Chebychev polynomial coefficients are provided in order to calculate the shift on X and Z.
    !!----    coordinates of the peak in mm.
    !!----
    !!----    Created: July 2010 (JRC)
    !!----    Updated: March 2012 ( Moved from Ref_UB_Laue), July-2012 (new normalisation as in Lauegen)
    !!----
    Subroutine Corr_xz_Chebychev(pos,LDiff,nl,c,shift,der)
      Type(Laue_Instrument_Type),     intent(in)  :: LDiff
      real, dimension(2),             intent(in)  :: pos   ! Input x,z position
      Integer,                        intent(in)  :: nl    ! Degree of polynomial
      real, dimension(:,:),           intent(in)  :: c     ! (nl,2) Chebychev coefficients
      real, dimension(2),             intent(out) :: shift ! Shifts to be applied to x,z positions
      real, dimension(:,:), optional, intent(out) :: der   ! (nl,2)
      !---- Local Variables ----!
      integer              :: i,j
      real                 :: c1,ri
      real, dimension(2)   :: p
      real,dimension(nl,2) :: dv

      p(1)=(2.0*pos(1)-LDiff%x_max-LDiff%x_min)/(LDiff%x_max-LDiff%x_min)
      p(2)=(2.0*pos(2)-LDiff%z_max-LDiff%z_min)/(LDiff%z_max-LDiff%z_min)
      do j=1,2
        shift(j)=-0.5*c(1,j)
        c1=acos(p(j))
        do i=1,nl
           ri=real(i)
           dv(i,j)=cos(ri*c1)
           shift(j) = shift(j) + c(i,j)* dv(i,j)
        end do
      end do

      if(present(der)) then
        der(1:nl,1:2)=dv(1:nl,1:2)
        der(1,1:2)=der(1,1:2)-(/0.5,0.5/)
      end if
      return
    End Subroutine Corr_xz_Chebychev

    !!----  Subroutine Corr_MOSFLM(dist,c,pos)
    !!----    Real,                       intent(in)     :: dist  ! distance detector sample
    !!----    real, dimension(2),         intent(in out) :: pos   ! Input x,z position
    !!----    Type(Image_Distortion_Type),intent(in)     :: c     ! Image distortion parameters
    !!----
    !!----    Subroutine for calculating the distortions of the image according to Lauegen/MOSFLM.
    !!----    See J.W. Campbell et al., J. Appl. Cryst. 28, 635-640 (1995)
    !!----    Should be applied after the tilts of detectors. The coordinates of the peak are in mm.
    !!----
    !!----    Created: July 2012 (JRC)
    !!----
    Subroutine Corr_MOSFLM(dist,c,pos)
      Real,                       intent(in)     :: dist  ! distance detector sample
      real, dimension(2),         intent(in out) :: pos   ! Input x,z position
      Type(Image_Distortion_Type),intent(in)     :: c     ! Image distortion parameters
      !---- Local Variables ----!
      real                 :: k_factor, conv,dist_cent

      conv=to_rad*0.01/dist
      dist_cent=sqrt(dot_product(pos,pos))
      k_factor= 1.0+ conv*(c%tilt*pos(1)+c%twist*pos(2)+c%bulge*dist_cent)
      pos=pos*k_factor
      pos(1)=pos(1)+c%cross*pos(2)
      return
    End Subroutine Corr_MOSFLM

End Module Laue_Mod

