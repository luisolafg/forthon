!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
 Module Laue_Utilities_Mod
   ! Module encapsulating utilities for use with console Laue applications
   Use CFML_GlobalDeps
   Use CFML_String_Utilities,          only: pack_string,l_case,Get_Logunit
   Use CFML_Math_General,              only: sort
   Use CFML_Atom_TypeDef,              only: Atom_List_Type, Write_Atom_List
   Use CFML_crystallographic_symmetry, only: Space_Group_Type, Write_SpaceGroup, Set_SpaceGroup
   Use CFML_Crystal_Metrics,           only: Crystal_Cell_Type, Write_Crystal_Cell
   Use CFML_Reflections_Utilities,     only: Reflection_List_Type, Hkl_Uni, Hkl_Gen_Sxtal, get_maxnumref, Hkl_Equiv_List
   Use CFML_IO_Formats,                only: Readn_Set_Xtal_Structure,Read_File_Spg,Read_File_Cell,err_form_mess,&
                                             err_form,file_list_type,File_to_filelist
   Use CFML_Geometry_SXTAL,            only: genb
   use CFML_Geometry_Calc,             only: Matrix_Rx, Matrix_Ry, Matrix_Rz
   Use Data_Trt,                       only: Excluded_Regions_Type, Finding_Peaks_Parameters_Type, Read_Excluded_Regions
   Use Laue_Mod
   Use Laue_TIFF_READ

   implicit none
   private

   public :: Read_Laue_CFL_File, Reflection_Generation, DaTim,  &
             Get_Found_Peaks, Calc_Visible_Reflections, Write_Indexed_Reflections,  &
             Get_Approx_Orientation, Re_Index_Reflections, Allocate_Simple_Peaks

   Type, public :: Simple_Peaks_Type   !Used in Ref_UB_Laue when treating a script
     Integer                        :: npeaks
     Real, dimension(:),allocatable :: x
     Real, dimension(:),allocatable :: z
     Real, dimension(:),allocatable :: Intensity
   End Type Simple_Peaks_Type

   Type, public :: Peaks_File_names
     integer                                      :: n_files
     character(len=132),dimension(:), allocatable :: Names
   End Type Peaks_File_names

   Type, public :: Orientation
     logical           :: or_given
     Real,dimension(3) :: Angles
   End Type Orientation

   ! Global variables of the module used for error control
   Logical,            public :: Error_Laue_utilities=.false.
   Character(len=512), public :: Error_Mess_Laue_utilities=" "

  !Type, public, extends (Orient_Solution_Type) :: Orienting_solution
  !      real  :: spda
  !End Type Orienting_solution

  Contains

   Subroutine Allocate_Simple_Peaks(n,Pk)
     integer,                intent(in)     :: n
     Type(Simple_Peaks_Type),intent(in out) :: Pk

     if(allocated(Pk%Intensity)) deallocate(Pk%Intensity)
     if(allocated(Pk%x)) deallocate(Pk%x)
     if(allocated(Pk%z)) deallocate(Pk%z)
     Pk%npeaks=n
     allocate(Pk%Intensity(n),Pk%x(n),Pk%z(n))
     Pk%Intensity=0.0; Pk%x=0.0; Pk%z=0.0
     return
   End Subroutine Allocate_Simple_Peaks

   Subroutine DaTim(lun)
     integer, intent(in) :: lun
     !--- Local variables ----!
     character (len=10) :: dat
     character (len=10) :: tim
     call date_and_time(date=dat,time=tim)
     write(unit=lun,fmt="(/,4a)") &
       " => Date: ",dat(7:8)//"/"//dat(5:6)//"/"//dat(1:4),      &
         "  Time: ",tim(1:2)//":"//tim(3:4)//":"//tim(5:10)
     return
   End Subroutine DaTim

   Subroutine Read_Laue_CFL_File(CFL_File, title, Cell,SpG,LaueDiff, Instrm_File, lun, &
                                  ExclR,Image_Files,Peak_Files,Fnd_par,Cnd,Orient,A)
     Character(len=*),                                 intent(in)   :: CFL_File
     Character(len=*),                                 intent(out)  :: title
     Type(Crystal_Cell_Type),                          intent(out)  :: Cell
     Type(Space_Group_Type),                           intent(out)  :: SpG
     Type(Laue_Instrument_Type),                       intent(out)  :: LaueDiff
     Character(len=*),                                 intent(out)  :: Instrm_File
     integer,                                Optional, intent(in)   :: lun
     Type(Excluded_Regions_Type),            Optional, intent(out)  :: ExclR
     Type(Image_File_Names),                                       Optional, intent(out)  :: Image_Files
     Type(Peaks_File_Names),                                       Optional, intent(out)  :: Peak_Files
     Type(Finding_Peaks_Parameters_Type),dimension(:), allocatable,Optional, intent(out)  :: Fnd_par
     Type(Orient_cond_Type),             dimension(:), allocatable,Optional, intent(out)  :: Cnd
     Type(Orientation),                                            Optional, intent(out)  :: Orient
     Type(Atom_List_type),                                         Optional, intent(out)  :: A

     !---- Local Variables ----!
     Type(File_List_Type)  :: fich
     character(len=20)     :: spg_symb
     character(len=132)    :: line
     integer               :: i, j, n, ier, k, nfiles
     logical               :: esta, loaded, ok, snail, indexc, peakfiles



     if(present(A)) then
        call Readn_Set_Xtal_Structure(CFL_File,Cell,SpG,A,Mode="CFL",file_list=fich)
     else
        call File_to_filelist(CFL_File,fich)
        call Read_File_Cell(fich%line,1,fich%nlines,Cell)
        If(err_form) then
          write(unit=*,fmt="(a)") trim(err_form_mess)
          Error_Laue_utilities=.true.
          Error_Mess_Laue_utilities=" From Read_File_Cell: "//trim(err_form_mess)
          return
        end if
        call Read_File_Spg(fich%line,1,fich%nlines,spg_symb)
        If(err_form) then
          write(unit=*,fmt="(a)") trim(err_form_mess)
          Error_Laue_utilities=.true.
          Error_Mess_Laue_utilities=" From Read_File_Spg: "//trim(err_form_mess)
          return
        end if
        call Set_SpaceGroup(spg_symb,SPG)
     end if

     If(err_form) then
       write(unit=*,fmt="(a)") trim(err_form_mess)
       Error_Laue_utilities=.true.
       Error_Mess_Laue_utilities=" From Read_File_Spg: "//trim(err_form_mess)
       return
     end if
     !
     ! Search for the title
     title=" "
     do i=1,fich%nlines
        line=adjustl(l_case(fich%line(i)))
        if(line(1:5) == "title") then
          line=adjustl(fich%line(i))
          title=fich%line(i)(6:)
          exit
        end if
     end do
     if(present(lun)) write(unit=lun,fmt="(/,a,/)")   " => TITLE of the job: "//trim(title)
     !Write Crystallographic information

     if(present(lun)) call Write_Crystal_Cell(Cell,lun)
     if(present(lun)) call Write_SpaceGroup(SpG,lun)
     if(present(A) .and. present(lun) ) then
       if(A%natoms /= 0) call Write_Atom_List(A,lun=lun)
     end if
     !
     ! Search for instrument file name, the excluded regions are also included in the same file
      Loaded=.false.
      do i=1,fich%nlines
        line=adjustl(l_case(fich%line(i)))
        if(line(1:5) == "instr") then
           j=index(fich%line(i)," ")
           Instrm_File=adjustl(fich%line(i)(j:))
           write(unit=*,fmt="(a)") " => Instrument file: "//trim(Instrm_File)
           inquire(file=trim(Instrm_File), exist=esta)
           if(.not. esta)  then
              write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => The instrument file: "//trim(Instrm_File)//"  doesn't exist!"
              Error_Laue_utilities=.true.
              write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
              return
           else
              call Read_Laue_Instrm(Instrm_File, LaueDiff)
              if(Error_Laue_Mod) then
                write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error!: "//trim(Error_Mess_Laue_Mod)
                Error_Laue_utilities=.true.
                write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                return
              end if
              !Writing information about the Laue instrument
              if(present(lun)) write(unit=lun,fmt="(/,/,/,a)")  " => Information about the Laue Diffractometer read in the file: "&
              //trim(Instrm_file)
              if(present(lun)) Call Write_Laue_Instrm(LaueDiff,lun)
              Loaded=.true.
           end if
           exit
        end if
     end do
     ! Look for excluded regions in the instrument file
     if(Loaded) then
        if(present(ExclR)) then
          if(present(lun)) then
             call Read_Excluded_Regions(Instrm_File,ExclR,ok,lun)
          else
             call Read_Excluded_Regions(Instrm_File,ExclR,ok)
          end if
          if(.not. ok) then
            write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading the excluded regions ... in "//trim(Instrm_File)
            Error_Laue_utilities=.true.
            write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
            return
          end if
        end if
     else
        write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => No instrument filename found in: "//trim(CFL_File)
        Error_Laue_utilities=.true.
        write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
        return
     end if

     ! Search for the initial orientation
     if(present(Orient)) then
        Orient%or_given=.false.
        do i=1,fich%nlines
           line=adjustl(l_case(fich%line(i)))
           if(line(1:6) == "orient") then
             read(unit=line(7:),fmt=*,iostat=ier) Orient%angles
             if(ier == 0) Orient%or_given=.true.
             exit
           end if
        end do
        if(present(lun)) write(unit=lun,fmt="(/,a,3f10.4/)")   " => ORIENTATION U-Matrix (Phi_x,Phi_y,Phi_z): ",Orient%angles
     End if

     ! Search for the names of the images to be read (and the conditions for peak finding in case of
     ! using the Luiso's subroutine)
     if(present(Image_Files) .or. present(Peak_files)) then
       loaded=.false.
       peakfiles=.false.
       do i=1,fich%nlines
          line=adjustl(l_case(fich%line(i)))

          Select Case(line(1:11))

            Case("image_files")

             j=index(line," ")
             read(unit=line(j:),fmt=*,iostat=ier) n
             if(ier /= 0) then
                write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading the number of images to load "
                Error_Laue_utilities=.true.
                write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                return
             end if
             Image_Files%n_images=n
             if(allocated(Image_Files%Names)) deallocate(Image_Files%Names)
             allocate(Image_Files%Names(n))

             if(present(Fnd_par)) then
                if(allocated(Fnd_par)) deallocate(Fnd_par)
                allocate(Fnd_par(n))
             end if

             if(present(Cnd)) then
                if(allocated(Cnd)) deallocate(Cnd)
                allocate(Cnd(n))
             end if

             n=i
             j=0
             do
               n=n+1
               line=adjustl(fich%line(n))
               if(len_trim(line) == 0) cycle
               j=j+1
               k=index(line," ")
               Image_Files%Names(j)= line(1:k-1)
               if(present(Fnd_par)) then
                 Fnd_par(j)= Finding_Peaks_Parameters_Type(5,0,6,3,0,0)
                 read(unit=line(k:),fmt=*,iostat=ier) Fnd_par(j)%d_min,Fnd_par(j)%m_pk,Fnd_par(j)%pk_ar
                 if(ier /= 0) then
                    Fnd_par(j)%d_min=12; Fnd_par(j)%m_pk=2; Fnd_par(j)%pk_ar=8
                 end if
               end if
               if( j == Image_Files%n_images) exit
               line=l_case(line)
               if(line(1:4) == "end_") then
                  Image_Files%n_images=j-1
                  exit
               end if
             end do
             loaded=.true.
             exit

            Case ("peaks_files")
             j=index(line," ")
             read(unit=line(j:),fmt=*,iostat=ier) n
             if(ier /= 0) then
                write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading the number of peak files to load "
                Error_Laue_utilities=.true.
                write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                return
             end if
             Peak_Files%n_files=n
             if(allocated(Peak_Files%Names)) deallocate(Peak_Files%Names)
             allocate(Peak_Files%Names(n))

             if(present(Fnd_par)) then
                if(allocated(Fnd_par)) deallocate(Fnd_par)
                allocate(Fnd_par(n))
             end if

             if(present(Cnd)) then
                if(allocated(Cnd)) deallocate(Cnd)
                allocate(Cnd(n))
             end if

             n=i
             j=0
             do
               n=n+1
               line=adjustl(fich%line(n))
               if(len_trim(line) == 0) cycle
               j=j+1
               k=index(line," ")
               Peak_Files%Names(j)= line(1:k-1)
               if(present(Fnd_par)) then
                 Fnd_par(j)= Finding_Peaks_Parameters_Type(5,0,6,3,0,0)
                 read(unit=line(k:),fmt=*,iostat=ier) Fnd_par(j)%d_min,Fnd_par(j)%m_pk,Fnd_par(j)%pk_ar
                 if(ier /= 0) then
                    Fnd_par(j)%d_min=12; Fnd_par(j)%m_pk=2; Fnd_par(j)%pk_ar=8
                 end if
               end if
               if( j == Peak_Files%n_files) exit
               line=l_case(line)
               if(line(1:4) == "end_") then
                  Peak_Files%n_files=j-1
                  exit
               end if
             end do
             peakfiles=.true.
             exit
          End Select
       end do

       if(.not. loaded .and. .not. peakfiles) then
          write(unit=*,fmt="(a)") " => No image or peak file names found in: "//trim(CFL_File)
          return
       end if

       ! Writing information about the Laue Images to be treated
       if (loaded) then
         if (present(lun)) then
           write(unit=lun,fmt="(/,/,/,a,i4)")  " => Number of Laue Images to be treated: ", Image_Files%n_images
           write(unit=lun,fmt="(a)")           " => File names of the Laue Images: "
           write(unit=*,fmt="(a)")             " => File names of the Laue Images: "
           do i=1, Image_Files%n_images
             write(unit=lun,fmt="(tr20,a)") trim(Image_Files%Names(i))
             write(unit=*,fmt="(tr20,a)") trim(Image_Files%Names(i))
           end do
         end if
         nfiles=Image_Files%n_images
       end if

       ! Writing information about the Peaks files to be treated
       if(peakfiles  ) then
         if (present(lun))then
           write(unit=lun,fmt="(/,/,/,a,i4)")  " => Number of Peak Files to be treated: ", Peak_Files%n_files
           write(unit=lun,fmt="(a)")           " => File names of the peak files: "
           write(unit=*,fmt="(a)")             " => File names of the peak files: "
           do i=1, Peak_Files%n_files
             write(unit=lun,fmt="(tr20,a)") trim(Peak_Files%Names(i))
             write(unit=*,fmt="(tr20,a)") trim(Peak_Files%Names(i))
           end do
         end if
         nfiles=Peak_Files%n_files
       end if

       If(present(Fnd_par) .and. present(Cnd)) then
         ! Search for conditions of peak finding (snail) and indexing
         i=0
         snail=.false.
         indexc=.false.
         do
            i=i+1
            if(i > fich%nlines) exit
            line=adjustl(l_case(fich%line(i)))

            Select Case (trim(line(1:13)))

              Case("peak_find_cw")
                 read(unit=line(14:),fmt=*,iostat=ier) Fnd_par(1)%CutOff, Fnd_par(1)%BlockSz,Fnd_par(1)%pk_ar,Fnd_par(1)%d_min
                 if(ier /= 0) then
                    Fnd_par(1)= Finding_Peaks_Parameters_Type(5,0,9,3,15,40)
                 end if
                 Fnd_par(2:nfiles)=Fnd_par(1)

              Case("peak_find_lfm")
                 read(unit=line(14:),fmt=*,iostat=ier) Fnd_par(1)%d_min,Fnd_par(1)%m_pk,Fnd_par(1)%pk_ar
                 if(ier /= 0) then
                    Fnd_par(1)= Finding_Peaks_Parameters_Type(5,0,6,3,0,0)
                 end if
                 Fnd_par(2:nfiles)=Fnd_par(1)

              Case("glb_indexing")
                 j=0
                 do
                   i=i+1
                   if(len_trim(fich%line(i)) == 0 .or. fich%line(i)(1:1) == "!" .or. fich%line(i)(1:1) == "#") cycle
                   j=j+1
                   Read(unit=fich%line(i),fmt=*,iostat=ier) Cnd(j)
                   if(ier /= 0) then
                     write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading 'indexing' conditions in "//trim(CFL_File)
                     Error_Laue_utilities=.true.
                     write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                     write(unit=*,fmt="(a)") " => Line read: "//trim(fich%line(i))
                     return
                   end if
                   if(j == 1) exit
                 end do
                 Cnd(2:j)=Cnd(1)
                 indexc=.true.

              Case("peakfind")
                 j=0
                 do
                   i=i+1
                   if(len_trim(fich%line(i)) == 0 .or. fich%line(i)(1:1) == "!" .or. fich%line(i)(1:1) == "#") cycle
                   j=j+1
                   Fnd_par(j)= Finding_Peaks_Parameters_Type(5,0,9,3,15,40)  !This supersedes conditions for Luiso's peak-find
                   Read(unit=fich%line(i),fmt=*,iostat=ier) Fnd_par(j)%CutOff,Fnd_par(j)%Blocksz,Fnd_par(j)%pk_ar,Fnd_par(j)%d_min
                   if(ier /= 0) then
                     write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading 'peakfind' threshold in : "//trim(CFL_File)
                     Error_Laue_utilities=.true.
                     write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                     write(unit=*,fmt="(a)") " => Line read: "//trim(fich%line(i))
                   end if
                   if(j == nfiles) exit
                 end do
                 snail=.true.

              Case("indexing")
                 j=0
                 do
                   i=i+1
                   if(len_trim(fich%line(i)) == 0 .or. fich%line(i)(1:1) == "!" .or. fich%line(i)(1:1) == "#") cycle
                   j=j+1
                   Read(unit=fich%line(i),fmt=*,iostat=ier) Cnd(j)
                   if(ier /= 0) then
                     write(unit=Error_Mess_Laue_utilities,fmt="(a)") " => Error reading 'indexing' conditions in "//trim(CFL_File)
                     Error_Laue_utilities=.true.
                     write(unit=*,fmt="(a)") trim(Error_Mess_Laue_utilities)
                     write(unit=*,fmt="(a)") " => Line read: "//trim(fich%line(i))
                   end if
                   if(j == nfiles) exit
                 end do
                 indexc=.true.
            End Select

         end do
         ! Writing information about the Peak-Find conditions
         if (present(lun)) then
           if(snail ) then
             write(unit=lun,fmt="(/,a)") &
             " => SNAIL (Peak_Find_Threshold, C.Wilkinson) algorithm will be used (CutOff, BlockSize, area, d_min): "
             do i=1, Image_Files%n_images
               write(unit=lun,fmt="(a,i3,a,4i4)") " => Conditions for Laue Image: "//trim(Image_Files%Names(i))//" # ",i, &
                 ": ",Fnd_par(i)%CutOff,Fnd_par(i)%Blocksz,Fnd_par(i)%pk_ar,Fnd_par(i)%d_min
             end do
           else
             write(unit=lun,fmt="(/,a)") " => Peak_Find (LFM) algorithm will be used (d_min, slope, area): "
             do i=1, Image_Files%n_images
               write(unit=lun,fmt="(a,i3,a,3i4)") " => Conditions for Laue Image: "//trim(Image_Files%Names(i))//" # ",i, &
                 ": ",Fnd_par(i)%d_min,Fnd_par(i)%m_pk,Fnd_par(i)%pk_ar
             end do
           end if

            if(indexc) then
             write(unit=lun,fmt="(/,a)") " => Conditions for indexing: "
             write(unit=lun,fmt="(a)") " => List of items: Ang_Type, coprime,  nmax_ref, nmax_sol"//  &
             ", norient, maxind, exhaust, angtol,  dmmtol, ang_min, ang_max "
             do i=1, Image_Files%n_images
               write(unit=lun,fmt="(a,i3,a,a,L7,5i4,4f8.3)") &
                " => Conditions for Laue Image: "//trim(Image_Files%Names(i))//" # ",i,": ", Cnd(i)
             end do
            end if
         end if
       End if ! Present Fnd_par .and. Cnd
    End If !Present Image_Files or Peak_Files
   return
   End Subroutine Read_Laue_CFL_File

   Subroutine Write_Indexed_Reflections(imgname,spindle,ORfL,RfL,S,tol,lun,iexp)
     character(len=*),           intent(in)     :: imgname
     real,                       intent(in)     :: spindle
     Type(Laue_Ref_List_Type),   intent(in out) :: ORfL
     Type(Laue_Ref_List_Type),   intent(in)     :: RfL
     Type(Orient_Solution_List), intent(in out) :: S
     Real,                       intent(in)     :: tol
     Integer,                    intent(in)     :: lun
     Integer, optional,          intent(in)     :: iexp !Logical unit for indexed file
     !---- Local variables ----!
     integer ::  n,j,k,is, ncoi
     real    :: dist, rms
     integer, dimension(ORfL%nref) :: io, poi
     integer, dimension(RfL%nref)  :: ic
     real, dimension(3,3)          :: RL

     !Test for a 10% decrease of tolerance
     call matching_4V(ORfL%LR(:)%x,ORfL%LR(:)%z,RfL%LR(:)%x,RfL%LR(:)%z,ORfL%nref,RfL%nref,tol*0.9,io,ic,ncoi)
     !Mapping of observed reflections into coincidences
     poi=0
     do is=1,ncoi
        j=io(is)
        k=ic(is)
        poi(j)=k
     end do

     RL=Matrix_Rz(spindle,"D")
     RL=Matmul(RL,S%Sol(1)%ub)     !Real UB-Matrix
     write(unit=lun,fmt="(/a,a)")                " => INDEXING of a Laue Pattern "
     write(unit=lun,fmt="(a,i3,a,f10.4,a)")      " => Initial RMS for solution 1 of ",S%nsol," solutions: ",S%Sol(1)%rms," mm"
     write(unit=lun,fmt="(a,i5)")                " => Initial number of indexed spots ",S%Sol(1)%num_indx
     write(unit=lun,fmt="(a,f8.2,tr30,a,/)") " => UB-matrix,  Spindle=",spindle,"Spindle=0.0 (UB, B-L matrix)"
     do j=1,3
        write(unit=lun,fmt="(tr10,3f12.5,tr10,3f12.5)") S%Sol(1)%ub(j,:),RL(j,:)
     end do
     write(unit=lun,fmt="(a,3f10.3)")            " => Rotation angles Phix, Phiy, Phiz (degrees): ",S%Sol(1)%ang
     write(unit=lun,fmt="(/,/,a)") &
      "    Spot#      Xobs      Zobs     Xcalc     Zcalc      Dist       H    K    L      Intensity"
     n=0
     rms=0.0
     do j=1,ORfL%nref
        k=poi(j)
        if( k /= 0) then
          n=n+1
          ORfL%LR(j)%h=RfL%LR(k)%h  !/RfL%LR(k)%nodal
          dist=(ORfL%LR(j)%x-RfL%LR(k)%x)**2+(ORfL%LR(j)%z-RfL%LR(k)%z)**2
          rms=rms+dist
          dist=sqrt(dist)
          write(unit=lun,fmt="(i9,5f10.3,tr3,3i5,f14.0)") j,ORfL%LR(j)%x,ORfL%LR(j)%z, RfL%LR(k)%x    ,&
                                                     RfL%LR(k)%z ,dist,nint(ORfL%LR(j)%h),ORfL%LR(j)%Obs_int
        else
          write(unit=lun,fmt="(i9,2f10.3,tr48,f14.0)") j,ORfL%LR(j)%x,ORfL%LR(j)%z,ORfL%LR(j)%Obs_int
        end if
     end do
     rms=sqrt(rms/real(n))

     if(present(iexp)) then
       write(unit=iexp,fmt="(a)") "NEW DATA SET :    "//trim(imgname)
       write(unit=iexp,fmt="(a,f10.4,i6,a,f8.4)") "SPINDLE ", spindle,n,"  reflections, initial RMS: ",rms
       do j=1,ORfL%nref
        k=poi(j)
        if( k /= 0) then
          write(unit=iexp,fmt="(2f11.4,tr3,3i5,f14.0)") ORfL%LR(j)%x,ORfL%LR(j)%z,nint(ORfL%LR(j)%h),ORfL%LR(j)%Obs_int
        end if
       end do
     end if

     write(unit=lun,fmt="(2(a,f8.4),a/a,i4)") &
         " => Root mean square error for 10% reduction of tolerance(",tol*0.9, "): ",rms," mm", &
         " => Number of final indexed spots: ",n
     S%sol(1)%num_indx=n
     S%sol(1)%rms=rms
     return
   End Subroutine Write_Indexed_Reflections

   Subroutine Get_Approx_Orientation(ns,nstep,delta,LaueDiff,hkl,ORfL,tol,RfL,spindle,UB,rms,nmax)
     integer,                    intent(in)     :: ns,nstep
     real,                       intent(in)     :: delta
     Type(Laue_Instrument_Type), intent(in)     :: LaueDiff
     Type(Reflection_List_Type), intent(in)     :: hkl
     Type(Laue_Ref_List_Type),   intent(in out) :: ORfL
     Real,                       intent(in)     :: tol
     Type(Laue_Ref_List_Type),   intent(out)    :: RfL
     real,                       intent(in out) :: spindle
     real, dimension(3,3),       intent(in out) :: UB
     real,                       intent(   out) :: rms
     integer,                    intent(   out) :: nmax
     !---- Local variables ----!
     integer :: i, n,j,k,is, ncoi
     real    :: dist, spi,spd, step
     integer, dimension(ORfL%nref) :: io, poi, ic
     real, dimension(3,3)          :: Ri,Rt,RL

     Ri  = UB
     spi = spindle-0.5*delta
     step=delta/real(nstep-1)
     nmax=0
     do i=1,nstep
       spd= spi+real(i-1)*step
       Rt=Matrix_Rz(-spd,"D") ! R
       Rt=Matmul(Rt,Ri)       ! RL = R.UB
       call Calc_Visible_Reflections(ns,LaueDiff,hkl,Rt,RfL)
       call matching_4V(ORfL%LR(:)%x,ORfL%LR(:)%z,RfL%LR(:)%x,RfL%LR(:)%z,ORfL%nref,RfL%nref,tol,io,ic,ncoi)
       if(ncoi > nmax) then
         !write(*,*) " => Number of coincidences: ",ncoi, "  Spindle: ",spd
         nmax=ncoi
         spindle=spd
         RL=Rt
         Rt=Matrix_Rz(spd,"D")
         UB=matmul(Rt,RL)
       end if
     end do
     !Perform the calculation with the best orientation matrix
     call Calc_Visible_Reflections(ns,LaueDiff,hkl,RL,RfL)
     call matching_4V(ORfL%LR(:)%x,ORfL%LR(:)%z,RfL%LR(:)%x,RfL%LR(:)%z,ORfL%nref,RfL%nref,tol,io,ic,nmax)

     !Mapping of observed reflections into coincidences
     poi=0
     do is=1,nmax
        j=io(is)
        k=ic(is)
        poi(j)=k
     end do

     n=0
     rms=0.0
     do j=1,ORfL%nref
        k=poi(j)
        if( k /= 0) then
          n=n+1
          ORfL%LR(j)%h=RfL%LR(k)%h  !/RfL%LR(k)%nodal
          dist=(ORfL%LR(j)%x-RfL%LR(k)%x)**2+(ORfL%LR(j)%z-RfL%LR(k)%z)**2
          rms=rms+dist
        end if
     end do
     rms=sqrt(rms/real(n))

     return
   End Subroutine Get_Approx_Orientation


   !!---- Subroutine Calc_Visible_Reflections(ns,LaueDiff,hkl,RL,Refl,distort)
   !!----   integer,                             intent(in)  :: ns
   !!----   Type(Laue_Instrument_Type),          intent(in)  :: LaueDiff
   !!----   Type(Reflection_List_Type),          intent(in)  :: hkl
   !!----   Real, dimension(3,3),                intent(in)  :: RL
   !!----   Type(Laue_Ref_List_Type),            intent(out) :: Refl
   !!----   Type(Image_Distortion_Type),optional,intent(in)  :: distort
   !!----
   !!----  Updated July-2012 (JRC): including "distort"
   !!----
   !!----
   Subroutine Calc_Visible_Reflections(ns,LaueDiff,hkl,RL,Refl,distort,mask)
     integer,                                 intent(in) :: ns
     Type(Laue_Instrument_Type),              intent(in) :: LaueDiff
     Type(Reflection_List_Type),              intent(in) :: hkl
     Real, dimension(3,3),                    intent(in) :: RL
     Type(Laue_Ref_List_Type),                intent(out):: Refl
     Type(Image_Distortion_Type),    optional,intent(in) :: distort
     integer(kind=1), dimension(:,:),optional,intent(in) :: mask

     !---- Local variables ----!
     integer :: n, ip, i, j
     real    :: ri,rj
     real, dimension(2)       :: pos, shift
     Type(Laue_Ref_Type)      :: Ref
     Type(Laue_Ref_List_Type) :: temp_Refl

      call Allocate_Laue_Ref_list(temp_Refl,ns)
      n=0
      call Init_Laue_Ref(Ref)
      do ip=1,hkl%nref   !Generate the reflections corresponding to the R.UB matrix
        Ref%h=hkl%Ref(ip)%h
        Call Calc_Laue_Spot_XZ_Angles(LaueDiff,Ref,RL)
        if(Ref%stats == 0) then  !Only for the allowed reflections in the given orientation
          if(Apply_Offsets) then  !Correct for distortions
            pos=(/Ref%x , Ref%z/)
            if(present(distort)) call Corr_MOSFLM(LaueDiff%D,distort,pos)
            !Only the dimensions of the detector are used in Corr_xz_Chebychev
            call Corr_xz_Chebychev(pos,LaueDiff,Ncoeff,Cheb,shift)
            Ref%x=pos(1)+shift(1) !Total distortions are now stored in the reflection type
            Ref%z=pos(2)+shift(2)
          end if
          if(present(Mask)) then
            call Mil_to_Pix(LaueDiff,Ref%x,Ref%z,rj,ri)
            i=nint(ri); j=nint(rj)
            if(Mask(i,j) == -1) cycle
          end if
          n=n+1
          temp_RefL%LR(n)=Ref
        end if
      end do
      !Allocate the visible spots in the current orientation
      call Allocate_Laue_Ref_list(Refl,n)
      do n=1,RefL%nref
         RefL%LR(n)=temp_RefL%LR(n)
         call Calc_Spherical_angles_from_xz(LaueDiff,RefL%LR(n),n)
      end do
      return
   End Subroutine Calc_Visible_Reflections

   Subroutine Reflection_Generation(Cell,SpG,LaueDiff,hkl,Ind,Ref,lun)
     Type(Crystal_Cell_Type),            intent(in)  :: Cell
     Type(Space_Group_Type),             intent(in)  :: SpG
     Type(Laue_Instrument_Type),         intent(in)  :: LaueDiff
     Type(Reflection_List_Type),         intent(out) :: hkl
     integer, dimension(:), allocatable, intent(out) :: Ind
     Type(Laue_Ref_List_Type),           intent(out) :: Ref
     integer, optional,                  intent(in)  :: lun
     !---- Local variables ----!
     integer, dimension(3) :: ord
     real                  :: stlmax
     integer               :: MaxNumRef, iou, n

     iou=6
     if(present(lun)) iou=lun
     ord=(/3,2,1/)
     stlmax=1.0/(2.0*LaueDiff%d_min)
     MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult=SpG%Multip)
     MaxNumRef=MaxNumRef*Spg%NumOps*max(Spg%Centred,1)
     write(unit=iou,fmt="(/,/,a)")       " => Information about generated reflections: "
     write(unit=iou,fmt="(a,i6,a,f8.4)") " => MaxNumRef: ",MaxNumRef," stlmax: ",stlmax
     call Hkl_gen_sxtal(Cell,SpG,0.0,stlmax,MaxNumRef,hkl,ord)
     write(unit=iou,fmt="(a,i8,a)")      " => Generated ",hkl%nref," reflections"

     !Calculate the expected maximum number of Laue Reflections that can be simultaneously
     !stimulated according to the limits stored in "Limits"
      if(LaueDiff%L_max < 2.0 * LaueDiff%d_min) then  !ITC vol C, p. 28
       n= 0.25*pi*(LaueDiff%L_max-LaueDiff%L_min)*Cell%CellVol/real(Spg%NumLat)/LaueDiff%d_min**4
      else
       n = 4.0/3.0*pi*((1.0/LaueDiff%L_min)**3-(1.0/LaueDiff%L_max)**3)*Cell%CellVol/real(Spg%NumLat)
       n = min(hkl%nref,n)
      end if
     write(unit=iou,fmt="(a,3f9.4)") " => Lambda min, max, d_min: ", LaueDiff%L_min, LaueDiff%L_max , LaueDiff%d_min
     write(unit=iou,fmt="(a,i6)")    " => Estimated maximum number of stimulated reflections: ", n

     !Allocation of memory and initialization of arrays
     call Allocate_Laue_Ref_list(Ref,n)

     if(allocated(Ind)) deallocate(Ind)
     Allocate(Ind(hkl%nref))
     call sort(hkl%Ref(:)%s,hkl%nref,Ind)  !Sort reflections by sinTheta/Lambda increasing
     Return
   End Subroutine Reflection_Generation

   Subroutine Get_Found_Peaks(Mask,Datam,LaueDiff,ORef,ok)
     integer(kind=1), dimension(:,:), intent(in)  :: Mask
     integer,         dimension(:,:), intent(in)  :: Datam
     Type(Laue_Instrument_Type),      intent(in)  :: LaueDiff
     Type(Laue_Ref_List_Type),        intent(out) :: ORef
     logical,                         intent(out) :: ok
     !---- Local variables ----!
     Type :: simple_peak
       Real       :: X,Z           !Position  of Maxima in Pixels
       Real       :: Intensity     !Intensity of Maxima
     End Type simple_peak
     type(simple_peak), dimension(:),allocatable :: peak
     type(simple_peak) :: tmp_pk
     integer :: i,j,row,col,Nrow,Ncol,peak_count,zop

     ok=.false.
     Nrow=size(Mask,1)
     Ncol=size(Mask,2)
     peak_count=0
     zop=1
     do col=1,Ncol
       do row=1,Nrow
         if ( Mask(row,col) == 1 ) peak_count=peak_count+1
       end do
     end do
     if(peak_count <= 1) return
     allocate(peak(peak_count))
     i=0
     do row=1,Nrow,1
       do col=1,Ncol,1
          if ( Mask(row,col) == 1 ) then
                 i=i+1
                 peak(i)%x=col
                 peak(i)%z=row
                 peak(i)%Intensity=Datam(row,col)
          end if
       end do
     end do
     do i=1 ,peak_count-1
         do j=i+1,peak_count
             if (peak(i)%Intensity < peak(j)%Intensity) then
                 tmp_pk=peak(j)
                 peak(j)=peak(i)
                 peak(i)=tmp_pk
             end if
         end do
     end do
     call Allocate_Laue_Ref_list(ORef,peak_count)
     do i=1,peak_count
         ORef%LR(i)%Obs_Int=peak(i)%Intensity
         call pix_to_mil(LaueDiff,peak(i)%x,peak(i)%z,ORef%LR(i)%x,ORef%LR(i)%z) !Get position in mm
         call Calc_Spherical_angles_from_xz(LaueDiff,ORef%LR(i),zop)
     end do
     ok=.true.
     return
   End Subroutine Get_Found_Peaks

   Subroutine Re_Index_Reflections(imgname,spindle,Pk,RfL,tol, n_index, n_unindex)
     character(len=*),           intent(in)     :: imgname  !Name of the image file for output
     real,                       intent(in)     :: spindle  !Used only for output in the file
     Type(Simple_Peaks_Type),    intent(in out) :: Pk       !Observed peaks (Indexed + unindexed)
     Type(Laue_Ref_List_Type),   intent(in)     :: RfL      !Visible reflections
     Real,                       intent(in)     :: tol      !tolerance for indexing
     Integer,                    intent(out)    :: n_index,n_unindex
     !---- Local variables ----!
     character(len=132)            :: fileiref   !file with data on indexed reflections
     character(len=132)            :: unindexed_name
     integer                       :: n,i,j,k,is, ncoi,unin,lun, iexp
     real                          :: dist, rms
     integer, dimension(Pk%npeaks) :: io, poi
     integer, dimension(RfL%nref)  :: ic

     !Opening the files containing indexed and unindexed peaks
     i=index(imgname,".",back=.true.)
     if(i /= 0) then
       fileiref=imgname(1:i-1)//"_peaks.inf"
       unindexed_name=imgname(1:i-1)//"_unindexed_peaks.inf"
     else
       fileiref=trim(imgname)//"_peaks.inf"
       unindexed_name=trim(imgname)//"_unindexed_peaks.inf"
     end if
     call Get_logUnit(iexp)
     open(unit=iexp,file=trim(fileiref),status="replace",action="write")
     write(unit=iexp,fmt="(a)") "NUMBER OF DATA SETS:  1"
     Call Get_LogUnit(lun)
     open(unit=lun,file=trim(unindexed_name),status="replace",action="write")
     write(unit=lun,fmt="(a)") "  UNINDEXED PEAKS OF IMAGE: "//trim(imgname)

     !Test for a 10% decrease of tolerance
     call matching_4V(Pk%x,Pk%z,RfL%LR(:)%x,RfL%LR(:)%z,Pk%npeaks,RfL%nref,tol*0.9,io,ic,ncoi)

     !Mapping of observed reflections into coincidences
     poi=0
     do is=1,ncoi
        j=io(is)
        k=ic(is)
        poi(j)=k
     end do
     n=0
     rms=0.0
     unin=0
     do j=1,Pk%npeaks
        k=poi(j)
        if( k /= 0) then
          n=n+1
          dist=(Pk%x(j)-RfL%LR(k)%x)**2+(Pk%z(j)-RfL%LR(k)%z)**2
          rms=rms+dist
          dist=sqrt(dist)
        else
          unin=unin+1
          write(unit=lun,fmt="(i9,2f10.3,tr48,f14.0)") j,Pk%x(j),Pk%z(j),Pk%intensity(j)
        end if
     end do
     n_index=n
     n_unindex=unin
     write(unit=lun,fmt="(a,i5)") "  TOTAL NUMBER OF UNINDEXED PEAKS: ",unin
     close(unit=lun)
     rms=sqrt(rms/real(n))
     write(unit=iexp,fmt="(a)") "NEW DATA SET :    "//trim(imgname)
     write(unit=iexp,fmt="(a,f10.4,i6,a,f8.4)") "SPINDLE ", spindle,n,"  reflections, initial RMS: ",rms
     do j=1,Pk%npeaks
      k=poi(j)
      if( k /= 0) then
        write(unit=iexp,fmt="(2f11.4,tr3,3i5,f14.0)") Pk%x(j),Pk%z(j),nint(RfL%LR(k)%h),Pk%intensity(j)
      end if
     end do
     flush(unit=iexp)
     close(unit=iexp)
     return
   End Subroutine Re_Index_Reflections

 End Module Laue_Utilities_Mod
