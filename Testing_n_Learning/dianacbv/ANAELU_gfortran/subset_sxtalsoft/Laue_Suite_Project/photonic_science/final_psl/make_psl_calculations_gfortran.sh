OPT1="-c -O3"
#OPT1="-c -g -debug full -CB"
INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LAUESUI="$SXTALSOFT/Laue_Suite_Project"

   echo "Compiling"
   echo "  Low level modules"
gfortran $LAUESUI/Laue_Modules/Src/gfortran_specific.f90            $OPT1
gfortran $LAUESUI/Laue_Modules/Src/Laue_Module.f90                  $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Data_Trt.f90                     $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Intensity.f90               $OPT1  $INC
gfortran $LAUESUI/Image_handling/Src/Laue_tiff_read.f90             $OPT1  $INC     -fno-range-check
gfortran $LAUESUI/Image_handling/Src/rnw_bin.f90                    $OPT1           -fno-range-check
   echo "  Data handling modules"
gfortran $LAUESUI/Laue_Modules/Src/Laue_Utilities_Mod.f90           $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Suite_Global_Parameters.f90 $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Sim_Mod.f90                 $OPT1  $INC     -fno-range-check

echo "Compilig f90"

gfortran lafg_IMG.f90  $OPT1 $INC
gfortran lafg.f90      $OPT1 $INC

#gfortran $OPT1  lafg.f90

#Linking
echo "Linking"
gfortran -o psl_calc.r *.o $LIB -lcrysfml

#
#Removing the files that are no longer needed

rm *.o *.mod

