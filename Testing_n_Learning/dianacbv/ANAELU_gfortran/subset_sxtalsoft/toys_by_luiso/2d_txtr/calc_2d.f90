!     This file is part of the Python - Fortran version of Anaelu project,
!     a tool for the treatment of 2D - XRD patterns of textured samples
!     Copyright (C) 2013
!     Luis Fuentes Montero (Luiso)
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU Lesser General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU Lesser General Public License for more details.
!
!     You should have received a copy of the GNU Lesser General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.

Module Calc_2D_pat
   ! -  -  -  -  Use Modules  -  -  -  - !
   use CFML_crystallographic_symmetry, only: space_group_type, Write_SpaceGroup
   use CFML_Atom_TypeDef,              only: Atom_List_Type, Write_Atom_List
   use CFML_Crystal_Metrics,           only: Crystal_Cell_Type, Write_Crystal_Cell
   use CFML_Reflections_Utilities,     only: Reflection_List_Type, Hkl_Uni, get_maxnumref
!   use CFML_IO_Formats,                only: Readn_set_Xtal_Structure,err_form_mess,err_form,file_list_type
   use CFML_IO_Formats,                only: err_form_mess,err_form

   use CFML_String_Utilities,          only: get_logunit
   use CFML_Structure_Factors,         only: Structure_Factors, Write_Structure_Factors, &
                                             Init_Structure_Factors,Calc_StrFactor
   use CFML_PowderProfiles_CW,         only: PseudoVoigt, Gaussian, Lorentzian
   use CFML_Reflections_Utilities,     only: Hkl_Equiv_List, Unit_Cart_Hkl
   use CFML_Crystallographic_Symmetry, only: Get_Laue_Pg, Set_Spacegroup
   use CFML_Math_3D,                   only: Get_Cart_from_Spher, Get_Spheric_Coord
   use CFML_Geometry_Calc,             only: Angle_Uv, Matrix_Ry, Matrix_Rz, Matrix_Rx
   Use CFML_Math_General,              only : Cosd, Sind, Acosd, atand!, cotan
   use compilers_specific
   ! -  -  -  -  Variables  -  -  -  - !
   implicit none
   private
    public ini_2d_det, ini_intens, ipf_ini, ipf_calc, calc_2D, wr_img_file, &
                pf_calc, area_det_intrum, sample_n_instrument_properties, &
                 Crystal_Cell_Type, space_group_type, Atom_list_Type
    Type   :: area_det_intrum
        integer :: xres,yres             ! res (pixels)
        real    :: lambda                ! wavelenght
        real    :: Thmin, Thmax          ! Min and Max angles
        real    :: dst_det,diam_det      ! Distances (mm)
        real    :: x_dr,y_dr             ! position of direct beam in the detector (pixels)
    End Type area_det_intrum

    Type   :: sample_n_instrument_properties
        real                    :: crys_size = 0.45     ! crystal size
        real                    :: itm_broad = 0.02     ! instrumental broadening in deg
        integer                 :: ipf_xres
        integer                 :: ipf_yres
        real                    :: prf_width
        real                    :: ipf_ang = 0.0
        character(len = 256)      :: pref_hkl_string
    End Type sample_n_instrument_properties

    Type   :: peak_details
        integer, dimension(3)           :: H            ! H K L
        real                            :: Its          ! calculated intensity
        real                            :: bragg        ! bragg angle
        real                            :: fwhm         ! full width at half maximum
        integer                         :: wt_px        ! width in pixels
        integer                         :: dst_px_from  ! distance in pixels from the direct beam position  (start)
        integer                         :: dst_px_to    ! distance in pixels from the direct beam position    (end)
        integer                         :: dst_px_max_p ! distance in pixels from the direct beam position (max pos)
        real, dimension(:),allocatable  :: i_px         ! intensity in function of distance in pixels
    end Type peak_details


    Type   :: peak_details_list
        integer                                         :: npk  ! Number of peaks
        Type (peak_details), dimension(:), allocatable  :: pk   ! 1D data of the peak
    end Type peak_details_list

! __________________________________________________ CrysFML types

    type (space_group_type)  , public , save  :: SpG
    type (Atom_list_Type)    , public , save  :: A
    type (Crystal_Cell_Type) , public , save  :: Cell
    type (sample_n_instrument_properties) , public , save   :: sm_prt ! parameters that affect the peak width and azimut
    type (area_det_intrum)                , public , save   :: par_2d   ! parameters of diffractometer with 2D detector
    type (Reflection_List_Type)           , public , save   :: hkl
    type (peak_details_list)              , public , save   :: pk_lst   ! list of calculated peaks
! it is not clear how gfortran imlpements public + save variables
    real                                  , public , save   :: pi = 3.14159265358979323

    character(len = 256)          :: fname
    real                :: stlmax           ! Maximum Sin(Theta) / Lambda
    real                :: to_deg, to_rad   ! for converting radians to degrees and vise versa



    real, dimension(:,:), allocatable , public , save  :: pat2d
    real, dimension(:,:), allocatable , public , save  :: pat2d_rot
    real, dimension(:,:), allocatable , public , save  :: ipf
    real, dimension(:,:), allocatable , public , save  :: pf
    real, dimension(:),   allocatable , public , save  :: tmp_pf
    real, dimension(:,:), allocatable , public , save  :: Debye_prof

    real                    :: pixel_siz    ! pixel size (mm)
    integer                 :: MaxNumRef
    integer                 :: k,j,i        ! Reused control cycle variables
    integer                 :: x, y

    integer             :: log_uni

contains
    subroutine ini_2d_det()
        to_rad = pi / 180.0
    !    integer             :: lun,n_lin
    !    character(len = 100)  :: lin_texto

    sm_prt%crys_size = 0.95     ! crystal size
    sm_prt%itm_broad = 0.02     ! instrumental broadening in deg

        pixel_siz =   par_2d%diam_det / ((real(par_2d%xres) + real(par_2d%xres)) / 2.0)

        ! 2D XRD calculated image
        allocate(pat2d(par_2d%xres,par_2d%yres))
        ! Rotated 2D XRD calculated image
        allocate(pat2d_rot(par_2d%xres,par_2d%yres))

        stlmax = sin(par_2d%Thmax * to_rad)  /  par_2d%lambda
        write(unit=*,fmt = "(/,/,6(a,/))")                                                  &
              "          ---------  PROGRAM Anaelu CLI   --------- "                      , &
              "     ******************************************************************"   , &
              "     * Calculates XRD - 2D pattern reading a *.CFL or a *.CIF file      *" , &
              "     ******************************************************************"   , &
              "                      (Luiso - JRC 2014 )"
        to_deg = 180.0 / pi
    return
    end subroutine ini_2d_det


    subroutine ini_intens
        real                :: LorentzF, Bragg, Intens, ang
        real                :: ss,cs,tt
        real                :: I_max = 0, I_scale = 1.0
        real                :: px_deg                         ! local amount of pixels per degree

        if (err_form) then
            write(unit=*,fmt = "(a)") trim(err_form_mess)
        else

            MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult = SpG%NumOps)
            call Hkl_Uni(Cell,Spg,.true.,0.0,stlmax,"s",MaxNumRef,hkl)

            call Init_Structure_Factors(hkl,A,Spg,mode = "XRA",lambda = par_2d%lambda)
            call Structure_Factors(A,SpG,hkl,mode = "XRA",lambda = par_2d%lambda)

            !call Init_Structure_Factors(hkl,A,Spg,mode = "XRA",lambda = .98)
            !call Structure_Factors(A,SpG,hkl,mode = "XRA",lambda = .98)

            call Write_Structure_Factors(6,hkl,mode = "XRA")

            pk_lst%npk = hkl%nref

            allocate(pk_lst%pk(hkl%nref))

            do i = 1,hkl%nref
                ss = par_2d%lambda * hkl%ref(i)%S
                cs = sqrt(abs(1.0 - ss * ss))
                !tt = ss / cs
                LorentzF = 0.5 / (ss * ss * cs)
                Bragg = 2.0 * asin(ss)  *  to_deg
                Intens =  (LorentzF * hkl%ref(i)%mult * hkl%ref(i)%Fc ** 2.0)
                !Intens =  LorentzF * hkl%ref(i)%Fc ** 2.0

                pk_lst%pk(i)%H = hkl%ref(i)%H
                pk_lst%pk(i)%Its = Intens

                if( Intens > I_max )then
                    I_max = Intens
                end if
                pk_lst%pk(i)%bragg = Bragg
            end do

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! start TMP hack
            if( I_max > 10 )then
                I_scale = 10000.0 / I_max
                do i = 1, hkl%nref
                    pk_lst%pk(i)%Its = I_scale * pk_lst%pk(i)%Its
                end do
            end if
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! end TMP hack
            do i = 1,hkl%nref

                pk_lst%pk(i)%fwhm = ( 0.9 * par_2d%lambda ) / ( cos(to_rad * pk_lst%pk(i)%bragg / 2.0) * sm_prt%crys_size )

!               average of the Pythagorean sum and linear sum of peak broadening
                pk_lst%pk(i)%fwhm = ( sqrt( pk_lst%pk(i)%fwhm ** 2.0 + sm_prt%itm_broad ** 2.0 ) &
                                        + ( pk_lst%pk(i)%fwhm + sm_prt%itm_broad ) ) &
                                                                                 / 2.0

                pk_lst%pk(i)%dst_px_max_p = par_2d%dst_det * tan((pk_lst%pk(i)%bragg) * to_rad) / pixel_siz
                px_deg = (par_2d%dst_det * tan((pk_lst%pk(i)%bragg + 0.5) * to_rad) -   &
                       par_2d%dst_det * tan((pk_lst%pk(i)%bragg - 0.5) * to_rad)) / pixel_siz

                pk_lst%pk(i)%wt_px = nint(pk_lst%pk(i)%fwhm * px_deg * 60.0)

                pk_lst%pk(i)%dst_px_from = pk_lst%pk(i)%dst_px_max_p - pk_lst%pk(i)%wt_px / 2.0
                pk_lst%pk(i)%dst_px_to = pk_lst%pk(i)%dst_px_max_p + pk_lst%pk(i)%wt_px / 2.0

                if( pk_lst%pk(i)%dst_px_from<0 )then
                    pk_lst%pk(i)%dst_px_from = 0
                end if
                allocate(pk_lst%pk(i)%i_px(1:pk_lst%pk(i)%wt_px))
                do j = 1, pk_lst%pk(i)%wt_px
                    ang = real(j) / real(px_deg)
                    pk_lst%pk(i)%i_px(j) = PseudoVoigt( ang, (/pk_lst%pk(i)%fwhm,0.5 /))
                end do
            end do

            write(unit=*,fmt=*) '____________________________________________________________________________________'
            write(unit=*,fmt=*) '  H   K   L             Angle       Intensities   fwhm(deg)   fwhm(pix)  '//&
           ' from(pix)     to(pix)   max_pos(pix)  '
            write(unit=*,fmt=*) ' '
            do i = 1,hkl%Nref
            write(unit=*,fmt = "(3i4, 2f18.5, f12.5, 4i12 )") hkl%ref(i)%h,pk_lst%pk(i)%bragg , &
                                 pk_lst%pk(i)%Its, pk_lst%pk(i)%fwhm, pk_lst%pk(i)%wt_px,  &
                                 pk_lst%pk(i)%dst_px_from, pk_lst%pk(i)%dst_px_to, &
                                 pk_lst%pk(i)%dst_px_max_p
            end do

        end if
    return
    end subroutine ini_intens
    !subroutine ipf_ini(sm_prt%ipf_xres, sys_crys)
    !subroutine ipf_ini( sys_crys)
    subroutine ipf_ini()
        !integer             ,intent(in)     :: sm_prt%ipf_xres
        !character(len=*)    ,intent(out)    :: sys_crys
        integer                             :: n_pn_spgr
        type (space_group_type)             :: L_Grp
        character(len = 10)                   :: l_gr_shar

        sm_prt%ipf_yres =  nint(sm_prt%ipf_xres / 2.0 )
        if( allocated(ipf) )then
            deallocate(ipf)
        end if
        if( allocated(pf) )then
            deallocate(pf)
        end if
        if( allocated(tmp_pf) )then
            deallocate(tmp_pf)
        end if
        allocate(ipf(1:sm_prt%ipf_yres,1:sm_prt%ipf_xres))
        allocate(pf(1:sm_prt%ipf_yres,1:hkl%nref))
        allocate(tmp_pf(1:sm_prt%ipf_yres))
        ipf = 0.0
        pf = 0.0
        call Get_Laue_Equiv(SpG%NumSpg,n_pn_spgr)
        write(unit = l_gr_shar,fmt = '(i3)') n_pn_spgr
        call Set_Spacegroup(SpaceGen = l_gr_shar, SpaceGroup = L_Grp)
        write(unit=*,fmt=*) 'Laue Group       ', L_Grp%CrystalSys
        !sys_crys = L_Grp%CrystalSys

    return
    end subroutine ipf_ini

    !subroutine ipf_calc(prf_hkl, sm_prt%prf_width, prf_max)
    subroutine ipf_calc() ! put here one unique variable
        !real,dimension(1:3)                ,intent(in)  :: prf_hkl      ! preferred H K L
        !real                               ,intent(in)  :: sm_prt%prf_width
        !real                               ,intent(in)  :: prf_max
    ! this variable might work as well if is it is a constant
        real                                :: prf_max = 10
        real,dimension(1:3)                 :: prf_hkl ! preferred H K L
        real,dimension(1:3)                 :: uni_vec
        integer                             :: n_op         ! n of  H K L generated
        character(len = 10)                   :: l_gr_shar, p_gr_shar
        integer                             :: n_pn_spgr,Multip
        real, dimension(:,:), allocatable   :: hkl_lst,tmp_lst
        real, dimension(:,:), allocatable   :: uni_vec_lst
        real                                :: lat, lon, p_ang_rad
        real                                :: ints_to_add, ang_dst
        type (space_group_type)             :: L_Grp
        read(unit = sm_prt%pref_hkl_string,fmt=*) prf_hkl
        write(unit=*,fmt=*) 'preferred   H  K  L = ', prf_hkl
        n_op = SpG%numops * 2
        write(unit=*,fmt=*) 'num ops (SpG)', n_op

        call Get_Laue_Equiv(SpG%NumSpg,n_pn_spgr)
        write(unit = l_gr_shar,fmt = '(i3)') n_pn_spgr
        call Set_Spacegroup(SpaceGen = l_gr_shar, SpaceGroup = L_Grp)

        write(unit=*,fmt=*) 'Laue Group       ', L_Grp%SPG_Symb,' num ', L_Grp%NumSpg
        n_op = L_Grp%numops * 2
        write(unit=*,fmt=*) 'num ops (Laue Group) ', n_op

        allocate(tmp_lst(1:3,1:n_op))
        call Hkl_Equiv_List(prf_hkl,L_Grp,.false.,Multip,tmp_lst)
        allocate(hkl_lst(1:3,1:Multip))
        write(unit=*,fmt=*) 'Multip  = ', Multip
        hkl_lst = tmp_lst(1:3,1:Multip)

        allocate(uni_vec_lst(1:3,1:Multip))
        do i = 1,Multip,1
            uni_vec_lst(1:3,i) = Unit_Cart_Hkl(hkl_lst(1:3,i), Cell)
        end do


        do y = 1, sm_prt%ipf_yres, 1
            call prog_bar(1,y,sm_prt%ipf_yres,' Calculating IPF')
            do x = 1, sm_prt%ipf_xres, 1
                lat = pi * (real(y) / real(sm_prt%ipf_yres))
                lon = pi * (real(x) / real(sm_prt%ipf_xres / 2))
                call Get_Cart_from_Spher(1.0,lat,lon,uni_vec,'rad')
                do i = 1,Multip,1
                    ang_dst = Angle_Uv(uni_vec_lst(1:3,i),uni_vec)
                    ints_to_add = Gaussian(ang_dst,(/sm_prt%prf_width/)) * prf_max
                    ipf(y,x) = ipf(y,x) + ints_to_add
                end do
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)

    return
    end subroutine ipf_calc

!____________________________________________________________________________________________________________


    subroutine pf_calc(h_vec, ref_num)
        real, dimension(1:3)               , intent(in) :: h_vec
        integer                            , intent(in) :: ref_num
        real, dimension(1:3)                            :: uni_vec
        integer                             :: p_ang
!        integer                             :: sm_prt%ipf_xres, sm_prt%ipf_yres
        integer                             :: pol_res
        real                                :: Ss,pl_ltt,pl_lng, sn_lt, cs_lt, ct_lt
        real                                :: lat, lon, p_ang_rad, cs_p_an, sn_p_an
        real                                :: t,psi_rad,subtot
        integer                             :: lat_i, lon_i, lt_pos, ln_pos

        !sm_prt%ipf_xres = ubound(ipf,2)
        !sm_prt%ipf_yres = ubound(ipf,1)
        pol_res =  sm_prt%ipf_yres
        uni_vec = Unit_Cart_Hkl(h_vec, Cell)
        call Get_Spheric_Coord(uni_vec,Ss,lat,lon,'rad')

     !!   call from_Cart_to_Spher(uni_vec,lat,lon)

        sn_lt = sin(lat)
        cs_lt = cos(lat)
        ct_lt = 1.0 / tan(abs(lat))
        do p_ang = 1,pol_res,1
            p_ang_rad = pi * (real(p_ang) / real(pol_res))
            cs_p_an = cos(p_ang_rad)
            sn_p_an = sin(p_ang_rad)
            t = 0.0
            do i = 1,pol_res * 2,1
                psi_rad = (real(i) / real(pol_res)) * pi
                pl_ltt = acos( cs_lt * cs_p_an + sn_lt * sn_p_an * cos(psi_rad) )
                pl_lng = lon - acos( (cs_p_an / (sin(pl_ltt) * sn_lt) ) - (1.0 / tan(pl_ltt)) * ct_lt )

                if(pl_ltt<0)then
                    pl_ltt = pl_ltt + pi
                elseif(pl_ltt>pi)then
                    pl_ltt = pl_ltt - pi
                end if

                if(pl_lng<0)then
                    pl_lng = pl_lng + 2.0 * pi
                elseif(pl_lng>2.0 * pi)then
                    pl_lng = pl_lng - 2.0 * pi
                end if
                lat_i = nint((pl_ltt / pi) * real(sm_prt%ipf_yres))
                lon_i = nint((pl_lng / (pi * 2.0)) * real(sm_prt%ipf_xres))

                if (lat_i<1 )then
                    lat_i = 1
                elseif( lat_i>sm_prt%ipf_yres )then
                    lat_i = sm_prt%ipf_yres
                end if

                if( lon_i<1 )then
                    lon_i = 1
                elseif( lon_i>sm_prt%ipf_xres )then
                    lon_i = sm_prt%ipf_xres
                end if
                if(lat_i == 1 .or. lat_i == sm_prt%ipf_yres .or. lon_i == 1 .or. lon_i == sm_prt%ipf_xres )then
                    t = t + ipf(lat_i,lon_i)
                else
                    subtot = 0
                    do lt_pos = lat_i - 1,lat_i + 1,1
                        do ln_pos = lon_i - 1,lon_i + 1,1
                            subtot = subtot + ipf(lt_pos,ln_pos)
                        end do
                    end do
                    t = t + subtot / 9.0
                end if
            end do
            pf(p_ang,ref_num) = real(t) / real(pol_res)
        end do

        do p_ang = 1,pol_res,1
            tmp_pf(p_ang) = (pf(p_ang,ref_num) + pf(pol_res - p_ang + 1,ref_num)) / 2.0
        end do
        pf(:,ref_num) = tmp_pf

    return
    end subroutine pf_calc


    subroutine wrt_pf()

        real, dimension(1:3)                :: hkl_vec
        integer                             :: pf_file_log_uni
        integer                             :: ysiz
        ysiz = ubound(pf,1)
        Call Get_Logunit(pf_file_log_uni)
        fname = '1d_pf.dat'
        OPEN(unit = pf_file_log_uni, file = fname, status = "replace", form = "formatted")
        do i = 1,pk_lst%npk,1
            hkl_vec(1:3) = real(pk_lst%pk(i)%H(1:3))

            write(unit = pf_file_log_uni,fmt=*) "___________________________________________________________________________"
            if( hkl_vec(1) == int(hkl_vec(1)) .and. hkl_vec(2) == int(hkl_vec(2)) .and. hkl_vec(3) == int(hkl_vec(3)))then
                write(unit = pf_file_log_uni,fmt=*) "(",int(hkl_vec),")  Pole Figure"
            else
                write(unit = pf_file_log_uni,fmt=*) "(",hkl_vec,")  Pole Figure"
            end if
            do y = 1,ysiz,1
                write(unit = pf_file_log_uni,fmt=*) (real(y) / real(ysiz)) * 180.0," , ", pf(y,i)
            end do
            write(unit = pf_file_log_uni,fmt=*) " "

        end do
        close(unit = pf_file_log_uni)

        write(unit=*,fmt = "(a)") " 1D Pole figures in File: "//fname

    return
    end subroutine wrt_pf


    subroutine write_debye_prof()

        real, dimension(1:3)                :: hkl_vec
        integer                             :: debye__ring_file_log_uni

        Call Get_Logunit(debye__ring_file_log_uni)
        fname = 'Debye_profile.dat'


        !allocate(Debye_prof(1:sm_prt%ipf_yres,1:pk_lst%npk))


        OPEN(unit = debye__ring_file_log_uni, file = fname, status = "replace", form = "formatted")


        do i = 1,pk_lst%npk,1
            hkl_vec(1:3) = real(pk_lst%pk(i)%H(1:3))

            write(unit = debye__ring_file_log_uni,fmt=*) "_______________________________________________________"
            if( hkl_vec(1) == int(hkl_vec(1)) .and. hkl_vec(2) == int(hkl_vec(2)) .and. hkl_vec(3) == int(hkl_vec(3)))then
                write(unit = debye__ring_file_log_uni,fmt=*) "(",int(hkl_vec),")  Profile along Debye ring"
            else
                write(unit = debye__ring_file_log_uni,fmt=*) "(",hkl_vec,")  Profile along Debye ring"
            end if
            do y = 1,sm_prt%ipf_yres,1
                write(unit = debye__ring_file_log_uni,fmt=*) (real(y) / real(sm_prt%ipf_yres)) * 180.0," , ", Debye_prof(y,i)
            end do
            write(unit = debye__ring_file_log_uni,fmt=*) " "

        end do
        close(unit = debye__ring_file_log_uni)

        write(unit=*,fmt = "(a)") " 1D Debye profile in File: "//fname

    return
    end subroutine write_debye_prof




    subroutine calc_2D()

        integer                 :: i, i_az
!        integer                 ::  sm_prt%ipf_xres, sm_prt%ipf_yres
        integer                 :: i_pol_ang
        real                    :: r_pol_ang, az
        real , dimension(1:3)   :: h_vec
        real                    :: prom, tot, lat
        real                    :: dx,dy,dst    ! two legs of the triangle to calculate distance
        integer                 :: pos    ! position in pixels
        integer                 :: x_rot, y_rot !totated pos in image


        do y = 1, sm_prt%ipf_yres, 1
            do x = 1, sm_prt%ipf_xres, 1
                lat = pi * (real(y) / real(sm_prt%ipf_yres))
                tot = tot + ipf(y,x) * sin(lat)
            end do
        end do
        write(*,*) "vefore avg: IPF(tot)  = ", tot
        prom = tot / (sm_prt%ipf_yres * sm_prt%ipf_xres)
        write(*,*) "IPF(avg)  = ", prom
        ipf = ipf / prom
        write(*,*) "after avg: IPF(tot)  = ", tot
        if( allocated(Debye_prof) )then
            deallocate(Debye_prof)
        end if

        allocate(Debye_prof(1:sm_prt%ipf_yres,1:pk_lst%npk))

        do i = 1,pk_lst%npk,1
            call prog_bar(1,i,pk_lst%npk,' Calculating PF')
            h_vec(1:3) = real(pk_lst%pk(i)%H(1:3))
            call pf_calc(h_vec, i)
            do i_az = 1,sm_prt%ipf_yres,1
                r_pol_ang = acos(cosd(pk_lst%pk(i)%bragg / 2.0) * cos(pi * real(i_az) / real(sm_prt%ipf_yres)))
                i_pol_ang = int(sm_prt%ipf_yres * (r_pol_ang / pi) + 1)
                Debye_prof(i_az,i) = pf(i_pol_ang,i)
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)

        call write_debye_prof()


        pat2d = 0.0
        do x = 1,par_2d%xres,1

            call prog_bar(1,x,par_2d%xres,' Calculating IMG')

            do y = 1,par_2d%yres,1
                dx = real(x) - par_2d%x_dr
                dy = real(y) - par_2d%y_dr
                dst = sqrt(dx * dx + dy * dy)
                az = atan2(dx,dy)
                ! start Rotating
                az = az + (sm_prt%ipf_ang / 180.0) * 3.14159
                if(az >  3.14159)then
                    az = az -  3.14159
                end if
                if(az < 0)then
                    az = az +  3.14159
                end if
                ! end Rotating
                i_az = abs(int(real(sm_prt%ipf_yres) * (az / pi)))

                if (i_az > sm_prt%ipf_yres) then
                    i_az = sm_prt%ipf_yres
                elseif (i_az < 1) then
                    i_az = 1
                end if
                do i = 1,pk_lst%npk
                    if(dst>pk_lst%pk(i)%dst_px_from .and. dst<pk_lst%pk(i)%dst_px_to)then
                        pos = abs(dst - pk_lst%pk(i)%dst_px_max_p) + 1
                        if(pos<1)then
                            pos = 1
                        else if (pos>pk_lst%pk(i)%wt_px)then
                            pos = pk_lst%pk(i)%wt_px
                        end if
                        pat2d(x,y) = pat2d(x,y) + pk_lst%pk(i)%i_px(pos) * pk_lst%pk(i)%Its * Debye_prof(i_az,i)
                    end if
                end do
                if( pat2d(x,y) == 0 )then
                     pat2d(x,y) = 0.00001
                end if
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)


        call wrt_pf()

    return
    end subroutine calc_2D


    subroutine wr_img_file()

        !srio integer(kind = 2), dimension(:,:), allocatable  :: pat2d_tmp
        real(kind = 8), dimension(:,:), allocatable  :: pat2d_raw
        !integer(kind = 4), dimension(:,:), allocatable  :: pat2d_edf
        real(kind = 4), dimension(:,:), allocatable  :: pat2d_edf
        integer(kind = 2) :: bin_output=10 ! 0=ascii, 1=raw 2=edf 10=all_bin 11=all

        allocate(pat2d_raw(par_2d%xres,par_2d%yres))
        allocate(pat2d_edf(par_2d%xres,par_2d%yres))
        do y = 1,par_2d%yres,1
            do x = 1,par_2d%xres,1
                pat2d_raw(x,y) = pat2d(x,y) ! * 0.001
                pat2d_edf(x,y) = pat2d(x,y) ! * 0.001
            end do
        end do



        if( bin_output == 0 .or.  bin_output == 11)then
            Call Get_Logunit(log_uni)
            fname = '2d_pat_1.asc'
            write(*,*) "Writing image into a file this might take a while"
            OPEN(unit = log_uni, file = fname, status = "replace", form = "formatted")

            write(unit = log_uni, fmt=*) 'Text output of image region:'
            write(unit = log_uni, fmt=*) 'Number of pixels in X direction  = ', par_2d%xres
            write(unit = log_uni, fmt=*) 'Number of pixels in Y direction  = ', par_2d%yres
            write(unit = log_uni, fmt=*) 'X starting pixel of ROI = 1'
            write(unit = log_uni, fmt=*) 'Y starting pixel of ROI = 1'
            write(unit = log_uni, fmt=*) 'Pixel values follow (X - direction changing fastest, bottom left first):'

            do y = 1,par_2d%yres,1
                call prog_bar(1,x,par_2d%xres,' writing IMG')
                do x = 1,par_2d%xres,1
                    write(unit = log_uni, fmt =*) pat2d(x,y)
                end do
            end do
            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname
        endif

        if( bin_output == 1 .or.  bin_output == 10 )then
            Call Get_Logunit(log_uni)
            fname = '2d_pat_1.raw'
            OPEN(unit = log_uni, file = fname, status = "replace", access = "stream", form = "unformatted")
            write(unit = log_uni) pat2d_raw

            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname
        end if


        if( bin_output == 2 .or. bin_output == 10 )then
            Call Get_Logunit(log_uni)
            fname = '2d_pat_1.edf'
            OPEN(unit = log_uni, file = fname, status = "replace", form = "formatted")

            write(unit = log_uni, fmt = '(A)') "{"
            write(unit = log_uni, fmt = '(A)') "HeaderID = EH:000001:000000:000000;"
            write(unit = log_uni, fmt = '(A)') "Image = 1 ;"
            write(unit = log_uni, fmt = '(A)') "ByteOrder = LowByteFirst ;"
            write(unit = log_uni, fmt = '(A)') "DataType = FloatValue ;"
            write(unit = log_uni, fmt = 1010 ) par_2d%xres
            write(unit = log_uni, fmt = 1011 ) par_2d%yres
            write(unit = log_uni, fmt = 1012 ) 4 * par_2d%yres * par_2d%xres
            !write(unit = log_uni, fmt = '(A)') "Size = 2097152 ;"
            write(unit = log_uni, fmt = '(A)') "Title = EDF file by ANAELU;}"


1010        format('Dim_1 = ',I8,' ;')
1011        format('Dim_2 = ',I8,' ;')
1012        format('Size = ',I8,' ;')

            close(unit = log_uni)
            OPEN(unit = log_uni, file = fname, position = "append", access = "stream", &
             form = "unformatted", convert="LITTLE_ENDIAN")

            write(unit = log_uni) pat2d_edf

            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname

        end if

    return
    end subroutine wr_img_file

    subroutine Get_Laue_Equiv(tmpgrp,pnspgr)
        integer , intent(in)    :: tmpgrp
        integer , intent(out)   :: pnspgr

        If (tmpgrp >=  1 .And. tmpgrp <=  2) Then
            pnspgr = 2
            write(unit=*,fmt=*) "Point Group is Triclin"
        ElseIf (tmpgrp >=  3 .And. tmpgrp <=  15) Then
            pnspgr = 10
            write(unit=*,fmt=*) "Point Group is Monoclin"
        ElseIf (tmpgrp >=  16 .And. tmpgrp <= 74) Then
            pnspgr = 47
            write(unit=*,fmt=*) "Point Group is Ortoromb"
        ElseIf (tmpgrp >=  75 .And. tmpgrp <=  88) Then
            pnspgr = 83
            write(unit=*,fmt=*) "Point Group is Tetrag"
        ElseIf (tmpgrp >=  89 .And. tmpgrp <=  142) Then
            pnspgr = 123
            write(unit=*,fmt=*) "Point Group is Tetrag"
        ElseIf (tmpgrp >=  143 .And. tmpgrp <=  148) Then
            pnspgr = 147
            write(unit=*,fmt=*) "Point Group is Trigon"
        ElseIf (tmpgrp >=  149 .And. tmpgrp <=  167) Then
            pnspgr = 164
            write(unit=*,fmt=*) "Point Group is Trigon"
        ElseIf (tmpgrp >=  168 .And. tmpgrp <=  176) Then
            pnspgr = 175
            write(unit=*,fmt=*) "Point Group is Hexagn"
        ElseIf (tmpgrp >=  177 .And. tmpgrp <=  194) Then
            pnspgr = 191
            write(unit=*,fmt=*) "Point Group is Hexagn"
        ElseIf (tmpgrp >=  195 .And. tmpgrp <=  206) Then
            pnspgr = 200
            write(unit=*,fmt=*) "Point Group is Cubic"
        ElseIf (tmpgrp >=  207 .And. tmpgrp <=  230) Then
            pnspgr = 221
            write(unit=*,fmt=*) "Point Group is Cubic"
        Else
            write(unit=*,fmt=*) "No Sym"
            pnspgr = tmpgrp
        End If

    return
    end subroutine Get_Laue_Equiv


End Module Calc_2D_pat

