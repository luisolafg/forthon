!     This file is part of the Python-Fortran version of Anaelu project,
!     a tool for the treatment of 2D-XRD patterns of textured samples
!     Copyright (C) 2013
!     Luis Fuentes Montero (Luiso)
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU Lesser General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU Lesser General Public License for more details.
!
!     You should have received a copy of the GNU Lesser General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.

module CLI_sample_data
    Use Calc_2D_pat, only:area_det_intrum, sample_n_instrument_properties, Crystal_Cell_Type, space_group_type, Atom_list_Type
    use CFML_String_Utilities,          only: get_logunit
    use CFML_IO_Formats,                only: Readn_set_Xtal_Structure,err_form_mess,err_form,file_list_type
    use compilers_specific

    contains


    subroutine dat_2d_det(param_2d)
        type (area_det_intrum)  , intent(    out)   :: param_2d
        integer                                 :: lun, pos_ini, pos_end
        character(len=128)                      :: lin_texto
        character(len=1)                        :: byte_text, prev_byte_text
        character(len=128), dimension(1:50)     :: raw_lin_cont
        character(len = 50), dimension(1:500)   :: char_cont
        integer                                 :: nm_lin, lin_num
        integer                                 :: i, k

        pi = 3.14159265358979323
        to_deg=180.0/pi
        Call Get_Logunit(lun)
        open(unit=lun, file="param.dat", action="read", position="rewind", form="formatted")

        do lin_num = 1, 50, 1
            raw_lin_cont(lin_num) = ""
        end do
        write(*,*) "starting to read"

        ! searching for label "end"

        do lin_num = 1, 50, 1
            if( trim(lin_texto)=="end" )then
                nm_lin = lin_num
                write(*,*) "ending"
                exit
            end if
            read(unit=lun,fmt="(a)") lin_texto
            lin_texto=trim(lin_texto)
            raw_lin_cont(lin_num) = lin_texto
            !write(*,*) "lin_num =", lin_num

        end do
        close(unit=lun)

        ! removing comments

        do lin_num = 1, nm_lin, 1
            write(*,*) trim(raw_lin_cont(lin_num))
            lin_texto = raw_lin_cont(lin_num)
            do i = 1, 128, 1
                if(lin_texto(i:i) == "!")then
                    raw_lin_cont(lin_num) = lin_texto(1:i-1)
                    exit
                end if
            end do
        end do

        ! separating labels from values

        pos_ini = 1
        k = 0
        do lin_num = 1, nm_lin, 1
            lin_texto = trim(raw_lin_cont(lin_num))
            do i = 1, len(lin_texto), 1
                byte_text = lin_texto(i:i)
                if(  trim(byte_text) == "" .or. trim(byte_text) == "," )then
                    byte_text = " "
                end if
                if( trim(byte_text) /= " " .and. trim(prev_byte_text) == " " )then
                    pos_ini = i
                elseif( trim(byte_text) == " " .and. trim(prev_byte_text) /= " " )then
                    pos_end = i
                    k = k + 1
                    char_cont(k) = lin_texto(pos_ini:pos_end - 1)
                end if
                prev_byte_text = byte_text
            end do
        end do

        ! writing values into param_2d

        lin_num = 1
        do
            lin_texto = trim(char_cont(lin_num))
            if( trim(lin_texto)=="end" .or. lin_num > k  )then
                write(*,*) "ending reading parameters"
                exit
            elseif( trim(lin_texto)=="xres" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%xres
                param_2d%x_dr=param_2d%xres/2.0
            elseif( trim(lin_texto)=="xbeam" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%x_dr
            elseif( trim(lin_texto)=="yres" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%yres
                param_2d%y_dr=param_2d%yres/2.0
            elseif( trim(lin_texto)=="ybeam" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%y_dr
            elseif( trim(lin_texto)=="lambda" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%lambda
            elseif( trim(lin_texto)=="dst_det" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%dst_det
            elseif( trim(lin_texto)=="diam_det" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%diam_det
            elseif( trim(lin_texto)=="thet_min" )then
                lin_num = lin_num + 1
                lin_texto = trim(char_cont(lin_num))
                read(unit=lin_texto,fmt=*) param_2d%thmin
            end if
            lin_num = lin_num + 1
        end do
        param_2d%Thmax=(atan2(param_2d%diam_det,param_2d%dst_det)/2.0) * to_deg

        write(*,*) "param_2d%xres = ", param_2d%xres
        write(*,*) "param_2d%yres = ", param_2d%yres
        write(*,*) "param_2d%x_dr = ", param_2d%x_dr
        write(*,*) "param_2d%y_dr = ", param_2d%y_dr
        write(*,*) "param_2d%lambda = ", param_2d%lambda
        write(*,*) "param_2d%dst_det = ", param_2d%dst_det
        write(*,*) "param_2d%diam_det = ", param_2d%diam_det
        write(*,*) "param_2d%thmin = ", param_2d%thmin



        return

    end subroutine dat_2d_det

    subroutine opn_fil(sampl_prop, UnitCell, SpGroup, At_list)
        type (sample_n_instrument_properties) , intent(out) :: sampl_prop ! parameters that affect the peak width and azimut
        type (Crystal_Cell_Type)              , intent(out) :: UnitCell
        type (space_group_type)               , intent(out) :: SpGroup
        type (Atom_list_Type)                 , intent(out) :: At_list

        ! Local vars
        character(len=255)                                  :: arg_cli

        character(len=132)                                  :: line
        integer                                             :: i, nlg_fil_arg
        type (file_list_type)                               :: fich_cfl
        Logical             :: esta

        call Com_arg(arg_cli)
        !write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        write(*,*) "arg_cli =", arg_cli
        !write(*,*) "trim(arg_cli) =", "<<<<<<", trim(arg_cli), ">>>>>>>"
        !write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

        if(len_trim(arg_cli) == 0) then
            arg_cli = "My_Cfl.cfl"
        end if
        inquire(file=trim(arg_cli), exist=esta)
        write(*,*) "Got here 01"
        if( esta )then
            write(*,*) "file ", arg_cli, "found"

            nlg_fil_arg = len_trim(arg_cli)

            write(*,*) "len(trim(arg_cli)) =", nlg_fil_arg
            write(*,*) "ext =<<", arg_cli(nlg_fil_arg - 3:nlg_fil_arg), ">>"
            if( arg_cli(nlg_fil_arg - 3:nlg_fil_arg) == ".cfl" .or. &
                arg_cli(nlg_fil_arg - 3:nlg_fil_arg) == ".Cfl" .or. &
                arg_cli(nlg_fil_arg - 3:nlg_fil_arg) == ".CFL" )then
                write(*,*) "cfl file found"
                call Readn_set_Xtal_Structure( trim(arg_cli), UnitCell, SpGroup, &
                                               At_list, Mode = "CFL", file_list = fich_cfl )
            end if
        else
            write(*,*) "No File Found"
            stop
        end if

        write(*,*) "Got here 02"


        do i=1,fich_cfl%nlines

            line=adjustl(fich_cfl%line(i))
            if(line(1:4) == "ATOM") exit

            if (Trim(line(1:7)) == "SIZE_LG") then
                read(unit=line(8:),fmt=*) sampl_prop%crys_size
            else if (Trim(line(1:8)) == "IPF_RES ") then
                read(unit=line(8:),fmt=*) sampl_prop%ipf_xres
            else if (Trim(line(1:12)) == "HKL_PREF_WH ") then
                read(unit=line(12:),fmt=*) sampl_prop%prf_width
            else if (Trim(line(1:8)) == "AZM_IPF ") then
                read(unit=line(8:),fmt=*) sampl_prop%ipf_ang
                write(*,*) ">>>>>________________________________________________<<<<<<<<<"
                write(*,*) "sampl_prop%ipf_ang =", sampl_prop%ipf_ang
                write(*,*) ">>>>>________________________________________________<<<<<<<<<"
            else if (Trim(line(1:9)) == "HKL_PREF ") then
                sampl_prop%pref_hkl_string = trim(line(9:))
                write(*,*) "pref_hkl_string =", trim(sampl_prop%pref_hkl_string)
            end if

        end do
        write(*,*) "Got here 01"
        write(*,*) "sampl_prop%crys_size =", sampl_prop%crys_size
        return
    end subroutine opn_fil


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! subroutine not in use
    subroutine wrt_ipf(dat_2d)
        real, dimension(:,:), allocatable, intent(in)   :: dat_2d
        character(len=256)      ::  fname
        integer     :: xsiz, ysiz,x,y,log_uni
        ysiz=ubound(dat_2d,1)
        xsiz=ubound(dat_2d,2)

        Call Get_Logunit(log_uni)
        fname='2d_ipf.bn'
        OPEN(unit=log_uni, file=fname, status="replace", access="stream", form="unformatted")

        do y=1,ysiz,1
            call prog_bar(1,y,ysiz,' Writing IPF')

            do x=1,xsiz,1
                write(unit=log_uni) dat_2d(y,x)
            end do
        end do
        close(unit=log_uni)

        write(unit=*,advance='yes',fmt='(A)') char(13)
        write(unit=*,fmt="(a)") " IPF in File: "//fname
        return
    end subroutine wrt_ipf


    subroutine Com_arg(arg_file)
        character(len = 255) , intent(out)  :: arg_file
        integer                             :: narg

        narg = COMMAND_ARGUMENT_COUNT()
        if(narg > 0)then

            write(*,*) " narg =", narg
            write(*,*) "arguments:"
            do i = 1, narg
                call GET_COMMAND_ARGUMENT(i, arg_file)
                write(*,*) trim(arg_file)
            end do
            write(*,*) ":end arguments"
        else
            arg_file = " "
        end if


        write(*,*) "Hi form Com_arg(subroutine)"
        return
    end subroutine Com_arg


end module CLI_sample_data



!    subroutine from_Cart_to_Spher(xo,lat,lon)
!        real, dimension(3), intent( in)             :: xo
!        real              , intent(out)             :: lat
!        real              , intent(out)             :: lon
!        real                                        :: d_h
!        d_h=sqrt(xo(1)*xo(1)+xo(2)*xo(2))
!        lat=atan2(d_h,xo(3))
!        lon=atan2(xo(2),xo(1))
!
!        if(lat > pi)then
!            lat=lat-pi
!        elseif (lat < 0) then
!            lat=lat+pi
!        end if
!
!        if(lon > pi*2.0)then
!            lon = lon-pi*2.0
!        elseif (lon < 0) then
!            lon = lon+pi*2.0
!        end if

!    return
!    end subroutine from_Cart_to_Spher
