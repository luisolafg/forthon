from Tkinter import *

bl = "up"
xold, yold = None, None

def main():
    root = Tk()

    drawing_area = Canvas(root, height = 150, width = 300, borderwidth = 10, relief = "ridge")
    drawing_area.pack()
    drawing_area.bind("<Motion>", motion)
    drawing_area.bind("<ButtonPress-1>", bldown)
    drawing_area.bind("<ButtonRelease-1>", blup)
    
    global slid01
    slid01 = Scale( root, orient = HORIZONTAL, length = 100, sliderlength = 10, from_=0, to=360)
    slid01.bind("<B1-Motion>", NewPoss)
    slid01.pack()
    
    root.mainloop()

def bldown(event):
    global bl
    bl = "down"

def blup(event):
    global bl, xold, yold
    bl = "up"
    xold = None
    yold = None

def motion(event):
    if bl == "down":
        global xold, yold
        if xold != None and yold != None:
   #         event.widget.create_line(xold,yold,event.x,event.y)
            print "Mouse Pointer moved"
            print "from =", xold,yold
            print "  to =", event.x,event.y
            print "________________________________"

        xold = event.x
        yold = event.y

def NewPoss(event):
    print "Slider Pos =", slid01.get()

if __name__ == "__main__":
    main()
