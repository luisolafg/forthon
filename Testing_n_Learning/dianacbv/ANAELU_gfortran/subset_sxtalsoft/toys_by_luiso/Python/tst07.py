from Tkinter import *
import os

bl = "up"
xold, yold = None, None

def main():
    root = Tk()
    
    rightframe = Frame(root)
    rightframe.pack( side = RIGHT )
    
    leftframe = Frame(root)
    leftframe.pack( side = LEFT )
    
    global slidxang
    slidxang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidxang.bind("<B1-Motion>", NewPoss)
    slidxang.pack( side = TOP)

    global slidyang
    slidyang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidyang.bind("<B1-Motion>", NewPoss)
    slidyang.pack( side = TOP)

    global slidzang
    slidzang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidzang.bind("<B1-Motion>", NewPoss)
    slidzang.pack( side = TOP)

    global drawing_area
    drawing_area = Canvas( rightframe, height = 316, width = 474, borderwidth = 10, relief = "ridge")
    drawing_area.pack( side = RIGHT)
    drawing_area.bind("<Motion>", motion)
    drawing_area.bind("<ButtonPress-1>", bldown)
    drawing_area.bind("<ButtonRelease-1>", blup)
    
    root.mainloop()

def bldown(event):
    global bl
    bl = "down"

def blup(event):
    global bl, xold, yold
    bl = "up"
    xold = None
    yold = None

def motion(event):
    if bl == "down":
        global xold, yold
        if xold != None and yold != None:
   #         event.widget.create_line(xold,yold,event.x,event.y)
            print "Mouse Pointer moved"
            print "from =", xold,yold
            print "  to =", event.x,event.y
            print "________________________________"

        xold = event.x
        yold = event.y

def NewPoss(event):
    angx = slidxang.get()
    angy = slidyang.get()
    angz = slidzang.get()
    print "Slider X Ang =", angx
    print "Slider Y Ang =", angy
    print "Slider Z Ang =", angz
    # Open a file
    myfile = open("angles.dat", "w")
    wrstring = ""
    wrstring =  str(angx) + ", " + str(angy) + ", " + str(angz) + "\n"
    myfile.write(wrstring);
    myfile.close()
    
    os.system("./psl_calc.r c")
    
    myfile = open("calc_pk.dat", "r")
    lines = myfile.readlines()
    myfile.close()
    drawing_area.delete("all")
    for line in lines:
        x,y = line.strip().split(",")
        x = int(x)
        y = int(y)
        drawing_area.create_rectangle(x - 2, y - 2, x + 2, y + 2, fill="blue")


if __name__ == "__main__":
    main()
