! Copyright (C) 2011
! Free Software Foundation, Inc.

! This file is part of the gtk-fortran gtk+ Fortran Interface library.

! This is free software; you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 3, or (at your option)
! any later version.

! This software is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License along with
! this program; see the files COPYING3 and COPYING.RUNTIME respectively.
! If not, see <http://www.gnu.org/licenses/>.
module gloval_xy
    real(kind=8), public :: x_m=0.0, y_m=0.0
end module gloval_xy
module handlers
  use iso_c_binding, only: c_ptr, c_int, c_associated, c_f_pointer
  use gtk, only: gtk_main_quit, FALSE, gtk_widget_get_window
  use cairo, only: cairo_set_line_width, cairo_set_source_rgb, cairo_move_to, cairo_line_to, cairo_stroke
  use gdk, only: gdk_cairo_create
use gdk_events
  implicit none

contains
  !*************************************
  ! User defined event handlers go here
  !*************************************
  ! Note that events are a special type of signals, coming from the
  ! X Window system. Then callback functions must have an event argument:
  function delete_event (widget, event, gdata) result(ret)  bind(c)
    integer(c_int)    :: ret
    type(c_ptr), value :: widget, event, gdata
    write(*,*) "my delete_event"
    ret = FALSE
  end function delete_event

  ! "destroy" is a GtkObject signal
  subroutine destroy (widget, gdata) bind(c)
    type(c_ptr), value :: widget, gdata
    write(*,*) "my destroy"
    call gtk_main_quit ()
  end subroutine destroy

  subroutine button_event(widget, event, gdata) bind(c)
    use gloval_xy
    type(c_ptr), value, intent(in) :: widget, event, gdata
    type(c_ptr)                    :: tst_cairo_context
    type(gdkeventbutton), pointer  :: bevent

    write(*,*) "Button press detected"
    if (c_associated(event)) then
       call c_f_pointer(event,bevent)
       write(*,*) "Clicked at:", int(bevent%x), int(bevent%y)
       write(*,*) "Type:", bevent%type
       write(*,*) "State, Button:", bevent%state, bevent%button
       write(*,*) "Root x,y:", int(bevent%x_root), int(bevent%y_root)
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       tst_cairo_context = gdk_cairo_create (gtk_widget_get_window(widget))

       call cairo_set_line_width(tst_cairo_context, 1d0)
       call cairo_set_source_rgb(tst_cairo_context, 1d0, 0d0, 65535d0)
       call cairo_move_to(tst_cairo_context, x_m, y_m)

       call cairo_line_to(tst_cairo_context, bevent%x  , bevent%y  )
       call cairo_stroke(tst_cairo_context)
       x_m=bevent%x
       y_m=bevent%y
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end if
    print *
  end subroutine button_event

  function button_hello_clicked (widget, gdata ) result(ret)  bind(c)
    integer(c_int)    :: ret
    type(c_ptr), value :: widget, gdata
    write(*,*) "Button 1 clicked!"
    ret = FALSE
  end function button_hello_clicked

  function button_count_clicked (widget, gdata ) result(ret)  bind(c)
    integer(c_int)    :: ret
    type(c_ptr), value :: widget, gdata
    integer, pointer :: val
    write(*,*) "Button 2 clicked!"
    ret = FALSE
    if (c_associated(gdata)) then
       call c_f_pointer(gdata, val)
       write(*,*) "Value =", val
       val = val + 1
    end if
  end function button_count_clicked
end module handlers

program gtkFortran
  use iso_c_binding, only: c_ptr, c_funloc, c_loc
  use gtk, only: gtk_init, gtk_window_new, GTK_WINDOW_TOPLEVEL, gtk_window_set_title,    &
      & gtk_container_set_border_width, g_signal_connect,  gtk_main, gtk_table_new,  &
      & gtk_button_new_with_label, gtk_box_pack_start, gtk_widget_show_all,    &
      & FALSE, c_null_char, TRUE, gtk_event_box_new, gtk_drawing_area_new, gtk_container_add,  &
      & gtk_widget_add_events, GDK_BUTTON_PRESS_MASK, gtk_window_set_default_size,       &
      & gtk_table_attach_defaults!, gtk_table_attach, GTK_EXPAND, GTK_SHRINK

  ! The "only" statement can divide the compilation time by a factor 10 !
  use handlers
  implicit none

  type(c_ptr) :: window
  type(c_ptr) :: table
  type(c_ptr) :: button_hello, button_count
  type(c_ptr) :: my_drawing_area
  integer, target :: val = 1

  call gtk_init ()
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL)
  call gtk_window_set_default_size(window, 600, 600)
  call gtk_window_set_title(window, "My GTK  first toy"//c_null_char)
  call gtk_container_set_border_width (window, 10)
  call g_signal_connect (window, "delete-event"//c_null_char, c_funloc(delete_event))
  call g_signal_connect (window, "destroy"//c_null_char, c_funloc(destroy))

  table = gtk_table_new (12, 2, true)
  call gtk_container_add (window, table)

  button_hello = gtk_button_new_with_label ("Button #1"//c_null_char)
  call gtk_table_attach_defaults (table, button_hello, 0, 1, 0, 1)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  call gtk_table_attach (table, button_hello, 0, 1, 0, 1, GTK_SHRINK, GTK_SHRINK,0,0)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  call g_signal_connect (button_hello, "clicked"//c_null_char, c_funloc(button_hello_clicked))

  button_count = gtk_button_new_with_label ("Counter Button "//c_null_char)
  call gtk_table_attach_defaults (table, button_count, 1, 2, 0, 1)
!  call gtk_table_attach (table, button_count, 1, 2, 0, 1, GTK_SHRINK, GTK_SHRINK,0,0)
  call g_signal_connect (button_count, "clicked"//c_null_char, c_funloc(button_count_clicked), &
       & c_loc(val))

  my_drawing_area = gtk_drawing_area_new()
  call gtk_table_attach_defaults (table, my_drawing_area, 0, 2, 1, 12)
  call gtk_widget_add_events(my_drawing_area, GDK_BUTTON_PRESS_MASK)
  call g_signal_connect (my_drawing_area, "button_press_event"//c_null_char, c_funloc(button_event))

  call gtk_widget_show_all (window)

  call gtk_main ()

end program gtkFortran
