echo "Compiling"
gfortran -c gtk_tst_03.f90 -I$GTKFORTRANLOCAL/include/gtk-3-fortran
echo "Linking with GTK"
gfortran *.o  $GTKFORTRANLOCAL/lib/libgtk-3-fortran.a  `pkg-config --cflags --libs gtk+-3.0` -o gtk_tst_03.r
./gtk_tst_03.r
rm *.o *.mod gtk_tst_03.r
