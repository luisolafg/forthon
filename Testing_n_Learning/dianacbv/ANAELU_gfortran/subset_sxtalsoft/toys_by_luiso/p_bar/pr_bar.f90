! this program shows a progress bar if is compiled with gfortran but not with ifort
program pr_bar
    implicit none
    character(len=15)    :: bar
    integer             :: i,j

    do i=1,15,1
        do j=1,15
            if (j<=i)then
                bar(j:j)='/'
            else
                bar(j:j)='-'
            end if
        end do
        write(unit=*,advance='no',fmt='(A)') char(13)
        write(unit=*,advance='no',fmt='(A)') '['//bar//']'
        call flush()
        CALL SLEEP(1)
    end do

    write(unit=*,advance='yes',fmt='(A)') ' '

end program pr_bar
