program tst_angle_calc
    implicit none
    real, Dimension(1:3)        :: vec_one, vec_two
    real                        :: pi = 3.14159235358
    real                        :: angle
        vec_one = (/0.0, 1.0, 1.0/)
        vec_two = (/0.0, 0.0, -200.0/)
        call calc_angle_betwen_vectors(vec_one, vec_two, angle)
    stop

    contains
    subroutine calc_angle_betwen_vectors(vec_ini, vec_fin, ang)
    real, Dimension(1:3), intent(in)    :: vec_ini, vec_fin
    real,                 intent(out)   :: ang
    real, Dimension(1:3)                :: vec_1, vec_2
    real                                :: angle, dst
        vec_1 = vec_ini
        vec_2 = vec_fin
        write(*,*) "vec_1 =", vec_1
        write(*,*) "vec_2 =", vec_2
        write(*,*) "ang =", ang

        dst=sqrt( vec_1(1)**2 + vec_1(2)**2 + vec_1(3)**2 )
        vec_1 = vec_1 / dst

        dst=sqrt( vec_2(1)**2 + vec_2(2)**2 + vec_2(3)**2 )
        vec_2 = vec_2 / dst

        dst=sqrt( (vec_2(1)-vec_1(1))**2 + (vec_2(2)-vec_1(2))**2 + (vec_2(3)-vec_1(3))**2 )
        ang=asin(dst/2.0)*2.0

        write(*,*) "vec_1 =", vec_1
        write(*,*) "vec_2 =", vec_2
        write(*,*) "ang =", ang/pi, "* pi"
    return
    end subroutine calc_angle_betwen_vectors

end program tst_angle_calc
