program bin_text_read
    implicit none
    integer(kind=1)                     :: byte_write
    character(len=250)                  :: txt_wr
    integer                             :: i,j
!    integer(kind=1), dimension(1:250)   :: k
    integer                             :: log_uni

    log_uni=10

    open(unit=log_uni, file='tst_mix.lui', status="old",action="read", position="rewind", access="stream", form="unformatted")

    do
        txt_wr=''
        do i=1,250
            read(unit=log_uni,end=999) byte_write
            txt_wr(i:i)=char(byte_write)
            write(unit=*,fmt='(i4)') byte_write
            if(byte_write==10)then
                exit
            end if
        end do
        write(unit=*,fmt='(a)') txt_wr
    end do


999 write(unit=*,fmt=*) 'end of file'

    close(unit=log_uni)
end program bin_text_read
