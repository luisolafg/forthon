!This program calculates the intensity for each pixel in the 
!bidimensional espectra.
PROGRAM bi_pow_pat
IMPLICIT NONE

REAL, DIMENSION(3450) :: x, y, xp, yp
REAL, DIMENSION(3450) :: C, theta, Inten, dos_theta
INTEGER :: xc, yc, i, j

xc=1725
yc=1725

OPEN(UNIT=2,FILE='bi_pow_pat.dat',STATUS='REPLACE',ACTION='WRITE')
WRITE(2,*) "          x          y       x(i)              y(i)            C(i)            theta(i)"

Do i=0,3450
  x(i)=(i)-xc
  y(i)=(3450-i)-xc
  C(i)=SQRT((x(i)**2)+(y(i)**2))
  theta(i)=atan((C(i))/862.5)*57.2957795
  !WRITE(2,*) i, (3450-i), x(i), y(i), C(i), theta(i)
End Do

CALL read_file( Inten, dos_theta, theta )

DO i=0, 3450
  WRITE(2,*) i, (3450-i), x(i), y(i), C(i), theta(i), Inten(i)
END DO


CONTAINS

  SUBROUTINE read_file( Inten, dos_theta, theta )
      real, DIMENSION(150), intent(in out) :: Inten
      real, DIMENSION(3450), intent(in out) :: theta
      REAL, DIMENSION(150), INTENT(out) :: dos_theta
      integer :: i, j
      REAL :: x
      
      OPEN(UNIT=3,FILE='element.dat',STATUS='OLD',ACTION='READ')
      
        Do j=1, 3450
          READ(*,*) theta(j)
	  Do i = 1,301
	      READ(3,*) dos_theta(i), Inten(i)
	      WRITE(*,*) "dos_theta, Inten", dos_theta(i), Inten(i)
		
		  !WRITE(*,*) theta(j)
		  IF  (theta(j) == dos_theta(i)) THEN
		      READ(3,*) dos_theta, Inten
		      WRITE(*,*) theta, Inten
		      
		  End If
		
	  End Do
	End Do
      

      CLOSE(UNIT=3)
  RETURN
  END SUBROUTINE read_file


End Program bi_pow_pat
 