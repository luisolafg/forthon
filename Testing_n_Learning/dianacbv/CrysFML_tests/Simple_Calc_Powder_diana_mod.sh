OPT1="-c -O2"
 INC="-I$CRYSFML/GFortran/LibC"
 LIB="-L$CRYSFML/GFortran/LibC"
 LIBSTATIC="-lcrysfml"
 echo "Compiling"
 echo $CRYSFML
 gfortran $OPT1  $INC Simple_Calc_Powder_diana_luiso_touch.f90
 echo "Linking"
 gfortran *.o  -o simple_calc_powder_diana  $LIB $LIBSTATIC

rm -rf *.o *.mod
