 OPT1="-c -O2"
 INC="-I$CRYSFML/GFortran/LibC"
 LIB="-L$CRYSFML/GFortran/LibC"
 LIBSTATIC="-lcrysfml"
 echo "Compiling"
 gfortran $OPT1  $INC Calc_Sfac.f90
 echo "Linking"
 gfortran *.o  -o calc_sfac  $LIB $LIBSTATIC

rm -rf *.o *.mod
