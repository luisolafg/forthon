!This program calculates the Structure Factor descriptive ecuation dependent of
!the atomic scattering factor (f) of each element. Reads the general data of the
!cell from a .dat file containing:
!Number of atoms
!Number of different atoms
!hkl positions (one per line separated by a tab)

PROGRAM STRUCTURE_FACTORS

IMPLICIT NONE

INTEGER :: N, Nd, ierror, i
REAL :: u, v, w, suma
REAL :: h, k, l
CHARACTER :: x

OPEN(UNIT=2, FILE='cell_data.dat', STATUS='OLD', IOSTAT=ierror)
READ(2,*) N
WRITE (*,*) "NUMBER OF ATOMS IN THE ASIMETRIC UNIT", N

READ(2,*) Nd
WRITE(*,*) "NUMBER OF DIFFERENT ATOMS", Nd

DO i=1,N
    READ(2,*) u, v, w
    WRITE(*,*) "ATOMIC POSISTIONS u, v, w", u, v, w
    IF (u+v+w==0) THEN
        WRITE(*,*) "e**0=1"
    ELSE 
        WRITE(*,*) "e**ipi(", 2*u,"h+", 2*v,"k+", 2*w,"l)"
    END IF
END DO

CLOSE (UNIT=2)

END PROGRAM STRUCTURE_FACTORS
