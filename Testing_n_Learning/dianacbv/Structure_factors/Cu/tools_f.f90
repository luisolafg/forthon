!Program to determine the hkl lines to appear in the diffraction pattern according to the
!crystal simetry.
MODULE tools_f
IMPLICIT NONE

CONTAINS

SUBROUTINE SIMP(cubic, sim2, lambda, a, element)
CHARACTER(LEN=10) :: cubic, element, x
INTEGER :: sim2, sum, i, j
INTEGER, DIMENSION(7,3) :: hkl = reshape( (/ 1,1,1,2,2,2,2, 0,1,1,0,1,1,2, 0,0,1,0,0,1,0 /), (/ 7, 3 /) )
INTEGER, DIMENSION(7) :: hkl2
INTEGER, DIMENSION(7) :: p = (/ 6, 12, 8, 6, 24, 24, 12 /)
REAL, DIMENSION(7) :: seno_2_theta, seno_theta, theta, seno_theta_lambda, f, StF, Lor_pol, Y, Dos_theta
REAL :: a, lambda, a1, a2, a3, a4, b1, b2, b3, b4, c1
REAL :: fk, x0, pi
REAL :: k
call read_file( a1, a2, a3, a4, b1, b2, b3, b4, c1 , element)

OPEN(UNIT=2,FILE='pow_pat.dat',STATUS='REPLACE',ACTION='WRITE')
OPEN(UNIT=3,FILE='element.dat',STATUS='REPLACE')

WRITE(*,*) "opening file"
hkl2=sum((hkl**2), DIM=2)

Do i = 1,7
    seno_2_theta(i) = ((lambda**2)/(4*(a**2)))*hkl2(i)
    seno_theta(i) = sqrt(seno_2_theta(i))
    theta(i) = (ASIN(seno_theta(i)))*57.2957795
    Dos_theta(i) = 2*theta(i)
    seno_theta_lambda(i) = (seno_theta(i))/lambda
    f(i) = (a1*exp(-b1*(seno_theta_lambda(i))**2)+c1)+(a2*exp(-b2*(seno_theta_lambda(i))**2)+c1)+  &
    (a3*exp(-b3*(seno_theta_lambda(i))**2)+c1)+(a4*exp(-b4*(seno_theta_lambda(i))**2)+c1)
    StF(i) = 16*((f(i))**2)
    Lor_pol(i) = ABS(((1.5)+((0.5)*(cos(4*theta(i)))))/((seno_2_theta(i))*(cos(theta(i)))))
    Y(i) = StF(i)*p(i)*Lor_pol(i)
    WRITE(2,*) (hkl(i,j), j = 1,3), (hkl2(i)), (seno_2_theta(i)), (seno_theta(i)), (theta(i)), (seno_theta_lambda(i)), (f(i)), &
    (StF(i)), (p(i)), (Lor_pol(i)), Y(i)
End Do

pi=4.*atan(1.)

  Do k = 0,150,0.5
    fk=0
    Do j = 1,7
      fk = fk+Y(j)*(0.5/(pi*((REAL(k-Dos_theta(j))**2.0)+0.5**2.0)))
    End Do
    WRITE(3,*) k, fk    
  End Do

CLOSE(UNIT=2)
CLOSE(UNIT=3)

return
END SUBROUTINE SIMP

SUBROUTINE FACE_CEN(cubic, sim2, lambda, a, element)
CHARACTER(LEN=10) :: cubic, element, x, Cu
INTEGER :: sim2, sum, i, j
INTEGER, DIMENSION(7,3) :: hkl = reshape( (/ 1,2,2,3,2,4,3, 1,0,2,1,2,0,3, 1,0,0,1,2,0,1 /), (/ 7, 3 /) )
INTEGER, DIMENSION(7) :: hkl2
INTEGER, DIMENSION(7) :: p = (/ 8, 6, 12, 24, 8, 6, 24 /)
REAL, DIMENSION(7) :: seno_2_theta, seno_theta, theta, seno_theta_lambda, f, StF, Lor_pol, Y, Dos_theta
REAL :: a, lambda, a1, a2, a3, a4, b1, b2, b3, b4, c1
REAL :: fk, x0, pi
real :: k

call read_file( a1, a2, a3, a4, b1, b2, b3, b4, c1 , element)

OPEN(UNIT=2,FILE='pow_pat.dat',STATUS='REPLACE',ACTION='WRITE')
OPEN(UNIT=3,FILE='element.dat',STATUS='REPLACE')

WRITE(*,*) "opening file"
hkl2=sum((hkl**2), DIM=2)

Do i = 1,7
    seno_2_theta(i) = ((lambda**2)/(4*(a**2)))*hkl2(i)
    seno_theta(i) = sqrt(seno_2_theta(i))
    theta(i) = (ASIN(seno_theta(i)))*57.2957795
    Dos_theta(i) = 2*theta(i)
    seno_theta_lambda(i) = (seno_theta(i))/lambda
    f(i) = (a1*exp(-b1*(seno_theta_lambda(i))**2)+c1)+(a2*exp(-b2*(seno_theta_lambda(i))**2)+c1)+  &
    (a3*exp(-b3*(seno_theta_lambda(i))**2)+c1)+(a4*exp(-b4*(seno_theta_lambda(i))**2)+c1)
    StF(i) = 16*((f(i))**2)
    Lor_pol(i) = ABS(((1.5)+((0.5)*(cos(4*theta(i)))))/((seno_2_theta(i))*(cos(theta(i)))))
    Y(i) = StF(i)*p(i)*Lor_pol(i)
    WRITE(2,*) (hkl(i,j), j = 1,3), (hkl2(i)), (seno_2_theta(i)), (seno_theta(i)), (theta(i)), (seno_theta_lambda(i)), (f(i)), &
    (StF(i)), (p(i)), (Lor_pol(i)), Y(i)
End Do

pi=4.*atan(1.)

  Do k = 0,150,0.5
    fk=0
    Do j = 1,7
      fk = fk+Y(j)*(0.5/(pi*((REAL(k-Dos_theta(j))**2.0)+0.5**2.0)))
    End Do
    WRITE(3,*) k, fk    
  End Do
  
CLOSE(UNIT=2)
CLOSE(UNIT=3)

return
END SUBROUTINE FACE_CEN

SUBROUTINE BODY_CEN(cubic, sim2, lambda, a, element)
CHARACTER(LEN=10) :: cubic, element, x
INTEGER :: sim2, sum, i, j
INTEGER, DIMENSION(7,3) :: hkl = reshape( (/ 1,2,2,2,3,2,3, 1,0,1,2,1,2,2, 0,0,1,0,0,2,1 /), (/ 7, 3 /) )
INTEGER, DIMENSION(7) :: hkl2
INTEGER, DIMENSION(7) :: p = (/ 8, 6, 12, 24, 8, 6, 24 /)
REAL, DIMENSION(7) :: seno_2_theta, seno_theta, theta, seno_theta_lambda, f, StF, Lor_pol, Y, Dos_theta
REAL :: a, lambda, a1, a2, a3, a4, b1, b2, b3, b4, c1
REAL :: fk, x0, pi
real :: k

call read_file( a1, a2, a3, a4, b1, b2, b3, b4, c1 , element)

OPEN(UNIT=2,FILE='pow_pat.dat',STATUS='REPLACE',ACTION='WRITE')
OPEN(UNIT=3,FILE='element.dat',STATUS='REPLACE')

WRITE(*,*) "opening file"
hkl2=sum((hkl**2), DIM=2)


Do i = 1,7
    seno_2_theta(i) = ((lambda**2)/(4*(a**2)))*hkl2(i)
    seno_theta(i) = sqrt(seno_2_theta(i))
    theta(i) = (ASIN(seno_theta(i)))*57.2957795
    Dos_theta(i) = 2*theta(i)
    seno_theta_lambda(i) = (seno_theta(i))/lambda
    f(i) = (a1*exp(-b1*(seno_theta_lambda(i))**2)+c1)+(a2*exp(-b2*(seno_theta_lambda(i))**2)+c1)+  &
    (a3*exp(-b3*(seno_theta_lambda(i))**2)+c1)+(a4*exp(-b4*(seno_theta_lambda(i))**2)+c1)
    StF(i) = 16*((f(i))**2)
    Lor_pol(i) = ABS(((1.5)+((0.5)*(cos(4*theta(i)))))/((seno_2_theta(i))*(cos(theta(i)))))
    Y(i) = StF(i)*p(i)*Lor_pol(i)
    WRITE(2,*) (hkl(i,j), j = 1,3), (hkl2(i)), (seno_2_theta(i)), (seno_theta(i)), (theta(i)), (seno_theta_lambda(i)), (f(i)), &
    (StF(i)), (p(i)), (Lor_pol(i)), Y(i)
    
End Do

pi=4.*atan(1.)

  Do k = 0,150,0.5
    
    fk=0
    Do j = 1,7
      fk = fk+Y(j)*(0.5/(pi*((REAL(k-Dos_theta(j))**2.0)+0.5**2.0)))
    End Do
    WRITE(3,*) k, fk    
  End Do

CLOSE(UNIT=2)
CLOSE(UNIT=3)

return
END SUBROUTINE BODY_CEN

SUBROUTINE read_file( a1, a2, a3, a4, b1, b2, b3, b4, c1, element )
    real , intent(in out) :: a1
    real , intent(in out) :: a2
    real , intent(in out) :: a3
    real , intent(in out) :: a4
    real , intent(in out) :: b1
    real , intent(in out) :: b2
    real , intent(in out) :: b3
    real , intent(in out) :: b4
    real , intent(in out) :: c1
    CHARACTER(LEN=10) , intent(in out) :: element
    CHARACTER(LEN=10) :: cubic, x
    integer i, j
    OPEN(UNIT=3,FILE='f_d_atm.dat',STATUS='OLD',ACTION='READ')
    Do i = 1,428
        READ(3,*) x
        WRITE(*,*) "x =", trim(x)
        IF (trim(x) == trim(element)) THEN

            READ(3,*) a1, a2, a3, a4, b1, b2, b3, b4, c1
            exit

        End If
    End Do
!    WRITE(*,*) "a1, a2, a3, a4, b1, b2, b3, b4, c1 = ", a1, a2, a3, a4, b1, b2, b3, b4, c1

    CLOSE(UNIT=3)

END SUBROUTINE read_file


END MODULE tools_f
