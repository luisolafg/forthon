PROGRAM test_polar
IMPLICIT NONE

REAL(KIND=8), DIMENSION(4,4) :: Y
REAL(KIND=8), DIMENSION(4,4) :: Z
REAL(KIND=8), DIMENSION(4,4) :: W
INTEGER :: i, j, k, l
!REAL :: r, a, ka, la
INTEGER :: IOstatus

open(unit= 2, file = 'test_polar.dat')

Do i=1, 4
    Do j=1, 4
        read(2, IOSTAT=IOstatus) Y(i,j)
        WRITE (*,*) Y(i,j)
    End Do
End Do

WRITE (*,*) "IOstatus=", IOstatus

CLOSE(2)

!Do i=1, 10
!    Do j=1, 4
!        k=10-i
!        l=(j-2)
!            If (l<0) THEN  !! NO estoy segura que sea necesario, pero de lo contratio ser�an negativos
!                l=l*(-1)
!            End If
!    End Do
!End Do
!
!WRITE(*,*) Z(k,l)

!Do k=1, 2300
!    Do l=1, 1150   !!! No estoy segura de que sea asi, pero se supone que el centro en la escala de l esta en el centro
!        r=((k**2)+(l**2))**(1/2)
!        la=l
!        ka=k
!        a=atan(la/ka)
!        Z=Z(k,l)   !!!Cuando pongo el programa a correr, se cuelga se le pongo �ste rengl�n
!    End Do
!End Do


END PROGRAM test_polar
