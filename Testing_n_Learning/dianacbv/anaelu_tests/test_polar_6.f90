PROGRAM test_polar_4

IMPLICIT NONE

REAL (KIND=8), DIMENSION (345, 345) :: MV
!REAL (KIND=8), DIMENSION (2300, 2300) :: DTh
!REAL (KIND=8), DIMENSION (2300, 2300) :: Alfa
REAL (KIND=8), DIMENSION (0:30, 0:180) :: MN
!REAL(KIND=8), ALLOCATABLE :: Inten (:,:)
REAL (KIND=8) :: dtheta
REAL (KIND=8) :: alpha
REAL (KIND=8) :: R
INTEGER :: k, l, m, n, i, j

character(len = 255) :: file_out

INTEGER :: IOstatus, log_uni
REAL (KIND=8) :: p, pi


OPEN(unit= 10, status='old',file = '2d_pat_1.raw',form='unformatted', access='stream', action='read')

Do m=1, 345
    Do n=1, 345
        read(10, IOSTAT=IOstatus) MV(m,n)
    End Do
End Do

WRITE (*,*) "IOstatus=", IOstatus

CLOSE(10)

p = 0.15   !! dimension en mm del pixcel para el detector MAR 2300
pi = 4.0 * atan(1.0)
!DO m=1,2300
!    Do n=1,2300
!        R = sqrt( ( ( ( n - 1150) * p ) * (  ( n - 1150) * p )  ) + ( ( (1150-m ) *  p  ) * ( (1150 - m ) * p ) ) )
!        DTh(m,n)=  ( (atan(R/150) ) *180)/pi !! distancia muestra-detector=150mm
!        Alfa(m,n)= ( atan( ( (n - 1150) * p) / ( (1150 - m) * p) ) * 180) / pi
!    End Do
!End Do

WRITE (*,*) 'STEP 1 DONE'

Do k = 0, 30, 1
    dtheta = k
    Do l = 0, 180, 1

        R =  tan( dtheta * pi / 180.0) * 150 !150= distancia muestra-detector
        alpha = l-(90)
        j=NINT( 172.5  + ( R/p * sin(alpha* pi / 180.0 ) )  )
        i=NINT( 172.5  - ( R/p * cos(alpha* pi / 180.0 ) )  )
        MN(k,l)=MV(i,j)
        !if( real(k) / 5.0 == int(real(k) / 5.0) .and. real(l) / 10.0 == int(real(l) / 10.0) )then
            write(*,*) "k =", k, "l =", l, "R=", R, "alfa=", alpha
            write(*,*) "i =", i, "j =", j, "MV=", MV(i,j)
            write(*,*) "MN(k, l) = ", MN(k,l)
        !end if

    End Do
End Do

WRITE (*,*) "IOstatus=", IOstatus

    log_uni = 4
    file_out = 'polar.raw'

    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")
    write(unit = log_uni) transpose(MN)
    close(unit=log_uni)


END PROGRAM test_polar_4
