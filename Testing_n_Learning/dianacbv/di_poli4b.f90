PROGRAM LEER
IMPLICIT NONE    
    INTEGER :: i
    REAL :: x, y
    OPEN (unit=2,file="di_poli4.dat")
    DO i=1,21
       READ (2,FMT='(2(F11.4))') x, y
       WRITE (*,FMT='(2(F11.4))') x, y
    END DO
    CLOSE (unit=2)
END PROGRAM LEER