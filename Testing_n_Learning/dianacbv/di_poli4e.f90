!Cuadrado de y(i)
PROGRAM Leer_y_escribir
IMPLICIT NONE
   

    INTEGER :: i
    REAL, DIMENSION(21) :: x, y, prod, prod2
    REAL :: x_tot=0.0, y_tot=0.0, prod_tot=0.0
    
    OPEN (unit = 2,file = "di_poli4.dat", status = 'old', action = 'read')
       DO i=1,21
           READ (2,*) x(i), y(i)
       END DO
    CLOSE (unit=2)
     
       DO i=1,21
           prod(i)=x(i)*y(i)
       END DO
       
       DO i=1,21
           prod2(i)=x(i)*y(i)**2
       END DO
       
       WRITE (*,*)"    x(i)           y(i)           x(i)*y(i)       x(i)*y(i)**2"
        
       DO i=1,21
           WRITE (*,*) x(i), y(i), prod(i), prod2(i)
       END DO
       
       DO i=1,21
           x_tot=x_tot+x(i)
           y_tot=y_tot+y(i)
           prod_tot=prod_tot+prod2(i)
       END DO
       
       WRITE (*,*)"La sumatoria de las x(i) es:", x_tot
       WRITE (*,*)"La sumatoria de y(i) es:", y_tot
       WRITE (*,*)"La sumatoria del producto x(i)y(i)**2 es:", prod_tot
       
END PROGRAM Leer_y_escribir