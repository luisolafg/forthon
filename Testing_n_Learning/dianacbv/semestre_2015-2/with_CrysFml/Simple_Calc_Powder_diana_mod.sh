OPT1="-c -O2"
 INC="-I$CRYSFML/GFortran/LibC"
 LIB="-L$CRYSFML/GFortran/LibC"
 LIBSTATIC="-lcrysfml"
 echo "Compiling"
 gfortran $OPT1  $INC Simple_Calc_Powder_diana.f90
 echo "Linking"
 gfortran *.o  -o simple_calc_powder_diana  $LIB $LIBSTATIC
 ./simple_calc_powder_diana
 python read_plot_xy.py
rm -rf *.o *.mod
