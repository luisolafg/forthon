#!/usr/bin/python
# input by Eduardo

import wx
import os
from readcfl import *


def write_cfl(dto):

    myfile = open("cfl_out.cfl", "w")
    wrstring = ""
    wrstring = "Title  " + dto.new_inputtitle + "\n"
    myfile.write(wrstring);
    wrstring = "!        a        b        c        alpha        beta        gamma\n"
    myfile.write(wrstring);
    wrstring =  "Cell    " + str(dto.new_input2) + "     " + str(dto.new_input3) + "     " + str(dto.new_input4) + "     " + str(dto.new_input5) + "       " + str(dto.new_input6) + "      " + str(dto.new_input7) + "\n"
    myfile.write(wrstring);
    wrstring = "!   Space Group\n"
    myfile.write(wrstring);
    wrstring = "!Spgr " + str(dto.new_input1)
    myfile.write(wrstring);

    myfile.close()

class MyForm(wx.Frame):

    def __init__(self):
        super(MyForm, self).__init__( None, -1, 'Input Data')
        self.panel_01 = CrystalPanel(self)


class CrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(CrystalPanel, self).__init__(parent)
        print "Hi"


        title = wx.StaticText(self, wx.ID_ANY, 'Title')
        self.inputtitle = wx.TextCtrl(self, wx.ID_ANY, '')

        subt1 = wx.StaticText(self, wx.ID_ANY, '*** CRYSTAL STRUCTURE ***')

        label1 = wx.StaticText(self, wx.ID_ANY, '  Space group  ')
        self.input1 = wx.TextCtrl(self, wx.ID_ANY, '')

        label2 = wx.StaticText(self, wx.ID_ANY, '           a')
        self.input2 = wx.TextCtrl(self, wx.ID_ANY, '')

        label3 = wx.StaticText(self, wx.ID_ANY, '           b')
        self.input3 = wx.TextCtrl(self, wx.ID_ANY, '')

        label4 = wx.StaticText(self, wx.ID_ANY, '           c')
        self.input4 = wx.TextCtrl(self, wx.ID_ANY, '')

        label5 = wx.StaticText(self, wx.ID_ANY, '      alpha')
        self.input5 = wx.TextCtrl(self, wx.ID_ANY, '')

        label6 = wx.StaticText(self, wx.ID_ANY, '       beta')
        self.input6 = wx.TextCtrl(self, wx.ID_ANY, '')

        label7 = wx.StaticText(self, wx.ID_ANY, '      gamma')
        self.input7 = wx.TextCtrl(self, wx.ID_ANY, '')



        ReadBtn = wx.Button(self, wx.ID_ANY, 'Read')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRead, ReadBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        titleSizer       = wx.BoxSizer(wx.HORIZONTAL)
        subt1Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        spcgrpSizer      = wx.BoxSizer(wx.HORIZONTAL)
        abcSizer         = wx.BoxSizer(wx.HORIZONTAL)
        anglesSizer      = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)

        inputa           = wx.BoxSizer(wx.VERTICAL)
        inputb           = wx.BoxSizer(wx.VERTICAL)
        inputc           = wx.BoxSizer(wx.VERTICAL)

        inputalpha       = wx.BoxSizer(wx.VERTICAL)
        inputbeta        = wx.BoxSizer(wx.VERTICAL)
        inputgamma       = wx.BoxSizer(wx.VERTICAL)


        inputa.Add(label2, 0, wx.ALL, 2)
        inputa.Add(self.input2, 2, wx.ALL| wx.EXPAND, 2)
        inputb.Add(label3, 0, wx.ALL, 2)
        inputb.Add(self.input3, 2, wx.ALL| wx.EXPAND, 2)
        inputc.Add(label4, 0, wx.ALL, 2)
        inputc.Add(self.input4, 2, wx.ALL| wx.EXPAND, 2)

        inputalpha.Add(label5, 0, wx.ALL, 2)
        inputalpha.Add(self.input5, 2, wx.ALL| wx.EXPAND, 2)
        inputbeta.Add(label6, 0, wx.ALL, 2)
        inputbeta.Add(self.input6, 2, wx.ALL| wx.EXPAND, 2)
        inputgamma.Add(label7, 0, wx.ALL, 2)
        inputgamma.Add(self.input7, 2, wx.ALL| wx.EXPAND, 2)


        titleSizer.Add(title, 0, wx.ALL, 2)
        titleSizer.Add(self.inputtitle, 2, wx.ALL| wx.EXPAND, 2)

        subt1Sizer.Add(subt1, 0, wx.ALL, 2)

        spcgrpSizer.Add(label1, 0, wx.ALL, 2)
        spcgrpSizer.Add(self.input1, 2, wx.ALL| wx.EXPAND, 2)

        abcSizer.Add(inputa, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputb, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputc, 0, wx.ALL| wx.EXPAND, 2)

        anglesSizer.Add(inputalpha, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputbeta, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputgamma, 0, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(ReadBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(titleSizer, 0, wx.EXPAND)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt1Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(spcgrpSizer, 0, wx.LEFT)
        MainSizer.Add(abcSizer, 0, wx.CENTER)
        MainSizer.Add(anglesSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRead(self, event):

        new_inputtitle = self.inputtitle.GetValue()
        new_input1 = self.input1.GetValue()
        new_input2 = self.input2.GetValue()
        new_input3 = self.input3.GetValue()
        new_input4 = self.input4.GetValue()
        new_input5 = self.input5.GetValue()
        new_input6 = self.input6.GetValue()
        new_input7 = self.input7.GetValue()

        dto = data()

        dto.new_inputtitle =  new_inputtitle
        dto.new_input1     =  new_input1
        dto.new_input2     =  new_input2
        dto.new_input3     =  new_input3
        dto.new_input4     =  new_input4
        dto.new_input5     =  new_input5
        dto.new_input6     =  new_input6
        dto.new_input7     =  new_input7


        print dto.new_inputtitle
        print dto.new_input1
        print dto.new_input2
        print dto.new_input3
        print dto.new_input4
        print dto.new_input5
        print dto.new_input6
        print dto.new_input7

        write_cfl(dto)

        return dto

    def onOpen(self, event):

        dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
                 path = dlg.GetPath()
                 mypath = os.path.basename(path)
        dlg.Destroy()

        my_cfl_content = read_cfl_file(my_file_path = mypath)

        self.inputtitle.SetValue(my_cfl_content.title)
        self.input1.SetValue(my_cfl_content.spgr)
        self.input2.SetValue(my_cfl_content.a)
        self.input3.SetValue(my_cfl_content.b)
        self.input4.SetValue(my_cfl_content.c)
        self.input5.SetValue(my_cfl_content.alpha)
        self.input6.SetValue(my_cfl_content.beta)
        self.input7.SetValue(my_cfl_content.gamma)

if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
