#!/usr/bin/python
# input data by Eduardo

import wx
import os
import wx.lib.scrolledpanel as scroll_pan
from readcfl import *
from readdat import *



def write_cfl(dto):

    myfile = open("cfl_out.cfl", "w")
    wrstring = "SIZE_LG " + dto.new_input8 + "\n"
    myfile.write(wrstring);
    wrstring = "IPF_RES " + dto.new_input16 + "\n"
    myfile.write(wrstring);
    wrstring =  "HKL_PREF " + dto.new_input11 + " " + dto.new_input12 + " " + dto.new_input13 + "\n"
    myfile.write(wrstring);
    wrstring = "HKL_PREF_WH " + dto.new_input14 + "\n"
    myfile.write(wrstring);
    wrstring = "AZM_IPF " + dto.new_input15 + "\n"
    myfile.write(wrstring);
    wrstring = "SIZE_LG " + dto.new_input8 + "\n"
    myfile.write(wrstring);
    wrstring = "IPF_RES " + dto.new_input16 + "\n"
    myfile.write(wrstring);
    wrstring =  "HKL_PREF " + dto.new_input11 + " " + dto.new_input12 + " " + dto.new_input13 + "\n"
    myfile.write(wrstring);
    wrstring = "HKL_PREF_WH " + dto.new_input14 + "\n"
    myfile.write(wrstring);
    wrstring = "AZM_IPF " + dto.new_input15 + "\n"
    myfile.write(wrstring);

    myfile.close()

def write_dat(dto):

    myfile = open("param_out.dat", "w")
    wrstring = "xres" + "\n" + dto.new_input21 + "\n"
    myfile.write(wrstring);
    wrstring = "yres" + "\n" + dto.new_input22 + "\n"
    myfile.write(wrstring);
    wrstring =  "xbeam" + "\n" + dto.new_input19 + "\n"
    myfile.write(wrstring);
    wrstring = "ybeam" + "\n" + dto.new_input20 + "\n"
    myfile.write(wrstring);
    wrstring = "lambda" + "\n" + dto.new_input17 + "\n"
    myfile.write(wrstring);
    wrstring = "dst_det" + "\n" + dto.new_input18 + "\n"
    myfile.write(wrstring);
    wrstring = "diam_det" + "\n" + dto.new_input23 + "\n"
    myfile.write(wrstring);
    wrstring = "thet_min" + "\n" + "0" + "\n" + "end" + "\n"
    myfile.write(wrstring);

    myfile.close()



class CrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(CrystalPanel, self).__init__(parent)



        title = wx.StaticText(self, wx.ID_ANY, 'Title')
        self.inputtitle = wx.TextCtrl(self, wx.ID_ANY, '')

        subt1 = wx.StaticText(self, wx.ID_ANY, '*** CRYSTAL STRUCTURE ***')

        label1 = wx.StaticText(self, wx.ID_ANY, '  Space group  ')
        self.input1 = wx.TextCtrl(self, wx.ID_ANY, '')

        label2 = wx.StaticText(self, wx.ID_ANY, '           a')
        self.input2 = wx.TextCtrl(self, wx.ID_ANY, '')

        label3 = wx.StaticText(self, wx.ID_ANY, '           b')
        self.input3 = wx.TextCtrl(self, wx.ID_ANY, '')

        label4 = wx.StaticText(self, wx.ID_ANY, '           c')
        self.input4 = wx.TextCtrl(self, wx.ID_ANY, '')

        label5 = wx.StaticText(self, wx.ID_ANY, '      alpha')
        self.input5 = wx.TextCtrl(self, wx.ID_ANY, '')

        label6 = wx.StaticText(self, wx.ID_ANY, '       beta')
        self.input6 = wx.TextCtrl(self, wx.ID_ANY, '')

        label7 = wx.StaticText(self, wx.ID_ANY, '      gamma')
        self.input7 = wx.TextCtrl(self, wx.ID_ANY, '')



        ReadBtn = wx.Button(self, wx.ID_ANY, 'Read')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRead, ReadBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        titleSizer       = wx.BoxSizer(wx.HORIZONTAL)
        subt1Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        spcgrpSizer      = wx.BoxSizer(wx.HORIZONTAL)
        abcSizer         = wx.BoxSizer(wx.HORIZONTAL)
        anglesSizer      = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)

        inputa           = wx.BoxSizer(wx.VERTICAL)
        inputb           = wx.BoxSizer(wx.VERTICAL)
        inputc           = wx.BoxSizer(wx.VERTICAL)

        inputalpha       = wx.BoxSizer(wx.VERTICAL)
        inputbeta        = wx.BoxSizer(wx.VERTICAL)
        inputgamma       = wx.BoxSizer(wx.VERTICAL)


        inputa.Add(label2, 0, wx.ALL, 2)
        inputa.Add(self.input2, 2, wx.ALL| wx.EXPAND, 2)
        inputb.Add(label3, 0, wx.ALL, 2)
        inputb.Add(self.input3, 2, wx.ALL| wx.EXPAND, 2)
        inputc.Add(label4, 0, wx.ALL, 2)
        inputc.Add(self.input4, 2, wx.ALL| wx.EXPAND, 2)

        inputalpha.Add(label5, 0, wx.ALL, 2)
        inputalpha.Add(self.input5, 2, wx.ALL| wx.EXPAND, 2)
        inputbeta.Add(label6, 0, wx.ALL, 2)
        inputbeta.Add(self.input6, 2, wx.ALL| wx.EXPAND, 2)
        inputgamma.Add(label7, 0, wx.ALL, 2)
        inputgamma.Add(self.input7, 2, wx.ALL| wx.EXPAND, 2)


        titleSizer.Add(title, 0, wx.ALL, 2)
        titleSizer.Add(self.inputtitle, 2, wx.ALL| wx.EXPAND, 2)

        subt1Sizer.Add(subt1, 0, wx.ALL, 2)

        spcgrpSizer.Add(label1, 0, wx.ALL, 2)
        spcgrpSizer.Add(self.input1, 2, wx.ALL| wx.EXPAND, 2)

        abcSizer.Add(inputa, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputb, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputc, 0, wx.ALL| wx.EXPAND, 2)

        anglesSizer.Add(inputalpha, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputbeta, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputgamma, 0, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(ReadBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(titleSizer, 0, wx.EXPAND)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt1Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(spcgrpSizer, 0, wx.LEFT)
        MainSizer.Add(abcSizer, 0, wx.CENTER)
        MainSizer.Add(anglesSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRead(self, event):

        new_inputtitle = self.inputtitle.GetValue()
        new_input1 = self.input1.GetValue()
        new_input2 = self.input2.GetValue()
        new_input3 = self.input3.GetValue()
        new_input4 = self.input4.GetValue()
        new_input5 = self.input5.GetValue()
        new_input6 = self.input6.GetValue()
        new_input7 = self.input7.GetValue()

        dto = data()

        dto.new_inputtitle =  new_inputtitle
        dto.new_input1     =  new_input1
        dto.new_input2     =  new_input2
        dto.new_input3     =  new_input3
        dto.new_input4     =  new_input4
        dto.new_input5     =  new_input5
        dto.new_input6     =  new_input6
        dto.new_input7     =  new_input7


        print dto.new_inputtitle
        print dto.new_input1
        print dto.new_input2
        print dto.new_input3
        print dto.new_input4
        print dto.new_input5
        print dto.new_input6
        print dto.new_input7

        write_cfl(dto)

        return dto

    def onOpen(self, event):

        dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
                 path = dlg.GetPath()
                 mypath = os.path.basename(path)
        dlg.Destroy()

        my_cfl_content = read_cfl_file(my_file_path = mypath)

        self.inputtitle.SetValue(my_cfl_content.title)
        self.input1.SetValue(my_cfl_content.spgr)
        self.input2.SetValue(my_cfl_content.a)
        self.input3.SetValue(my_cfl_content.b)
        self.input4.SetValue(my_cfl_content.c)
        self.input5.SetValue(my_cfl_content.alpha)
        self.input6.SetValue(my_cfl_content.beta)
        self.input7.SetValue(my_cfl_content.gamma)



class PoliCrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(PoliCrystalPanel, self).__init__(parent)



        subt2 = wx.StaticText(self, wx.ID_ANY, '*** POLYCRYSTAL STRUCTURE ***')

        label8 = wx.StaticText(self, wx.ID_ANY, 'Average crystal size ')
        self.input8 = wx.TextCtrl(self, wx.ID_ANY, '')
        label81 = wx.StaticText(self, wx.ID_ANY, ' (Angstrom)')

        label9 = wx.StaticText(self, wx.ID_ANY, 'Average strain          ')
        self.input9 = wx.TextCtrl(self, wx.ID_ANY, '')
        label91 = wx.StaticText(self, wx.ID_ANY, ' (%)')

        label10 = wx.StaticText(self, wx.ID_ANY, '                   Preferred orientation')
        label11 = wx.StaticText(self, wx.ID_ANY, '           H')
        self.input11 = wx.TextCtrl(self, wx.ID_ANY, '')
        label12 = wx.StaticText(self, wx.ID_ANY, '           K')
        self.input12 = wx.TextCtrl(self, wx.ID_ANY, '')
        label13 = wx.StaticText(self, wx.ID_ANY, '           L')
        self.input13 = wx.TextCtrl(self, wx.ID_ANY, '')

        label14 = wx.StaticText(self, wx.ID_ANY, 'IPF Gaussian distribution FWHM')
        self.input14 = wx.TextCtrl(self, wx.ID_ANY, '')

        label15 = wx.StaticText(self, wx.ID_ANY, 'Inverse pole figure azimuth      ')
        self.input15 = wx.TextCtrl(self, wx.ID_ANY, '')

        label16 = wx.StaticText(self, wx.ID_ANY, 'IPF number of horizontal steps ')
        self.input16 = wx.TextCtrl(self, wx.ID_ANY, '')

        RunBtn = wx.Button(self, wx.ID_ANY, 'Read')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRun, RunBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)


        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        subt2Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        avgeszSizer      = wx.BoxSizer(wx.HORIZONTAL)
        avgestrSizer     = wx.BoxSizer(wx.HORIZONTAL)
        hklSizer         = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer        = wx.BoxSizer(wx.HORIZONTAL)
        ipfazSizer       = wx.BoxSizer(wx.HORIZONTAL)
        ipfnsSizer       = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)


        hSizer           = wx.BoxSizer(wx.VERTICAL)
        kSizer           = wx.BoxSizer(wx.VERTICAL)
        lSizer           = wx.BoxSizer(wx.VERTICAL)
        hklhSizer        = wx.BoxSizer(wx.HORIZONTAL)

        hSizer.Add(label11, 0, wx.ALL, 2)
        hSizer.Add(self.input11, 2, wx.ALL| wx.EXPAND, 2)
        kSizer.Add(label12, 0, wx.ALL, 2)
        kSizer.Add(self.input12, 2, wx.ALL| wx.EXPAND, 2)
        lSizer.Add(label13, 0, wx.ALL, 2)
        lSizer.Add(self.input13, 2, wx.ALL| wx.EXPAND, 2)

        hklhSizer.Add(hSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(kSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(lSizer, 0, wx.ALL| wx.EXPAND, 2)

        subt2Sizer.Add(subt2, 0, wx.ALL, 2)

        avgeszSizer.Add(label8, 0, wx.ALL, 2)
        avgeszSizer.Add(self.input8, 2, wx.ALL| wx.EXPAND, 2)
        avgeszSizer.Add(label81, 0, wx.ALL, 2)

        avgestrSizer.Add(label9, 0, wx.ALL, 2)
        avgestrSizer.Add(self.input9, 2, wx.ALL| wx.EXPAND, 2)
        avgestrSizer.Add(label91, 0, wx.ALL, 2)

        hklSizer.Add(label10, 0, wx.ALL, 2)
        hklSizer.Add(hklhSizer, 0, wx.ALL| wx.EXPAND, 2)

        fwhmSizer.Add(label14, 0, wx.ALL, 2)
        fwhmSizer.Add(self.input14, 2, wx.ALL| wx.EXPAND, 2)

        ipfazSizer.Add(label15, 0, wx.ALL, 2)
        ipfazSizer.Add(self.input15, 2, wx.ALL| wx.EXPAND, 2)

        ipfnsSizer.Add(label16, 0, wx.ALL, 2)
        ipfnsSizer.Add(self.input16, 2, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(RunBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt2Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(avgeszSizer, 0, wx.LEFT)
        MainSizer.Add(avgestrSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(hklSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(fwhmSizer, 0, wx.LEFT)
        MainSizer.Add(ipfazSizer, 0, wx.LEFT)
        MainSizer.Add(ipfnsSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRun(self, event):

        new_input8  = self.input8.GetValue()
        new_input9  = self.input9.GetValue()
        new_input11 = self.input11.GetValue()
        new_input12 = self.input12.GetValue()
        new_input13 = self.input13.GetValue()
        new_input14 = self.input14.GetValue()
        new_input15 = self.input15.GetValue()
        new_input16 = self.input16.GetValue()

        dto = data()

        dto.new_input8  =  new_input8
        dto.new_input9  =  new_input9
        dto.new_input11 =  new_input11
        dto.new_input12 =  new_input12
        dto.new_input13 =  new_input13
        dto.new_input14 =  new_input14
        dto.new_input15 =  new_input15
        dto.new_input16 =  new_input16


        print dto.new_input8
        print dto.new_input9
        print dto.new_input11
        print dto.new_input12
        print dto.new_input13
        print dto.new_input14
        print dto.new_input15
        print dto.new_input16

        write_cfl(dto)

        return dto


    def onOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
       dlg.Destroy()

       my_cfl_content = read_cfl_file(my_file_path = mypath)

       self.input8.SetValue(my_cfl_content.size_lg)
       self.input9.SetValue('none')
       self.input11.SetValue(my_cfl_content.h)
       self.input12.SetValue(my_cfl_content.k)
       self.input13.SetValue(my_cfl_content.l)
       self.input14.SetValue(my_cfl_content.hklwh)
       self.input15.SetValue(my_cfl_content.azm_ipf)
       self.input16.SetValue(my_cfl_content.ipf_res)



class ParamPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ParamPanel, self).__init__(parent)



        subt3 = wx.StaticText(self, wx.ID_ANY, '*** EXPERIMENTAL DATA ***')

        label17= wx.StaticText(self, wx.ID_ANY, '           Wavelength            ')
        self.input17= wx.TextCtrl(self, wx.ID_ANY, '')
        label171 = wx.StaticText(self, wx.ID_ANY, ' (Angstrom)')

        label18= wx.StaticText(self, wx.ID_ANY, 'Sample-detector distance')
        self.input18= wx.TextCtrl(self, wx.ID_ANY, '')
        label181 = wx.StaticText(self, wx.ID_ANY, ' (mm)')

        label19 = wx.StaticText(self, wx.ID_ANY, 'X Beam centre')
        self.input19 = wx.TextCtrl(self, wx.ID_ANY, '')
        label20 = wx.StaticText(self, wx.ID_ANY, 'Y Beam centre')
        self.input20 = wx.TextCtrl(self, wx.ID_ANY, '')

        label21 = wx.StaticText(self, wx.ID_ANY, '       X Max')
        self.input21 = wx.TextCtrl(self, wx.ID_ANY, '')
        label22 = wx.StaticText(self, wx.ID_ANY, '       Y Max')
        self.input22 = wx.TextCtrl(self, wx.ID_ANY, '')

        label23= wx.StaticText(self, wx.ID_ANY, 'Detector diameter            ')
        self.input23= wx.TextCtrl(self, wx.ID_ANY, '')
        label231 = wx.StaticText(self, wx.ID_ANY, ' (mm)')


        RunBtn = wx.Button(self, wx.ID_ANY, 'Read')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .dat')
        self.Bind(wx.EVT_BUTTON, self.onRun, RunBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        subt3Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        wlSizer          = wx.BoxSizer(wx.HORIZONTAL)
        sddSizer         = wx.BoxSizer(wx.HORIZONTAL)
        xySizer          = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer        = wx.BoxSizer(wx.HORIZONTAL)
        ddSizer          = wx.BoxSizer(wx.VERTICAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)


        xbSizer = wx.BoxSizer(wx.VERTICAL)
        ybSizer = wx.BoxSizer(wx.VERTICAL)
        xmSizer = wx.BoxSizer(wx.VERTICAL)
        ymSizer = wx.BoxSizer(wx.VERTICAL)

        xybSizer = wx.BoxSizer(wx.HORIZONTAL)
        xymSizer = wx.BoxSizer(wx.HORIZONTAL)

        ddhSizer = wx.BoxSizer(wx.HORIZONTAL)

        xbSizer.Add(label19, 0, wx.ALL, 2)
        xbSizer.Add(self.input19, 2, wx.ALL| wx.EXPAND, 2)
        ybSizer.Add(label20, 0, wx.ALL, 2)
        ybSizer.Add(self.input20, 2, wx.ALL| wx.EXPAND, 2)

        xmSizer.Add(label21, 0, wx.ALL, 2)
        xmSizer.Add(self.input21, 2, wx.ALL| wx.EXPAND, 2)
        ymSizer.Add(label22, 0, wx.ALL, 2)
        ymSizer.Add(self.input22, 2, wx.ALL| wx.EXPAND, 2)

        xybSizer.Add(xbSizer, 0, wx.ALL| wx.EXPAND, 2)
        xybSizer.Add(ybSizer, 0, wx.ALL| wx.EXPAND, 2)

        xymSizer.Add(xmSizer, 0, wx.ALL| wx.EXPAND, 2)
        xymSizer.Add(ymSizer, 0, wx.ALL| wx.EXPAND, 2)


        subt3Sizer.Add(subt3, 0, wx.ALL,2)

        wlSizer.Add(label17, 0, wx.ALL, 2)
        wlSizer.Add(self.input17, 2, wx.ALL| wx.EXPAND, 2)
        wlSizer.Add(label171, 0, wx.ALL, 2)

        sddSizer.Add(label18, 0, wx.ALL, 2)
        sddSizer.Add(self.input18, 2, wx.ALL| wx.EXPAND, 2)
        sddSizer.Add(label181, 0, wx.ALL, 2)

        xySizer.Add(xybSizer, 0, wx.ALL| wx.EXPAND, 2)
        xySizer.Add(xymSizer, 0, wx.ALL| wx.EXPAND, 2)

        ddSizer.Add(label23, 0, wx.LEFT, 2)

        ddhSizer.Add(self.input23, 2, wx.ALL| wx.EXPAND, 2)
        ddhSizer.Add(label231, 0, wx.ALL, 2)

        ddSizer.Add(ddhSizer, 0, wx.CENTER)

        btnSizer.Add(RunBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)



        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt3Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(wlSizer, 0, wx.LEFT)
        MainSizer.Add(sddSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(xySizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(ddSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRun(self, event):

        new_input17 = self.input17.GetValue()
        new_input18 = self.input18.GetValue()
        new_input19 = self.input19.GetValue()
        new_input20 = self.input20.GetValue()
        new_input21 = self.input21.GetValue()
        new_input22 = self.input22.GetValue()
        new_input23 = self.input23.GetValue()


        dto = data()

        dto.new_input17 =  new_input17
        dto.new_input18 =  new_input18
        dto.new_input19 =  new_input19
        dto.new_input20 =  new_input20
        dto.new_input21 =  new_input21
        dto.new_input22 =  new_input22
        dto.new_input23 =  new_input23

        print dto.new_input17
        print dto.new_input18
        print dto.new_input19
        print dto.new_input20
        print dto.new_input21
        print dto.new_input22
        print dto.new_input23

        write_dat(dto)

    def onOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
       dlg.Destroy()

       my_dat_content = read_dat_file(my_file_path = mypath)

       self.input17.SetValue(my_dat_content.lambd)
       self.input18.SetValue(my_dat_content.dst_det)
       self.input19.SetValue(my_dat_content.xbeam)
       self.input20.SetValue(my_dat_content.ybeam)
       self.input21.SetValue(my_dat_content.xres)
       self.input22.SetValue(my_dat_content.yres)
       self.input23.SetValue(my_dat_content.diam_det)



class MainPanel(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(MainPanel, self).__init__(parent)
        self.set_scroll_content()


    def set_scroll_content(self):

        self.panel_01 = CrystalPanel(self)
        self.panel_02 = PoliCrystalPanel(self)
        self.panel_03 = ParamPanel(self)

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(self.panel_01, 0, wx.ALL|wx.TOP, 5)
        main_sizer.Add(self.panel_02, 0, wx.ALL|wx.TOP, 5)
        main_sizer.Add(self.panel_03, 0, wx.ALL|wx.TOP, 5)


        self.SetSizer(main_sizer)
        self.SetupScrolling()
        self.Layout()


class MyForm(wx.Frame):
    def __init__(self):
        super(MyForm, self).__init__( None, -1, 'Input Data')


        self.main_panel = MainPanel(self)

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainSizer.Add(self.main_panel, proportion = 1, flag = wx.ALL|wx.EXPAND,border = 5)

        self.SetSizer(MainSizer)
        self.Centre()



if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
