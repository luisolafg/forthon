#!/usr/bin/python
# input data by Eduardo

import wx
import os
import wx.lib.scrolledpanel as scroll_pan
from readcfl import *
from readdat import *


class CrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(CrystalPanel, self).__init__(parent)

        self.parent_widget = parent

        label1 = wx.StaticText(self, wx.ID_ANY, '  Space group  ')
        self.input1 = wx.TextCtrl(self, wx.ID_ANY, '')

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        spcgrpSizer = wx.BoxSizer(wx.HORIZONTAL)

        spcgrpSizer.Add(label1, 0, wx.ALL, 2)
        spcgrpSizer.Add(self.input1, 2, wx.ALL| wx.EXPAND, 2)

        MainSizer.Add(spcgrpSizer, 0, wx.LEFT)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)


class PoliCrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(PoliCrystalPanel, self).__init__(parent)

        self.parent_widget = parent

        label1 = wx.StaticText(self, wx.ID_ANY, '  Space group  ')
        self.input1 = wx.TextCtrl(self, wx.ID_ANY, '')

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        spcgrpSizer = wx.BoxSizer(wx.HORIZONTAL)

        spcgrpSizer.Add(label1, 0, wx.ALL, 2)
        spcgrpSizer.Add(self.input1, 2, wx.ALL| wx.EXPAND, 2)

        MainSizer.Add(spcgrpSizer, 0, wx.LEFT)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)


class ParamPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ParamPanel, self).__init__(parent)

        self.parent_widget = parent
        ReadBtn = wx.Button(self, wx.ID_ANY, 'Read')
        self.Bind(wx.EVT_BUTTON, self.onRead, ReadBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(ReadBtn, 0, wx.ALL, 5)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRead(self, event):
        #self.parent_widget.set_value("cualquier palabra")
        self.parent_widget.panel_01.input1.SetValue("otra palabra")
        self.parent_widget.panel_02.input1.SetValue("una palabra")

class MainPanel(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(MainPanel, self).__init__(parent)
        self.set_scroll_content()

    def set_scroll_content(self):

        self.panel_01 = CrystalPanel(self)
        self.panel_02 = PoliCrystalPanel(self)
        self.panel_03 = ParamPanel(self)

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(self.panel_01, 0, wx.ALL|wx.TOP, 5)
        main_sizer.Add(self.panel_02, 0, wx.ALL|wx.TOP, 5)
        main_sizer.Add(self.panel_03, 0, wx.ALL|wx.TOP, 5)

        self.SetSizer(main_sizer)
        self.SetupScrolling()
        self.Layout()

    '''
    def set_value(self, value):

        self.panel_01.input1.SetValue(value)
    '''


class MyForm(wx.Frame):
    def __init__(self):
        super(MyForm, self).__init__( None, -1, 'Input Data')

        self.main_panel = MainPanel(self)


        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainSizer.Add(self.main_panel, proportion = 1, flag = wx.ALL|wx.EXPAND,border = 5)

        self.SetSizer(MainSizer)
        self.Centre()


if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
