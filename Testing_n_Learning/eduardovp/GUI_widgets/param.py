#!/usr/bin/python
# input by Eduardo

import wx
import os
from readdat import *

def write_cfl(dto):

    myfile = open("param_out.dat", "w")
    wrstring = "xres" + "\n" + dto.new_input21 + "\n"
    myfile.write(wrstring);
    wrstring = "yres" + "\n" + dto.new_input22 + "\n"
    myfile.write(wrstring);
    wrstring =  "xbeam" + "\n" + dto.new_input19 + "\n"
    myfile.write(wrstring);
    wrstring = "ybeam" + "\n" + dto.new_input20 + "\n"
    myfile.write(wrstring);
    wrstring = "lambda" + "\n" + dto.new_input17 + "\n"
    myfile.write(wrstring);
    wrstring = "dst_det" + "\n" + dto.new_input18 + "\n"
    myfile.write(wrstring);
    wrstring = "diam_det" + "\n" + dto.new_input23 + "\n"
    myfile.write(wrstring);
    wrstring = "thet_min" + "\n" + "0" + "\n" + "end" + "\n" + "\n"
    myfile.write(wrstring);

    myfile.close()

class MyForm(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, title='Param')


        self.panel = wx.Panel(self, wx.ID_ANY)


        subt3 = wx.StaticText(self.panel, wx.ID_ANY, '*** EXPERIMENTAL DATA ***')

        label17= wx.StaticText(self.panel, wx.ID_ANY, '           Wavelength            ')
        self.input17= wx.TextCtrl(self.panel, wx.ID_ANY, '')
        label171 = wx.StaticText(self.panel, wx.ID_ANY, ' (Angstrom)')

        label18= wx.StaticText(self.panel, wx.ID_ANY, 'Sample-detector distance')
        self.input18= wx.TextCtrl(self.panel, wx.ID_ANY, '')
        label181 = wx.StaticText(self.panel, wx.ID_ANY, ' (mm)')

        label19 = wx.StaticText(self.panel, wx.ID_ANY, 'X Beam centre')
        self.input19 = wx.TextCtrl(self.panel, wx.ID_ANY, '')
        label20 = wx.StaticText(self.panel, wx.ID_ANY, 'Y Beam centre')
        self.input20 = wx.TextCtrl(self.panel, wx.ID_ANY, '')

        label21 = wx.StaticText(self.panel, wx.ID_ANY, '       X Max')
        self.input21 = wx.TextCtrl(self.panel, wx.ID_ANY, '')
        label22 = wx.StaticText(self.panel, wx.ID_ANY, '       Y Max')
        self.input22 = wx.TextCtrl(self.panel, wx.ID_ANY, '')

        label23= wx.StaticText(self.panel, wx.ID_ANY, 'Detector diameter            ')
        self.input23= wx.TextCtrl(self.panel, wx.ID_ANY, '')
        label231 = wx.StaticText(self.panel, wx.ID_ANY, ' (mm)')


        RunBtn = wx.Button(self.panel, wx.ID_ANY, 'Run')
        OpenBtn = wx.Button(self.panel, wx.ID_ANY, 'Open .dat')
        self.Bind(wx.EVT_BUTTON, self.onRun, RunBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        subt3Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        wlSizer          = wx.BoxSizer(wx.HORIZONTAL)
        sddSizer         = wx.BoxSizer(wx.HORIZONTAL)
        xySizer          = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer        = wx.BoxSizer(wx.HORIZONTAL)
        ddSizer          = wx.BoxSizer(wx.VERTICAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)


        xbSizer = wx.BoxSizer(wx.VERTICAL)
        ybSizer = wx.BoxSizer(wx.VERTICAL)
        xmSizer = wx.BoxSizer(wx.VERTICAL)
        ymSizer = wx.BoxSizer(wx.VERTICAL)

        xybSizer = wx.BoxSizer(wx.HORIZONTAL)
        xymSizer = wx.BoxSizer(wx.HORIZONTAL)

        ddhSizer = wx.BoxSizer(wx.HORIZONTAL)

        xbSizer.Add(label19, 0, wx.ALL, 2)
        xbSizer.Add(self.input19, 2, wx.ALL| wx.EXPAND, 2)
        ybSizer.Add(label20, 0, wx.ALL, 2)
        ybSizer.Add(self.input20, 2, wx.ALL| wx.EXPAND, 2)

        xmSizer.Add(label21, 0, wx.ALL, 2)
        xmSizer.Add(self.input21, 2, wx.ALL| wx.EXPAND, 2)
        ymSizer.Add(label22, 0, wx.ALL, 2)
        ymSizer.Add(self.input22, 2, wx.ALL| wx.EXPAND, 2)

        xybSizer.Add(xbSizer, 0, wx.ALL| wx.EXPAND, 2)
        xybSizer.Add(ybSizer, 0, wx.ALL| wx.EXPAND, 2)

        xymSizer.Add(xmSizer, 0, wx.ALL| wx.EXPAND, 2)
        xymSizer.Add(ymSizer, 0, wx.ALL| wx.EXPAND, 2)


        subt3Sizer.Add(subt3, 0, wx.ALL,2)

        wlSizer.Add(label17, 0, wx.ALL, 2)
        wlSizer.Add(self.input17, 2, wx.ALL| wx.EXPAND, 2)
        wlSizer.Add(label171, 0, wx.ALL, 2)

        sddSizer.Add(label18, 0, wx.ALL, 2)
        sddSizer.Add(self.input18, 2, wx.ALL| wx.EXPAND, 2)
        sddSizer.Add(label181, 0, wx.ALL, 2)

        xySizer.Add(xybSizer, 0, wx.ALL| wx.EXPAND, 2)
        xySizer.Add(xymSizer, 0, wx.ALL| wx.EXPAND, 2)

        ddSizer.Add(label23, 0, wx.LEFT, 2)

        ddhSizer.Add(self.input23, 2, wx.ALL| wx.EXPAND, 2)
        ddhSizer.Add(label231, 0, wx.ALL, 2)

        ddSizer.Add(ddhSizer, 0, wx.CENTER)

        btnSizer.Add(RunBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)



        MainSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt3Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(wlSizer, 0, wx.LEFT)
        MainSizer.Add(sddSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(xySizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(ddSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.panel.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRun(self, event):

        new_input17 = self.input17.GetValue()
        new_input18 = self.input18.GetValue()
        new_input19 = self.input19.GetValue()
        new_input20 = self.input20.GetValue()
        new_input21 = self.input21.GetValue()
        new_input22 = self.input22.GetValue()
        new_input23 = self.input23.GetValue()


        dto = data()

        dto.new_input17 =  new_input17
        dto.new_input18 =  new_input18
        dto.new_input19 =  new_input19
        dto.new_input20 =  new_input20
        dto.new_input21 =  new_input21
        dto.new_input22 =  new_input22
        dto.new_input23 =  new_input23

        print dto.new_input17
        print dto.new_input18
        print dto.new_input19
        print dto.new_input20
        print dto.new_input21
        print dto.new_input22
        print dto.new_input23

        write_cfl(dto)

    def onOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
       dlg.Destroy()

       my_cfl_content = read_cfl_file(my_file_path = mypath)

       self.input17.SetValue(my_cfl_content.lambd)
       self.input18.SetValue(my_cfl_content.dst_det)
       self.input19.SetValue(my_cfl_content.xbeam)
       self.input20.SetValue(my_cfl_content.ybeam)
       self.input21.SetValue(my_cfl_content.xres)
       self.input22.SetValue(my_cfl_content.yres)
       self.input23.SetValue(my_cfl_content.diam_det)

if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
