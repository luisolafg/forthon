#!/usr/bin/python
# input by Eduardo

import wx
import os
from readcfl import *
from crystal import write_cfl

def write_cfl(dto):

    myfile = open("cfl_out.cfl", "w")
    wrstring = "SIZE_LG " + dto.new_input8 + "\n"
    myfile.write(wrstring);
    wrstring = "IPF_RES " + dto.new_input16 + "\n"
    myfile.write(wrstring);
    wrstring =  "HKL_PREF " + dto.new_input11 + " " + dto.new_input12 + " " + dto.new_input13 + "\n"
    myfile.write(wrstring);
    wrstring = "HKL_PREF_WH " + dto.new_input14 + "\n"
    myfile.write(wrstring);
    wrstring = "AZM_IPF " + dto.new_input15 + "\n"
    myfile.write(wrstring);

    myfile.close()


class MyForm(wx.Frame):
    def __init__(self):
        super(MyForm, self).__init__( None, -1, 'Input Data')
        self.panel_02 = PoliCrystalPanel(self)

class PoliCrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(PoliCrystalPanel, self).__init__(parent)



        subt2 = wx.StaticText(self, wx.ID_ANY, '*** POLYCRYSTAL STRUCTURE ***')

        label8 = wx.StaticText(self, wx.ID_ANY, 'Average crystal size ')
        self.input8 = wx.TextCtrl(self, wx.ID_ANY, '')
        label81 = wx.StaticText(self, wx.ID_ANY, ' (Angstrom)')

        label9 = wx.StaticText(self, wx.ID_ANY, 'Average strain          ')
        self.input9 = wx.TextCtrl(self, wx.ID_ANY, '')
        label91 = wx.StaticText(self, wx.ID_ANY, ' (%)')

        label10 = wx.StaticText(self, wx.ID_ANY, '                   Preferred orientation')
        label11 = wx.StaticText(self, wx.ID_ANY, '           H')
        self.input11 = wx.TextCtrl(self, wx.ID_ANY, '')
        label12 = wx.StaticText(self, wx.ID_ANY, '           K')
        self.input12 = wx.TextCtrl(self, wx.ID_ANY, '')
        label13 = wx.StaticText(self, wx.ID_ANY, '           L')
        self.input13 = wx.TextCtrl(self, wx.ID_ANY, '')

        label14 = wx.StaticText(self, wx.ID_ANY, 'IPF Gaussian distribution FWHM')
        self.input14 = wx.TextCtrl(self, wx.ID_ANY, '')

        label15 = wx.StaticText(self, wx.ID_ANY, 'Inverse pole figure azimuth      ')
        self.input15 = wx.TextCtrl(self, wx.ID_ANY, '')

        label16 = wx.StaticText(self, wx.ID_ANY, 'IPF number of horizontal steps ')
        self.input16 = wx.TextCtrl(self, wx.ID_ANY, '')

        RunBtn = wx.Button(self, wx.ID_ANY, 'Run')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRun, RunBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)


        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        subt2Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        avgeszSizer      = wx.BoxSizer(wx.HORIZONTAL)
        avgestrSizer     = wx.BoxSizer(wx.HORIZONTAL)
        hklSizer         = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer        = wx.BoxSizer(wx.HORIZONTAL)
        ipfazSizer       = wx.BoxSizer(wx.HORIZONTAL)
        ipfnsSizer       = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)


        hSizer           = wx.BoxSizer(wx.VERTICAL)
        kSizer           = wx.BoxSizer(wx.VERTICAL)
        lSizer           = wx.BoxSizer(wx.VERTICAL)
        hklhSizer        = wx.BoxSizer(wx.HORIZONTAL)

        hSizer.Add(label11, 0, wx.ALL, 2)
        hSizer.Add(self.input11, 2, wx.ALL| wx.EXPAND, 2)
        kSizer.Add(label12, 0, wx.ALL, 2)
        kSizer.Add(self.input12, 2, wx.ALL| wx.EXPAND, 2)
        lSizer.Add(label13, 0, wx.ALL, 2)
        lSizer.Add(self.input13, 2, wx.ALL| wx.EXPAND, 2)

        hklhSizer.Add(hSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(kSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(lSizer, 0, wx.ALL| wx.EXPAND, 2)

        subt2Sizer.Add(subt2, 0, wx.ALL, 2)

        avgeszSizer.Add(label8, 0, wx.ALL, 2)
        avgeszSizer.Add(self.input8, 2, wx.ALL| wx.EXPAND, 2)
        avgeszSizer.Add(label81, 0, wx.ALL, 2)

        avgestrSizer.Add(label9, 0, wx.ALL, 2)
        avgestrSizer.Add(self.input9, 2, wx.ALL| wx.EXPAND, 2)
        avgestrSizer.Add(label91, 0, wx.ALL, 2)

        hklSizer.Add(label10, 0, wx.ALL, 2)
        hklSizer.Add(hklhSizer, 0, wx.ALL| wx.EXPAND, 2)

        fwhmSizer.Add(label14, 0, wx.ALL, 2)
        fwhmSizer.Add(self.input14, 2, wx.ALL| wx.EXPAND, 2)

        ipfazSizer.Add(label15, 0, wx.ALL, 2)
        ipfazSizer.Add(self.input15, 2, wx.ALL| wx.EXPAND, 2)

        ipfnsSizer.Add(label16, 0, wx.ALL, 2)
        ipfnsSizer.Add(self.input16, 2, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(RunBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt2Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(avgeszSizer, 0, wx.LEFT)
        MainSizer.Add(avgestrSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(hklSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(fwhmSizer, 0, wx.LEFT)
        MainSizer.Add(ipfazSizer, 0, wx.LEFT)
        MainSizer.Add(ipfnsSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRun(self, event):

        new_input8  = self.input8.GetValue()
        new_input9  = self.input9.GetValue()
        new_input11 = self.input11.GetValue()
        new_input12 = self.input12.GetValue()
        new_input13 = self.input13.GetValue()
        new_input14 = self.input14.GetValue()
        new_input15 = self.input15.GetValue()
        new_input16 = self.input16.GetValue()

        dto = data()

        dto.new_input8  =  new_input8
        dto.new_input9  =  new_input9
        dto.new_input11 =  new_input11
        dto.new_input12 =  new_input12
        dto.new_input13 =  new_input13
        dto.new_input14 =  new_input14
        dto.new_input15 =  new_input15
        dto.new_input16 =  new_input16


        print dto.new_input8
        print dto.new_input9
        print dto.new_input11
        print dto.new_input12
        print dto.new_input13
        print dto.new_input14
        print dto.new_input15
        print dto.new_input16

        write_cfl(dto)

        return dto


    def onOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
       dlg.Destroy()

       my_cfl_content = read_cfl_file(my_file_path = mypath)

       self.input8.SetValue(my_cfl_content.size_lg)
       self.input9.SetValue('none')
       self.input11.SetValue(my_cfl_content.h)
       self.input12.SetValue(my_cfl_content.k)
       self.input13.SetValue(my_cfl_content.l)
       self.input14.SetValue(my_cfl_content.hklwh)
       self.input15.SetValue(my_cfl_content.azm_ipf)
       self.input16.SetValue(my_cfl_content.ipf_res)


if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
