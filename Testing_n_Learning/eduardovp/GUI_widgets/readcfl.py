class data(object):
    pass

def read_cfl_file(my_file_path = "My_Cfl.cfl"):


    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;=:':
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)


    title = lst_dat[1]
    a = lst_dat[3]
    b = lst_dat[4]
    c = lst_dat[5]
    alpha = lst_dat[6]
    beta = lst_dat[7]
    gamma = lst_dat[8]
    spgr = lst_dat[10]
    size_lg = lst_dat[12]
    ipf_res = lst_dat[14]
    h = lst_dat[16]
    k = lst_dat[17]
    l = lst_dat[18]
    hklwh = lst_dat[20]
    azm_ipf = lst_dat[22]
    atom1 = lst_dat[24]
    atom2 = lst_dat[26]

    dt = data()

    dt.title   = title
    dt.a       = a
    dt.b       = b
    dt.c       = c
    dt.alpha   = alpha
    dt.beta    = beta
    dt.gamma   = gamma
    dt.spgr    = spgr
    dt.size_lg = size_lg
    dt.ipf_res = ipf_res
    dt.h       = h
    dt.k       = k
    dt.l       = l
    dt.hklwh   = hklwh
    dt.azm_ipf = azm_ipf
    dt.atom1   = atom1
    dt.atom2   = atom2

    return dt

if( __name__ == "__main__" ):
    cfl_file = read_cfl_file(my_file_path = "My_Cfl.cfl")
