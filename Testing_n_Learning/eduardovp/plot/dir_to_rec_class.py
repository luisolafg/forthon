#!/usr/bin/env python

import numpy as np

class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):


        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta  = beta
        self.gamma = gamma

        self.a1 = np.array([a, 0, 0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

        self.b1 = ((2 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


data = DirectToReciprocal(4, 4, 2, 90, 90, 120)

print data.a1
print data.a2
print data.a3
print data.b1
print data.b2
print data.b3
