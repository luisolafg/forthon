# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from mpl_toolkits.mplot3d import proj3d

import wx
import numpy
import matplotlib


fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim([-8,8])
ax.set_ylim([-8,8])
ax.set_zlim([-8,8])
#ax.set_aspect("equal")


class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):


        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta  = beta
        self.gamma = gamma

        self.a1 = np.array([a, 0, 0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

        self.b1 = ((2 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


data = DirectToReciprocal(1.0, 1.0, 1.0, 90, 90, 90)

#print data.a1
#print data.a2
#print data.a3

#print data.b1
#print data.b2
#print data.b3

lmb = 1.0
r = 5.0
h = 0.0
k1= 0.0
l = 0.0
c = 1 / lmb



min_lattice_range = -20
max_lattice_range = +20


for i_int in range(min_lattice_range, max_lattice_range):
    for j_int in range(min_lattice_range, max_lattice_range):
        for k_int in range(min_lattice_range, max_lattice_range):

            i = float(i_int)
            j = float(j_int)
            k = float(k_int)

            x = i * data.a1[0] + j * data.a1[1] + k * data.a1[2]
            y = i * data.a2[0] + j * data.a2[1] + k * data.a2[2]
            z = i * data.a3[0] + j * data.a3[1] + k * data.a3[2]

            sph = ( (x-h)**2 + (y-k1)**2 + (z-l)**2 )
            ra = r**2
            dif = sph - ra

            if dif >= -c and  dif <= c:

                ax.scatter(x, y, z, color="r", s=10)



            #ax.scatter(x, y, z, color="b", s=10)




'''for i in range(min_lattice_range, max_lattice_range):
    for j in range(min_lattice_range, max_lattice_range):
        for k in range(min_lattice_range, max_lattice_range):

            i = float(i)
            j = float(j)
            k = float(k)

            xr = i * data.b1[0] + j * data.b1[1] + k * data.b1[2]
            yr = i * data.b2[0] + j * data.b2[1] + k * data.b2[2]
            zr = i * data.b3[0] + j * data.b3[1] + k * data.b3[2]

            ax.scatter(xr, yr, zr, color="r", s=10)'''

plt.show()

