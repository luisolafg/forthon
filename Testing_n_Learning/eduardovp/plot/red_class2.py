# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
#ax.set_xlim([-4,4])
#ax.set_ylim([-4,4])
#ax.set_zlim([-4,4])
#ax.set_aspect("equal")


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):


        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta  = beta
        self.gamma = gamma

        self.a1 = np.array([a, 0, 0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

        self.b1 = ((2 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


data = DirectToReciprocal(3, 4, 5, 90, 45, 120)

print data.a1
print data.a2
print data.a3
print data.b1
print data.b2
print data.b3


for i in range(-3,3):
    for j in range(-3,3):
        for k in range(-3,3):

            i = float(i)
            j = float(j)
            k = float(k)

            x = i * data.a1[0] + j * data.a1[1] + k * data.a1[2]
            y = i * data.a2[0] + j * data.a2[1] + k * data.a2[2]
            z = i * data.a3[0] + j * data.a3[1] + k * data.a3[2]

            ax.scatter(x, y, z, color="b", s=10)


for i in range(-3,3):
    for j in range(-3,3):
        for k in range(-3,3):

            i = float(i)
            j = float(j)
            k = float(k)

            xr = i * data.b1[0] + j * data.b1[1] + k * data.b1[2]
            yr = i * data.b2[0] + j * data.b2[1] + k * data.b2[2]
            zr = i * data.b3[0] + j * data.b3[1] + k * data.b3[2]

            ax.scatter(xr, yr, zr, color="r", s=10)


plt.show()
