class Point3D(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __repr__(self):
        return "(%d, %d, %d)" %(self.x, self.y, self.z)
    def __call__(self):
        return [self.x, self.y, self.z]



my_point = Point3D(1,2,3)
print my_point

a = my_point()
print a

print "__repr__ = ", my_point
print "__call__[0]= ", my_point()[0]
