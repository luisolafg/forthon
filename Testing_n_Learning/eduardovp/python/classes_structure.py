class Fruit(object):
  
    # this is the structure for working with classes by Eduardo
    def __init__(self, name, color, flavor, poisonous):
        self.name = name
        self.color = color
        self.flavor = flavor
        self.poisonous = poisonous

    def description(self):
        print "\n I'm a %s %s and I taste %s.\n" % (self.color, self.name, self.flavor)

    def is_eatable(self):
        if not self.poisonous:
            print " Yes! I'm eatable.\n"
        else:
            print " Don't eat me! I am super poisonous.\n"

if __name__ == "__main__":

    lemon = Fruit("lemon", "yellow", "sour", False)

    lemon.description()
    lemon.is_eatable()