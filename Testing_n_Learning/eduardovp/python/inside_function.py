doubles_by_3 = [x*2 for x in range(1,6) if (x*2) % 3 == 0]

even_squares = [i**2 for i in range(1,11) if (i**2) % 2 == 0]
print even_squares