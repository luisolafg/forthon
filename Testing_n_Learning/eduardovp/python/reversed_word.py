def reverse(text):
  word = ""
  index = len(text) - 1
  while index >= 0:
    word += text[index]
    index -= 1
  return word
  
print reverse("abcd@#$1234")