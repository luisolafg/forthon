import sys

def sys_arg():
    print "sys.argv =", sys.argv
    readsys = sys.argv[1:]
    return readsys


def get_par(par_def, lst_in):
    print "par_def:", par_def
    print "lst_in:", lst_in

    par_out = []
    for par in par_def:
        par_out.append([par[0], par[1]])

    print "len(lst_in) =", len(lst_in)

    lst_split_tst = []
    for par in lst_in:
        lst_split_tst.append(par.split("="))

    lng_n1 = len(lst_split_tst[0])
    for lng_tst in lst_split_tst:
        if(len(lng_tst) != lng_n1):
            lng_n1 = None
            break


    if(lng_n1 == None):
        print "Error 01"

    elif(lng_n1 == 1):
        for pos, par in enumerate(lst_in):
            par_out[pos][1] = lst_in[pos]

    elif(lng_n1 == 2):
        for par in lst_in:
            lf_rg_lst=par.split("=")
            print "lf_rg_lst=", lf_rg_lst
            for pos, iter_par in enumerate(par_def):
                if(iter_par[0] == lf_rg_lst[0]):
                    par_out[pos][1] = lf_rg_lst[1]

    else:
        print "Error 02"

    return par_out

if(__name__ == "__main__"):
    par_def_01 =(("cfl_in", "My_Cfl.cfl"),
                 ("dat_in", "my_dat.dat"),
                 ("dummy_var", 5.2),
                 ("file_out", "my_edf.edf"))

    lst_in_tst = sys_arg()
    par_lst_out = get_par(par_def_01, lst_in_tst)

    print "par_def_01(test) =", par_def_01
    print "par_lst_out(final) =", par_lst_out
