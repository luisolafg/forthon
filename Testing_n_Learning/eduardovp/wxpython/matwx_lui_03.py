import wx
import wx.lib.scrolledpanel as scroll_pan

from numpy import arange, sin, pi
import matplotlib
matplotlib.use('WXAgg')

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

class CanvasPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        figure = Figure()
        self.axes = figure.add_subplot(111)
        canvas = FigureCanvas(self, -1, figure)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(sizer)
        self.Fit()


    def draw(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2 * pi * t)
        self.axes.plot(t, s)


class OuterPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.iner_panel = CanvasPanel(self)
        self.iner_panel.draw()
        sizer.Add(self.iner_panel, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(sizer)
        self.Fit()

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "One scrolled panel", size=(780, 220))
        sz = wx.BoxSizer(wx.HORIZONTAL)
        self.o_panel = OuterPanel(self)
        sz.Add(self.o_panel, 1, wx.EXPAND)
        self.cmd_but = wx.Button(self, -1, "Butt One ...")
        sz.Add(self.cmd_but, 1, wx.EXPAND)
        self.SetSizer(sz)
        self.cmd_but.Bind(wx.EVT_BUTTON, self.On_CmdBut)


    def On_CmdBut(self, event):
        print ("Clicked here")


if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
