#!/usr/bin/python

import wx

class MyMenu(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.DefaultPosition, wx.Size(350, 250))

        menubar = wx.MenuBar()
        mn_file = wx.Menu()
        mn_edit = wx.Menu()
        mn_help = wx.Menu()
        mn_file.Append(101, '&Open\tCtrl+O', 'Open a new document')
        mn_file.Append(102, '&Save\tCtrl+S', 'Save the document')
        mn_file.AppendSeparator()
        mn_quit = wx.MenuItem(mn_file, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        #mn_quit.SetBitmap(wx.Image('cute.jpg', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        mn_file.AppendItem(mn_quit)
        
        mn_edit.Append(201, 'main item1', '', wx.ITEM_CHECK)
        mn_edit.Append(202, ' main item2', kind=wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        mn_edit.AppendMenu(203, 'submenu', submenu)

        menubar.Append(mn_file, '&File')
        menubar.Append(mn_edit, '&Edit')
        menubar.Append(mn_help, '&Help')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        
    def OnQuit(self, event):
        self.Close()

class MyApp(wx.App):
    def OnInit(self):
        frame = MyMenu(None, -1, 'menu Eduardo')
        frame.Show(True)
        return True

app = MyApp(0)
app.MainLoop()
