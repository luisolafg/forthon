#!/usr/bin/python

# Menu by Eduardo

import wx
import os

class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "ANAELU", size=(800, 400))

        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        edit_menu = wx.Menu()
        help_menu = wx.Menu()
        help_menu.Append(001, '&FAQ\tCtrl+H', 'Frequently Asked Questions')
        file_menu.Append(101, '&Open\tCtrl+O', 'Open a new document')
        file_menu.Append(102, '&Save\tCtrl+S', 'Save the document')
        file_menu.AppendSeparator()
        quit = wx.MenuItem(file_menu, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        #quit.SetBitmap(wx.Image('stock_exit.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file_menu.AppendItem(quit)

        edit_menu.Append(201, 'main item1', '', wx.ITEM_CHECK)
        edit_menu.Append(202, 'main item2', kind=wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        edit_menu.AppendMenu(203, 'submenu', submenu)

        menubar.Append(file_menu, '&File')
        menubar.Append(edit_menu, '&Edit')
        menubar.Append(help_menu, '&Help')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()

        self.Bind(wx.EVT_MENU, self.OnOpen, id=101)
        self.Bind(wx.EVT_MENU, self.OnSaveAs, id=102)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)

    def OnOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
                self.SetStatusText("You selected: %s" % mypath)
       dlg.Destroy()


    def OnSaveAs(self, event):

        saveFileDialog = wx.FileDialog(self, "Save XYZ file", "", "",
                                       "XYZ files (*.xyz)|*.xyz", wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if saveFileDialog.ShowModal() == wx.ID_CANCEL:
            return     # the user changed idea...

        # save the current contents in the file
        # this can be done with e.g. wxPython output streams:
        output_stream = wx.FileOutputStream(saveFileDialog.GetPath())

        if not output_stream.IsOk():
            wx.LogError("Cannot save current contents in file '%s'."%saveFileDialog.GetPath())
            return

    def OnQuit(self, event):
        self.Close()


if __name__ == "__main__":

    app = wx.App()
    frame = MainFrame()
    frame.Show()
    app.MainLoop()
