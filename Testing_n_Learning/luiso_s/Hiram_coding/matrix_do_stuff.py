# Matrix do some stuff
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import numpy as np
from matplotlib import pyplot as plt
import fabio


def treat(i1 = None, i2 = None, my_const = 93675.0):
    '''
    processing matrix data
    '''
    print "min(i1) =" , np.amin(i1)
    print "max(i1) =" , np.amax(i1)

    print "min(i2) =" , np.amin(i2)
    print "max(i2) =" , np.amax(i2)

    rs = ((100.0 - i1[:,:]) / 100.0) * ( 1.0 / (my_const / i2[:,:]) )

    print "min =" , np.amin(rs)
    print "max =" , np.amax(rs)

    return rs

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):

    my_type = "float32"

    file_arr_iod = "grenoble13_arr_iodet_0001_0000.edf"
    print "opening ", file_arr_iod
    img_iodt = fabio.open(file_arr_iod).data.astype(my_type)
    plott_img(img_iodt)


    file_xmap_dt = "grenoble13_xmap_dt_00_0001_0000.edf"
    print "opening ", file_xmap_dt
    img_xmap = fabio.open(file_xmap_dt).data.astype(my_type)
    plott_img(img_xmap)

    img_res = treat(img_xmap, img_iodt, my_const = 93675.0)
    plott_img(img_res)
