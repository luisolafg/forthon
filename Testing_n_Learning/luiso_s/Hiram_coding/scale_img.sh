# scaling all IMGs in a dir by calling a python code
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo " "
echo " "
echo "par(1) = $1"
echo " "
echo "par(2) = $2"
echo " "
echo " "
python $SCALER_PATH/treat_img_app.py $1 $2
