# Matrix do some stuff
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
from matrix_do_stuff import treat, plott_img
import fabio
import os.path

def loop_over_nums(pref = None, my_const = 93675.0):

    for num in xrange(9999):
        str_num = str(num)

        extra_lenght = 4 - len(str_num)
        extra_zeros = ""
        for nm_add in xrange(extra_lenght):
            extra_zeros = extra_zeros + "0"

        str_num = extra_zeros + str_num
        file_arr_iod = pref + "_arr_iodet_" + str_num + "_0000.edf"
        file_xmap_dt = pref + "_xmap_dt_00_" + str_num + "_0000.edf"

        my_type = "float32"

        if( os.path.isfile(file_arr_iod) and os.path.isfile(file_xmap_dt) ):

            try:
                print "opening ", file_xmap_dt
                img_xmap = fabio.open(file_xmap_dt)
                #plott_img(img_xmap.data)

                print "opening ", file_arr_iod
                img_iodt = fabio.open(file_arr_iod)

                img_xmap.data = treat(img_xmap.data, img_iodt.data, float(my_const))
                #plott_img(img_xmap.data)

                file_out = pref + "_treated_" + str_num + "_0000.edf"
                img_xmap.write(file_out)

            except:
                print "something went wrong with index", num



if __name__ == "__main__":

    import sys
    all_args = sys.argv

    print "len(all_args) =", len(all_args)
    print "all_args =", all_args
    print "all_args[1] =", all_args[1]

    if len(all_args) <= 1 :
        print '''
        not enough parameters
        usage examples:

        scale_img.sh grenoble13

        scale_img.sh grenoble13 93675.0
        '''

    elif len(all_args) == 2 :
        my_prefix = all_args[1]
        loop_over_nums(my_prefix)

    elif len(all_args) == 3 :
        my_prefix = all_args[1]
        par_const = all_args[2]
        loop_over_nums(my_prefix, my_const = par_const)



