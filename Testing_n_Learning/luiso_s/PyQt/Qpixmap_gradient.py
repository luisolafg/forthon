#!/usr/bin/python

import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Example(QWidget):

    def __init__(self):
        super(Example, self).__init__()
        self.initUI()

    def initUI(self):

        hbox = QHBoxLayout(self)

        pixmap  = QPixmap(QSize(400,400))
        painter = QPainter(pixmap)
        gradient    = QLinearGradient(QPointF(pixmap.rect().topLeft()),
                      QPointF(pixmap.rect().bottomLeft()))

        gradient.setColorAt(0,  Qt.blue)
        gradient.setColorAt(0.4,    Qt.cyan)
        gradient.setColorAt(1,  Qt.green)

        brush   = QBrush(gradient)
        painter.fillRect( QRectF(0, 0, 400, 400), brush)
        painter.drawText( QRectF(0, 0, 400, 400), Qt.AlignCenter,
                  "This is an image created with QPainter and QPixmap")

        lbl = QLabel(self)
        lbl.setPixmap(pixmap)

        hbox.addWidget(lbl)
        self.setLayout(hbox)

        self.move(300, 200)
        self.setWindowTitle('Red Rock')
        self.show()

def main():

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
