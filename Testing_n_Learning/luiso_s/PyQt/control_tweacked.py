#!/usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import call as shell_func
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MySpinBx(QDoubleSpinBox):
    def __init__(self, parent):
        super(MySpinBx, self).__init__()
        self.setRange(-99999999, 99999999)
        '''
        self.move(x_real_pos, y_pos)
        self.opt_box1 = QComboBox(self)
        self.opt_box1.lin_ed = self
        self.opt_box1.lst_opt=[]
        self.opt_box1.lst_opt.append("55")
        self.opt_box1.lst_opt.append("66.5")
        self.opt_box1.lst_opt.append("70.00001")
        for lst_itm in self.opt_box1.lst_opt:
            self.opt_box1.addItem(lst_itm)
        self.opt_box1.setCurrentIndex(0)
        self.opt_box1.currentIndexChanged.connect(parent.combobox_changed)
        self.opt_box1.move(x_real_pos, y_pos + 26)
        FreqLabel = QLabel(parent = self, text = "Frequency(MHz) ")
        FreqLabel.move(x_label_pos, y_pos)
        '''


class Example( QWidget):

    def __init__(self):
        super(Example, self).__init__()

        cb = QCheckBox('ECHO', self)
        cb.move(50, 40)
        cb.stateChanged.connect(self.changeCheck)


        size_double_box = 26
        box_dst = 80
        x_real_pos = 550
        x_label_pos = 350
        y_pos = 330


        multi_choise = QComboBox(self)
        multi_choise.lst_opt=[]
        multi_choise.lst_opt.append("5")
        multi_choise.lst_opt.append("6.5")
        multi_choise.lst_opt.append("0.00001")
        for lst_itm in multi_choise.lst_opt:
            multi_choise.addItem(lst_itm)
        multi_choise.setCurrentIndex(0)
        multi_choise.currentIndexChanged.connect(self.choise_changed)
        multi_choise.move(50, y_pos + size_double_box)
        NdropLabel = QLabel(parent = self, text = "choise wathever here  ")
        NdropLabel.move(50, y_pos)

        y_pos = 50


        test_spin = MySpinBx(self)
        test_spin.move(x_real_pos, y_pos)


        y_pos_down = y_pos + 85


        self.btn_load =  QPushButton(' Load ', self)
        self.btn_load.move(150, y_pos_down)
        self.btn_load.clicked.connect(self.clickeado1)

        self.btn_save =  QPushButton(' Save ', self)
        self.btn_save.move(250, y_pos_down)
        self.btn_save.clicked.connect(self.clickeado2)

        self.btn_go =  QPushButton(' \n            Go   \n', self)
        self.btn_go.move(x_real_pos + 50, y_pos_down + 20)
        self.btn_go.clicked.connect(self.clickeado3)

        self.setGeometry(300, y_pos_down + 35, 800, 580)
        self.setWindowTitle('Shell dialog')
        self.show()


    def combobox_changed(self, value):
        sender = self.sender()
        print "combobox_changed to: ",
        str_value = str(sender.lst_opt[value])
        print "value =", float(str_value)
        sender.lin_ed.setValue(float(str_value))

    def choise_changed(self, value):
        sender = self.sender()
        print "choise_changed to: ",
        str_value = str(sender.lst_opt[value])
        print str_value

    def changeCheck(self):
        print "changeCheck"

    def clickeado1(self):
        print "clickeado1"

        num = self.lin_txt3.value()

        print "num =", num


    def clickeado2(self):
        print "clickeado2"

    def clickeado3(self):
        print "clickeado3"


def main():

    app =  QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
