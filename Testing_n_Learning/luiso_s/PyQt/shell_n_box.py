#!/usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import call as shell_func
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *


class Example( QWidget):

    def __init__(self):
        super(Example, self).__init__()
        self.initUI()

    def initUI(self):

        self.btn =  QPushButton('Go Do it', self)
        self.btn.move(20, 20)
        self.btn.clicked.connect(self.clickeado)

        self.lin_txt =  QLineEdit(self)
        self.lin_txt.move(130, 22)

        self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('Shell dialog')
        self.show()

    def clickeado(self):

        shell_str = str(self.lin_txt.text())
        shell_func(shell_str, shell=True)

        self.lin_txt.setText(str(""))


def main():

    app =  QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
