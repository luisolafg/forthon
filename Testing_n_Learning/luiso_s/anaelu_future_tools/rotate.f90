
module rot_inner
    implicit none

    real(kind=8),  dimension(:,:), allocatable :: img_out
    contains

    subroutine rotate_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in

        integer                     :: xmax, ymax, x, y
        real(kind=8)                :: e=0.00000000001

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        allocate(img_out(ymax, xmax))

        do x = 1, xmax
            do y = 1, ymax
                img_out(y, x) = img_in(xmax - x + 1, y)
            end do
        end do

    end subroutine rotate_img

end module rot_inner
