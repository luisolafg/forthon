dir_output = '''
['COMPRESSORS', 'FilenameObject', 'GEimage', 'HiPiCimage', 'OXDimage', '__author__', '__builtins__',
'__contact__', '__copyright__', '__date__', '__doc__', '__file__', '__license__', '__name__',
'__package__', '__path__', '__status__', '_cif', '_version', 'absolute_import', 'adscimage',
'binaryimage', 'bruker100image', 'brukerimage', 'cbfimage', 'compression', 'construct_filename',
'converters', 'deconstruct_filename', 'division', 'dm3image', 'edfimage', 'extract_filenumber',
'fabioimage', 'fabioutils', 'filename_object', 'fit2dmaskimage', 'fit2dspreadsheetimage', 'getnum',
'hdf5image', 'hexversion', 'jump_filename', 'kcdimage', 'logging', 'mar345image', 'marccdimage',
'next_filename', 'numpyimage', 'open', 'openheader', 'openimage', 'pilatusimage', 'pixiimage',
'pnmimage', 'previous_filename', 'print_function', 'raxisimage', 'readbytestream', 'tests',
'third_party', 'tifimage', 'version', 'version_info', 'xsdimage']
'''
import sys
import numpy as np
import fabio

if( __name__ == "__main__" ):
    path_to_img = sys.argv[1]
    print "path_to_img =", path_to_img

    '''
    img = fabio.open(path_to_img)
    print "type(img) =", type(img)
    '''

    new_img = fabio.edfimage.edfimage()
    print "type(new_img) =", type(new_img)

    np_img = np.zeros((70,50), "float64")
    np_img[20:30, 10:40] = 4.0
    np_img[30:40, 20:30] = 8.0
    np_img[40:60, 10:20] = 8.0
    tr_img = np.transpose(np_img)

    new_img.data = tr_img
    file_out = "output_tst.edf"
    new_img.write(file_out)
