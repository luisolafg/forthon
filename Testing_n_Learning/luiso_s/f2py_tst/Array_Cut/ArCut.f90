! for some odd reason f2py needs this code to be far to indented

      SUBROUTINE cut(img_arr_in, x1, y1, x2, y2, prof_cut)
       ! makes a cut along a line inside a numpy array

       implicit none

       ! interface vars
       INTEGER    img_arr_in(50, 50)
       INTEGER    x1, y1, x2, y2
       INTEGER*8  prof_cut(5)

       ! Local vars
       integer i

Cf2py intent(in) img_arr_in
Cf2py intent(in) x1
Cf2py intent(in) y1
Cf2py intent(in) x2
Cf2py intent(in) y2
Cf2py intent(out) prof_cut
Cf2py depend(img_arr_in) prof_cut, x1, x2, y1, y2

       write(*,*) "Hi"
       !write(*,*) "img_arr_in =", img_arr_in

       prof_cut = 10
       do i = 1,5
           prof_cut(i) = real(i) ** 2.0

       end do
       write(*, *) "img_arr_in(3, 1) =", img_arr_in(3, 1)

       write(*, *) "x1, y1 =", x1, y1
       write(*, *) "x2, y2 =", x2, y2

       prof_cut(3) = img_arr_in(3, 1) + x1 + y1
       prof_cut(4) = img_arr_in(4, 1) + x2 + y2

      END SUBROUTINE cut
