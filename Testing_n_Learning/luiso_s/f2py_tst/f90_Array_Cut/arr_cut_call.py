import arr_cut
print arr_cut.mod.__doc__

import numpy
#test_matrix = '''
ar_2d_img = numpy.zeros((12,12))
ar_2d_img[3:5,3:5] = 5
arr_cut.mod.row_ini = 9
arr_cut.mod.col_ini = 6

arr_cut.mod.row_end = 3
arr_cut.mod.col_end = 6
#arr_cut.mod.dlt_ang = 0.01
arr_cut.mod.dlt_ang = 3.14159 / 2.0
#'''

print "allocating arr_cut.mod.img_2d"
arr_cut.mod.img_2d = ar_2d_img

print "arr_cut.mod.img_2d =\n", arr_cut.mod.img_2d

print "calling arr_cut.mod.lin_cut() after allocate"
arr_cut.mod.lin_cut()

print "arr_cut.mod.img_2d_tmp =\n", arr_cut.mod.img_2d_tmp
print "arr_cut.mod.cut_1d_total =\n", arr_cut.mod.cut_1d_total

profl = arr_cut.mod.cut_1d_total
