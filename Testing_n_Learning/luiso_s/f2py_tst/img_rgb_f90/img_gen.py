import numpy as np
import rgb_img

def make_bmp_img(ar_2d_img):
    print "allocating rgb_img.mod.in_data_2d"
    rgb_img.mod.in_data_2d = ar_2d_img
    rgb_img.mod.black_n_white = False
    print "rgb_img.mod.in_data_2d =\n", rgb_img.mod.in_data_2d
    print "calling rgb_img.mod.rgb_img_gen() after allocate"
    rgb_img.mod.rgb_img_gen()

    print "rgb_img.mod.in_data_2d =\n", rgb_img.mod.in_data_2d


    rgb_bmp = rgb_img.mod.rgb_2d

    return rgb_bmp

if(__name__ == "__main__"):
    print rgb_img.mod.__doc__
    ysize = 5
    xsize = 7
    xrd_data = np.arange( ysize * xsize, dtype = 'float' ).reshape( ysize, xsize )

    ar_2d_img = xrd_data
    ar_2d_img[3,3] = 9

    tmp_img = make_bmp_img(ar_2d_img)

    print
    print "tmp_img =\n", tmp_img
