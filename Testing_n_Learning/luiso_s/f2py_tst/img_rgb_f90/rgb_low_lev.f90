module mod
    real, allocatable,    dimension(:,:)        :: in_data_2d
    logical                                     :: black_n_white = .False.
    integer, allocatable, dimension(:,:,:)      :: rgb_2d

    contains

    subroutine rgb_img_gen

        integer                                 :: xsize, ysize, row, col
        real                                    :: i_max, i_min, i_dif
        integer, allocatable, dimension(:,:)    :: scaled_data_2d


        if (allocated(in_data_2d)) then

            xsize = ubound(in_data_2d, 1)
            ysize = ubound(in_data_2d, 2)

            write(*,*) "xsize =", xsize
            write(*,*) "ysize =", ysize
            i_max = maxval(in_data_2d)
            i_min = minval(in_data_2d)
            i_dif = i_max - i_min

            allocate(rgb_2d(1:xsize, 1:ysize, 1:3))
            allocate(scaled_data_2d(1:xsize, 1:ysize))

            if( black_n_white .eqv. .False. )then
                scaled_data_2d = real(in_data_2d - i_min) * 3.0 * 255.0 / i_dif
                do col = 1, xsize
                    do row = 1, ysize
                        if( scaled_data_2d(col,row) < 255 )then
                            rgb_2d(col,row,1) = scaled_data_2d(col,row)
                            rgb_2d(col,row,2) = 0
                            rgb_2d(col,row,3) = 0
                        elseif(scaled_data_2d(col,row) >= 255 .and. &
                               scaled_data_2d(col,row) <= 255 * 2     )then
                            rgb_2d(col,row,1) = 255
                            rgb_2d(col,row,2) = scaled_data_2d(col,row) - 255
                            rgb_2d(col,row,3) = 0
                        else
                            rgb_2d(col,row,1) = 255
                            rgb_2d(col,row,2) = 255
                            rgb_2d(col,row,3) = scaled_data_2d(col,row) - 255 * 2

                        end if
                    end do
                end do
                write(*,*) "Hot pattern"
            else
                scaled_data_2d = real(in_data_2d - i_min) * 255.0 / i_dif
                rgb_2d(:,:,1) = scaled_data_2d(:,:)
                rgb_2d(:,:,2) = scaled_data_2d(:,:)
                rgb_2d(:,:,3) = scaled_data_2d(:,:)
                write(*,*) "Black n White pattern"
            end if



        else
            write(*,*) "in_data_2d is not allocated"
        end if
    end subroutine rgb_img_gen
end module mod
