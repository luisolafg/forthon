#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
OPT1="-c -O2"
echo "removing traces of previous runs"
rm read_cif.r

echo " Program Compilation -static"
gfortran $OPT1 $INC                        read_cif.f90

echo " Linking"
gfortran -o read_cif.r *.o  $LIB $LIBSTATIC

rm *.o
#rm *.mod
./read_cif.r LiFePO4.cif

