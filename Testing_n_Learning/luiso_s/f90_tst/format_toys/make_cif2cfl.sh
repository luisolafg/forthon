#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
OPT1="-c -O2"
echo "removing traces of previous runs"
rm cif2cfl.r
rm *cfl

echo "Program Compilation -static"
gfortran $OPT1 $INC                        cif2cfl.f90

echo "Linking"
gfortran -o cif2cfl.r *.o  $LIB $LIBSTATIC
rm *.o
echo "running"
echo " "

#rm *.mod
echo "tst 1"
./cif2cfl.r

./cif2cfl.r cif_in=../../../../miscellaneous/cif_dat/1001056.cif \
 cfl_out=1001056.cfl

./cif2cfl.r cif_in=../../../../miscellaneous/cif_dat/1001055.cif \
 cfl_out=1001055.cfl

./cif2cfl.r cif_in=../../../../miscellaneous/cif_dat/1001157.cif \
 cfl_out=1001157.cfl

./cif2cfl.r cif_in=../../../../miscellaneous/cif_dat/1000071.cif \
 cfl_out=1000071.cfl

