#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

LIBSTATIC=" -static"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
OPT1="-c -O2"
echo "removing traces of previous runs"
rm format_toy.r
rm *cfl

echo "Program Compilation -static"
gfortran $OPT1   example_01.f90

echo "Linking"
gfortran -o format_toy.r *.o  $LIBSTATIC
rm *.o
echo "running"
echo " "

rm *.mod
echo "tst 1: ./format_toy.r"
./format_toy.r
echo "tst 2: ./format_toy.r LiFePO4.cif"
./format_toy.r LiFePO4.cif
echo "tst 3: ./format_toy.r Pt_COD.cif aaaaPt.cfl"
./format_toy.r Pt_COD.cif aaaaPt.cfl
echo "tst 4: ./format_toy.r cif_in=LiFePO4.cif cfl_out=tst.cfl"
./format_toy.r cif_in=LiFePO4.cif cfl_out=tst.cfl
echo "tst 5: ./format_toy.r cfl_out=tst.cfl cif_in=LiFePO4.cif"
./format_toy.r cfl_out=tst.cfl cif_in=LiFePO4.cif
echo "tst 6: ./format_toy.r cfl_out=tst.cfl"
./format_toy.r cfl_out=tst.cfl
echo "tst 7: ./format_toy.r cif_in=LiFePO4.cif"
./format_toy.r cif_in=LiFePO4.cif
echo "tst 8: ./format_toy.r LiFePO4.cif cfl_out=tst.cfl"
./format_toy.r LiFePO4.cif cfl_out=tst.cfl
echo "tst 9: ./format_toy.r cfl_out=tst.cfl LiFePO4.cif"
./format_toy.r cfl_out=tst.cfl LiFePO4.cif
echo "tst 10: ./format_toy.r aaa_out=tst.cfl bbb_in=LiFePO4.cif"
./format_toy.r aaa_out=tst.cfl bbb_in=LiFePO4.cif
echo "tst 11: ./format_toy.r cif_in=../../../../miscellaneous/cif_dat/1001056.cif \
 cfl_out=1001056.cfl"
./format_toy.r cif_in=../../../../miscellaneous/cif_dat/1001056.cif \
 cfl_out=1001056.cfl

echo "tst end"

