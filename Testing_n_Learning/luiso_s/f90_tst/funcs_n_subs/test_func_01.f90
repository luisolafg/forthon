module func_deps
    contains
    Function get_cartesian(ad_coord) Result(xy_coord)
       real,  dimension(2),  intent(in)         :: ad_coord
       real,  dimension(2)                      :: xy_coord
       xy_coord(1) = ad_coord(2) * sin(ad_coord(1))
       xy_coord(2) = ad_coord(2) * cos(ad_coord(1))

       return
    End Function get_cartesian
end module func_deps

program main_caller
    use func_deps

    implicit none

    real, dimension(2)    :: xy_coord
    xy_coord = get_cartesian([(3.14159235358 / 4.0), (10.0)])
    write(*,*) "coord =", xy_coord

end program main_caller
