echo "deleting results from previous runs"
rm peak_cutter.r
rm img_w_cuts.raw

echo "compiling"
gfortran -c -Wall cuts.f90
gfortran -c -Wall cuts_caller.f90

echo "linking"
gfortran -o peak_cutter.r *.o

echo "deleting .o and .mod files"
rm *.o
rm *.mod

echo "running"
./peak_cutter.r
echo "plotting"
python plot_img.py
