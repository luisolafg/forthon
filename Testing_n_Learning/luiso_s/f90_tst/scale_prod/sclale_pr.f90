program scale_product

    use iterate_scale, only: iter_me
    implicit none

    real, dimension(5,5)                :: exp_img
    real, dimension(:,:), allocatable   :: tmp_img, calc_img
    integer                     :: i, j, k
    integer                     :: xres, yres
    real                        :: scalar

    exp_img = 3.0

    scalar = 1.00004



    xres = ubound(exp_img, 1)
    yres = ubound(exp_img, 2)


    allocate(tmp_img(xres, yres))
    allocate(calc_img(xres, yres))

    calc_img = 0.0

    write(*,*) "xres, yres =", xres, yres


    forall(i = 1:xres, j = 1:yres) exp_img(i, j) = real(i) + real(j)

    forall(i = 1:xres, j = 1:yres) calc_img(i, j) = real(i)*1.3 + real(j)*1.6


    write(*,*) "exp_img ="
    write(*,*) transpose(exp_img)
    write(*,*) "calc_img ="
    write(*,*) transpose(calc_img)

    call iter_me(calc_img, exp_img)

    write(*,*) "==============================="
    write(*,*) "exp_img ="
    write(*,*) transpose(exp_img)
    write(*,*) "calc_img ="
    write(*,*) transpose(calc_img)

end program scale_product
