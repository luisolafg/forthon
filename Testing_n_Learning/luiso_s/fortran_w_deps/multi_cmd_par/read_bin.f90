program read_bin_file

    use cli_params, only: In_Par, params_data

    integer(kind = 4), allocatable  :: dat_unflip(:,:)
    integer(kind = 4), allocatable  :: dat_fliped(:,:)
    integer(kind = 2), allocatable  :: tmp_dat(:,:)
    integer                         :: col, row, max_col, max_row
    integer                         :: narg
    character(len = 255)            :: file_in, file_out, tmp_char = ""
    Type(params_data)               :: params

    call In_Par(params)

    write(*,*) "params%niters = ", params%niters
    write(*,*) "params%xres = ", params%xres
    write(*,*) "params%yres = ", params%yres
    write(*,*) "params%file_in = ", trim(params%file_in)
    write(*,*) "params%file_out = ", trim(params%file_out)

    !
    !lun = 25
    !max_col = 2300
    !max_row = 2300
    !file_in = "./PFN_phi_0.5_to_2.5_180s_01_01_06102016.raw.bin"
    !allocate(tmp_dat(1:max_row,1:max_col))
    !allocate(dat_unflip(1:max_row,1:max_col))
    !allocate(dat_fliped(1:max_row,1:max_col))
    !
    !open(unit = lun, file = trim(file_in), action = "read", access = "stream")
    !read(unit=lun) tmp_dat
    !close(unit=lun)
    !
    !write(unit = *,fmt = "(a)") 'open => end'
    !do col = 1, max_col, 1
    !    do row = 1, max_row, 1
    !        if( tmp_dat(row, col) < 0 )then
    !            dat_unflip(row,col) = tmp_dat(row,col) + 65536
    !        else
    !            dat_unflip(row,col) = tmp_dat(row,col)
    !        end if
    !    end do
    !end do
    !
    !do col = 1, max_col, 1
    !    do row = 1, max_row, 1
    !        dat_fliped(row,col) = dat_unflip(row,max_col - col + 1)
    !    end do
    !end do
    !
    !write(unit = *, fmt="(a)") 'transform => end'
    !!now writing to a text file
    !lun = 30
    !
    !open(unit = lun, file = trim(file_out), status = "replace", action = "write")
    !!tmp_char = "original (bin) IMG = " // trim(file_in)
    !!write(lun,*) trim(tmp_char)
    !!tmp_char = "number of pixels in x direction =  " // trim(arg_x_size)
    !!write(lun,*) trim(tmp_char)
    !!tmp_char = "number of pixels in y direction =  " // trim(arg_y_size)
    !!write(lun,*) trim(tmp_char)
    !!tmp_char = "X(cols) growing faster "
    !!write(lun,*) trim(tmp_char)
    !!tmp_char = "????:"
    !!write(lun,*) trim(tmp_char)
    !!tmp_char = "txt IMG:"
    !!write(lun,*) trim(tmp_char)
    !
    !do col = 1, max_col, 1
    !    do row = 1, max_row, 1
    !        write(lun,*) dat_fliped(row,col)
    !    end do
    !end do
    !close(unit=lun)
    !write(unit=*,fmt="(a)") 'write => end'

end program read_bin_file
