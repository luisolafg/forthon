PROGRAM BIN
    IMPLICIT NONE

    INTEGER :: i, j
    real(kind = 4), allocatable     :: Inten(:,:)
    real(kind = 4), allocatable               :: Test(:,:)
    integer(kind = 4), allocatable  :: dat_conv(:,:)
    integer(kind = 2), allocatable  :: tmp_dat(:,:)

    !INTEGER :: IOstatus
    INTEGER :: log_uni
    INTEGER :: x
    integer :: max_i, max_j
    CHARACTER(len = 256) :: fname
    !CHARACTER(len = 256) :: inp_fname = '/home/lui-dev/f90lafg/f90lafg-test/2d_pat_1.raw'

    max_i = 2300
    max_j = 2300

    allocate(tmp_dat(1:max_i,1:max_j))
    allocate(dat_conv(1:max_i,1:max_j))
    allocate(Inten(1:max_i,1:max_j))
    allocate(Test(1:max_i,1:max_j))

    !!!!!!!!!!!  Copieed from running convert_to_bin.f90 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    open(unit = 10, file = "apt73.bin", action = "read", access = "stream")
    read(unit = 10) tmp_dat
    close(unit = 10)

    write(unit = *,fmt = "(a)") 'open => end'
    do j = 1, max_j, 1
        do i = 1, max_i, 1
            if( tmp_dat(i, j) < 0 )then
                dat_conv(i,j) = tmp_dat(i,j) + 65536
            else
                dat_conv(i,j) = tmp_dat(i,j)
            end if
        end do
    end do



    do j = 1, max_j, 1
        do i = 1, max_i, 1
            Inten(i,j) = dat_conv(i,j)
        end do
    end do

    ! smoothing
    Do x=1,50
        Do i=2, max_i - 1
            Do j=2, max_j - 1
                Test(i,j)=(Inten(i,j-1)+Inten(i-1,j)+Inten(i,j+1)+Inten(i+1,j))/4
            End Do
        End Do
        Do i=2, max_i - 1
            Do j=2, max_j - 1
                Inten(i,j)=Test(i,j)
            End Do
        End Do
    End Do

    WRITE (*,*) "writing file"
    log_uni = 4
    fname = 'Test.bin'

    OPEN(unit = log_uni, file = fname, status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) Inten
    close(unit = log_uni)

    write(unit=*,fmt = "(a)") " Result image in File: "//trim(fname)

END PROGRAM BIN
