public class class_n_func{

    public static int func_tst( int w ){
        int u;
        u = 2 * w;
        return u;
    }

    public static class tst_cls{
        public int x = 5;

        public int tst_v1( int w){
            System.out.println("w = " + w);
            return w + x;
        }
    }

    public static void main(String[] args){
        System.out.println("Hi");
        int b;
        tst_cls a = new tst_cls();
        b = a.tst_v1(3);
        System.out.println("b = a.tst_v1(1) = " + b);

        System.out.println("func_tst(3) = " + func_tst(3));
    }
}
