class Mum():
  '''
  example of a very simple class
  '''
  def __init__(self):
    self.pi = 3.14159
  def prn1(self):
    print "A1"

class Son(Mum):
  '''
  a class that inherits from Mum
  '''
  def prn2(self):
    print "B2"
  def prn_py(self):
    print "pi =", self.pi


if(__name__ == "__main__"):
  print "from __main__"
  m = Mum()
  m.prn1()
  s = Son()
  s.prn1()
  s.prn2()
  print "printing pi"
  s.prn_py()

  print "dir(m) =", dir(m)
  print "dir(s) =", dir(s)
  print "m.__doc__ = ", m.__doc__
  print "s.__doc__ = ", s.__doc__
