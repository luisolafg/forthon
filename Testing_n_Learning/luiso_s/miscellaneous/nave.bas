SCREEN 12
xmax = 540
'_FULLSCREEN


LINE (50, 50)-(60, 70), 4, BF
LINE (55, 40)-(50, 50), 14
LINE (55, 40)-(60, 50), 14
LINE (50, 50)-(60, 50), 14
PAINT (55, 45), 14, 14
LINE (49, 60)-(30, 70), 11
LINE (49, 70)-(30, 70), 11
LINE (49, 70)-(49, 60), 11
PAINT (45, 68), 11, 11

LINE (61, 60)-(80, 70), 11
LINE (61, 70)-(80, 70), 11
LINE (61, 70)-(61, 60), 11
PAINT (65, 68), 11, 11
DIM img_nave(1 TO 250)
GET (29, 39)-(81, 71), img_nave()
PUT (29, 39), img_nave()

stp = 5
x = 200
y = 100
PUT (x, y), img_nave()

DO
    PUT (x, y), img_nave()
    a$ = INKEY$
    ' for ease of use in future
    ' right = 0 + 77
    ' left  = 0 + 75
    ' up    = 0 + 72
    ' down  = 0 + 80
    IF a$ = CHR$(0) + CHR$(77) THEN
        'PRINT " Right "
        x = x + stp
    ELSEIF a$ = CHR$(0) + CHR$(75) THEN
        'PRINT " Left "
        x = x - stp
    ELSEIF a$ = CHR$(0) + CHR$(72) THEN
        'PRINT " Up "
        y = y - stp
    ELSEIF a$ = CHR$(0) + CHR$(80) THEN
        'PRINT " Down "
        y = y + stp
    ELSEIF a$ = CHR$(27) THEN
        END
    END IF
    IF x > xmax - 45 THEN
        x = 10
    ELSEIF x < 5 THEN
        x = xmax - 50
    END IF
    PUT (x, y), img_nave()
LOOP

