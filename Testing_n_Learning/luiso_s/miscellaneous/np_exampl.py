import numpy as np

a = np.zeros((4, 4), "float")
a[0, 0] = 1

b = np.zeros((4, 4), "float")
b[:, :] = a[:, :]

b[2, 2] = 5

c = np.copy(a)
c[2,2] = 8

d = np.zeros((4, 4), "float")
d[1:3, 1:3] = 4
print "a = \n", a
print "b = \n", b
print "c = \n", c
print "d = \n", d