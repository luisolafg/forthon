
subroutine increment(n) bind(c)
    USE iso_c_binding
    integer(c_int) , intent(in out) :: n

    write(*,*) "from Fortran ( before summation ) n =", n
    n = n + 8
    write(*,*) "from Fortran ( after summation ) n =", n

    return

end subroutine
