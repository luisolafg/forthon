#include<stdio.h>

int main(int argc, char **argv) {
    int i = 10;
    long w = 0;
    printf("from c ( before calling Fortran ) i =  %d\n",i);
    printf("from c ( before calling Fortran ) w =  %d\n",w);
    increment(&i, &w);
    printf("from c ( after calling Fortran ) i =  %d\n",i);
    printf("from c ( after calling Fortran ) w =  %d\n",w);
    return;
}
