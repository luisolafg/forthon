
subroutine increment(n, x) bind(c)
    USE iso_c_binding
    integer(c_int) , intent(in out)         :: n
    INTEGER (C_LONG), INTENT(OUT)           :: x
    write(*,*) "from Fortran ( before summation ) n =", n
    n = n + 5
    write(*,*) "from Fortran ( after summation ) n =", n
    write(*,*) "from Fortran ( before assignation ) x =", x
    x = 20
    write(*,*) "from Fortran ( after assignation ) x =", x
    return

end subroutine


