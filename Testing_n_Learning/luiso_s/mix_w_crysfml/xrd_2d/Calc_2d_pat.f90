!!----
!!---- Program: CALC_SFAC
!!----          Example of simple program using CFML
!!----
!!
Program Calc_2d_pat_toy
    !---- Use Modules ----!
    !use f2kcli  !Comment for Fortran 2003 compliant (Environment module) compilers
    use CFML_crystallographic_symmetry, only: space_group_type, Write_SpaceGroup
    use CFML_Atom_TypeDef,              only: Atom_List_Type, Write_Atom_List
    use CFML_Crystal_Metrics,           only: Crystal_Cell_Type, Write_Crystal_Cell
    use CFML_Reflections_Utilities,     only: Reflection_List_Type, Hkl_Uni, get_maxnumref
    use CFML_IO_Formats,                only: Readn_set_Xtal_Structure,err_form_mess,err_form,file_list_type
    use CFML_Structure_Factors,         only: Structure_Factors, Write_Structure_Factors, &
                                             Init_Structure_Factors,Calc_StrFactor
    !---- Variables ----!
    implicit none

    type (file_list_type)       :: fich_cfl
    type (space_group_type)     :: SpG
    type (Atom_list_Type)       :: A
    type (Crystal_Cell_Type)    :: Cell
    type (Reflection_List_Type) :: hkl

    character(len=256)          :: filcod     !Name of the input file
    character(len=15)           :: sinthlamb  !String with stlmax (2nd cmdline argument)
    real                        :: stlmax     !Maximum Sin(Theta)/Lambda
    real                        :: sn,sf2
    integer                     :: MaxNumRef, Num, lun=1, ier,i
    complex                     :: fc

    integer                     :: narg
    Logical                     :: esta, arggiven=.false.,sthlgiven=.false.

    !---- Arguments on the command line ----!
    narg=command_argument_count()

    if (narg > 0) then
        call get_command_argument(1,filcod)
        arggiven=.true.
    end if

    if (narg > 1) then
        call get_command_argument(2,sinthlamb)
        read(unit=sinthlamb,fmt=*,iostat=ier) stlmax
        if (ier == 0) sthlgiven=.true.
    end if

    write(unit=*,fmt="(/,/,6(a,/))")                                      &
        "            ------ PROGRAM 2D XRD PATTERN ------"              , &
        "                     ( LFM March 2015 )"
    write(unit=*,fmt=*) " "

    if (.not. arggiven) then
        write(unit=*,fmt="(a)", advance='no') " => Code of the file xx.cif(cfl) (give xx): "
        read(unit=*,fmt="(a)") filcod
        if(len_trim(filcod) == 0) stop
    end if

    stlmax = 90.0

    inquire(file=trim(filcod)//".cif",exist=esta)
    if (esta) then
        call Readn_set_Xtal_Structure(trim(filcod)//".cif",Cell,SpG,A,Mode="CIF")
    else
        write(*,*) "Only supported .cif format"
    end if

    if (err_form) then
        write(unit=*,fmt="(a)") trim(err_form_mess)
    else
        MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult=SpG%NumOps)
        call Hkl_Uni(Cell,Spg,.true.,0.0,stlmax,"s",MaxNumRef,hkl)
        write(unit=*,fmt="(a)") " Normal End of: PROGRAM STRUCTURE FACTORS "
    end if

End Program Calc_2d_pat_toy
