#include<boost/python.hpp>
#include <iostream>

char const* greet()
{
    return "hello, world";
}
int give_int()
{
    std::cout << "\n tst \n";
    return 5;
}


BOOST_PYTHON_MODULE(hello_ext)
{
    using namespace boost::python;
    def("greet",greet);
    def("give_int",give_int);
}
