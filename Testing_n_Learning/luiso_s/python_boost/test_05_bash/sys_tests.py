import sys
print dir(sys), "\n\n"
print sys.api_version
sys_ver = sys.version
print "sys_ver =", sys_ver
print "type(sys_ver) =", type(sys_ver)
#print "sys.path =", sys.path
print "sys.platform =", sys.platform
print '\n'.join(sys.path)
different_result_than = '''
python -c "import sys; print '\n'.join(sys.path)"
'''
import numpy
print "numpy.__path__ =", numpy.__path__

import boost
print "boost.__file__ =", boost.__file__
print "boost.__path__ =", boost.__path__

import imp
print "imp.find_module('numpy') =", imp.find_module('numpy')
print "imp.find_module('boost') =", imp.find_module('boost')

print "imp.find_module('sys') =", imp.find_module('sys')

import site
site.__file__
print "site.__file__ =", site.__file__



from distutils import sysconfig
print "sysconfig.get_python_lib() =", sysconfig.get_python_lib()
#'/home/jim/anaconda3/lib/python3.5/site-packages'
print "sysconfig.get_python_inc() =", sysconfig.get_python_inc()
#'/home/jim/anaconda3/include/python3.5m'


#print "dir(sysconfig)", dir(sysconfig), "\n"
#get_config_h_filename'get_config_h_filename'
not_needed_for_now = '''
print '\n sysconfig.get_config_var(\'BINLIBDEST\')  =', sysconfig.get_config_var('BINLIBDEST')
print '\n sysconfig.get_config_var(\'MACHDESTLIB\') =', sysconfig.get_config_var('MACHDESTLIB')
print '\n sysconfig.get_config_var(\'LIBP\')        =', sysconfig.get_config_var('LIBP')
print '\n sysconfig.get_config_var(\'LIBDEST\')     =', sysconfig.get_config_var('LIBDEST')
print '\n sysconfig.get_config_var(\'DESTLIB\')     =', sysconfig.get_config_var('DESTLIB')
print '\n sysconfig.get_makefile_filename() =', sysconfig.get_makefile_filename()
print '\n sysconfig.get_python_inc()        =', sysconfig.get_python_inc()
print '\n sysconfig.get_python_lib()        =', sysconfig.get_python_lib()
print '\n sysconfig.get_python_version()    =', sysconfig.get_python_version()
'''

bkp01 = '''
import sys
print dir(sys), "\n\n"
print sys.api_version
sys_ver = sys.version
print "sys_ver =", sys_ver
print "type(sys_ver) =", type(sys_ver)
#print "sys.path =", sys.path
print "sys.platform =", sys.platform
print '\n'.join(sys.path)

import numpy
print "numpy.__path__ =", numpy.__path__

import boost
print "boost.__file__ =", boost.__file__
print "boost.__path__ =", boost.__path__

import imp
print "imp.find_module('numpy') =", imp.find_module('numpy')
print "imp.find_module('boost') =", imp.find_module('boost')

print "imp.find_module('sys') =", imp.find_module('sys')

import site
site.__file__
print "site.__file__ =", site.__file__
'''


bkp_02 = '''
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
except KeyError:
    user_paths = []

print "user_paths -", user_paths

print "dir(os.environ) =", dir(os.environ)

print "os.environ.keys() =", os.environ.keys()
'''
