
from distutils import sysconfig

obj_name = "array_ext"
inc_path = sysconfig.get_python_inc()
print "\n\n sysconfig.get_python_inc() =", inc_path
for pos, single_shar in enumerate(inc_path):
    if(single_shar == "/" ):
        cut_inc_path = inc_path[0:pos]
        print "cut_inc_path =", cut_inc_path

com_lin_01 = "g++ -I" + inc_path + " -I" + cut_inc_path +  " -fPIC -c " + obj_name + ".cpp"
print "com_lin_01 =\n", com_lin_01

lib_path = sysconfig.get_python_lib()
print "\n sysconfig.get_python_lib() =", lib_path
for pos, single_shar in enumerate(lib_path):
    if(single_shar == "/" ):
        cut_lib_path = lib_path[0:pos]

for pos, single_shar in enumerate(cut_lib_path):
    if(single_shar == "/" ):
        cut_cut_lib_path = cut_lib_path[0:pos]

com_lin_02 = "g++ -shared " + obj_name + ".o -L" + cut_cut_lib_path + " -lboost_python -L" + \
             cut_lib_path + "/config -lpython2.7 -o " + obj_name + ".so"

print "com_lin_02 =\n", com_lin_02

expected_output = '''
g++ -I/usr/include/python2.7 -I/usr/include -fPIC -c array_ext.cpp
g++ -shared array_ext.o -L/usr/lib -lboost_python -L/usr/lib/python2.7/config -lpython2.7 -o array_ext.so
'''
print "\nexpected_output(debian) =", expected_output
