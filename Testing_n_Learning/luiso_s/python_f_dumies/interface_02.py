def get_par(par_def, lst_in):
    print "par_def:", par_def
    print "lst_in:", lst_in

    print "\n dir(par_def): \n", dir(par_def), "\n"
    for tst_items in par_def.iteritems():
        print "item:", tst_items

    for tst_key in par_def.iterkeys():
        print "key:", tst_key

    for val in par_def.itervalues():
        print "val:", val

    for key02 in par_def.keys():
        print "key02:", key02

    par_out = par_def

    return par_out

if(__name__ == "__main__"):
    par_def_01 ={"cfl_in"    : "my_cfl.cfl"   ,
                 "dat_in"    : "my_dat.dat"   ,
                 "dummy_var" : 5.2            ,
                 "file_out"  : "my_edf.edf"   }

    lst_in_01 = ["file1.cfl","file2.dat"]
    lst_in_02 = ["cfl_in=file1.cfl","dat_in=file2.dat", "file_out=tst"]
    lst_in_03 = ["file1.cfl","file2.dat", "tst"]
    lst_in_04 = ["file1.cfl","x=file2.dat", "tst"]
    lst_in_05 = ["x=file1.cfl=y","z=file2.dat=w", "1=tst=2"]
    lst_in_06 = ["cfl_in=file1.cfl","dat_in=file2.dat", "file_out=tst", "dummy_var=3.4"]

    par_lst_out = get_par(par_def_01, lst_in_06)

    print "par_out =", par_lst_out
    print "par_def_01(overwrite) =", par_def_01
