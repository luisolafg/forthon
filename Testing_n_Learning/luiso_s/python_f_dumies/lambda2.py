f = lambda x: x + 2.0
g = lambda y: f(y) / 2.0

does_the_same = '''
def f(x):
    return x + 2

def g(y):
    return f(y) / 2.0
'''

if __name__ == "__main__":
    print g(6.0)

