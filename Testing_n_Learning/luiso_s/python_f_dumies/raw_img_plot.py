# Matrix do some stuff
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import numpy as np
from matplotlib import pyplot as plt
import math

def read_bin_file(path_to_img = "my_bin_img.bin"):

    '''function to read de image in .bin format'''
    print "reading ", path_to_img, " file"

    data_in = np.fromfile(path_to_img, dtype=np.float64)
    lng_fl = len(data_in)
    cuad_lng = int(math.sqrt(len(data_in)))

    xres = cuad_lng
    yres = cuad_lng
    print "xres, yres =", xres, yres
    read_data = data_in.reshape((xres, yres))

    old_way = '''
    read_data = data_in.reshape((xres, yres), order="FORTRAN")
    if(fortran_dat == True):
        read_data = np.transpose(read_data)
    '''
    print "Done reading 2D data"
    return read_data

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):

    img_arr = read_bin_file("img.raw")
    plott_img(img_arr)

    tmp_off = '''
    with open("img.raw", "w") as my_file:
        my_file.write(img_arr)

    if my_file.closed == False:
        my_file.close()
    '''
