
/*

List<int> fixedLengthList = new List(5);
fixedLengthList.length = 0;  // Error
fixedLengthList.add(499);    // Error
fixedLengthList[0] = 87;
List<int> growableList = [1, 2];
growableList.length = 0;
growableList.add(499);
growableList[0] = 87;

*/
main(){
    //var my_lst = [];

    List<int> my_lst = new List(5);

    for(int i = 0; i < 5; i++){
    //my_lst.add(0);
        my_lst[i] = i * 3;
        print("i =${i}");
        print("my_lst[${i}] =");
        print(my_lst[i]);
    }
}
