//-*-java-*-
/*
emacs example with dart language use

the first line of this file is to tell
emacs that this file should be highlighted
with java syntax style

to open menu press [ M-x ]

then type "menu-bar-open"

[ M- ] = press "Alt" or "Meta" key
[ x ] = "x" letter key
 */

import 'dart:math';
class cuad_func{
    float a, b, c;
    float x1 = Null, x2 = Null;

    cuad_func(this.a, this.b, this.c){
    print("entered:\n $a, $b, $c");

    }
    solve(){
        //print("a, b, c: $a $b $c");
        float D = Null;
        D = b*b - 4 * a * c;
        //D = pow(b,2.0) - 4.0 * a * c;
        if( D < 0.0 ){
            print("D < 0");
        } else if ( D == 0 ) {
            print("D = 0");
            x1 = ( -b )/( 2 * a );
        } else if( D > 0 ){
            print("D > 0");
            x1 = (-b + sqrt(D) ) / ( 2 * a);
            x2 = (-b - sqrt(D) ) / ( 2 * a);
        } else {
            print("something is wrong here");
        }
    }
    print_res(){
        if( this.x1 == Null && this.x2 == Null ){
                print("No solution found");
        }
        /*
        next line has different use of "this" just
        to show an example that behaves the same way
        */
        else if(x1 != Null && x2 == Null){
            print("one solution found");
            print("x = ${x1}");
        }else if(this.x1 != Null && this.x2 != Null){
            print("two solutions");
            print("x1, x2 = ${x1} ,  ${x2}");
        }else{
            print("unexpected vejaviour");
        }
    }

}

void main() {
    var fnc = new cuad_func(1,3,1);
    fnc.solve();
    fnc.print_res();
}
