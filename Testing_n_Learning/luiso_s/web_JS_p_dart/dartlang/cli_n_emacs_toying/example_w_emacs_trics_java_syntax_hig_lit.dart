//-*-java-*-
/*
emacs example with dart language use

the first line of this file is to tell
emacs that this file hould be highlighted
with c++ syntax style

to open menu press [ M-x ]

then type "menu-bar-open"

[ M- ] = press "Alt" or "Meta" key
[ x ] = "x" letter key

 */


import 'dart:html';

InputElement toDoInput;
UListElement toDoList;

void main() {
  toDoInput = querySelector("#to-do-input");
  toDoList = querySelector("#to-do-list");
  toDoInput.onChange.listen(addToDoItem);
}

void addToDoItem(Event e) {
  var newToDo = new LIElement();
  newToDo.text = toDoInput.value;
  toDoInput.value = '';
  toDoList.children.add(newToDo);
}
