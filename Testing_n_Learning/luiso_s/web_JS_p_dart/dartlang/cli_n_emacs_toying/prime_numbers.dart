/*
 *
 * Prime numbers generator toy in DART
 *
*/

import 'dart:math';
void main() {
    boolean found;
    List<int> p_nums = [];

    for(int i = 1; i < 500; i++){
        found = false;
        for(int j = 2; j < i; j++){
            for(int k = j; k < i; k++){
                if( j * k == i ){
                    found = true;
                }
            }
        }
        if( found == false ){
            p_nums.add(i);
        }
    }
    print("list(prime nums) = \n$p_nums");
}
