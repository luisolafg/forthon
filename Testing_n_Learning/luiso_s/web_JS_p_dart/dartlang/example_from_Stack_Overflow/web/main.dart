library main;

import 'dart:async';
import 'dart:convert';
import 'dart:html';

class WebsocketService {

  WebSocket webSocket;

  WebsocketService() {
    connect();
  }

  void connect() {
    webSocket = new WebSocket('ws://127.0.0.1:9223/ws');
    webSocket.onOpen.first.then((_) {
      onConnected();
      sendws("Hello websocket server");
      webSocket.onClose.first.then((_) {
        print("Connection disconnected to ${webSocket.url}");
        onDisconnected();
      });
    });
    webSocket.onError.first.then((_) {
      print("Failed to connect to ${webSocket.url}. "
            "Please run bin/server.dart and try again.");
      onDisconnected();
    });
  }

  void onConnected() {
    webSocket.onMessage.listen((e) {
      onMessage(e.data);
    });
  }

  void onDisconnected() {
    print("Disconnected, trying again in 3s");
    new Timer(new Duration(seconds:3), (){
      connect();
    });
  }

  void onMessage(data) {
    var json = JSON.decode(data);
    var echoFromServer = json['response'];
    print("Received message: $echoFromServer");
    var output=querySelector('#output');
    output.text="Received message: $echoFromServer";

    new Timer(new Duration(seconds:3), (){ //Send a new message to server after 3s
      String now = new DateTime.now().toString();
      sendws("Time: $now");
    });

  }

  void sendws(String msg){
    var request = '{"echo": "$msg"}';
    print("Send message to server: $request");
    webSocket.send(request);
  }
}

void main() {
  WebsocketService ws=new WebsocketService();
}
