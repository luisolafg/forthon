// Copyright (c) 2016, Luisolafg

import 'dart:html';

InputElement input_txt;
UListElement lst_prn_txt;

void main_add_text(Event e) {
  var txt_prn = new LIElement();
  txt_prn.text = input_txt.value;
  input_txt.value = '';
  lst_prn_txt.children.add(txt_prn);
}
void func_text(Event e) {
  var txt_prn = new InputElement();
  //txt_prn.text = '';
  lst_prn_txt.children.add(txt_prn);
}
/*
void func_text(Event e) {
  var txt_prn = new LIElement();
  txt_prn.text = '';
  lst_prn_txt.children.add(txt_prn);
}*/
void main() {
  input_txt = querySelector("#to_run-input");
  lst_prn_txt = querySelector("#to_run-list");

  var new_input = new InputElement();
  lst_prn_txt.children.add(new_input);

  input_txt.onChange.listen(main_add_text);
  new_input.onChange.listen(func_text);
}
