// Copyright (c) 2016, Luisolafg

import 'dart:async';
import 'dart:convert';
import 'dart:html';


import 'dart:html';
UListElement lst_prn_txt;
var rad_btt_01 = new RadioButtonInputElement();
var rad_btt_02 = new RadioButtonInputElement();
void func_text_01(Event e) {
  var txt_prn = new InputElement();
  lst_prn_txt.children.add(txt_prn);

  if (rad_btt_01.checked == true) {
    rad_btt_01.checked = false;
  }
}
void func_text_02(Event e) {
  var txt_prn = new InputElement();
  lst_prn_txt.children.add(txt_prn);
  if (rad_btt_02.checked == true)  {
    rad_btt_02.checked = false;
  }
}
void main() {
  lst_prn_txt = querySelector("#to_run-list");
  var new_input = new InputElement();
  lst_prn_txt.children.add(new_input);
  lst_prn_txt.children.add(rad_btt_01);
  lst_prn_txt.children.add(rad_btt_02);

  var num_imp = new NumberInputElement();
  lst_prn_txt.children.add(num_imp);

  rad_btt_01.onChange.listen(func_text_02);
  rad_btt_02.onChange.listen(func_text_01);

}
