// Copyright (c) 2016, Luisolafg
import 'dart:html';
UListElement lst_prn_txt;
var check_btt_01 = new CheckboxInputElement();
var check_btt_02 = new CheckboxInputElement();

void func_text(Event e) {
  var new_lst_elem = new LIElement();
  var cad_chr = "";
  if( check_btt_01.checked == true ){
    cad_chr = cad_chr + " #1 checked";
  }
  if( check_btt_02.checked == true ){
    cad_chr = cad_chr + " #2 checked";
  }
  if( check_btt_01.checked == false && check_btt_02.checked == false ){
    cad_chr = "None checked";
  }
  new_lst_elem.text = cad_chr;
  lst_prn_txt.append(new_lst_elem);
  //lst_prn_txt.children.add(new_lst_elem);

}
void main() {
  lst_prn_txt = querySelector("#to_run-list");
  var new_input = new InputElement();
  lst_prn_txt.append(new_input);

  lst_prn_txt.append(check_btt_01);
  lst_prn_txt.append(check_btt_02);

  var num_imp = new NumberInputElement();
  lst_prn_txt.append(num_imp);

  check_btt_01.onChange.listen(func_text);
  check_btt_02.onChange.listen(func_text);
}
