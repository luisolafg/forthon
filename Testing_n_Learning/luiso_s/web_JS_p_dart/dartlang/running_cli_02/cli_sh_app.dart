import 'dart:io';
List<String> coms_lst(cmd_in) {
    int num_spaces;
    List<String> lst_com = cmd_in.split(' ');
    num_spaces = lst_com.length;
    String com1 = lst_com[0];

    if( num_spaces > 1 ){
        print("more than one command");
        lst_com.removeAt(0);
    }else{
        lst_com = [];
    }
    List split_list = [];
    split_list.add(com1);
    split_list.add(lst_com);
    return split_list;
}

void main() {
    //String inp_cmd = "ls -al ../";

    stdout.writeln('command:');
    String inp_cmd = stdin.readLineSync();
    //stdout.writeln('You typed: $inp_cmd');

    List command_list = [];
    command_list = coms_lst(inp_cmd);

    print("\n commands and params = $command_list");
    print(" running($inp_cmd):\n");
    Process.run(command_list[0], command_list[1]).then(
    (ProcessResult results){ print(results.stdout); });
}
