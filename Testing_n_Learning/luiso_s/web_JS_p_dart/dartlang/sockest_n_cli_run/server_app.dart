import 'dart:io';
import 'dart:convert';

void main() {
  HttpServer.bind(InternetAddress.ANY_IP_V4, 8080).then((HttpServer server) {
    print("HttpServer listening...");
    server.serverHeader = "DartEcho (1.0) by James Slocum";
    server.listen((HttpRequest request) {
      if (WebSocketTransformer.isUpgradeRequest(request)){
        WebSocketTransformer.upgrade(request).then(handleWebSocket);
      }
      else {
        print("Regular ${request.method} request for: ${request.uri.path}");
        serveRequest(request);
      }
    });
  });
}

void handleWebSocket(WebSocket socket){

    print('Client connected!');
    socket.listen((String char_str) {

        int num_spaces;
        List<String> lst_com = char_str.split(' ');
        num_spaces = lst_com.length;
        String com1 = lst_com[0];

        if( num_spaces >= 1 ){
            print("more than one command");
            lst_com.removeAt(0);
        }else{
            lst_com = [];
        }
        print("\n command num 1 = $com1");
        print(" rest of the commands = $lst_com");
        socket.add("user typed >> $char_str");
        print("running($char_str):\n");

        Process.run(com1, lst_com).then((ProcessResult results) {
            print(results.stdout);
        });
    },
  onDone: () {
    print('Client disconnected');
  });
}

void serveRequest(HttpRequest request){
  request.response.statusCode = HttpStatus.FORBIDDEN;
  request.response.reasonPhrase = "WebSocket connections only";
  request.response.close();
}
