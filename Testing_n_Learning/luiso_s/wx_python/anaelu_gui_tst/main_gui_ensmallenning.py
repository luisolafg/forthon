#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import os
import math
import wx.lib.scrolledpanel as scroll_pan
import numpy as np

from subprocess import call as shell_func
#from read_2d_numpy import read_bin_file, read_file_w_fabio
#from np_nditer_RGB.looping import np_to_bmp
#from sample_n_instrument_in_n_out import DataIntroPanel


class ScrolledImg(scroll_pan.ScrolledPanel):


    '''all the functions to manipulate both xrd images (experimental and calculated)'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ScrolledImg, self).__init__(parent)

        ''' to recognize the pointer potition on the images'''
        self.Prnt_Wg = parent
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.scroll_rot = 0
        self.Bdwn = False
        self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y = 0, 0
        self.Prnt_Wg.img_scale = 0.06
        self.SetupScrolling()

    def intro_img_path(self, path_to_img = None, use_fabio = False):
        '''reading the image from the .bin file and getting the dimensions'''

        if(path_to_img != None):
            if( use_fabio == True ):
                self.data2d = read_file_w_fabio(path_to_img)

            else:
                #self.data2d = read_bin_file(path_to_img, fortran_dat = True)
                self.data2d = read_bin_file(path_to_img)

            data_shape = np.shape(self.data2d)

            self.Prnt_Wg.data_panel.param_panel.input_x_res.SetValue(str(data_shape[0]))
            self.Prnt_Wg.data_panel.param_panel.input_y_res.SetValue(str(data_shape[1]))

            print "2D data shape =", data_shape

            tmp_img = np_to_bmp()
            self.img = tmp_img.img_2d_rgb(self.data2d)
            self.i_width = self.img.GetWidth()
            self.i_height = self.img.GetHeight()

        else:
            self.img = None

        self.DestroyChildren()
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.set_img_n_bmp()
        self.SetupScrolling()

    def set_img_n_bmp(self):

        '''setting new dimensions of the image to control de zoom-unzoom'''

        if( self.img != None ):
            my_img = self.img.Scale(self.i_width * self.Prnt_Wg.img_scale,
                                    self.i_height * self.Prnt_Wg.img_scale,
                                    wx.IMAGE_QUALITY_NORMAL)
            my_bitmap = wx.BitmapFromImage(my_img)
            my_img.Destroy()
            self.my_st_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
            my_bitmap.Destroy()

            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
            self.my_bindings()

        else:
            load_img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
            self.Bind(wx.EVT_BUTTON, self.onLoadImgBtn, load_img_btn)
            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(load_img_btn, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)

        self.SetSizer(self.img_vert_sizer)
        self.Scrolling_to()


    def onLoadImgBtn(self, event):

        '''open dialog to open the experimental image'''
        #dlg = wx.FileDialog(self, "Choose an image file", os.getcwd(), "", "*.bin", wx.OPEN)
        dlg = wx.FileDialog(self, "Choose an image file", os.getcwd(),
                            "", "*.mar2300;*.tif;*.edf", wx.OPEN)

        if dlg.ShowModal() == wx.ID_OK:
                 self.exp_img_path = dlg.GetPath()
        dlg.Destroy()

        self.intro_img_path(path_to_img = self.exp_img_path, use_fabio = True)


    def my_bindings(self):

        '''binding the mouse functions'''

        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def Scrolling_to(self):

        """setting the scrolling recognition to do the zoom-unzoom"""
        VwStart = self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y
        ClSize = self.GetClientSize()
        Vsize = self.GetVirtualSize()
        x_pos_start, y_pos_start = (VwStart[0] + ClSize[0], VwStart[1] + ClSize[1])

        if(self.Prnt_Wg.scroll_pos_x < 0):
            self.Prnt_Wg.scroll_pos_x = 0

        if(self.Prnt_Wg.scroll_pos_y < 0):
            self.Prnt_Wg.scroll_pos_y = 0

        if(x_pos_start > Vsize[0]):
            x_pos_start = Vsize[0]
            self.Prnt_Wg.scroll_pos_x = x_pos_start - ClSize[0]

        if(y_pos_start > Vsize[1]):
            y_pos_start = Vsize[1]
            self.Prnt_Wg.scroll_pos_y = y_pos_start - ClSize[1]

        self.SetScrollRate(1, 1)
        self.Scroll(int(self.Prnt_Wg.scroll_pos_x), int(self.Prnt_Wg.scroll_pos_y))
        self.Layout()
        self.Prnt_Wg.Layout()

    def OnLeftButDown(self, event):

        self.Bdwn = True
        self.old_Mouse_Pos_x, self.old_Mouse_Pos_y = event.GetPosition()

    def OnLeftButUp(self, event):

        self.Bdwn = False

    def OnMouseMotion(self, event):

        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()

        virt_x = self.Mouse_Pos_x + self.Prnt_Wg.scroll_pos_x
        virt_y = self.Mouse_Pos_y + self.Prnt_Wg.scroll_pos_y

        #FIXME the magic 5.0 used in next two lines should be calculated properly
        x_pix = int((float(virt_x) / self.Prnt_Wg.img_scale) / 5.0)
        y_pix = int((float(virt_y) / self.Prnt_Wg.img_scale) / 5.0)

        label_str = "Pixel Intensity (" + str(x_pix)  + ", " + str(y_pix) +") = " \
                     + str(self.data2d[y_pix, x_pix])

        #self.Prnt_Wg.label_data_info.SetLabelText(label_str)

        failed_test = '''
        self.my_st_bmp.DevCont = wx.PaintDC(self)
        #self.my_st_bmp.DevCont.Clear()
        self.my_st_bmp.DevCont.SetPen(wx.Pen("BLUE", 4))
        self.my_st_bmp.DevCont.DrawLine(0, 0, self.Mouse_Pos_x, self.Mouse_Pos_y)
        '''


        self.my_st_bmp.Layout()




    def OnMouseWheel(self, event):

        '''function to rescale the image from the wheel move'''

        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        v_size_x, v_size_y = self.GetVirtualSize()
        self.x_scroll_uni = float( self.Prnt_Wg.scroll_pos_x ) / float(v_size_x)
        self.y_scroll_uni = float( self.Prnt_Wg.scroll_pos_y ) / float(v_size_y)

    def OnIdle(self, event):

        '''math to do re rescaling of the image'''
        if( self.scroll_rot != 0 ):
            new_scale = self.Prnt_Wg.img_scale * ( 1.0 + self.scroll_rot * 0.01 )
            if( new_scale > 0.3 ):
                new_scale = 0.3
            elif( new_scale < 0.033333 ):
                new_scale = 0.033333

            if( new_scale != self.Prnt_Wg.img_scale ):
                self.Prnt_Wg.img_scale = new_scale
                self.Prnt_Wg.update_scale()

                v_size_x, v_size_y = self.GetVirtualSize()
                self.Prnt_Wg.scroll_pos_x = float(self.x_scroll_uni * v_size_x)
                self.Prnt_Wg.scroll_pos_y = float(self.y_scroll_uni * v_size_y)
                self.Prnt_Wg.update_scroll_pos()

            self.scroll_rot = 0

        elif( (self.old_Mouse_Pos_x != -self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != -self.Mouse_Pos_y )and self.Bdwn == True):

            dx = self.Mouse_Pos_x - self.old_Mouse_Pos_x
            dy = self.Mouse_Pos_y - self.old_Mouse_Pos_y
            self.Prnt_Wg.scroll_pos_x = self.Prnt_Wg.scroll_pos_x - dx
            self.Prnt_Wg.scroll_pos_y = self.Prnt_Wg.scroll_pos_y - dy

            self.Prnt_Wg.update_scroll_pos()
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y

        else:
            self.scroll_rot = 0
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y
            self.Prnt_Wg.update_scroll_pos()


class MainFrame(wx.Frame):


    '''the main frame of the entire gui, with all the menus and panels'''
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "ANAELU 2.0", size = (1200,600))

        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        edit_menu = wx.Menu()
        help_menu = wx.Menu()
        run_menu  = wx.Menu()

        file_menu.Append(100, '&Open .cfl\tCtrl+O', 'Open the .cfl file')
        file_menu.Append(101, '&Open .dat\tCtrl+P', 'Open the .dat file')
        file_menu.Append(102, '&Save .cfl\tCtrl+S', 'Save the .cfl file')
        file_menu.Append(103, '&Save .dat\tCtrl+W', 'Save the .dat file')
        help_menu.Append(104, '&FAQ\tCtrl+F', 'Frequently Asked Questions')
        help_menu.Append(104, '&About\tCtrl+A', 'Credits and Licenses')
        file_menu.AppendSeparator()
        quit = wx.MenuItem(file_menu, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        #quit.SetBitmap(wx.Image('stock_exit.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file_menu.AppendItem(quit)
        run_menu.Append(106, '&Run\tCtrl+R', 'Save and Run The Calculating XRD Image')

        edit_menu.Append(201, 'main item1', kind = wx.ITEM_CHECK)
        edit_menu.Append(202, 'main item2', kind = wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        edit_menu.AppendMenu(203, 'submenu', submenu)

        menubar.Append(file_menu, '&File')
        menubar.Append(edit_menu, '&Edit')
        menubar.Append(help_menu, '&Help')
        menubar.Append(run_menu, '&Run')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()


        self.Bind(wx.EVT_MENU, self.OnOpencfl, id=100)
        self.Bind(wx.EVT_MENU, self.OnOpendat, id=101)
        self.Bind(wx.EVT_MENU, self.OnSavecfl, id=102)
        self.Bind(wx.EVT_MENU, self.OnSavedat, id=103)
        self.Bind(wx.EVT_MENU, self.OnAboutBox, id=104)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        self.Bind(wx.EVT_MENU, self.OnRun, id=106)


        main_panel_box = wx.BoxSizer(wx.HORIZONTAL)
        self.scrolled_panel_lst = []

        for i in xrange(4):
            self.scrolled_panel_lst.append(ScrolledImg(self))

        for pnl in self.scrolled_panel_lst:
            pnl.intro_img_path(None)

        #self.data_panel = DataIntroPanel(self)


        #main_panel_box.Add(self.data_panel, 9, wx.EXPAND)

        img_right_panel_box = wx.BoxSizer(wx.VERTICAL)
        muliple_img_box = wx.BoxSizer(wx.HORIZONTAL)

        for singlepanel in  self.scrolled_panel_lst:
            muliple_img_box.Add(singlepanel, 20, wx.EXPAND)

        tmp_diasbled = '''
        img_buts = wx.BoxSizer(wx.HORIZONTAL)

        btn_cut = wx.Button(self, wx.ID_ANY, 'Cut mode')
        self.Bind(wx.EVT_BUTTON, self.on_btn_cut, btn_cut)
        img_buts.Add(btn_cut, flag = wx.TOP)

        btn_drag = wx.Button(self, wx.ID_ANY, 'Drag IMG mode')
        self.Bind(wx.EVT_BUTTON, self.on_btn_drag, btn_drag)
        img_buts.Add(btn_drag, flag = wx.TOP)

        img_right_panel_box.Add(img_buts, 2, wx.EXPAND)
        '''

        img_right_panel_box.Add(muliple_img_box, 20, wx.EXPAND)

        self.label_data_info = wx.StaticText(self, wx.ID_ANY, ' No Data to display yet  ')
        img_right_panel_box.Add(self.label_data_info)
        #self.label_data_info.SetLabelText("Mouse pointer pos info goes here")

        main_panel_box.Add(img_right_panel_box, 20, wx.EXPAND)

        self.SetSizer(main_panel_box)

    def on_btn_cut(self, event):
        print "on_btn_cut"

    def on_btn_drag(self, event):
        print "on_btn_drag"

    def OnOpencfl(self, event):
        self.data_panel.onOpenCfl(event)


    def OnOpendat(self, event):
        self.data_panel.onOpenDat(event)


    def OnSavecfl(self, event):
        self.data_panel.onRunCfl(event)


    def OnSavedat(self, event):
        self.data_panel.onRunDat(event)


    def OnAboutBox(self, e):

        description = """ANAELU is a computer programa for the treatment of 2D-XRD patterns of textured samples
"""

        licence = """

---------------------------------------------------
ANAELU ()
---------------------------------------------------
 The Anaelu project is subject to the terms of the

          Mozilla Public License, v. 2.0.

 If a copy of the MPL was not distributed with this program,
 You can obtain one at http://mozilla.org/MPL/2.0/.


 Copyright (C) 2013-2016  CIMAV / DLS


 Authors: Luis Fuentes Montero (DLS)
          Eduardo Villalobos Portillo (CIMAV)
          Diana C. Burciaga (CIMAV)
          Luis E. Fuentes Cobas (CIMAV)

 Contributors: Juan Rodrgiguez Carvajal (ILL)
               Jose Alfredo (S. Queretaro)
               Javier (ULL)

"""


        info = wx.AboutDialogInfo()

        #info.SetIcon(wx.Icon('Anaelu.png', wx.BITMAP_TYPE_PNG))
        info.SetName('ANAELU')
        info.SetVersion('2.0')
        info.SetDescription(description)
        info.SetCopyright('(C) 2010 - 2014 Luis Fuentes Montero')
        info.SetWebSite('https://sourceforge.net/projects/f90lafg/')
        info.SetLicence(licence)
        info.AddDeveloper('The Anaelu group \n Luis Fuentes-Montero \n Eduardo Villalobos-Portillo \n Diana C. Burciaga')
        info.AddDocWriter('Luis Fuentes Montero and Eduardo Villalobos')
        info.AddArtist('Eduardo Villalobos')

        wx.AboutBox(info)



    def OnQuit(self, event):
        self.Close()


    def OnRun(self, event):

        from time import time
        t1 = time()

        print "Cleaning old data"
        shell_func("rm param.dat cfl_out.cfl 2d_pat_1.raw", shell=True)

        self.data_panel.onRunCfl(event)
        self.data_panel.onRunDat(event)

        print "Running"
        shell_func("anaelu_gfortran.r param.dat cfl_out.cfl", shell=True)

        print "Done running Anaelu\n\nUpdating Img"

        self.scrolled_panel_lst[0].intro_img_path("2d_pat_1.raw")
        t2 = time()

        tdif = t2 - t1

        print "timedif =", tdif


    def update_scroll_pos(self):
        for pnl in self.scrolled_panel_lst:
            pnl.Scrolling_to()


    def update_scale(self):
        for pnl in self.scrolled_panel_lst:
            pnl.set_img_n_bmp()


if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    frame = MainFrame()
    frame.Show()
    wxapp.MainLoop()
