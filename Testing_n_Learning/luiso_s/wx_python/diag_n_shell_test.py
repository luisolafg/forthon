from subprocess import call as shell_func

import wx
class ExamplePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.button =wx.Button(self, label="Go, do it", pos=(200, 125))
        self.shell_txt = wx.TextCtrl(self, value="", pos=(150, 60), size=(140,-1))
        self.button.Bind(wx.EVT_BUTTON, self.OnButton)

    def OnButton(self, event):
        print "Clicked"
        shell_str = self.shell_txt.GetValue()
        shell_func(shell_str, shell=True)


app = wx.App(False)
frame = wx.Frame(None)
panel = ExamplePanel(frame)
frame.Show()
app.MainLoop()


