#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import os
import math
import numpy as np
import wx.lib.scrolledpanel as scroll_pan

class ScrolledImg(scroll_pan.ScrolledPanel):
    '''all the functions to manipulate both xrd images (experimental and calculated)'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ScrolledImg, self).__init__(parent)
        self.Prnt_Wg = parent
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.scroll_rot = 0
        self.Bdwn = False
        self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y = 0, 0
        self.Prnt_Wg.img_scale = 0.06
        self.SetupScrolling()

    def intro_img_path(self, path_to_img = None, use_fabio = False):
        self.img = None
        self.DestroyChildren()
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.set_img_n_bmp()
        self.SetupScrolling()

    def set_img_n_bmp(self):
        if( self.img != None ):
            my_img = self.img.Scale(self.i_width * self.Prnt_Wg.img_scale,
                                    self.i_height * self.Prnt_Wg.img_scale,
                                    wx.IMAGE_QUALITY_NORMAL)
            my_bitmap = wx.BitmapFromImage(my_img)
            my_img.Destroy()
            self.my_st_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
            my_bitmap.Destroy()

            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
            self.my_bindings()

        else:
            load_img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
            self.Bind(wx.EVT_BUTTON, self.onLoadImgBtn, load_img_btn)
            self.img_vert_sizer.Clear(True)
            self.img_vert_sizer.Add(load_img_btn, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)

        self.SetSizer(self.img_vert_sizer)
        self.Scrolling_to()


    def onLoadImgBtn(self, event):

        self.img = wx.Image('../../../../miscellaneous/lena.jpeg', wx.BITMAP_TYPE_JPEG)
        self.i_width = self.img.GetWidth()
        self.i_height = self.img.GetHeight()
        self.set_img_n_bmp()

    def my_bindings(self):
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def Scrolling_to(self):
        VwStart = self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y
        ClSize = self.GetClientSize()
        Vsize = self.GetVirtualSize()
        x_pos_start, y_pos_start = (VwStart[0] + ClSize[0], VwStart[1] + ClSize[1])

        if(self.Prnt_Wg.scroll_pos_x < 0):
            self.Prnt_Wg.scroll_pos_x = 0

        if(self.Prnt_Wg.scroll_pos_y < 0):
            self.Prnt_Wg.scroll_pos_y = 0

        if(x_pos_start > Vsize[0]):
            x_pos_start = Vsize[0]
            self.Prnt_Wg.scroll_pos_x = x_pos_start - ClSize[0]

        if(y_pos_start > Vsize[1]):
            y_pos_start = Vsize[1]
            self.Prnt_Wg.scroll_pos_y = y_pos_start - ClSize[1]

        self.SetScrollRate(1, 1)
        self.Scroll(int(self.Prnt_Wg.scroll_pos_x), int(self.Prnt_Wg.scroll_pos_y))
        self.Layout()
        self.Prnt_Wg.Layout()

    def OnLeftButDown(self, event):
        self.Bdwn = True
        virt_x, virt_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        self.old_Mouse_Pos_x = virt_x - self.Prnt_Wg.scroll_pos_x
        self.old_Mouse_Pos_y = virt_y - self.Prnt_Wg.scroll_pos_y

    def OnLeftButUp(self, event):
        self.Bdwn = False

    def OnMouseMotion(self, event):
        virt_x, virt_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        self.Mouse_Pos_x = virt_x - self.Prnt_Wg.scroll_pos_x
        self.Mouse_Pos_y = virt_y - self.Prnt_Wg.scroll_pos_y

        #FIXME the magic 5.0 used in next two lines should be calculated properly
        x_pix = int((float(virt_x) / self.Prnt_Wg.img_scale) / 5.0)
        y_pix = int((float(virt_y) / self.Prnt_Wg.img_scale) / 5.0)

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        v_size_x, v_size_y = self.GetVirtualSize()
        self.x_scroll_uni = float( self.Prnt_Wg.scroll_pos_x ) / float(v_size_x)
        self.y_scroll_uni = float( self.Prnt_Wg.scroll_pos_y ) / float(v_size_y)

    def OnIdle(self, event):
        if( self.scroll_rot != 0 ):
            new_scale = self.Prnt_Wg.img_scale * ( 1.0 + self.scroll_rot * 0.01 )
            if( new_scale > 30 ):
                new_scale = 30

            elif( new_scale < 0.33333 ):
                new_scale = 0.33333

            if( new_scale != self.Prnt_Wg.img_scale ):
                self.Prnt_Wg.img_scale = new_scale
                self.Prnt_Wg.update_scale()

                v_size_x, v_size_y = self.GetVirtualSize()
                self.Prnt_Wg.scroll_pos_x = float(self.x_scroll_uni * v_size_x)
                self.Prnt_Wg.scroll_pos_y = float(self.y_scroll_uni * v_size_y)
                self.Prnt_Wg.update_scroll_pos()

            self.scroll_rot = 0

        elif( (self.old_Mouse_Pos_x != -self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != -self.Mouse_Pos_y )and self.Bdwn == True):

            dx = self.Mouse_Pos_x - self.old_Mouse_Pos_x
            dy = self.Mouse_Pos_y - self.old_Mouse_Pos_y
            self.Prnt_Wg.scroll_pos_x = self.Prnt_Wg.scroll_pos_x - dx
            self.Prnt_Wg.scroll_pos_y = self.Prnt_Wg.scroll_pos_y - dy

            self.Prnt_Wg.update_scroll_pos()
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y


class MultiImagePanel(wx.Panel):

    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(MultiImagePanel, self).__init__(parent)
        self.parent_widget = parent

        main_panel_box = wx.BoxSizer(wx.VERTICAL)
        self.scrolled_panel_lst = []

        num_of_panels = 2

        mid_panel_num = num_of_panels / 2
        for i in xrange(num_of_panels):
            self.scrolled_panel_lst.append(ScrolledImg(self))

        for pnl in self.scrolled_panel_lst:
            pnl.intro_img_path(None)

        img_right_panel_box = wx.BoxSizer(wx.HORIZONTAL)

        top_muliple_img_box = wx.BoxSizer(wx.VERTICAL)
        for singlepanel in  self.scrolled_panel_lst[0:mid_panel_num]:
            top_muliple_img_box.Add(singlepanel, 20, wx.EXPAND)

        bot_muliple_img_box = wx.BoxSizer(wx.VERTICAL)
        for singlepanel in  self.scrolled_panel_lst[mid_panel_num:num_of_panels]:
            bot_muliple_img_box.Add(singlepanel, 20, wx.EXPAND)

        img_right_panel_box.Add(top_muliple_img_box, 20, wx.EXPAND)
        img_right_panel_box.Add(bot_muliple_img_box, 20, wx.EXPAND)

        self.label_data_info = wx.StaticText(self, wx.ID_ANY, ' No Data to display yet  ')
        main_panel_box.Add(img_right_panel_box, 20, wx.EXPAND)
        main_panel_box.Add(self.label_data_info)

        self.SetSizer(main_panel_box)

    def update_scroll_pos(self):
        for pnl in self.scrolled_panel_lst:
            pnl.Scrolling_to()

    def update_scale(self):
        for pnl in self.scrolled_panel_lst:
            pnl.set_img_n_bmp()

class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "ANAELU 2.0", size = (1200,600))
        self.mPanel = MultiImagePanel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.mPanel, 1, wx.EXPAND)
        self.SetSizer(sizer)
        self.Show()

if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    frame = MainFrame()
    frame.Show()
    wxapp.MainLoop()

