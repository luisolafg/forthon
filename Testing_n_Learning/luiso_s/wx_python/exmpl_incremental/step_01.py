import wx
import wx.lib.scrolledpanel as scroll_pan

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        print ("Launching main frame")
        sz = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(sz)

if(__name__ == "__main__"):
    print ("Hi there, before the GUI starts")

    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()

    print ("Bye After the GUI starts")
