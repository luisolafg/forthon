import wx
import wx.lib.scrolledpanel as scroll_pan
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np

def GetBitmap_from_np_array(data2d):

    lc_fig = plt.figure()
    plt.imshow(np.transpose(data2d), interpolation = "nearest", cmap = 'hot')
    lc_fig.canvas.draw()
    width, height = lc_fig.canvas.get_width_height()
    np_buf = np.fromstring (lc_fig.canvas.tostring_rgb(), dtype=np.uint8)
    np_buf.shape = (width, height, 3)
    np_buf = np.roll(np_buf, 3, axis = 2)
    wx_image = wx.EmptyImage(width, height)
    wx_image.SetData(np_buf )
    wxBitmap = wx_image.ConvertToBitmap()

    plt.close(lc_fig)

    return wxBitmap




class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.set_scroll_content()


    def set_scroll_content(self):
        arr = np.arange(12 * 8).reshape(12, 8)

        bmp = GetBitmap_from_np_array(arr)

        st_bmp = wx.StaticBitmap(self, bitmap=bmp)

        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img_sizer.Add(st_bmp)

        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.Layout()


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "One scrolled panel", size=(180, 120))
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)

        self.scrolled_panel_02 = Scrolled_Img(self)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)


if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
