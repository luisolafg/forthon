import wx
import wx.lib.scrolledpanel as scroll_pan

class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)

        bmp = img.ConvertToBitmap()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)

        my_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        my_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        my_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.Bdwn = False

        img_sizer.Add(my_bmp)
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.Layout()

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButDown, GetViewStart() =", self.GetViewStart()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButUp(x,y) =", self.pos_x, self.pos_y

    def OnMouseMotion(self, event):
        new_x, new_y = event.GetPosition()
        if( self.Bdwn == True ):

            dx = new_x - self.pos_x
            dy = new_y - self.pos_y

            bar_x, bar_y = self.GetViewStart()
            wx.CallAfter(self.Scroll, bar_x - dx, bar_y - dy)

        self.pos_x, self.pos_y = new_x, new_y

        #VwStart = self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y
        '''
        ClSize = self.GetClientSize()
        Vsize = self.GetVirtualSize()
        x_pos_start, y_pos_start = (Vsize[0] + ClSize[0], Vsize[1] + ClSize[1])
        print "x_pos_start, y_pos_start =", x_pos_start, y_pos_start
        #self.Scroll(int(self.pos_x), int(self.pos_x))
        '''

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))

        self.scrolled_panel_01 = Scrolled_Img(self)
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)

        self.SetSizer(sz)
if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
