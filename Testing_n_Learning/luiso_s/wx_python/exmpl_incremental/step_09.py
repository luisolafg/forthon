import math
import wx
import wx.lib.scrolledpanel as scroll_pan
class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.Prnt_Wg = parent
        self.SetBackgroundStyle(wx.BG_STYLE_PAINT) #TODO study this line
        self.img_sizer = wx.BoxSizer(wx.VERTICAL)
        self.my_scale = 1.0
        self.img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        self.img_w, self.img_h = self.img.GetSize()
        self.scale_my_bmp()
        self.SetSizer(self.img_sizer)
        self.Layout()

    def scale_my_bmp(self):
        tmp_img = self.img.Scale(self.img_w * self.my_scale, self.img_h * self.my_scale, wx.IMAGE_QUALITY_HIGH)
        self.my_bmp = wx.StaticBitmap(self, bitmap = tmp_img.ConvertToBitmap())
        self.img_sizer.Clear(True)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.img_sizer.Add(self.my_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
        self.SetupScrolling()

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        if( sn_mov == 1.0 ):
            self.my_scale *=1.1

        else:
            self.my_scale *=0.9

        self.scale_my_bmp()
        self.Layout()
        self.Prnt_Wg.Layout()

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))
        self.scrolled_panel_01 = Scrolled_Img(self)
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        self.SetSizer(sz)

if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
