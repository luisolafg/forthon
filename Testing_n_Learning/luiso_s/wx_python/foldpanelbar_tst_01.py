import wx
import wx.lib.agw.foldpanelbar as fpb
import wx.grid as gridlib


class tst_panel(wx.Panel):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, parent):
        """Constructor"""
        wx.Panel.__init__(self, parent = parent)

        #txt_01 = wx.StaticText(self, wx.ID_ANY, "tst un texto txt_01")
        txt_02 = wx.StaticText(self, wx.ID_ANY, "testeando otro txt_02")

        sizer = wx.BoxSizer(wx.VERTICAL)
        #sizer.Add(txt_01, 0, wx.EXPAND)
        sizer.Add(txt_02, 0, wx.EXPAND)
        self.SetSizer(sizer)

class MyFrame(wx.Frame):

    def __init__(self, parent):

        wx.Frame.__init__(self, parent, -1, "FoldPanelBar Demo")

        text_ctrl = wx.TextCtrl(self, -1, size=(400, 100), style=wx.TE_MULTILINE)

        panel_bar = fpb.FoldPanelBar(self, -1, agwStyle=fpb.FPB_VERTICAL)

        fold_panel = panel_bar.AddFoldPanel("Thing")

        thing = tst_panel(fold_panel)

        panel_bar.AddFoldPanelWindow(fold_panel, thing)

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(text_ctrl, 1, wx.EXPAND)
        main_sizer.Add(panel_bar, 1, wx.EXPAND)

        self.SetSizer(main_sizer)


app = wx.App(0)

frame = MyFrame(None)
app.SetTopWindow(frame)
frame.Show()

print "Hi"

app.MainLoop()
