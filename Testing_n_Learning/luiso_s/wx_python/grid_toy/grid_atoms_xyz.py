
import wx
import wx.grid as gridlib
import wx.lib.scrolledpanel as scroll_pan


class atom_data(object):
    def __init__(self):
        self.Zn  = None
        self.Lbl = None
        self.x   = None
        self.y   = None
        self.z   = None
        self.b   = None


def read_cfl_atoms(my_file_path = "cfl_out.cfl"):


    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;=:':
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)

    #TODO make more stable the next thing

    lst_pos = []
    for pos, dat in enumerate(lst_dat):
        print "pos =", pos
        print "dat =", dat
        if(dat[0:4] == "Atom"):
            print "Found"
            lst_pos.append(pos)


        atm_lst = []
        for n_i, pos in enumerate(lst_pos):
            atm = atom_data()
            atm.Zn = lst_dat[pos + 1]
            atm.Lbl = lst_dat[pos + 2]
            atm.x = lst_dat[pos + 3]
            atm.y = lst_dat[pos + 4]
            atm.z = lst_dat[pos + 5]
            try:
                atm.b = lst_dat[pos + 6]
                if(atm.b == "Atom"):
                    atm.b = None
            except:
                atm.b = None
            atm_lst.append(atm)

    return atm_lst




def write_cfl_athom(dto_lts):

    print "writing atom list data"

    myfile = open("cfl_out.cfl", "w")




    for atm in dto_lts:
        wrstring = "Atom   "
        myfile.write(wrstring);

        wrstring = atm.Zn + "  " + atm.Lbl + "  " + \
                    atm.x + "   " + atm.y + "   " + atm.z + "   " + \
                    atm.b + "   " + "\n"
        '''
        print " atm.Zn  =", atm.Zn
        print " atm.Lbl =", atm.Lbl
        print " atm.x   =", atm.x
        print " atm.y   =", atm.y
        print " atm.z   =", atm.z
        print " atm.b   =", atm.b
        '''


        myfile.write(wrstring);

    myfile.close()

    print "writing done"


class my_gen_grid(gridlib.Grid):

    def __init__(self, parent):
        """Constructor"""
        super(my_gen_grid, self).__init__(parent)
        self.parent_widget = parent
        self.nm_atoms = 1
        self.CreateGrid(self.nm_atoms, 6)
        self.SetColLabelValue(0, "Z(num)")
        self.SetColLabelValue(1, "Label")
        self.SetColLabelValue(2, "coord( X )")
        self.SetColLabelValue(3, "coord( Y )")
        self.SetColLabelValue(4, "coord( Z )")
        self.SetColLabelValue(5, "  B  ")

        self.Bind(gridlib.EVT_GRID_CELL_LEFT_CLICK, self.OnCellLeftClick)
        self.Bind(gridlib.EVT_GRID_CELL_CHANGE, self.OnCellChange)

        #print "dir(gridlib.Grid) =" , dir(gridlib.Grid)
        print "GetWindowStyleFlag =", self.GetWindowStyleFlag()


    def OnCellLeftClick(self, evt):
        print "OnCellLeftClick: (%d,%d) %s\n" % (evt.GetRow(),
                                                 evt.GetCol(),
                                                 evt.GetPosition())
        evt.Skip()

    def OnCellChange(self, evt):
        print "OnCellChange: (%d,%d) %s\n" % (evt.GetRow(), evt.GetCol(),
                                              evt.GetPosition())
        value = self.GetCellValue(evt.GetRow(), evt.GetCol())
        print "value =", value


    def set_my_values(self, lst_data):
        print "self.nm_atoms =", self.nm_atoms
        print "len(lst_data) =", len(lst_data)

        #TODO
        #clean out the grid in full before starting to fill with data


        if(self.nm_atoms > len(lst_data)):
            dif = self.nm_atoms - len(lst_data)
            self.del_row(dif)

        elif(self.nm_atoms < len(lst_data)):
            dif = len(lst_data) - self.nm_atoms
            self.appn_row(dif)

        for row, tmp_atom in enumerate(lst_data):
            print ( "row # =", tmp_atom.Zn, "  ", tmp_atom.Lbl, "  " , tmp_atom.x,
                  "  " , tmp_atom.y, "  " , "  " , tmp_atom.z, "  ", tmp_atom.b )
            if(tmp_atom.Zn != None):
                self.SetCellValue(row, 0, tmp_atom.Zn)

            if(tmp_atom.Lbl != None):
                self.SetCellValue(row, 1, tmp_atom.Lbl)

            if(tmp_atom.x != None):
                self.SetCellValue(row, 2, tmp_atom.x  )

            if(tmp_atom.y != None):
                self.SetCellValue(row, 3, tmp_atom.y  )

            if(tmp_atom.z != None):
                self.SetCellValue(row, 4, tmp_atom.z  )

            if(tmp_atom.b != None):
                self.SetCellValue(row, 5, tmp_atom.b  )


    def get_my_values(self):
        print "from get_my_values"
        lst_data = []
        for row in xrange(self.nm_atoms):

            tmp_atom = atom_data()
            tmp_atom.Zn  = str(self.GetCellValue(row, 0))
            tmp_atom.Lbl = str(self.GetCellValue(row, 1))
            tmp_atom.x   = str(self.GetCellValue(row, 2))
            tmp_atom.y   = str(self.GetCellValue(row, 3))
            tmp_atom.z   = str(self.GetCellValue(row, 4))
            tmp_atom.b   = str(self.GetCellValue(row, 5))
            lst_data.append(tmp_atom)

        return lst_data

    def appn_row(self, n_dif = 1):
        print "here appn_row"
        print "n_dif =", n_dif

        self.AppendRows(n_dif)

        self.nm_atoms = self.GetNumberRows()
        print "self.nm_atoms =", self.nm_atoms
        print "self.GetNumberRows(self) =", self.GetNumberRows()


        # The scroll bars aren't resized (at least on windows)
        # Jiggling the size of the window rescales the scrollbars
        '''
        h,w = self.GetSize()
        self.SetSize((h+1, w))
        self.SetSize((h, w))
        self.ForceRefresh()
        '''
        self.parent_widget.sizer.Fit (self)
        self.ForceRefresh()
        self.Refresh()


    def del_row(self, n_dif = 1):
        print "here del_row"
        self.DeleteRows(pos = 0, numRows = n_dif, updateLabels = True)
        self.Refresh()
        #self.nm_atoms = self.nm_atoms - 1
        self.nm_atoms = self.GetNumberRows()
        print "self.nm_atoms =", self.nm_atoms
        print "self.GetNumberRows(self) =", self.GetNumberRows()



class AtomsPanel(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(AtomsPanel, self).__init__(parent)

        self.myGrid = my_gen_grid(self)

        I_Button = wx.Button(self, label="Save Lst")
        I_Button.Bind(wx.EVT_BUTTON, self.OnSaveButt)

        G_Button = wx.Button(self, label="Load Lst")
        G_Button.Bind(wx.EVT_BUTTON, self.OnLoadButt)

        A_Button = wx.Button(self, label="Add rw")
        A_Button.Bind(wx.EVT_BUTTON, self.OnAddButt)

        D_Button = wx.Button(self, label="Del rw")
        D_Button.Bind(wx.EVT_BUTTON, self.OnDelBut)
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        self.sizer.Add(I_Button, 0, wx.ALL|wx.TOP, 5)
        self.sizer.Add(G_Button, 0, wx.ALL|wx.TOP, 5)
        self.sizer.Add(A_Button, 0, wx.ALL|wx.TOP, 5)
        self.sizer.Add(D_Button, 0, wx.ALL|wx.TOP, 5)

        self.sizer.Add(self.myGrid, 0, wx.ALL|wx.TOP, 5)
        #self.sizer.Add(self.myGrid)


        self.SetSizer(self.sizer)
    def OnLoadButt(self, event):
        self.atm_lst = read_cfl_atoms()
        self.myGrid.set_my_values(self.atm_lst)

    def OnSaveButt(self, event):
        self.atm_lst = self.myGrid.get_my_values()
        write_cfl_athom(self.atm_lst)

    def OnAddButt(self, event):
        self.myGrid.appn_row()

    def OnDelBut(self, event):
        self.myGrid.del_row()



class MyForm(wx.Frame):

    def __init__(self):
        """Constructor"""
        super(MyForm, self).__init__(parent=None, title="An Grid for toying")

        self.main_panel = AtomsPanel(self)
        MainSizer = wx.BoxSizer(wx.VERTICAL)

        #MainSizer.Add(self.main_panel, proportion = 1, flag = wx.ALL|wx.EXPAND,border = 5)
        #MainSizer.Add(self.main_panel, 0, wx.ALL|wx.TOP, 5)
        MainSizer.Add(self.main_panel, 0, wx.ALL|wx.TOP, 5)
        self.SetSizer(MainSizer)
        self.Centre()


class MyApp(wx.App):
    def OnInit(self):
        frame = MyForm()
        frame.Show()
        return True

if(__name__ == "__main__"):
    app = MyApp(redirect=False)
    app.MainLoop()

