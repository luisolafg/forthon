# exercise with grids copied from:
# http://www.blog.pythonlibrary.org/2010/03/18/wxpython-an-introduction-to-grids/
# then evolved to our needs and to our coding style

import wx
import wx.grid as gridlib

class my_gen_grid(gridlib.Grid):

    def __init__(self, parent):
        """Constructor"""
        super(my_gen_grid, self).__init__(parent)
        self.CreateGrid(8, 4)
        self.SetColLabelValue(5, "test")
        # test all the events
        self.Bind(gridlib.EVT_GRID_CELL_LEFT_CLICK, self.OnCellLeftClick)
        self.Bind(gridlib.EVT_GRID_CELL_CHANGE, self.OnCellChange)


    def OnCellLeftClick(self, evt):
        print "OnCellLeftClick: (%d,%d) %s\n" % (evt.GetRow(),
                                                 evt.GetCol(),
                                                 evt.GetPosition())
        evt.Skip()

    def OnCellChange(self, evt):
        print "OnCellChange: (%d,%d) %s\n" % (evt.GetRow(), evt.GetCol(), evt.GetPosition())
        value = self.GetCellValue(evt.GetRow(), evt.GetCol())
        print "value =", value
    def get_my_values(self):
        print "from get_my_values"
        lst_data = []
        for row in xrange(8):
            row_lst = []
            for col in xrange(4):
                row_lst.append(str(self.GetCellValue(row, col)))
            lst_data.append(row_lst)

        return lst_data


class MyForm(wx.Frame):

  def __init__(self):
    """Constructor"""
    super(MyForm, self).__init__(parent=None, title="An Grid for toying")
    panel = wx.Panel(self)

    self.myGrid = my_gen_grid(panel)

    I_Button = wx.Button(panel, label="get I")
    I_Button.Bind(wx.EVT_BUTTON, self.OnIBut)


    sizer = wx.BoxSizer(wx.VERTICAL)

    sizer.Add(I_Button)
    sizer.Add(self.myGrid)



    panel.SetSizer(sizer)

  def OnIBut(self, event):
    print "Hi"
    a = self.myGrid.get_my_values()
    print "a = ", a


class MyApp(wx.App):
  def OnInit(self):
    frame = MyForm().Show()
    return True

if(__name__ == "__main__"):
  app = MyApp(redirect=False)

  app.MainLoop()

