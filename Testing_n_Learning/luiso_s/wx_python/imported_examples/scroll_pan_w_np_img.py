import wx
import wx.lib.scrolledpanel as scroll_pan
import math

from read_2d_numpy import read_bin_file
from np_nditer_RGB.looping import np_to_bmp

class Scrolled_Img(scroll_pan.ScrolledPanel):

    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.my_bitmap = None
        self.Prnt_Wg = parent
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.scroll_rot = 0
        self.Bdwn = False

        self.w, self.h = 765, 765
        self.img_scale = 1

    def __call__(self, path_to_img = None, real_yn = ""):
        if( self.my_bitmap == None and path_to_img != None ):
            data2d = read_bin_file(path_to_img, real_yn)
            bmp_dat = np_to_bmp()
            self.my_bitmap = bmp_dat.img_2d_rgb(data2d)
            self.my_image = wx.ImageFromBitmap(self.my_bitmap)
            self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)

        self.set_scroll_content()


    def set_scroll_content(self):

        self.my_st_bmp = wx.StaticBitmap(self, bitmap = self.my_bitmap)
        self.img_vert_sizer.Clear(True)
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
        self.SetSizer(self.img_vert_sizer)

        self.Refresh()
        self.my_bindings()

        self.SetScrollRate(1, 1)
        self.SetupScrolling()
        self.Layout()
        self.Prnt_Wg.Layout()


    def my_bindings(self):
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)


    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()


    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.old_Mouse_Pos_x, self.old_Mouse_Pos_y = event.GetPosition()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        Mouse_Pos_x, Mouse_Pos_y = event.GetPosition()

    def OnMouseWheel(self, event):

        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        self.img_scale += self.img_scale * sn_mov * 0.05

        if(self.img_scale > 5.0):
            self.img_scale = 5.0
        elif(self.img_scale < 1.0):
            self.img_scale = 1.0

        v_size_x, v_size_y = self.GetVirtualSize()
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()
        View_start_x, View_start_y = self.GetViewStart()

        self.x_uni = float( View_start_x + self.Mouse_Pos_x ) / float(v_size_x)
        self.y_uni = float( View_start_y + self.Mouse_Pos_y ) / float(v_size_y)

    def OnIdle(self, event):

        new_scroll_pos_x, new_scroll_pos_y = -1, -1
        if( self.scroll_rot != 0 ):
            self.SetScrollRate(1, 1)


            NewW = self.img_scale * 765
            NewH = self.img_scale * 765

            image = self.my_image.Scale(NewW, NewH, wx.IMAGE_QUALITY_HIGH)
            self.my_bitmap = wx.BitmapFromImage(image)

            self.set_scroll_content()

            #'''
            v_size_x, v_size_y = self.GetVirtualSize()
            new_scroll_pos_x = float(self.x_uni * v_size_x - self.Mouse_Pos_x)
            new_scroll_pos_y = float(self.y_uni * v_size_y - self.Mouse_Pos_y)
            #'''

            self.Scroll(new_scroll_pos_x, new_scroll_pos_y)
            self.scroll_rot = 0

        else:

            self.SetScrollRate(1, 1)
            if( ( self.old_Mouse_Pos_x != self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != self.Mouse_Pos_y ) and
               self.Bdwn == True):

                View_start_x, View_start_y = self.GetViewStart()
                dx = self.Mouse_Pos_x - self.old_Mouse_Pos_x
                dy = self.Mouse_Pos_y - self.old_Mouse_Pos_y
                new_scroll_pos_x = View_start_x - dx
                new_scroll_pos_y = View_start_y - dy
                self.Scroll(new_scroll_pos_x, new_scroll_pos_y)

            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y

        self.Prnt_Wg.update_all(scroll_pos = (new_scroll_pos_x, new_scroll_pos_y))

    def Scrolling_to(self, new_scroll_pos_x, new_scroll_pos_y):
        self.Scroll(new_scroll_pos_x, new_scroll_pos_y)
        self.Layout()
        self.Prnt_Wg.Layout()

