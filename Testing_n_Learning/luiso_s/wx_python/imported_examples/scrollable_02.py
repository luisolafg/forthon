import wx
import wx.lib.scrolledpanel as scroll_pan

from bitmap_from_numpy_w_matplotlib_better_done \
     import GetBitmap_from_np_array, build_np_img

from read_2d_numpy import read_bin_file
#from read_2d_numpy import read_file

class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

    def set_scroll_content(self, path_to_img):
        img_lst_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        data2d = read_bin_file(path_to_img)
        bitmap = GetBitmap_from_np_array(data2d)
        my_bmp = wx.StaticBitmap(self, bitmap=bitmap)
        img_lst_vert_sizer.Add(my_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 6)
        self.SetSizer(img_lst_vert_sizer)
        self.SetupScrolling()

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        sz = wx.BoxSizer(wx.HORIZONTAL)

        scrolled_01 = Scrolled_Img(self)
        #scrolled_01.set_scroll_content("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.asc")
        scrolled_01.set_scroll_content("../../../../local_data/exp_01/2d_pat_1.asc")
        #scrolled_01.set_scroll_content("../../../../mar_convert/mar_img/PFN_phi_0.5_to_2.5_180s_01_01_06102016.mar2300.bin")
        #scrolled_01.set_scroll_content("../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.mar2300.bin")



        scrolled_02 = Scrolled_Img(self)
        scrolled_02.set_scroll_content("../../../../local_data/exp_01/APT73_d122_from_m02to02__01_41.mar2300.asc")

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(scrolled_01, 1, wx.EXPAND)
        sz.Add(scrolled_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)

    def OnBut_1(self, event):
        print "OnBut_1"

    def OnBut_2(self, event):
        print "OnBut_2"


if( __name__ == '__main__' ):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
