import wx
import wx.lib.scrolledpanel as scroll_pan

from bitmap_from_numpy_w_matplotlib_better_done \
     import GetBitmap_from_np_array, build_np_img

from subprocess import call as shell_func

from read_2d_numpy import read_bin_file

class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.Prnt_Panel = parent

    def set_scroll_content(self, path_to_img):
        img_lst_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        data2d = read_bin_file(path_to_img)
        bitmap = GetBitmap_from_np_array(data2d)
        my_bmp = wx.StaticBitmap(self, bitmap=bitmap)
        img_lst_vert_sizer.Add(my_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 6)
        self.SetSizer(img_lst_vert_sizer)
        self.SetupScrolling()
        self.Layout()
        self.Prnt_Panel.Layout()

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        self.scrolled_panel_01.set_scroll_content("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw")

        self.scrolled_panel_02 = Scrolled_Img(self)
        self.scrolled_panel_02.set_scroll_content("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw")

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)

    def OnBut_1(self, event):
        print "OnBut_1"
        shell_func("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/Anaelu_gfortran.exe")

        print "Done running Anaelu\n\nUpdating Img"
        self.scrolled_panel_01.set_scroll_content("2d_pat_1.asc")


    def OnBut_2(self, event):
        print "OnBut_2"


if( __name__ == '__main__' ):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
