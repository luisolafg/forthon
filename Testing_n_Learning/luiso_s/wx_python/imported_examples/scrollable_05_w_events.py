import wx

from subprocess import call as shell_func
from scroll_pan_w_np_img import Scrolled_Img

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        self.scrolled_panel_02 = Scrolled_Img(self)

        self.scrolled_panel_01("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw", real_yn = True)
        self.scrolled_panel_02("../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.mar2300.bin", real_yn = False)

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)



    def OnBut_1(self, event):
        #print "OnBut_1"
        shell_func("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/Anaelu_gfortran.exe")

        #print "Done running Anaelu\n\nUpdating Img"
        self.scrolled_panel_01.set_scroll_content("2d_pat_1.asc")


    def OnBut_2(self, event):
        print "OnBut_2"

    def update_all(self, scroll_pos = (-1, -1)):
        if( scroll_pos != (-1, -1) ):
            #print "Hi updating all"
            self.scrolled_panel_01.Scrolling_to(scroll_pos[0], scroll_pos[1])
            self.scrolled_panel_02.Scrolling_to(scroll_pos[0], scroll_pos[1])


if( __name__ == '__main__' ):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()

