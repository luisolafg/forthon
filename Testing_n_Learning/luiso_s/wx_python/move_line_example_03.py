import wx

class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def OnMouseMotion(self, event):
        Mouse_Pos_x, Mouse_Pos_y = event.GetPosition()
        DevCont = wx.PaintDC(self)
        DevCont.SetPen(wx.Pen("BLACK", 4))
        DevCont.Clear()
        DevCont.DrawLine(0, 0, Mouse_Pos_x, Mouse_Pos_y)

        self.Layout()

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)
        self.Show(True)

class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
