import wx
import numpy as np

class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        self.mat = np.zeros([3, 3], dtype=np.float64)
        for diag in xrange(3):
            self.mat[diag, diag] = 10.0

        print "self.mat =\n", self.mat

        self.ang_int = 0.0
        self.old_x_mouse = None
        self.old_y_mouse = None

    def OnRot(self):

        self.ang_01 = float(self.ang_int) * 0.1

        zrot = [[ np.cos(self.ang_01),  - np.sin(self.ang_01) , 0.0 ],
                [ np.sin(self.ang_01),    np.cos(self.ang_01) , 0.0 ],
                [ 0.0                ,    0.0                 , 1.0 ]]
        zrot = np.asarray(zrot, dtype=np.float64)
        #print "zrot = \n", zrot

        mat_uni = [[ 1.0,  0.0 , 0.0 ],
                   [ 0.0,  1.0 , 0.0 ],
                   [ 0.0,  0.0 , 1.0 ]]
        mat_uni = np.asarray(mat_uni, dtype=np.float64)

        DevCont = wx.PaintDC(self)
        DevCont.SetDeviceOrigin(150.0, 150.0)
        DevCont.SetLogicalScale(10.0, 10.0)
        DevCont.SetPen(wx.Pen("BLACK", 0.4))
        DevCont.Clear()

        DevCont.SetPen(wx.Pen("BLUE", 0.4))

        mat_01 = np.matmul(zrot, self.mat)
        for i in xrange(3):
            DevCont.DrawLine(0.0, 0.0, mat_01[0, i], mat_01[1, i])

        self.Layout()
        '''
        yrot = [[   np.cos(self.ang_01),  0.0 , np.sin(self.ang_01) ],
                [   0.0,                  1.0 ,                0.0  ],
                [ - np.sin(self.ang_01),  0.0 , np.cos(self.ang_01) ]]
        '''

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)
        main_panel_box = wx.BoxSizer(wx.VERTICAL)

        self.panel_paint = PaintAreaPanel(self)

        btn_lft = wx.Button(self, wx.ID_ANY, 'turn left')
        self.Bind(wx.EVT_BUTTON, self.on_lft_btn_drag, btn_lft)
        main_panel_box.Add(btn_lft, flag = wx.TOP)

        btn_rgt = wx.Button(self, wx.ID_ANY, 'turn right')
        self.Bind(wx.EVT_BUTTON, self.on_rgt_btn_drag, btn_rgt)
        main_panel_box.Add(btn_rgt, flag = wx.TOP)

        main_panel_box.Add(self.panel_paint, 2, wx.EXPAND)

        self.SetSizer(main_panel_box)
        self.Show(True)

    def on_lft_btn_drag(self, event):
        self.panel_paint.ang_int += 1
        self.panel_paint.OnRot()

    def on_rgt_btn_drag(self, event):
        self.panel_paint.ang_int -= 1
        self.panel_paint.OnRot()


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
