import wx
import numpy as np

def ang_to_mat( ang_x, ang_y, ang_z ):

        mat_uni = [[ 10.0,  0.0 , 0.0  ],
                   [ 0.0 , 10.0 , 0.0  ],
                   [ 0.0 ,  0.0 , 10.0 ]]
        mat_uni = np.asarray(mat_uni, dtype=np.float64)

        tmp_ang = float(ang_z) * 0.1
        zrot = [[ np.cos(tmp_ang),  - np.sin(tmp_ang) , 0.0 ],
                [ np.sin(tmp_ang),    np.cos(tmp_ang) , 0.0 ],
                [ 0.0            ,    0.0             , 1.0 ]]
        zrot = np.asarray(zrot, dtype=np.float64)

        tmp_ang = float(ang_y) * 0.1
        yrot = [[   np.cos(tmp_ang),  0.0 , np.sin(tmp_ang) ],
                [   0.0,              1.0 ,            0.0  ],
                [ - np.sin(tmp_ang),  0.0 , np.cos(tmp_ang) ]]
        yrot = np.asarray(yrot, dtype=np.float64)

        tmp_ang = float(ang_x) * 0.1
        xrot = [[ 1.0 ,             0.0 ,              0.0  ],
                [ 0.0 , np.cos(tmp_ang) , - np.sin(tmp_ang) ],
                [ 0.0 , np.sin(tmp_ang) ,   np.cos(tmp_ang) ]]
        xrot = np.asarray(xrot, dtype=np.float64)

        mat_01 = np.matmul(zrot, mat_uni)
        mat_02 = np.matmul(yrot, mat_01)
        mat_03 = np.matmul(xrot, mat_02)

        return mat_03


class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        self.x_ang_int = 0.0
        self.y_ang_int = 0.0
        self.z_ang_int = 0.0

    def UpdateVectors(self):


        DevCont = wx.PaintDC(self)
        DevCont.SetDeviceOrigin(150.0, 150.0)
        DevCont.SetLogicalScale(10.0, 10.0)
        DevCont.Clear()

        DevCont.SetPen(wx.Pen("BLUE", 0.4))

        mat_rot = ang_to_mat( self.x_ang_int, self.y_ang_int, self.z_ang_int )

        for i in xrange(3):
            DevCont.DrawLine(0.0, 0.0, mat_rot[0, i], mat_rot[1, i])

        self.Layout()

        print "x_ang_int, y_ang_int, z_ang_int", \
              self.x_ang_int, self.y_ang_int, self.z_ang_int


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)
        rot_tools_box = wx.BoxSizer(wx.VERTICAL)

        self.panel_paint = PaintAreaPanel(self)

        btn_z_pls = wx.Button(self, wx.ID_ANY, 'Z + rot')
        self.Bind(wx.EVT_BUTTON, self.on_z_pls_btn, btn_z_pls)
        rot_tools_box.Add(btn_z_pls, flag = wx.TOP)

        btn_z_min = wx.Button(self, wx.ID_ANY, 'Z - rot')
        self.Bind(wx.EVT_BUTTON, self.on_z_min_btn, btn_z_min)
        rot_tools_box.Add(btn_z_min, flag = wx.TOP)

        btn_y_pls = wx.Button(self, wx.ID_ANY, 'Y + rot')
        self.Bind(wx.EVT_BUTTON, self.on_y_pls_btn, btn_y_pls)
        rot_tools_box.Add(btn_y_pls, flag = wx.TOP)

        btn_y_min = wx.Button(self, wx.ID_ANY, 'Y - rot')
        self.Bind(wx.EVT_BUTTON, self.on_y_min_btn, btn_y_min)
        rot_tools_box.Add(btn_y_min, flag = wx.TOP)

        btn_x_pls = wx.Button(self, wx.ID_ANY, 'X + rot')
        self.Bind(wx.EVT_BUTTON, self.on_x_pls_btn, btn_x_pls)
        rot_tools_box.Add(btn_x_pls, flag = wx.TOP)

        btn_x_min = wx.Button(self, wx.ID_ANY, 'X - rot')
        self.Bind(wx.EVT_BUTTON, self.on_x_min_btn, btn_x_min)
        rot_tools_box.Add(btn_x_min, flag = wx.TOP)

        main_panel_box = wx.BoxSizer(wx.HORIZONTAL)
        main_panel_box.Add(rot_tools_box, 1, wx.EXPAND)
        main_panel_box.Add(self.panel_paint, 3, wx.EXPAND)

        self.SetSizer(main_panel_box)
        self.Show(True)

    def on_z_pls_btn(self, event):
        self.panel_paint.z_ang_int += 1
        self.panel_paint.UpdateVectors()

    def on_z_min_btn(self, event):
        self.panel_paint.z_ang_int -= 1
        self.panel_paint.UpdateVectors()

    def on_y_pls_btn(self, event):
        self.panel_paint.y_ang_int += 1
        self.panel_paint.UpdateVectors()

    def on_y_min_btn(self, event):
        self.panel_paint.y_ang_int -= 1
        self.panel_paint.UpdateVectors()

    def on_x_pls_btn(self, event):
        self.panel_paint.x_ang_int += 1
        self.panel_paint.UpdateVectors()

    def on_x_min_btn(self, event):
        self.panel_paint.x_ang_int -= 1
        self.panel_paint.UpdateVectors()

class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
