import wx

import wx.lib.scrolledpanel as scroll_pan

class PaintAreaPanel(scroll_pan.ScrolledPanel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.SetupScrolling()


    def OnPaint(self, event):
        if(self.Mouse_Pos_x > 0 and self.Mouse_Pos_y > 0):
            self.DevCont = wx.PaintDC(self)
            #self.DevCont.Clear()
            self.DevCont.SetPen(wx.Pen("BLACK", 4))
            self.DevCont.Clear()
            self.DevCont.DrawLine(0, 0, self.Mouse_Pos_x, self.Mouse_Pos_y)

        self.Layout()

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()
        self.OnPaint(event)

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)
        '''
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(panel, 0, wx.LEFT | wx.ALL,8)
        self.SetSizer(sz)
        '''
        self.Show(True)

        copied = '''
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        self.scrolled_panel_01.set_scroll_content("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw", real_yn = True)

        self.scrolled_panel_02 = Scrolled_Img(self)
        self.scrolled_panel_02.set_scroll_content("../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.mar2300.bin", real_yn = False)

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)
        '''

class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
