import wx

import wx.lib.scrolledpanel as scroll_pan

class PaintAreaPanel(scroll_pan.ScrolledPanel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.Mouse_Left_clk = False
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)

        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        #self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.SetupScrolling()
        self.Layout()

    def OnPaint(self, event):
        if(self.Mouse_Pos_x > 0 and self.Mouse_Pos_y > 0):
            self.DevCont = wx.PaintDC(self)
            #self.DevCont.Clear()
            self.DevCont.SetPen(wx.Pen("BLACK", 4))
            self.DevCont.DrawLine(0, 0, self.Mouse_Pos_x, self.Mouse_Pos_y)
            print "OnPaint"
            self.Layout()


    def OnLeftButDown(self, event):
        print "OnLeftButDown"
        self.Mouse_Left_clk = True


    def OnLeftButUp(self, event):
        print "OnLeftButUp"
        self.Mouse_Left_clk = False

    def OnIdle(self, event):
        print "OnIdle"
        if( self.Mouse_Left_clk == True):
            print "self.Mouse_Pos_x, self.Mouse_Pos_y =", self.Mouse_Pos_x, self.Mouse_Pos_y
            self.OnPaint(event)

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)
        ''''
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(panel, 0, wx.LEFT | wx.ALL,8)
        self.SetSizer(sz)
        #'''
        self.Show(True)


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
