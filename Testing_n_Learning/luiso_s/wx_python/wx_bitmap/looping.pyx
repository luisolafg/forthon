def loops_2d(data2d_scale):

  import numpy as np

  width = np.size( data2d_scale[0:1, :] )
  height = np.size( data2d_scale[:, 0:1] )
  img_array = np.zeros( (height ,width, 3),'uint8')

  for col in range(0, width):
    for row in range(0, height):
      if(data2d_scale[row,col] <= 255):
        img_array[row,col,0] = data2d_scale[row,col]
        img_array[row,col,1] = 0
        img_array[row,col,2] = 0

      elif( data2d_scale[row,col] > 255 and
            data2d_scale[row,col] < 256 * 2):
        img_array[row,col,0] = 255
        img_array[row,col,1] = data2d_scale[row,col] - 256
        img_array[row,col,2] = 0

      else:
        img_array[row,col,0] = 255
        img_array[row,col,1] = 255
        img_array[row,col,2] = data2d_scale[row,col] - 256 * 2 - 1

  return img_array
