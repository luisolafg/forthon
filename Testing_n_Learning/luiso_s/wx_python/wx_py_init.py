import os
import wx
#from bitmap_from_numpy_w_matplotlib_better_done import GetBitmap_from_np_array \
#                                                       , build_np_img
class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos = wx.DefaultPosition, size = wx.DefaultSize,
                 style = wx.DEFAULT_FRAME_STYLE, name = "MyFrame"):

        super(MyFrame, self).__init__(parent = None, title = title)

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, title = "tst interfs", pos = (150, 150))

        self.frame.panel = wx.Panel(self.frame)

        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True


if(__name__ == "__main__"):
    app = MyApp(redirect = False)
    app.MainLoop()

