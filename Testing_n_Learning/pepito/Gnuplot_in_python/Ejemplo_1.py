#Program_1
# Programa grafica una funcion cuadratica
# en los primero 1000 enteros positivos utilizando Gnuplot.py
# Visitar http://gnuplot-py.sourceforge.net/


#Cargando la libreria de Gnuplot 
import Gnuplot
import numpy as np

#Coleccion de puntos
x = np.arange(1000)

#Elevando al cuadrado
y=x**2
# persist=1 para imprimir imagen
g = Gnuplot.Gnuplot(persist=1)
d = Gnuplot.Data(x, y, with_="l")
g.plot(d)
