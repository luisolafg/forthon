# Ejemplo_6 
# Crea una grafica de x**2 + y**2, definiendo operador de grafica
# Con posibilidad para exportar a png

import sys, math
import Gnuplot, Gnuplot.funcutils
import numpy as np

def generatePlot3D(filename,*y):
    plot = Gnuplot.Gnuplot(persist=1)
    plot("set terminal pngcairo size 640,480 enhanced font 'Verdana,10")
    plot("set output '{}'".format(filename))
    plot("set border 4095 front linetype 0 linewidth 1.000")
    plot("set view 60,30")
    #plot("set logscale z")
    plot("set ztics offset 1")
    plot("set zrange [-10:50]")
    plot("set multiplot")
    for y1 in y :
        plot.splot(Gnuplot.Data(y1,using=(1,2,3), with_="lines lc rgb 'violet'"))
    plot("unset multiplot")
    return

ax = np.arange(1000)
x = []
y = []

xyz = []

for i in ax:
   i=i+0.0
   i=(i/100)-5
   x.append(i)
   
y = x


for i in x:
    for j in y:
        b = j**2 + i**2
        a = [i, j, b]
        xyz.append(a)
       
#for i in xyz:
#    print i       
        
generatePlot3D("3d.png",xyz)
