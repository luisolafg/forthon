!
! This is a examples of operations matrix, We will use iso_c_binding 

Module MatOper
    
   use iso_c_binding
   implicit none
   contains

   subroutine increase(n,m) bind(c, name="increase")
   integer (C_int), intent(in) :: n
   integer (C_int), intent(out) :: m
    write(*,*) "from Fortran (before summation ) n =", n
    m = n + 8
    write(*,*) "from Fortra (after summation ) n = ", m
    

    end subroutine increase

!==================Second Routine ======================

    subroutine square(n, m)  bind(c, name="square")
    integer (C_int), intent(in) :: n
    integer (C_int), intent(out) :: m
    write(*,*) "From Fortran (before square ) m = ", n
    m = n*n
    write(*,*) "From Fortran (after square ) m = ", m
    
    
    end subroutine square
 
 !==================Second Routine ======================

    subroutine sum_matrix(A, B, C)  bind(c, name="sum_matrix")   
    
    integer, dimension(3,3) :: A, B
    integer, dimension(3,3) :: C
    integer :: i, j
    
    
    do i=1, 3
        do j=1, 3
        C(i,j)=A(i,j)+B(i,j)
        write(*,*) "El value of Matrix is ", C(i,j)
        end do    
    end do
    
    
    
    end subroutine sum_matrix

End Module MatOper