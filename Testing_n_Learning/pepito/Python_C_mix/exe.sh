#!/bin/bash
# -*- ENCODING: UTF-8 -*

echo "==========================================="
echo "borrando la carpeta build y el archivo *.so"
rm -r build
rm *.so
echo "==========================================="
echo "ejecutando python setup.py"
echo "python setup.py y almacenando el resultado en build"
python setup.py build
echo "Copiando el archivo *.so a la carpeta:"
pwd
cp build/lib.linux-x86_64-2.7/hello.so hello.so
echo "Ejecutando el programa de test.py"
python test.py
echo "==========================================="
echo "borrando la carpeta build y el archivo *.so"
rm -r build
rm *.so
echo "Adiuuu..."
echo "==========================================="

