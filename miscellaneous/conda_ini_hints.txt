
chmod +x Miniconda2-latest-Linux-x86_64.sh

./Miniconda2-latest-Linux-x86_64.sh
conda install numpy
conda install wxpython
conda install lxml
conda install -c fable fabio=0.3.0
conda install -c fable pycifrw=4.1.1 # not sure this is needed
# conda install matplotlib  # developers only

git clone https://gitlab.com/luisolafg/anaelu.git
git clone https://gitlab.com/luisolafg/forthon.git
